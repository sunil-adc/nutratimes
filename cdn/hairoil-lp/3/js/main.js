$(document).ready(function(){
    $('.singleItem').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    dots:true,
    });

    var $window = $(window);
    $window.scroll(function () {
        if ($window.scrollTop() > 20) {          

          $(".navbar").addClass('navbar-default');

        }else{
          $(".navbar").removeClass('navbar-default');
        }
    });
  });