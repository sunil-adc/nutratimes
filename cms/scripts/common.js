/*window.SITE_URL="http://shopaaj.com/app/new_svaiza/";
window.FULL_CMS_URL="http://shopaaj.com/app/new_svaiza/index.php/cms"; */

$(document).ready(function(e) {
    $("#submit").click(function(e) {
	   $(".button").addClass("disable_button").attr('value','Please wait...');
	});
});

function change_status(e,t,n,r,i,s,o){
	$.ajax({
		url:e+"/changeStatus/index/"+t+"/"+n+"/"+o+"/"+r+"/"+i+"/"+s,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!");
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
$(document).on('click', '.change_payment_status', function() {
	var _data_id = $(this).attr("data-id");
	var _siteurl = $(this).attr("data-siteurl");
	var requestStr = "&data=send&id=" + _data_id;
	if (requestStr != '' && _siteurl != '') {
		$.ajax({
			url: _siteurl + "/orderproducts/changePaymentStatus/",
			type: "post",
			data: requestStr,
			beforeSend:function(){
				
			},
			success:function(e){
				$("#pcsajax").html(e);		
			},
			error:function(e,t,n){
				alert(e.responseText)
			}
		})
	}
});
function change_category_status(e,t,n,r,i,s,o){
	$.ajax({
		url:e+"/changeCategoryStatus/index/"+t+"/"+n+"/"+o+"/"+r+"/"+i+"/"+s,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!");
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function change_fraud_status(e,t,n,r,i,s,o){
	$.ajax({
		url:e+"/changeFraudStatus/index/"+t+"/"+n+"/"+o+"/"+r+"/"+i+"/"+s,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!");
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function frm_order_product(){
	var product_name = "#product_name";
	var pid = "#pid";
	var product_size = "#product_size";
	var quantity = "#quantity";
	
	if($(product_name).val() == '' || $(pid).val() == '') {
		$("#product_name").css("border-color", "red");
		return false;
	}
	
	if($(product_size).children().length > 0 && typeof $(product_size).val() <= 0) {
		$(product_size).css("border-color", "red");
		return false;
	}
	if($(quantity).val() == '') {
		$(quantity).css("border-color", "red");
		return false;
	}
	return true;
}
function set_default_image(url, imageid, _this){
	if(url != "" && imageid != "") {
		$.ajax({
			url:url,
			type:"post",
			data:"&data=send",
			beforeSend:function(){
				$("#"+imageid).append('<span class="image_change_load_bg" id="loader_' + imageid + '"><span class="loader_bg"></span></span>');
			},
			success:function(e){
				$(".cms_prod_image").removeClass('default_product_image');
				$(_this).addClass('default_product_image');
				$("#loader_" + imageid).remove();
			},
			error:function(e,t,n){
				alert(e.responseText)
			}
		});
	} else {
		alert('Refresh your page and try again !!');
	}
}
function change_oos(e,t,n,r,i,s,o)
{
	$.ajax({
		url: e+"/changeOos/index/"+t+"/"+n+"/"+o+"/"+r+"/"+i+"/"+s,
		type: "post",
		data: "&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function change_verifiy(e,t,n,r,i,s,o)
{
	$.ajax({
		url: e+"/changeVerify/index/"+t+"/"+n+"/"+o+"/"+r+"/"+i+"/"+s,
		type: "post",
		data: "&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function display_ajax(e,t,n,r,i){
	$.ajax({
		url:e+"/changeAccountProductname/index/"+t+"/"+n+"/"+r+"/"+i,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+t).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+t).html(e);
				if(r=="label"){
					$("#"+t).focus()
				}else{
				}
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function _delete(e,t){
	if(e!=""){
		var n=confirm(" Are you sure you want to delete this "+t+" ?");
		if(n){
			window.location.href=e
		}
	}
	return false
}
function get_subcat(e,t,n,r,mode){
	$.ajax({
		url:e+"/AjaxGetCategory/index/"+t+"/"+n,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+r).html("Wait...")
		},
		success:function(e) {
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+r).html(e);
				if(mode == 'cng') {
					if($(".size_availability").length > 0 ){
						$(".size_availability").val(0);
					}
					if($("#ajax_stock_available").length > 0 ){
						$("#ajax_stock_available").html('Select Size Availability First !!');
					}
				}
			}
		},
		error:function(e,t,n){
			$("#ajax_stock_available").html(e.responseText);
		}
	})
}
function get_product_genre(e,t,n){
	$.ajax({
		url:e+"/AjaxGetGenre/index/"+t,
		type:"post",
		data:"&data=send",
		beforeSend:function(){
			$("#"+n).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+n).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
function load_chart(e,t,n,r){
	$.ajax({
		url:e+"/chartDashboard/index/",
		type:"post",
		data:"&data=send",
		beforeSend:function() {
		},
		success:function(z) {
			var parsedJSON = $.parseJSON(z);
			
			$('#total_orders').html(commaSeparateNumber(parsedJSON.total_orders));
			$('#total_notdelivered_orders').html(commaSeparateNumber(parsedJSON.total_notdelivered_orders));
			$('#today_sale').html(commaSeparateNumber(parsedJSON.today_sale));
			$('#expected_revenue').html(commaSeparateNumber(parsedJSON.expected_revenue));
			$('#lead_to_conversion').html(commaSeparateNumber(Math.round(parsedJSON.lead_to_conversion)) + "%");
			$('#today_revenue').html(commaSeparateNumber(parsedJSON.today_revenue));
			$("#cod_qualify").html(commaSeparateNumber(parsedJSON.cod_qualify));
			$("#growth_rate").html(commaSeparateNumber(Math.round(parsedJSON.growth_rate)) + "%");
			$("#qa_qualify").html(commaSeparateNumber(parsedJSON.qa_qualify));
			$("#total_sent").html(commaSeparateNumber(parsedJSON.total_sent));
			$("#fire_leads").html(commaSeparateNumber(parsedJSON.fire_leads));
			$("#today_user").html(commaSeparateNumber(parsedJSON.today_user));
				  
			$(document).ready(function(){
				
				$.getJSON(t+"/order_chart.json",null,function(e){
					var t=0,
					n="",
					i=Array();
					for(key in e[0]){
						if(t==0){
							n=e[0][key]
							}else{
								i[t]=e[0][key]
							}
							t++
					}
					var s=new Highcharts.Chart({
						chart:{
							renderTo:r,
							type:"column"
						},
						title:{
							text:"Order Summary"
						},
						xAxis:{
							categories:n
						},
						credits:{
							enabled:false
						},
						series:[{
							name:"Total Orders",
							data:i[1]
						},{
							name:"Total Qualified Order",
							data:i[2]
						},{
							name:"Total Sent Order",
							data:i[3]
						},{
							name:"Total Return Order",
							data:i[4]
						}]
					})
				})
			
			})
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}


function load_chart_new(e,t,n,r,ext){

	if(ext == 1){
		var k = "&d="+ext;
	}else if(ext == 2){
		var k = "&d="+ext+"&sr_dt="+$("#reportrange").val();
	}

	$.ajax({
		url:e+"/chartDashboard/index/"+ext,
		type:"post",
		data:"&data=send"+k,
		beforeSend:function() {
		},
		success:function(z) {
			
			var parsedJSON = $.parseJSON(z);
			
			$('#total_prepaid').html(commaSeparateNumber(parsedJSON.total_prepaid));
			$('#total_cod').html(commaSeparateNumber(parsedJSON.total_cod));
			$('#total_conversion').html(commaSeparateNumber(parsedJSON.total_conversion));
			$('#total_notdelivered_orders').html(commaSeparateNumber(parsedJSON.total_notdelivered_orders));
			$('#cod_send').html(commaSeparateNumber(parsedJSON.total_cod_send));
			$('#prepaid_send').html(commaSeparateNumber(parsedJSON.total_prepaid_send));
			//$('#total_sale').html(commaSeparateNumber(parsedJSON.total_sale));
			//$('#expected_revenue').html(commaSeparateNumber(parsedJSON.expected_revenue));
			//$('#lead_to_conversion').html(commaSeparateNumber(Math.round(parsedJSON.lead_to_conversion)) + "%");
			$('#total_revenue').html(commaSeparateNumber(parsedJSON.total_revenue));
			$("#cod_qualify").html(commaSeparateNumber(parsedJSON.cod_qualify));
			//$("#growth_rate").html(commaSeparateNumber(Math.round(parsedJSON.growth_rate)) + "%");
			$("#qa_qualify").html(commaSeparateNumber(parsedJSON.qa_qualify));
			//$("#total_sent").html(commaSeparateNumber(parsedJSON.total_sent));
			//$("#fire_leads").html(commaSeparateNumber(parsedJSON.fire_leads));
			$("#total_leads").html(commaSeparateNumber(parsedJSON.total_leads));
			
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
$(document).ready(function(e){
	$(document).on('click', '.prod_header', function() {
		$(this).next().slideToggle();
	});
	$(document).on('change', '#search_by', function() {
		$('#search_param').val($(this).val());
		$('#product_name').val('');
		$('#ajax_auto_suggestion').html('');
	});
	
	
	$(document).on('change', '.size_availability', function() {
		var total_stock = $("#ajax_stock_available").attr("data-total-stock");
		var _divid = "#ajax_stock_available";
		if ($(this).val() == 1) {
			var _cmsurl = $(this).attr("data-cmsurl");
			var _prodid = $(this).attr("data-prodid");
			var _catid = $("#cat_id").val();
			var _subcatid = $("#sub_cat_id").val();
			var dataString = "&catid=" + _catid + "&subcatid=" + _subcatid + "&prodid=" + _prodid;
			
			if (_cmsurl != "") {
				$.ajax({
					type: "POST",
					url: _cmsurl + "/ajaxgetsize/index/",
					data: dataString,
					cache: false,
					beforeSend : function (){
						$(_divid).html("Please wait...").show();
					},
					success: function(e) {
						if(e == 'refresh') {
							alert("You need to refresh the page and try again.");
						} else {
							$(_divid).html(e);
						}
					}
				});
			} else {
				$(_divid).html("Need to refresh page !!");
			}
		} else {
			$(_divid).html('<p><label><span class="color_blue">Number of Stocks : </span></label><input class="text-input small-input" type="text" id="total_stock" name="total_stock" value="' + total_stock + '" tabindex="8" /></p>');
		}
	});
	
	$(document).on('keyup', '#product_name', function() {
		    var dataString = '';
			var searchbox = $(this).val();
			var cms_url = $("#cms_url_hidden").val();
			if(searchbox.length == 0){
				$("#ajax_auto_suggestion").html("").hide();
			}
			
			if(searchbox.length >= 3) {  
				var search_param = $("#search_param").val();	
				var size_param = $("#product_size").val();	
				
				if(typeof search_param != 'undefined') {
					var dataString = dataString + '&1=1&product_name='+searchbox+'&search_param='+search_param;		
				} else {
					var dataString = dataString + '&1=1&product_name='+searchbox+'&search_param=name';
				}
				
				if (searchbox=='') {
				} else {
					var fast_search = $.ajax({
										type: "POST",
										url: cms_url + "/ajaxproductname/index/",
										data: dataString,
										cache: false,
										beforeSend : function (){
											$("#ajax_auto_suggestion").html("<div class='suggested_entity_error'>Please wait...</div>").show();
										},
										success: function(html) {
											if(html != 'refresh') {
												$("#ajax_auto_suggestion").hide().html(html).show();
												$(".suggested_entity_link").click( function() {
													$('#product_name').val($(this).attr("entity-data"));
													$('#pid').val($(this).attr("entity-dataid"));
													$("#ajax_auto_suggestion").hide();
													
													// DO AJAX INSIDE AJAX
													if(typeof size_param != 'undefined') {
														var insideAjaxDataString = "&prodid=" + $(this).attr("entity-dataid");
														var fast_getsize = $.ajax({
															type: "POST",
															url: cms_url + "/ajaxproductname/getsize/",
															data: insideAjaxDataString,
															cache: false,
															beforeSend : function (){
																$("#product_size").html('<option>Please wait...</option>').attr('disabled','disabled');
															},
															success: function(res) {
																$("#product_size").html(res).removeAttr('disabled');
															}
														});
													}
												});
											}
										}
									});
				}
			}
			return false;    
		});
		
		$(document).on('focus', '#product_name', function() {
			if ($('#product_name').val() != "" && $("#ajax_auto_suggestion").html() != "") {
				$("#ajax_auto_suggestion").show();
			}
		});
	
		$(document).on('blur', '#product_name', function() {
			//setTimeout(function(){ $("#ajax_auto_suggestion").hide(); }, 100)
		});	
	
	$(".only_numeric").keydown(function(e){
		
		if(e.ctrlKey==true && (e.keyCode == '118' || e.keyCode == '86') || e.keyCode==17||e.keyCode==2||e.keyCode==46||e.keyCode==8||e.keyCode==9||e.keyCode==27||e.keyCode==13||e.keyCode==65&&e.ctrlKey===true||e.keyCode>=35&&e.keyCode<=39){return}else{if(e.shiftKey||(e.keyCode<48||e.keyCode>57)&&(e.keyCode<96||e.keyCode>105)){e.preventDefault()}}})})
		
		
function delete_order_product(url) {
	if(confirm("Are you sure you want to delete this product ??")) {
		window.location.href = url;	
	}
}
function set_seourl(name, seourl) {
	var name = $("#" + name).val();
	$("#" + seourl).val(name.toLowerCase().replace(/[_\W]+/g, "-"));
}
$(document).ready(function(e) {	
	$(document).on('blur', '#seourl', function(){
		if($(this).val() != "") {
			var _this 	= this
			var datatbl = $(_this).attr("data-tbl");
			var seourl  = $(_this).val();
			var siteurl = $(_this).attr("data-siteurl");
			var id_val 	= $(_this).attr("data-id");
			var id_name	= $(_this).attr("data-name");
			
			
			$.ajax({
				url: siteurl + "/checkSeoUrl/index/" + datatbl + "/" + seourl + "/" + id_val + "/" + id_name,
				type: "post",
				data: "&data=send",
				beforeSend:function(){
					$("#seourl_err").html("Checking Please wait...");
					$(_this).attr('readonly','readonly');
				},
				success:function(e) {
					$(_this).removeAttr('readonly');
					if(e == '1'){
						$("#seourl_err").html("<span class='color_green'>It's Unique SeoURL, Please proceed !!</span>");
					} else if(e == '0'){
						$(_this).parent().parent().addClass("has-error");
						$("#seourl_err").html("<span class='color_red'>Duplicate found change it!!</span>");
						$(_this).focus();
					} else if(e == 'refresh') {
						alert("Refresh your page and try again !!");
					}
				}
			});	
		}
	});	
});

function updateQuickNet(data_id) {
	var data_id = data_id;
	var dataString = "&data_id=" + data_id + 
					 "&pincode_type=" + $("#pincode_type_" + data_id).val() + 
					 "&handle_type=" + $("#handle_type_" + data_id).val() + 
					 "&fire_type=" + $("#fire_type_" + data_id).val() + 
					 "&goal=" + $("#goal_" + data_id).val() + 
					 "&primary_id=" + $("#primary_id_" + data_id).val() + 
					 "&primary_id_value=" + $("#primary_id_value_" + data_id).val() + 
					 "&from_date=" + $("#from_date_" + data_id).val() + 
					 "&timezone=" + $("#timezone").val() + 
					 "&to_date=" + $("#to_date_" + data_id).val() + 
					 "&net_id=" + $("#net_id_" + data_id).val() + 
					 "&search_form=" + $("#search_form").val() + 
					 "&data=send";
	var siteurl = $("#siteurl").val();
	
	if(typeof siteurl != 'undefined' && typeof dataString != 'undefined') {
		$.ajax({
			url: siteurl + "/ajaxnetworkupdate/nupdate/",
			type: "post",
			data: dataString,
			beforeSend:function(){
				$("#quick_upd_msg").hide();
			},
			success:function(e) {
				$("#quick_upd_msg").html(e).hide().slideDown(1000);
			}
		});
	}
}
function updateQuickPub(data_id) {
	var data_id = data_id;
	var dataString = "&data_id=" + data_id + 
					 "&pincode_type=" + $("#pincode_type_" + data_id).val() + 
					 "&fire_type=" + $("#fire_type_" + data_id).val() + 
					 "&pubname=" + $("#pub_name_" + data_id).val() + 
					 "&handle_type=" + $("#handle_type_" + data_id).val() + 
					 "&goal=" + $("#goal_" + data_id).val() + 
					 "&timezone=" + $("#timezone").val() + 
					 "&primary_id=" + $("#primary_id_" + data_id).val() + 
					 "&primary_id_value=" + $("#primary_id_value_" + data_id).val() + 
					 "&from_date=" + $("#from_date_" + data_id).val() + 
					 "&to_date=" + $("#to_date_" + data_id).val() + 
					 "&net_id=" + $("#net_id_" + data_id).val() + 
					 "&search_form=" + $("#search_form").val() + 
					 "&data=send";
	var siteurl = $("#siteurl").val();
	
	if(typeof siteurl != 'undefined' && typeof dataString != 'undefined') {
		$.ajax({
			url: siteurl + "/ajaxnetworkupdate/pupdate/",
			type: "post",
			data: dataString,
			beforeSend:function(){
				$("#quick_upd_msg").hide();
			},
			success:function(e) {
				$("#quick_upd_msg").html(e).hide().slideDown(1000);
			}
		});
	}
}
function create_order() {
	var _siteurl = $("#mode").attr('data-siteurl');
	var _name 	= $("#name").val();
	var _mobile = $("#mobile").val();
	var _email = $("#email").val();
	var _address = $("#address").val();
	var _city = $("#city").val();
	var _area = $("#area").val();
	var _state = $("#state").val();
	var _pincode = $("#pincode").val();
	var _alternate_phone = $("#alternate_phone").val();
	var _payment_status = $("#payment_status").val();
	var _mode = $("#mode").val();
	var _net = $("#net").val();
	

	if(_mode != '') {
		if(_mode.toLowerCase() == 'first') {
			$.ajax({
				url: _siteurl + "/createorder/add/",
				type: "post",
				data: "&submit=submit&name="+_name+"&mobile="+_mobile+"&email="+_email+"&address="+_address+"&city="+_city+"&area="+_area+"&state="+_state+"&pincode="+_pincode+"&alternate_phone="+_alternate_phone+"&payment_status="+_payment_status+"&mode="+_mode+"&net="+_net,
				beforeSend:function(){
					$("#submit_btn").html('<input class="button" type="button" value="Please wait..." />');
				},
				success:function(e){
					if(e == 'require_blank') {
						$("#error_msg").html('<div class="notification error png_bg"><a href="#" class="close"><img src="'+_siteurl+'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>Make sure you have entered valid value for require fields.</div></div>');
						$("#submit_btn").html('<input class="button" type="submit" name="submit" id="submit" value="NEXT >>" />');
					} else if(e == 'temp_error') {
						$("#error_msg").html('<div class="notification error png_bg"><a href="#" class="close"><img src="'+_siteurl+'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>* Temporary error occure refresh and try again.</div></div>');
						$("#submit_btn").html('<input class="button" type="submit" name="submit" id="submit" value="NEXT >>" />');
					} else {
						window.location.href = e;
					}
					$('html, body').animate({scrollTop: $("#atab").offset().top}, 1000);
				},
				error:function(e){
					console.log(e)
				}
			});
		}
	} else {
		alert('Something went wrong!! please refresh your page.');
	}
}
function update_order_product(siteurl, i, ord_id, ord_prod_id) {
	
	if (($("#prod_qty_" + i).val() != $("#quantity_" + i).val()) &&  $("#product_price_" + i).val() != '') {	
		
		$.ajax({
			url: siteurl + "/orderproducts/update/" + ord_id + "/" + ord_prod_id + "/" + $("#prod_qty_" + i).val() + "/" + $("#product_price_" + i).val() + "/" + $("#prod_price_" + i).val() + "/" + $("#pay_md_" + i).val(),
			type: "post",
			data: "&data=send",
			beforeSend:function(){
				$("#tbl_ord_prod_" + i).hide();
				$("#loader_" + i).show();
			},
			success:function(e){
				var parsed = $.parseJSON(e);
				
				// PRODUCT PRICE UPDATE WITH HIDDEN AND SHOWN FIELD
				$("#prod_price_" + i).val(parsed.price);
				$("#product_price_" + i).val(parsed.price);
				
				// PRODUCT QTY UPDATE WITH HIDDEN AND SHOWN FIELD
				$("#prod_qty_" + i).val(parsed.quantity);
				$("#quantity_" + i).val(parsed.quantity);
				
				// CHANGE DATE UPDATED
				$("#dt_upd_" + i).html(parsed.dateupdated);
				
				// PRODUCT TOTAL PRICE UPDATE WITH HIDDEN AND SHOWN FIELD
				$("#ttl_price_" + i).html(parsed.total_price);
				
				// HIDE THE LODER AND DISPLAY DETAILS
				$("#loader_" + i).hide();
				$("#tbl_ord_prod_" + i).show();
				
				$("#tbl_ord_prod_" + i).css('background-color','#B2DFFF')
										.css("-webkit-transition","all 1s ease")
										.css("-moz-transition","all 1s ease")
										.css("-o-transition","all 1s ease")
										.css("-ms-transition","all 1s ease");
				setTimeout(function() {$("#tbl_ord_prod_" + i).css('background-color','#f3f3f3');}, 2000);

			},
			error:function(e,t,n){
				alert(e.responseText)
			}
		});
	} else {
		$('#msg_' + i).show().html('<span class="small_red_msg">No changed detected</span>');
		setTimeout(function() {$('#msg_' + i).fadeOut(1000);}, 2000);
	}
}
function order_logs(siteurl, ordid) {
	
	if (siteurl != "" && ordid != "" && ordid > 0) {	
		
		$.ajax({
			url: siteurl + "/ajaxorderlogs/index",
			type: "post",
			data: "&data=send&ordid=" + ordid,
			beforeSend:function(){
				$("#order_logs").html('Please wait ...');
			},
			success:function(e){
				$("#order_logs").html(e);
			},
			error:function(e,t,n){
				alert(e.responseText)
			}
		});
	} else {
		alert('Please refresh the page and try again');
	}
}
function credit_logs(siteurl, usrid) {
	
	if (siteurl != "" && usrid != "" && usrid > 0) {	
		
		$.ajax({
			url: siteurl + "/ajaxcreditlogs/index",
			type: "post",
			data: "&data=send&usrid=" + usrid,
			beforeSend:function(){
				$("#credit_logs").html('Please wait ...');
			},
			success:function(e){
				$("#credit_logs").html(e);
			},
			error:function(e,t,n){
				alert(e.responseText)
			}
		});
	} else {
		alert('Please refresh the page and try again');
	}
}
function commaSeparateNumber(val){
	if (typeof val != 'undefined') {
		while (/(\d+)(\d{3})/.test(val.toString())){
		  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}
	}
	return val;
}
$(document).on('keyup', '.account_keyword_suggestion', function() {
    var dataString = '';
    var searchbox = $(this).val();
    var curId = $(this).attr('id');
    var cms_url = $("#cms_url_hidden").val();
    if(searchbox.length == 0){
        $("#ajax_auto_suggestion").html("").hide();
    }
    
    if(searchbox.length >= 1) {  
        var dataString = dataString + '&1=1&product_name='+searchbox;
        
        if (searchbox=='') {
        } else {
            var fast_search = 
				$.ajax({
					type: "POST",
					url: cms_url + "/changeAccountProductname/prdSuggestion/",
					data: dataString,
					cache: false,
					beforeSend : function (){
					},
					success: function(html) {
						if(html != 'refresh') {
							$("#ajax_auto_suggestion").hide().html(html).show();
							$(".suggested_entity_link").click( function() {
								$('#' + curId).val($(this).attr("entity-data"));
								$("#ajax_auto_suggestion").hide();
							});
							$(".account_keyword_suggestion").focus(function() {
								$("#ajax_auto_suggestion").show();
							});
						}
					}
				});
        }
    }
    return false;    
});
$(document).ready(function(e) {
	$(document).on('change', '#netGetPub', function() {
		var th = th;
		var cmsurl = $(this).attr("data-cmsurl");
		
		if (cmsurl != '' && th != '') {
			$.ajax({
				type: "POST",
				url: cmsurl + "/reportnetwise/getPubs/",
				data: 'net=' + $(this).val(),
				beforeSend : function (){
					$("#ajaxPublisher").html('Fetching Publisher...');
				},
				success: function(html) {
					$("#ajaxPublisher").html(html);
				}
			});
		} else {
			alert('refresh your page try again');
		}
	});    
});


function send_diet_chart(o_id, ul, s ){
	
	$.ajax({ 
		url:ul+"/"+o_id,
		type:"post",
		data:"&data=diet",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			
			if(e=="refresh"){
				alert("Please refresh your page and try again !!");
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e){
			alert(e.responseText)
		}
	})
}


$(function(){        
    /* reportrange */
    if($("#reportrange").length > 0){   
        $("#reportrange").daterangepicker({                    
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().format('YYYY-MM-DD')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'YYYY-MM-DD',
            separator: ' - ',
            startDate: moment(),
            endDate: moment()            
          },function(start, end) {
			  $('#reportrange ').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        });
        	
        $("#reportrange ").val(moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));
        //$("#reportrange ").val(moment().subtract('days', 29).format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'));

    }
});