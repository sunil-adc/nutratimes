<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']  = "homepage";
$route['aboutus'] 			  = "info/aboutus";
$route['about-us'] 			  = "info/aboutus";
$route['privacy-policy'] 	  = "info/privacypolicy";
$route['disclaimer-policy']   = "info/disclaimer_policy";
$route['terms-and-conditions']= "info/terms_conditions";
$route['return-policy'] 	  = "info/returnpolicy";
$route['privacy-policy']	  = "info/privacypolicy";
$route['cancellation-policy'] = "info/cancellationpolicy";
$route['refund-policy']	      = "info/refundpolicy";
$route['shipping-policy']	  = "info/shippingpolicy";
$route['contactus']           = "info/contactus";
$route['contact-us']          = "info/contactus";
$route['404_override'] 		  = 'default_page';
$route['garcinia-cambogia-ingredients'] = 'ingredients';
$route['garcinia-benefits']   = 'benefits';
$route['garcinia-cambogia-testimonials'] = 'testimonials';
$route['how-it-work'] 	      = 'howitwork';
$route['blog/(:any)'] 		  = 'blog/details/$1';

$route['product/cart_handler']= 'product/cart_handler';

$route['product/(:any)'] 	  = 'product/details/$1';
$route['buyregister'] 	      = 'product/user_register';
$route['products'] 	  		  = 'search';

/* End of file routes.php */
/* Location: ./application/config/routes.php */