<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// LOAD THE DEFAULT TIMEZONE IN WHOLE PROJECT START 
date_default_timezone_set('Asia/Calcutta');
// LOAD THE DEFAULT TIMEZONE IN WHOLE PROJECT END

/*
|--------------------------------------------------------------------------
| File and Directory Modes 
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define("CMS_FOLDER_NAME", 			"cms");
define("PROTOCOL",					"http");
define("HTTP_PROTOCOL",				"http");
define("HTTPS_PROTOCOL",			"https");

if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'panelss.in' ) {
	define("SITE_URL", 			PROTOCOL."://".$_SERVER['HTTP_HOST'] . "/Dropbox/trunk/nutratimes/");
	define("SITE_URL_NEW", 		PROTOCOL."://".$_SERVER['HTTP_HOST'] . "/Dropbox/trunk/nutratimes/");
	define("DB_NAME",			"nutra_times");
} else {
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$domainName = $_SERVER['HTTP_HOST'].'/';
    
	//define("SITE_URL", 			PROTOCOL."://".$_SERVER['HTTP_HOST'] . "/");
	define("SITE_URL", 			$protocol.$_SERVER['HTTP_HOST'] . "/");
	define("SITE_URL_NEW", 		$protocol.$_SERVER['HTTP_HOST'] );
	define("DB_NAME",			"nutra_times");
}


define("S3_URL", 			SITE_URL);

// AMAZON S3 ACCESS KEYS
define('AWS_ACCESS_KEY', 		'AKIAJS55MWWEG5DLH2IA');
define('AWS_SECRET_KEY', 		'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');
define('S3_BUCKET', 			'NutraTimes');
define("S3_CMS_URL", 			S3_URL."cms/");	

/* CUSTOM CONSTANTS FOR shophunk.COM START */
define("INDEX_FILE",				"");
define("FRONT_INC",					"inc/");
define("ADMIN_DEFAULT_CONTROLLER", 	"cmslogin");
define("ADMIN_LOGOUT_CONTROLLER", 	"logout");
define("FULL_SITE_URL", 			SITE_URL.INDEX_FILE);
define("CMS_URL", 					SITE_URL.CMS_FOLDER_NAME."/");
define("FULL_CMS_URL", 				SITE_URL.INDEX_FILE.CMS_FOLDER_NAME);
define("SITE_NAME", 				"NutraTimes");
define("SITE_TITLE", 				"NutraTimes : Online Natural Remedies");
define("ADMIN_TITLE", 				"NutraTimes CMS");
/* CUSTOM CONSTANTS FOR Shophunk.COM END */

define("CITRUS_ACCESS_KEY", "1AGMG20FE7Z13NKBC8EQ");
define("CITRUS_SECRET_KEY", "1fe1209b5ac5046e9a0477ac8fca8effac7dcce3");

/* TABLE CONSTANTS START */

define('ADMIN', 			'tbl_admin_users');
define('ADMIN_LOGIN', 		'tbl_admin_login');
define('ADMIN_ROLE', 		'tbl_admin_role');
define('PRODUCT', 			'tbl_product');
define('PRODUCT_IMAGE', 	'tbl_product_image');
define('PRODUCT_LOGS', 		'tbl_product_logs');
define('CREDIT_LOGS', 		'tbl_credit_logs');
define('ORDER_LOGS', 		'tbl_order_logs');
define('COUNTRY', 			'tbl_country');
define('CATEGORY', 			'tbl_category');
define('BANNERS', 			'tbl_banner');
define('BRANDS', 			'tbl_brands');
define('STATIC_PAGE', 		'tbl_static_page');
define('GENRE', 			'tbl_genre');
define('PROVIDERS', 		'tbl_providers');
define('PARENT_PINCODE', 	'tbl_parent_pincode');
define('PIXELS', 			'tbl_net_pixel');
define('ORDER', 			'tbl_orders');
define('ORDER_PG', 			'tbl_orders_pg');
define('PAYMENT_GATEWAY', 	'tbl_payment_gateway');
define('AWB_BLUEDART', 		'tbl_awb_bluedart');
define('AWB_DTDC', 			'tbl_awb_dtdc');
define('AWB_FIRSTFLIGHT', 	'tbl_awb_firstflight');
define('AWB_GMS', 			'tbl_awb_gms');
define('AWB_SEAJIN', 		'tbl_awb_seajin');
define('AWB_RED_EXPRESS', 	'tbl_red_express');
define('AWB_ECOM_EXPRESS', 	'tbl_ecom_express');
define('BLUDART_CODE', 		'tbl_bluedart_code');
define('USERS', 			'tbl_users');
define('USER_PRODUCTS', 	'tbl_user_products');
define('POINT_HISTORY', 	'tbl_points_history');
define('SETTINGS', 			'tbl_settings');
define('PUBS',				'tbl_pubs');
define('PRODUCT_GENRE',		'tbl_product_genre');
define('NEWSLETTER',		'tbl_newsletter');
define('INVOICE_HISTORY',	'tbl_invoice_history');
define('ACCOUNT_KEYWORDS',	'tbl_account_keywords');
define('SLIDER',			'tbl_slider');
define('FEEDBACK',			'tbl_feedback');
define('COMPLAINT',			'tbl_complaints');
define('CONTACTUS',			'tbl_contact_details');
define('AFFILATE',			'tbl_affilate');
define('WISHLIST',			'tbl_wishlist');
define('COLORS',			'tbl_color');
define('DB_PINCODE',		'tbl_pincode');
define('RECONCILE_LOG',		'tbl_reconcile_log');
define('REMINDER_SENT',		'tbl_email_counter');
define('REFUND_STATUS',		'tbl_refund_request');
define('SUPPORT_TICKET',	'tbl_support_ticket');
define('PRODUCT_COMBO',		'tbl_product_combo');
define('ACCOUNT_MANAGER',	'tbl_account_manager');
define('XPRESSBEE_TMP',		'tbl_xpressbee_tmp');
define('BLOG',				'tbl_blog');


define('AWB_XPRESSBEE', 'tbl_awb_xpress_bee');


/* TABLE CONSTANTS END */

/* VARIABLE CONSTANTS START */
define('IMAGE_UPLOAD_MAX_SIZE', 4*1024*1024);
/* VARIABLE CONSTANTS END */

/* FOLDER NAME CONSTANT START */
define("CACHE_EXTRA", "basdASDFASFAsfasdgas1");
define("CACHE_LONGTIME", "43200");
define("CACHE_LAZY", '600');
define("CACHE_QUICK", '300');
define("CONTENT_DIR", "cdn/");
define("MAIN_CONTENT_FLDR", S3_URL.CONTENT_DIR."/");
define('INTER_USE_DIR', CONTENT_DIR.'/sys_d235f52dsaqer521');

// PRODUCT DIR CONSTANTS
define("PRODUCT_DIR_NAME", "prd_r325fc224q");
define("PRODUCT_DIR_PATH", CONTENT_DIR."/".PRODUCT_DIR_NAME."/");

// 200 BY 250 - START
define("PRODUCT_THUMB_200_250", "prd_thumb_200_250");
define("PRODUCT_THUMB_200_250_PATH", PRODUCT_DIR_PATH.PRODUCT_THUMB_200_250."/");
// 200 BY 250 - END


// 90 BY 120 - START
define("PRODUCT_THUMB_200_300", "prd_thumb_200_300");
define("PRODUCT_THUMB_200_300_PATH", PRODUCT_DIR_PATH.PRODUCT_THUMB_200_300."/");
// 90 BY 120 - END

// 130 BY 130 - START
define("PRODUCT_THUMB_120_150", "prd_thumb_120_150");
define("PRODUCT_THUMB_120_150_PATH", PRODUCT_DIR_PATH.PRODUCT_THUMB_120_150."/");
// 130 BY 130 - END

// 300 BY 400 - START
define("PRODUCT_THUMB_350_450", "prd_thumb_350_450");
define("PRODUCT_THUMB_350_450_PATH", PRODUCT_DIR_PATH.PRODUCT_THUMB_350_450."/");
// 300 BY 400 - END

// BRAND DIR CONSTANTS
define("BRAND_DIR_NAME", "brnd_ds7853rdas");
define("BRAND_THUMB_DIR_NAME", "brnd_thumb_2g62346qd");
define("BRAND_DIR_PATH", CONTENT_DIR."/".BRAND_DIR_NAME."/");
define("BRAND_THUMB_IMG_DIR", BRAND_DIR_PATH.BRAND_THUMB_DIR_NAME."/");

define("THUMB_PREFIX", "thumb-");
define("PLACE_HOLDER", "./images/placeholder.png");

// CATEGORY DIR CONSTANTS
define("CATEGORY_DIR_NAME", "cat_9cfs43fjd60n");
define("CATEGORY_THUMB_DIR_NAME", "cat_thumb_casdn5qf");
define("CATEGORY_DIR_PATH", CONTENT_DIR."/".CATEGORY_DIR_NAME."/");
define("CATEGORY_THUMB_IMG_DIR", CATEGORY_DIR_PATH.CATEGORY_THUMB_DIR_NAME."/");
/* FOLDER NAME CONSTANT END */

define("BANNER_DIR_NAME", "affiliate_banner");
define("BANNER_THUMB_DIR_NAME", "affiliate_banner_thumb");
define("BANNER_DIR_PATH", CONTENT_DIR."/".BANNER_DIR_NAME."/");
define("BANNER_THUMB_IMG_DIR", BANNER_DIR_PATH.BANNER_THUMB_DIR_NAME."/");

define('PRODUCT_COST','200');
define('SHIPMENT_COST_DELIVERED','110');
define('MANPOWER_COST','16000');
define('RETURN_COST','170');

// SENSITIVE PAGES CONSTANTS
define("PD_PAGE",'product');
define("PD_FUNC",'details');
// SENSITIVE PAGES CONSTANTS

// ADMIN EMAIL ADDRESS FOR MAILING - START
define("SETTING_ID", "1");
// ADMIN EMAIL ADDRESS FOR MAILING - END

define("BLOG_IMAGE_FOLDER", "cdn/image/blog/");
define("BLOG_CROP_FOLDER", "cdn/image/blog/blog_crop/");


define("ALLOWED_IP", "106.51.76.48, 125.99.253.162");

define("PRODUCT_CONTEST_SEND", "3342");

define("GST", "29AAPFN3973L1ZD");
define("HSNCODE",  "30039011");    
define("SITE_ADDR_1", "NATURESCOPE WELLNESS LLP , #565, Axis Pennmark, 4th Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, Bengaluru-560078");
define("SITE_ADDR_2", "");
define("SITE_ADDR_3", "");
define("RETURN_PINCODE", "560078");
define("office_address", "NATURESCOPE WELLNESS LLP  #565 Axis Pennmark 4th Floor JP Nagar 3rd Phase 9th Cross Near Sony Center");
define("office_state", 	 "karnataka");
define("office_city", 	 "Bengaluru");
define("office_pincode", "560078");
define("office_number", "08046328320");

define("ADMIN_FILE_DOWNLOAD_ROLE", "1,4,7,9,11,12,14,15,18");
define("ADMIN_FILE_USER_VIEW", "1,4,7,9,10,11,18");
define("ADMIN_EXCEL_DOWNLOAD_ROLE", "10,12,14,15,16");
define("SEARCH_ORDER_NETWORK", "1,7,13,16");
define("ADMIN_MARKETING_BNR_DOWNLOAD", "1,7");
/*define("ADMIN_PAYMENT_ROLES", "1,10,15");
define("ADMIN_PAYMENT_USERS", "1,219,220,223,315,329");*/
//--//Koushik. 23-Jan-2015 Changed the users permission based on the Anil's feedback
define("ADMIN_PAYMENT_ROLES", "1");
define("ADMIN_PAYMENT_USERS", "5, 7");
define("ADMIN_SEND_AFTER_ROLES", "1,10");
define("ADMIN_USER_QUICK_LOGIN", "56,187,189");
define("ADMIN_SAVE_ORDER", "329");
define("ADMIN_PRODUCT_EXCEL", "1,4,15");
define("EMAIL_DOWNLOAD_EMAIL", "atul@adcanopus.com");

define("DISCOUNT_PERCENTAGE", "15"); 

/* End of file constants.php */
/* Location: ./application/config/constants.php */

/* Location: ./application/config/constants.php */


//////////END///////////////////

// Slider DIR CONSTANTS
define("SLIDER_DIR_NAME", "slide_9cfs43fjd60n");
define("SLIDER_DIR_PATH", CONTENT_DIR."/".SLIDER_DIR_NAME."/");
/* End of file constants.php */
