<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists ('is_Admin_Login') ) {
	function is_Admin_Login($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
			
			if(strtolower($cur_controller) == 'extra_call') {
				// IF ITS AJAX CALL THEN DONT CHECK FOR PERMISSION
				return true;
			} else {
				// OTHERWISE CHECK FOR PERMISSION

				if (check_role_permission ($admin_role_details, $cur_controller) != false || $cur_controller == "dashboard") {
					// IF USER IS ALLOWED THEN KEEP HE/SHE TO THAT PAGE	
					return true;
				} else {
					// ELSE REFIRECT TO DASHBOARD AND GIVE WARNING MESSAGE
					@session_start();
					$_SESSION['permission_warning'] = 'You are not permitted to view that page, Consider this message as a warning !!';
					redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);		
				}
			}
		}else{
			// THEN REDIRECT
			redirect(FULL_CMS_URL."/".ADMIN_LOGOUT_CONTROLLER);
		}
	}
}
if(! function_exists('check_role_permission')) {
	function check_role_permission($role_details, $cur_controller) {
				
		// CONVERT PERMISSION IN SINGLE ARRAY AND MAKE IT CLEAN PER ELEMENT ONE CONTROLLER
		$arr_permission = create_permission_array($role_details);
		
		// CHECK USER REQUESTED PAGE IS PERMITTED TO THAT USER OR ROLE WHICH IS GRANTED TO USER
		if (in_array(strtolower($cur_controller), $arr_permission)) {
			return true;	
		} else {
			return false;
		}
	}
}
if(! function_exists('create_permission_array')) {
	function create_permission_array($role_details) {
		// UNSERIALIZE ARRAY AND STORE IN VARIABLE
		$arr_role_details = unserialize($role_details);
		
		// SEPERATE THE DOUBLE VARIALE
		foreach($arr_role_details as $k => $v) {
			
			// EXTRACT THE STRIG FROM COMMA
			$arr_cur_val = explode(",", $v);
			foreach ($arr_cur_val as $k1 => $v1) {
				$arr_permission[] = strtolower($v1);
			}
		}
		return $arr_permission;
	}	
}
if(! function_exists ('is_Admin_Login_Redirect') ) {
	function is_Admin_Login_Redirect ($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {	
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
		
			// THEN REFIRECT 
			redirect(FULL_CMS_URL."/dashboard");
		} else {
			// DO NOTHING
			return true;
		}
	}	
}
function array_admin_file_download_role($role_id) {
	$arr_role = explode(",", ADMIN_FILE_DOWNLOAD_ROLE);
	
	if(is_array($arr_role) && in_array($role_id, $arr_role)) {
		return true;
	} 
	return false;
}
function array_admin_user_view($role_id) {
	$arr_role = explode(",", ADMIN_FILE_USER_VIEW);
	
	if(is_array($arr_role) && in_array($role_id, $arr_role)) {
		return true;
	} 
	return false;
}
function array_excel_file_download_role($role_id) {
	$arr_role = explode(",", ADMIN_EXCEL_DOWNLOAD_ROLE);
	
	if(is_array($arr_role) && in_array($role_id, $arr_role)) {
		return true;
	} 
	return false;
}
function array_admin_marketing_bnr($role_id) {
	$arr_role = explode(",", ADMIN_MARKETING_BNR_DOWNLOAD);
	
	if(is_array($arr_role) && in_array($role_id, $arr_role)) {
		return true;
	} 
	return false;
}
function array_constant_check($role_id, $constant=0) {
	$arr_role = explode(",", $constant);
	
	if(is_array($arr_role) && in_array($role_id, $arr_role)) {
		return true;
	} 
	return false;
}
if(! function_exists('is_ip_allowed')) {
	function is_ip_allowed() {
		return true;
		/*$arr_ip_range = explode(", ", ALLOWED_IP);
		if(in_array($_SERVER['REMOTE_ADDR'], $arr_ip_range)) {
			return true;
		} else {
			redirect(SITE_URL);
		}*/
	}
}
if(! function_exists('is_ip_login_allowed')) {
	function is_ip_login_allowed() {
		
		$arr_ip_range = explode(", ", ALLOWED_IP);
		if(in_array($_SERVER['REMOTE_ADDR'], $arr_ip_range)) {
			return true;
		} else {
			return false;
		}
	}
}
if(! function_exists('is_ip_download_allowed')) {
	function is_ip_download_allowed() {

		$arr_ip_range = explode(", ", ALLOWED_IP);
		if(in_array($_SERVER['REMOTE_ADDR'], $arr_ip_range)) {
			return true;
		} else {
			return false;
		}
	}
}
if(! function_exists('is_user_login')) {
	function is_user_login($user_id, $email, $mobile){
		if(strlen (trim ($user_id)) > 0 && strlen (trim ($email)) > 0 && strlen (trim ($mobile)) > 0 ) {
			return true;
		} else {
			return false;
		}
	}
}
if(! function_exists ('getOS') ) {
	function getOS() { 
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$os_platform    =   "Unknown OS Platform";
		$os_array       =   array(
								'/windows nt 6.2/i'     =>  'Windows 8',
								'/windows nt 6.1/i'     =>  'Windows 7',
								'/windows nt 6.0/i'     =>  'Windows Vista',
								'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
								'/windows nt 5.1/i'     =>  'Windows XP',
								'/windows xp/i'         =>  'Windows XP',
								'/windows nt 5.0/i'     =>  'Windows 2000',
								'/windows me/i'         =>  'Windows ME',
								'/win98/i'              =>  'Windows 98',
								'/win95/i'              =>  'Windows 95',
								'/win16/i'              =>  'Windows 3.11',
								'/macintosh|mac os x/i' =>  'Mac OS X',
								'/mac_powerpc/i'        =>  'Mac OS 9',
								'/linux/i'              =>  'Linux',
								'/ubuntu/i'             =>  'Ubuntu',
								'/iphone/i'             =>  'iPhone',
								'/ipod/i'               =>  'iPod',
								'/ipad/i'               =>  'iPad',
								'/android/i'            =>  'Android',
								'/blackberry/i'         =>  'BlackBerry',
								'/webos/i'              =>  'Mobile'
							);
	
		foreach ($os_array as $regex => $value) { 
	
			if (preg_match($regex, $user_agent)) {
				$os_platform    =   $value;
			}
		}   
		return $os_platform;	
	}
}
if(! function_exists ('getBrowser') ){	
	function getBrowser() {
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$browser        =   "Unknown Browser";
		$browser_array  =   array(
								'/msie/i'       =>  'Internet Explorer',
								'/firefox/i'    =>  'Firefox',
								'/safari/i'     =>  'Safari',
								'/chrome/i'     =>  'Chrome',
								'/opera/i'      =>  'Opera',
								'/netscape/i'   =>  'Netscape',
								'/maxthon/i'    =>  'Maxthon',
								'/konqueror/i'  =>  'Konqueror',
								'/mobile/i'     =>  'Handheld Browser'
							);
	
		foreach ($browser_array as $regex => $value) { 
			if (preg_match($regex, $user_agent)) {
				$browser    =   $value;
			}
		}
		return $browser;
	}	
}
if(! function_exists("get_IP")) {
	 function get_IP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }	
}
if(! function_exists("time_diff") ){	
	function time_diff($created_time) {
         //Change as per your default time
        $str = strtotime($created_time);
        $today = strtotime(date('Y-m-d H:i:s'));
		
		// It returns the time difference in Seconds...
        $time_differnce = $today-$str;

        // To Calculate the time difference in Years...
        $years = 60*60*24*365;

        // To Calculate the time difference in Months...
        $months = 60*60*24*30;

        // To Calculate the time difference in Days...
        $days = 60*60*24;

        // To Calculate the time difference in Hours...
        $hours = 60*60;

        // To Calculate the time difference in Minutes...
        $minutes = 60;

        if(intval($time_differnce / $years) > 1)
        	return intval($time_differnce / $years)." years ago";
		else if(intval($time_differnce / $years) > 0)
            return intval($time_differnce / $years)." year ago";
        else if(intval($time_differnce / $months) > 1)
            return intval($time_differnce / $months)." months ago";
        else if(intval(($time_differnce / $months)) > 0)
            return intval(($time_differnce / $months))." month ago";
        else if(intval(($time_differnce / $days)) > 1)
            return intval(($time_differnce / $days))." days ago";
        else if (intval(($time_differnce / $days)) > 0) 
            return intval(($time_differnce / $days))." day ago";
        else if (intval(($time_differnce / $hours)) > 1) 
            return intval(($time_differnce / $hours))." hours ago";
        else if (intval(($time_differnce / $hours)) > 0) 
            return intval(($time_differnce / $hours))." hour ago";
        else if (intval(($time_differnce / $minutes)) > 1) 
            return intval(($time_differnce / $minutes))." minutes ago";
        else if (intval(($time_differnce / $minutes)) > 0) 
            return intval(($time_differnce / $minutes))." minute ago";
        else if (intval(($time_differnce)) > 1) 
            return intval(($time_differnce))." seconds ago";
        else
            return "few seconds ago";
  }
}

if (! function_exists("_file_exist") ) {
	function _file_exist($file, $dir) {
		if (is_file($dir.$file)) { 
			return true;
		}else if (is_file("../".$dir.$file)) { 
			return true;
		} else {
			return false;
		}
	}
}

if (! function_exists("_random_word") ) {
	function _random_word( $length, $case=0 ) {
	   $word = ""; 
	   for ( $ix = 1; $ix <= $length; $ix++) { 
		  $word .= chr(rand(97, 122)); 
	   }
	   if($case == 1) { 
		  $word = strtoupper($word); 
	   } elseif($case == 2) { 
		  $word = ucwords($word); 	   
	   } 
	   return($word); 
	}
}

if (! function_exists("_pre") ) {
	function _pre($arr) {
		if(is_array($arr)){
			return "<pre>".print_r($arr)."</pre>";
		}else{
			return $arr;
		}
	}
}

if (! function_exists("array_lower")) {
	function array_lower($arr) {
		if(is_array($arr)) {
			foreach ($arr as $k => $v) {
				if(is_array($v)) {
					$new_arr[$k] = array_lower($v);
				} else {
					$new_arr[$k] = strtolower($v);
				}
			}
		}
		return $new_arr; 
	}
}

if( ! function_exists("_isset")) { 
	function _isset($str, $echo_or_return="echo") {
		if ( isset ($str)) {
			if ( trim ($str) != "") {
				if ($echo_or_return == "echo") {
					echo $str;
				} else {
					return $str;
				}
			}
		}
	}
}

if( ! function_exists("_isset_req")) { 
	function _isset_req($str, $check = "0") {
		if ( isset ($str)) {
			if ( trim ($str) != "") {
				if ($str == $check) {
					return 'selected="selected"';
				}
			}
		}	
		return false;
	}
}

if( ! function_exists("_isset_trim")) { 
	function _isset_trim($req="") {
		if ( isset ($req)) {
			if ( trim ($req) != "") {
				return true;
			}
		}	
		return false;
	}
}

if( ! function_exists("full_url")) { 
	function full_url()
	{
	   $ci=& get_instance();
	   $return = $ci->config->site_url()."/".$ci->uri->uri_string();
	   if(count($_GET) > 0)
	   {
		  $get =  array();
		  foreach($_GET as $key => $val)
		  {
			 echo $val;
			 $get[] = $key.'='.$val;
		  }
		  $return .= '?'.implode('&',$get);
	   }
	   return $return;
	} 
}
if( ! function_exists("_get_size_wise_directory")) {
	function _get_size_wise_directory($DirectoryPath) {
		
		$Size = 0;
		$arr = array();
		$Dir = opendir($DirectoryPath);
	 
		if (!$Dir)
			return -1;
	 	
		$i = 0;
		while (($File = readdir($Dir)) !== false) {
	 
			// Skip file pointers
			if ($File[0] == '.') continue; 
	 
			// Go recursive down, or add the file size
			if (is_dir($DirectoryPath . $File))            
				$arr[$i][0] = $File;
				$arr[$i][1] = calculate_dir_size($DirectoryPath . $File . DIRECTORY_SEPARATOR);
			$i++;
		}
	 
		closedir($Dir);
		return (count($arr) > 0) ? $arr : false;
	}
}
if( ! function_exists("calculate_dir_size")) {
	 
	function calculate_dir_size($DirectoryPath) {
	 
		$Size = 0;
	 
		$Dir = opendir($DirectoryPath);
	 
		if (!$Dir)
			return -1;
	 
		while (($File = readdir($Dir)) !== false) {
	 
			// Skip file pointers
			if ($File[0] == '.') continue; 
	 
			// Go recursive down, or add the file size
			if (is_dir($DirectoryPath . $File))            
				$Size += calculate_dir_size($DirectoryPath . $File . DIRECTORY_SEPARATOR);
			else 
				$Size += filesize($DirectoryPath . $File);        
		}
	 
		closedir($Dir);
	 
		return $Size;
	}
}
if( ! function_exists("byteFormat")) {
	function byteFormat($bytes) {
			if ($bytes >= 1073741824) {
				$bytes = number_format($bytes / 1073741824, 2) . ' GB';
			} elseif ($bytes >= 1048576) {
				$bytes = number_format($bytes / 1048576, 2) . ' MB';
			} elseif ($bytes >= 1024) {
				$bytes = number_format($bytes / 1024, 2) . ' KB';
			} elseif ($bytes > 1) {
				$bytes = $bytes . ' bytes';
			} elseif ($bytes == 1) {
				$bytes = $bytes . ' byte';
			} else {
				$bytes = '0 bytes';
			}
			return $bytes;
	}
}
if( ! function_exists("_add_slashes")) {
	function _add_slashes($arr) {
		if( is_array($arr)){
			foreach($arr as $k => $v) {
				$arr[$k] = add_slashes($v);
			}
			return $arr;
		} else {
			return addslashes($arr);
		}
	}
}
if( ! function_exists("_strip_slashes")) {
	function _strip_slashes($str) {
		if (is_array($str)) {
			foreach ($str as $k => $v) {
				$str[$k] = strip_slashes($v);
			}
		} else {
			$str = stripslashes($str);
		}
		return $str;
	}
}

if( ! function_exists("allowed_extensions")) {
	function allowed_extensions($filename = "", $type = "image") {
		
		$allow_ext = array("image" => array("jpg", "jpeg", "png", "gif", "bmp"));
					
		if(trim($filename) != "") {
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(in_array(strtolower($ext), $allow_ext[$type])) {
				return strtolower($ext);
			}
		}
		return false;
	}
}
if( ! function_exists("get_rand_chracter")) {
	function get_rand_chracter($word_size = '8', $underscore = "0") {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    	$result = '';
    	if($underscore > 0){
			for($j = 1; $j <= $underscore; $j++) {
				for ($i = 0; $i < $word_size; $i++) {
					$result .= $characters[mt_rand(0, 61)];
				}
				$result .= "_";
			}
		}
		return trim($result,"_");
	}
}
if( ! function_exists("make_img_thumb")) {
	function make_img_thumb($src, $dest, $desired_width) {
	
		$ext = pathinfo($src, PATHINFO_EXTENSION);
		
		if(strtolower($ext) == 'jpeg' || strtolower($ext) == 'jpg') {
			$source_image = imagecreatefromjpeg($src);					
		} else if(strtolower($ext) == 'png') {
			$source_image = imagecreatefrompng($src);					
		} else if(strtolower($ext) == 'gif') {
			$source_image = imagecreatefromgif($src);					
		}
		
		$width = imagesx($source_image);
		$height = imagesy($source_image);
		
		if($width > $desired_width + 100) {
		
			/* find the "desired height" of this thumbnail, relative to the desired width  */
			$desired_height = floor($height * ($desired_width / $width));
			
			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			
			if(strtolower($ext) == "gif" or strtolower($ext) == "png"){
				$white = imagecolorallocate($virtual_image, 255, 255, 255);
				imagecolortransparent($virtual_image, $white);
				imagefill($virtual_image, 0, 0, $white);

			}
			
			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			
			/* create the physical thumbnail image to its destination */
			
			if(strtolower($ext) == 'jpeg' || strtolower($ext) == 'jpg') {
				imagejpeg($virtual_image, $dest);					
			} else if(strtolower($ext) == 'png') {
				imagepng($virtual_image, $dest);					
			} else if(strtolower($ext) == 'gif') {
				imagegif($virtual_image, $dest);					
			}
			return true;
		} else {
			if (!copy($src, $dest)) {
				return true;
			} else {
				return false;
			}
		}
	}
}
if( ! function_exists("is_file_available")) {
	function is_file_available($url) {
		if(is_file($url)) {
			return true;
		} else {
			if(is_file("../".$url)) {
				return true;
			}else {
				return false;
			}
		}
	}
}
if( ! function_exists("product_or_placeholder")) {
	function product_or_placeholder($url, $placeholder = PLACE_HOLDER, $s3_search = false) {
		return $url;
		/*if(is_file(S3_URL.$url) != "") {
			return $url;
		} else {
			return CONTENT_DIR."/".PLACE_HOLDER;
		}*/
	}
}

if( ! function_exists("all_arrays")) {
	function all_arrays() {
		
				// PAYMENT ARRAY
		$payment_arr = array (	
								'1' => "Prepaid",
								'2' => "cod",
								//'3' => "cbd"
							);
							
		// DELIEVERY ARRAY
		$delivery_status_arr = array (	
									'1' => "pending",
									'2' => "delivered",
									'3' => "rejected",
									'4' => "return recieved",
									'5' => "Intransit",
									'6' => "Pickup");
		
		// PROVIDER ARRAY	
		$provider_arr = array (	
								'1' => "XpressBee",
								'2' => "Bluedart",
								'3' => "IndiaPost",
								'4' => "DTDC",
								'5' => "Self-delivery",
								'6' => "Shiprocket"
								);
		
		// CALLER STATUS ARRAY	
		$caller_status_arr = array (	
									'1'	=> "pending",
									'2' => "qualified",
									'3' => "send",
									'4'	=> "rejected",
									'5'	=> "Not Send",
									'6' => "Cheque Qualified",
									'7' => "Review",
									'8' => "FollowUP",
									'9' => "COD Prequalified");
		
		// CALLER STATUS ARRAY	
		$zipdial_status_arr = array (	
									'0'	=> "Not Touch",
									'1' => "Message Sent",
									);
		
		// CALLER STATUS ARRAY	
		$caller_status_codarr = array (	
									'1'	=> "pending",
									'4'	=> "rejected");
		
		// REFUND STATUS ARRAY	
		$refund_status_arr = array (	
									'1' => "Refund Initiated",
									'2' => "Refund through cheque",
									'3'	=> "Refund through Netbanking");
		
		// QA STATUS ARRAY
		$qa_status_arr = array (	
								'1' => "QA Pending",
								'2' => "QA Qualified",
								'3' => "QA Rejected",
								'4' => "QA Not Touched");
						
		// DEPARTMENT ARRAY	
		$dept_arr = array (	
							'1' => "sales",
							'2' => "QA",
							'3' => "Customer Service");
		
		$date_arr = array(	
					"1"	=>	"Qualified Date",
					"2"	=>	"Sent Date",
					"3"	=>	"Return Date",
					"4"	=>	"Refund Date"
					);
					
		$yes_no_arr = array(
							"1"	=>	"Yes",
							"2"	=>	"no"
							);
		
		$order_arr = array(	
							"1"	=>	"DESC",
							"2"	=>	"ASC"
							);
							
		$issue_arr = array(
							"1"	=>	"customer care : Not sent",
							"2"	=>	"customer care : courier issue",
							"3"	=>	"customer care : Reconcilation issue",
							"4"	=>	"customer care : Refund",
							"5"	=>	"customer care : Replacement",
							"6"	=>	"customer care : Wrong product",
							"7"	=>	"customer care : points not given",
							"8"	=>	"customer care : cod issue points to be given",
							"9"	=>	"points to be given product rcvd",
							"10"=>	"money refund product rcvd",
							"11"=>	"Purchase : External buy",
							"12"=>	"Purchase : Delay from existing vendor",
							"13"=>	"Purchase : Other issue",
							"14"=>	"Delivery : no service",
							"15"=>	"resolved"
						);
		$notpaid_arr = array(
							"1"	=>	"Nutratimes slow",
							"2"	=>	"Nutratimes error in cart page",
							"3"	=>	"Nutratimes error in form page",
							"4"	=>	"Nutratimes not opening",
							"5"	=>	"Nutratimes other errors",
							"6"	=>	"Nutratimes high offer cost",
							"7"	=>	"Nutratimes Negative reviews",
							"8"	=>	"Nutratimes Gateway Slow",
							"9"	=>	"Nutratimes Gateway card option missing",
							"10"=>	"Nutratimes Gateway error",
							"11"=>	"Nutratimes interested"
						);
		$state_arr = array(	
							"1"	=>	"Maharashtra",
							"2"	=>	"Karnataka",
							"3"	=>	"Tamil Nadu",
							"4"	=>	"Delhi",
							"5"	=>	"Andra Pradesh",
							"6"	=>	"Arunachal Pradesh",
							"7"	=>	"Assam",
							"8"	=>	"Andaman and Nicobar Islands",
							"9"	=>	"Bihar",
							"10"=>	"Chandigarh",
							"11"=>	"Chhattisgarh",
							"12"=>	"Dadar and Nagar Haveli",
							"13"=>	"Daman and Diu",
							"14"=>	"Goa",
							"15"=>	"Gujarat",
							"16"=>	"Haryana",
							"17"=>	"Himachal Pradesh",
							"18"=>	"Jammu and Kashmir",
							"19"=>	"Jharkhand",
							"20"=>	"Kerala",
							"21"=>	"Lakshadeep",
							"22"=>	"Madya Pradesh",
							"23"=>	"Manipur",
							"24"=>	"Meghalaya",
							"25"=>	"Mizoram",
							"26"=>	"Nagaland",
							"27"=>	"Orissa",
							"28"=>	"Punjab",
							"29"=>	"Pondicherry",
							"30"=>	"Rajasthan",
							"31"=>	"Sikkim",
							"32"=>	"Tripura",
							"33"=>	"Uttaranchal",
							"34"=>	"Uttar Pradesh",
							"35"=>	"West Bengal",
							"36"=>	"Telangana");
							
							
		
				$feedback_arr = array(
						   "1" => "Shipment Status",
						   "2" => "Change Order Address",
						   "3" => "Want to Modify the Order",
						   "4" => "I want to cancel, return or exchange my Order",
						   "5" => "Where is my money/refund",
						   "6" => "I have a query regarding shophunk service"
			   );
			   
			   $sort_by_arr = array(
						   "1" => "Popularity",
						   "2" => "Price -- High to Low",
						   "3" => "Price -- Low to High",
						   "4" => "Latest Products"
			   );
			
			$ticket_arr = array(
						   "1" => "Open",
						   "2" => "Pending",
						   "3" => "Closed"
			   );

			
			$offer_per = array(
						   "1" => "2%",
						   "2" => "5%",
						   "3" => "7%",
						   "4" => "10%",
						   "5" => "15%",
						   "6" => "20%",
						   "7" => "25%",
						   "8" => "30%",
						   "9" => "35%",
						   "10"=> "40%",
						   "11" => "28%",
						   "12" => "29%",
						   "13" => "37.5%",
						   "14" => "26.5%",
						   "15" => "14%", 
						   "16" => "33.33%", 
						   "17" => "50%", 
						);
		
		$ga_url     = array(
							"1" => "https://propagatin.com/ga/",
							"2" => "https://tracemails.com/ga/"
						);		
				
		$bottle_arr = array("1" => "1", "2" => "2", "3" => "4", "4" => "6");	

		$main_arr = array();
		$main_arr['ARR_PAYMENT'] = $payment_arr;
		$main_arr['ARR_ZIPDIAL_STATUS'] = $zipdial_status_arr;
		$main_arr['ARR_COD_CALLER_STATUS'] = $caller_status_codarr;
		$main_arr['ARR_TICKET_STATUS'] = $ticket_arr;
		$main_arr['ARR_DELIVERY_STATUS'] = $delivery_status_arr;
		$main_arr['ARR_PROVIDER'] = $provider_arr;
		$main_arr['ARR_CALLER_STATUS'] = $caller_status_arr;
		$main_arr['ARR_QA_STATUS'] = $qa_status_arr;
		$main_arr['ARR_DEPARTMENT'] = $dept_arr;
		$main_arr['ARR_DATE'] = $date_arr;
		$main_arr['ARR_REFUND'] = $refund_status_arr;
		$main_arr['ARR_YES_NO'] = $yes_no_arr;
		$main_arr['ARR_ORDERS'] = $order_arr;
		$main_arr['ARR_ISSUE'] = $issue_arr;
		$main_arr['ARR_STATE'] = $state_arr;
		$main_arr['ARR_FEEDBACK'] = $feedback_arr;
		$main_arr['ARR_SORTBY'] = $sort_by_arr;
		$main_arr['ARR_NOTPAID'] = $notpaid_arr;
		$main_arr['ARR_OFFER_PER'] = $offer_per;		
		$main_arr['MLKT_URL'] = $ga_url;
		$main_arr['BOTTLE_ARR'] = $bottle_arr;

		return $main_arr;
	}
}

if( ! function_exists("custom_dropdown")) {
	function custom_dropdown($name, $id="", $arr, $default, $extra, $without_select_option=false, $default_not_null=false) {
		$dropdown = NULL;
		$id = ($id == '') ? $name : $id;
		$dropdown .= '<select name="'.$name.'" id="'.$id.'" '.$extra.'>';
		if (isset($arr) && is_array($arr)) {
			if ($without_select_option == false) {
				$dropdown .= "<option value=''>Select Option</option>";
			}
			foreach ($arr as $k => $v) {
				if ($default_not_null == true) {
					$selected = ($k == $default && $default != '') ? 'selected="selected"' : '';
				} else {
					$selected = ($k == $default) ? 'selected="selected"' : '';
				}
				$dropdown .= '<option value="'.$k.'" '.$selected.'>'.$v.'</option>';
			}
		}
		$dropdown .= "</select>";
		return $dropdown;
	}
}

if(! function_exists("search_dir")) {
	function search_dir($dir, $keyword) {
		if ($handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				if($entry != "." && $entry != "..") {
					if($entry == $keyword) {
						return true;
					}
				}
			}
			closedir($handle);
		}
		return false;
	}
}

if(! function_exists("get_full_url")) {
	function get_full_url(){
		$full_url = full_url();
		$arr_uri = preg_replace('~^/+|/+$|/(?=/)~', '', $full_url);
		
		// IF HTTP THEN EXECUTE THIS
		$arr_http = explode("http:/", $arr_uri);
		if(count($arr_http) > 1) {
			$final_uri = (trim($arr_http[0]) == "") ? "http://".$arr_http[1] : $arr_http[0];
		}
		
		// IF HTTPS THEN EXECUTE THIS
		$arr_http = explode("https:/", $arr_uri);
		if(count($arr_http) > 1) {
			$final_uri = (trim($arr_http[0]) == "") ? "https://".$arr_http[1] : $arr_http[0];
		}
		return $final_uri;
	}
}

if(! function_exists("check_is_cached")) {
	function check_is_cached($result, $dir) {
		
		// CALCULATE DIRECTORY SIZE
		$size_of_cache = calculate_dir_size($dir);
		$total_cache = ($size_of_cache/1024);
		$cache_size = byteFormat($size_of_cache);
		
		if($total_cache == 0) {
			$cache_type = "<span class='color_black'>no</span>";
			$class = "white_bg";
		}else if ($total_cache < 1000) {
			$cache_type = "<span class='color_black'>light</span>";
			$class = "white_bg";
		}
		if($total_cache > 1000) {
			$cache_type = "<span class='color_black'>normal</span>";
			$class = "blue_bg";
		} 
		if (($total_cache/1024) > 100) {
			$cache_type = "<span class='color_black'>average</span>";
			$class = "green_bg";
		}
		if (($total_cache/1024/1024) > 1) {
			$cache_type = "<span class='color_white'>heavy</span>";
			$class = "red_bg";
		}
		if (($total_cache/1024/1024/1024) > 1) {
			$cache_type = "<span  class='color_white'>huge</span>";
			$class = "dark_red_bg";
		}
		
		if($result == true) {
			?>
            <div class="<?php echo $class;?> abosolute_msg highlight">
              <div>Site is running on <?php echo $cache_size." of ".$cache_type;?> caching. (Ignore this message, if you don't know what it is !!)</div>
            </div>
			<?php
		} else {
			?>
            <div class="white_bg abosolute_msg highlight">
              <div>This page is not cached & Site is running on <?php echo $cache_size." of ".$cache_type;?> caching. (Ignore this message, if you don't know what it is !!)</div>
            </div>
			<?php
		}
	}
}
if( !function_exists('make_space')) {
	function make_space($chr='&nbsp;', $cnt = 1, $extchr="&nbsp;") {
		$string = "";
		if($cnt > 1) {
			for($i=1; $i<=$cnt; $i++) {
				$string .= $chr.$chr.$chr;
			}
			$string .= "&ndash;".$extchr;
		}else{
		 	$string .= "&rarr;".$extchr;
		}
		return $string;
	}
}
function wordHighLight($string, $words) {
    $split_words = explode( " " , $words);
	foreach ($split_words as $word) {
		$string = preg_replace("|($word)|Ui" ,"<span class=\"highlight_word\">$1</span>" , $string);
	}
	return $string;
}
function __unserialize($data) {
	$data = @unserialize(_strip_slashes($data));

	if (is_array($data)) {
		foreach ($data as $key => $val) {
			if (is_string($val)) {
				$data[$key] = str_replace('{{slash}}', '\\', $val);
			}
		}
		return $data;
	}
	return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
}
function __serialize($data) {
	if (is_array($data)) {
		foreach ($data as $k => $v) {
			if (is_string($v)) {
				$data[$k] = str_replace('\\', '{{slash}}', $v);
			}
		}
	} else {
		if (is_string($data)) {
			$data = str_replace('\\', '{{slash}}', $data);
		}
	}
	return serialize($data);
}
function _get_np() {
	$np = "";
	// UNSERIALIZE THE ARRAY FOR FURTHER USE
	if(isset($_COOKIE['newshk_ci_session'])) {
		$arr_svz = unserialize($_COOKIE['newshk_ci_session']);
	}
	
	// IF REQUESTED THAN SET IT
	if (isset($_REQUEST['net']) || isset($_REQUEST['pubid'])) {
		$np .= "&net=".$_REQUEST['net']."&pubid=".$_REQUEST['pubid'];
	} else if(isset($arr_svz['net']) || isset($arr_svz['pubid'])) {
		$np .= "&net=".$arr_svz['net']."&pubid=".$arr_svz['pubid'];
	}
	return $np;
}
function sub_string($str, $show) {
	if(strlen($str) > $show) {
		return substr($str,0,$show)."...";
	}else{
		return  $str;
	}
}
function disc_percent($mrp=0, $sale_price=0) {
	if(($sale_price > 0) && ($mrp > 0) && ($mrp > $sale_price)) {
		return '<div class="offer-off">'.round(100 - (($sale_price * 100) / $mrp)).'% OFF </div>';
	}
}
function sel_price($mrp, $sale_price) {
	if($mrp > $sale_price && $sale_price > 0) {
		return '<label class="mrp-price">MRP '.number_format($mrp).'</label><label class="item-price">Our price Rs. '.number_format($sale_price).'</label>';
	} else {
		return '<label class="item-price">Our price Rs. '.number_format($mrp).'</label>';
	}
}

function generate_smart_price($min_price, $max_price, $arr_req="") {
	if(isset($arr_req) && trim($arr_req) != "") {
		$arr_price = explode('|',$arr_req);
	}
	
	$price_diff = $max_price - $min_price;
	if($price_diff >= 0 && $price_diff <= 1000) {
		$start_gap = 100;
	} else if($price_diff >= 1001 && $price_diff <= 5000) {
		$start_gap = 500;
	} else if($price_diff >= 5001 && $price_diff <= 10000) {
		$start_gap = 2000;
	} else if($price_diff >= 10001 && $price_diff <= 50000) {
		$start_gap = 3000;
	} else if($price_diff >= 50001 && $price_diff <= 100000) {
		$start_gap = 10000;
	} else if($price_diff >= 100001 && $price_diff <= 500000) {
		$start_gap = 30000;
	} else if($price_diff >= 500001 && $price_diff <= 1000000) {
		$start_gap = 70000;
	} else if($price_diff >= 1000001 && $price_diff <= 5000000) {
		$start_gap = 100000;
	}
	
	$cur_sequence = $next_sequence = 0;
	for($i = 1; $i <= round($max_price / $start_gap); $i++) {
		$cur_sequence = $next_sequence;
		
		$next_sequence = $next_sequence + $start_gap;
		$check = (isset($arr_price) && is_array($arr_price) && in_array(round($cur_sequence)."-".round($next_sequence), $arr_price))  ? $check = "checked" : "";
		?>
		<div class="genre-entity">
            <div class="left entity-chk"><input <?php echo $check;?> type="checkbox" class="price-chkbox-sidebar radio-sidebar" value="<?php echo round($cur_sequence)."-".round($next_sequence);?>" name="price-range[]" id="price-range[]" /></div>
            <div class="left entity-chk-desc"><?php echo "Rs. ".round($cur_sequence)." - Rs. ".round($next_sequence); ?></div>
            <div class="clr"></div>
        </div>
        <?php
	}
}
function prod_details_price($mrp, $sale_price){
	if($mrp > $sale_price && $sale_price != 0) {
		
		// FIND THE DISCOUNT PERCENTAGE
		$disc_percentage = 100 - (($sale_price * 100) / $mrp);
		
		if (is_int($disc_percentage)) {
			// CHECK DISCOUNT PERCENTAGE IS INTEGER OR NOT
			$disc_price = $disc_percentage." %";
		} else {
			// ELSE DISPLAY DISCOUNT FIGURE
			$disc_price = _r($mrp - $sale_price);
		}
		echo _r($sale_price).'<span class="mrpRs">'._r($mrp, 'MRP ').'/-</span>';
		echo '<div class="ourPriceRs-small">( '.$disc_price.' OFF )</div>';
	}else {
		echo _r($mrp); 
	}
}
function _r($price, $symbol='Rs. ') {
	return $symbol.$price;
}
function display_category($category = false, $cur_controller = 'index') {
	if($category != false) {
		?>
        <div class="category-wrapper" <?php echo ($cur_controller != 'homepage') ? 'id="category-menu"' : '';?>>
         <?php
            if(count($category) > 0) {
                echo "<ul class='subCtg'>";
                foreach ($category as $k => $v) {
                    echo '<li><a href="'.$v->seourl.'">'.$v->name.'</a></li>';
                }
                echo "</ul>";
            }
        ?>
        </div>
        <?php
	}
}
function get_random_chracter($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false) {
	$length = rand($chars_min, $chars_max);
	$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
	if($include_numbers) {
		$selection .= "1234567890";
	}
	if($include_special_chars) {
		$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
	}
							
	$password = "";
	for($i=0; $i<$length; $i++) {
		$current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
		$password .=  $current_letter;
	}
	return $password;
}
function profile_sidebar() {
	//--//Koushik - Removed refund policy on 9th Dec 2014 based on Rohit Tiwari's feedback
	return '<div class="side-wrapper left">
			  <div class="category-wrapper">
				<ul class="subCtg-profile">
				  <li><a href="'.SITE_URL.'profile">Profile</a></li>
				  <li><a href="'.SITE_URL.'orderhistory">CASH Orders History</a></li>
				  <li><a href="'.SITE_URL.'orderhistorycod">COD Orders History</a></li>
				  <li><a href="'.SITE_URL.'returnorders">Returns</a></li>
				  <li><a href="'.SITE_URL.'changePasswordUser">Change Password</a></li>
				  <li><a href="'.SITE_URL.'addfeedback">Feedback</a></li>
				  <!--<li><a href="'.SITE_URL.'refund">Refund Request</a></li>-->
				  <li><a href="'.SITE_URL.'fulladdress">Full Address</a></li>
		      <!--<li><a href="'.SITE_URL.'referFriend">Refer Friend</a></li>-->
				  <li><a href="'.SITE_URL.'userwishlist">Wish List</a></li>
				</ul>
			  </div>
			</div>';
}

function history_manager($prod_id, $session_history) {
	if ($session_history) {
		if (trim($session_history) != "") {
			$product_history = unserialize($session_history);
			if(is_array($product_history)) {
				if(array_search($prod_id, $product_history) == '') {
					$product_history[] = $prod_id;
				}
			}
			return serialize($product_history);
		} else {
			// IF ITS NULL THEN SIMPLY STORE IT
			$product_history[] = $prod_id;
			return serialize($product_history);
		}
	} else {
		// IF ITS NULL THEN SIMPLY STORE IT
		$product_history[] = $prod_id;
		return serialize($product_history);
	}
}
function normal_product_list($v, $i) {
	echo ($i % 2 == 0) ? " " : "<div>";
	?>
	
	<div class="product-thumb transition">
	   <div class="image">
		  <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>"> 
		  <img style="width: 200px;" alt="<?php echo $v->name?>" src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_200_300_PATH.$v->image);?>" title="<?php echo $v->name?>" class="img-responsive" />
		  </a>
		  <div class="sale-label"><span><?php echo disc_percent($v->mrp, $v->sale_price); ?></span></div>
	   </div>
	   <div class="caption">
		  <div class="name">
			 <h3><a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>"><?php echo sub_string($v->name,45)?></a></h3>
		  </div>
		  <!-- <div class="description">
			 ...
			 </div> -->
		  <p class="price">
		     
			 <span class="price-new">Rs <?php echo $v->sale_price ?></span> <span class="price-old"><?php echo $v->mrp;?></span>
		  </p>
		  <div class="button-group main-product-btn product-item-action">
			 <div class="button-inner">
				<div class="hover-actions">
				   
					<!--<span class="icon"> fa fa-shopping-cart </span>-->
					<a style="margin: 0 auto;" class="addtocart btn-color button btn"  href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>" data-id="<?php echo $v->prod_id;?>">Buy Now</a>
				   
				</div>
			 </div>
		  </div>
		  <div class="rating-summary-block">
			 <div class="rating">
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-star-half-o"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
			 </div>
		  </div>
	   </div>
	</div>
	
    <?php
	echo ($i % 2 == 0) ? "</div>" : "";
}
function normal_product_list_search($v, $i) {
	?>
	
	<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-6">
        <div class="product-thumb">
           <div class="image">
		  <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>">
		  <img style="width: 200px;" alt="<?php echo $v->name?>" src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_200_300_PATH.$v->image);?>" title="<?php echo $v->name?>" class="img-responsive" />
		  </a>
		  <div class="sale-label"><span><?php echo disc_percent($v->mrp, $v->sale_price); ?></span></div>
	   </div>
	   
	   <div class="caption">
		  <div class="name">
			 <h3><a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>"><?php echo sub_string($v->name,45)?></a></h3>
		  </div>
		  <!-- <div class="description">
			 ...
			 </div> -->
		  <p class="price">
		     
			 <span class="price-new">Rs <?php echo $v->sale_price ?></span> <span class="price-old"><?php echo $v->mrp;?></span>
		  </p>
		  <div class="button-group main-product-btn product-item-action">
			 <div class="button-inner">
				<div class="hover-actions">
				   
					<!--<span class="icon"> fa fa-shopping-cart </span>-->
					<a style="margin: 0 auto;" class="addtocart btn-color button btn"  href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>" data-id="<?php echo $v->prod_id;?>">Buy Now</a>
				   
				</div>
			 </div>
		  </div>
		  <div class="rating-summary-block">
			 <div class="rating">
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-star-half-o"></i></span>
				<span class="fa fa-stack"><i class="fa fa-star fa-star-o"></i></span>
			 </div>
		  </div>
	   </div>
	</div>
	</div>
    <?php
	
}



function get_admin_file_download_links($full_sql="", $role_id="", $select_fields="", $table_fields="", $where_fields="", $search_criteria='') {
	if(array_admin_file_download_role($role_id)) {
		?>
		<div class="download-area">
			<form action="<?php echo FULL_CMS_URL?>/searchdownload/" method="post" target="_blank">
				<input type="submit" name="xpressbee" class="button" value="Xpressbee" />
				<!--<input type="submit" name="firstflight" class="button" value="FirstFlight" />
				<input type="submit" name="bluedart" class="button" value="Bluedart" />
				<input type="submit" name="dtdc" class="button" value="DTDC" />
                <input type="submit" name="redexpress" class="button" value="Red Express" />
                <input type="submit" name="ecomexpress" class="button" value="ECOM Express" />-->
                <input type="submit" name="address" class="button" value="Address"  />
                <!--<input type="submit" name="fedex" class="button" value="Fedex" />-->
				<input type="hidden" name="search_criteria" id="search_criteria" value="<?php echo urlencode(addslashes($search_criteria)); ?>" />
                <input type="submit" name="excel" class="button" value="Excel" />
				<input type="hidden" name="select_fields" id="select_fields" value="<?php echo addslashes($select_fields); ?>" />
                <input type="hidden" name="table_fields" id="table_fields" value="<?php echo addslashes($table_fields); ?>" />
                <input type="hidden" name="where_fields" id="where_fields" value="<?php echo addslashes($where_fields); ?>" />
                <input type="hidden" name="full_sql" id="full_sql" value="<?php echo addslashes($full_sql)?>" />
                <a name="xpressbee_delivery_report" class="button" href="<?php echo SITE_URL?>cms/update_expressbee/xpressbee_download" style="padding: 7px !important">Xpressbee Delivery Report</a>
			</form>
		</div>
		<?php
	} else if (array_excel_file_download_role($role_id)) {
		?>
		<div class="download-area">
			<form action="<?php echo FULL_CMS_URL?>/searchdownload/" method="post">
				<input type="submit" name="excel" class="button" value="Excel" />
				<input type="hidden" name="search_criteria" id="search_criteria" value="<?php echo urlencode(addslashes($search_criteria)); ?>" />
                <input type="hidden" name="select_fields" id="select_fields" value="<?php echo addslashes($select_fields); ?>" />
                <input type="hidden" name="table_fields" id="table_fields" value="<?php echo addslashes($table_fields); ?>" />
                <input type="hidden" name="where_fields" id="where_fields" value="<?php echo addslashes($where_fields); ?>" />
                <input type="hidden" name="full_sql" id="full_sql" value="<?php echo addslashes($full_sql)?>" />
			</form>
		</div>
		<?php
	}else{
	?>
		<div class="download-area">
			 <a name="xpressbee_delivery_report" class="button" href="<?php echo SITE_URL?>cms/update_expressbee/xpressbee_download" style="padding: 7px !important">Xpressbee Delivery Report</a>
		</div>
	<?php
	}
}

function _get_barcode($code = '') {
	
	define("f2B", "11");
	define("f2W", "00");
	define("f2b", "10");
	define("f2w", "01");

	$_code = array();

	$_codes_39 = array(
		32 => 100011011001110110,
		36 => 100010001000100110,
		37 => 100110001000100010,
		42 => 100010011101110110,
		43 => 100010011000100010,
		45 => 100010011001110111,
		46 => 110010011001110110,
		47 => 100010001001100010,
		48 => 100110001101110110,
		49 => 110110001001100111,
		50 => 100111001001100111,
		51 => 110111001001100110,
		52 => 100110001101100111,
		53 => 110110001101100110,
		54 => 100111001101100110,
		55 => 100110001001110111,
		56 => 110110001001110110,
		57 => 100111001001110110,
		65 => 110110011000100111,
		66 => 100111011000100111,
		67 => 110111011000100110,
		68 => 100110011100100111,
		69 => 110110011100100110,
		70 => 100111011100100110,
		71 => 100110011000110111,
		72 => 110110011000110110,
		73 => 100111011000110110,
		74 => 100110011100110110,
		75 => 110110011001100011,
		76 => 100111011001100011,
		77 => 110111011001100010,
		78 => 100110011101100011,
		79 => 110110011101100010,
		80 => 100111011101100010,
		81 => 100110011001110011,
		82 => 110110011001110010,
		83 => 100111011001110010,
		84 => 100110011101110010,
		85 => 110010011001100111,
		86 => 100011011001100111,
		87 => 110011011001100110,
		88 => 100010011101100111,
		89 => 110010011101100110,
		90 => 100011011101100110
	);

	$barcode_bar_thick = 3;

	$barcode_bar_thin = 1;

	$barcode_bg_rgb = array(255, 255, 255);

	$barcode_height = 80;

	$barcode_padding = 5;

	$barcode_text = true;

	$barcode_text_size = 3;

	$barcode_use_dynamic_width = true;

	$barcode_width = 400;

	
	// format and code
	$code = (string)strtoupper($code);

	// convert code to code array
	$i = 0;
	while(isset($code[$i])) {
		$_code[] = $code[$i++];
	}

	// add start and stop symbols
	array_unshift($_code, "*");
	array_push($_code, "*");
	draw($_code, $_codes_39, $barcode_padding, $barcode_height, $barcode_bg_rgb, $barcode_text_size, $barcode_text, $barcode_width, $barcode_use_dynamic_width, $barcode_bar_thick, $barcode_bar_thin);
}

function draw($_code, $_codes_39, $barcode_padding, $barcode_height, $barcode_bg_rgb, $barcode_text_size, $barcode_text, $barcode_width, $barcode_use_dynamic_width, $barcode_bar_thick, $barcode_bar_thin, $filename = null) {
		// check if GB library functions installed
		if(!function_exists("imagegif")) {
			return false;
		}

		// check for valid code
		if(!is_array($_code) || !count($_code)) {
			return false;
		}

		// bars coordinates and params
		$bars = array();

		// position pointer
		$pos = $barcode_padding;

		// barcode text
		$barcode_string = null;

		// set code 39 codes
		$i = 0;
		foreach($_code as $k => $v) {
			// check for valid code
			if(isset($_codes_39[ord($v)])) {
				// valid code add code 39, also add separator between characters if not first character
				$code = ( $i ? f2w : null ) . $_codes_39[ord($v)];

				// check for valid code 39 code
				if($code) {
					// add to barcode text
					$barcode_string .= " {$v}";

					// init params
					$w = 0;
					$f2 = $fill = null;

					// add each bar coordinates and params
					for($j = 0; $j < strlen($code); $j++) {
						// format 2 code
						$f2 .= (string)$code[$j];

						// valid format 2 code
						if(strlen($f2) == 2) {
							// set bar fill
							$fill = $f2 == f2B || $f2 == f2b ? "_000" : "_fff";

							// set bar width
							$w = $f2 == f2B || $f2 == f2W ? $barcode_bar_thick  : $barcode_bar_thin;

							// check for valid bar params
							if($w && $fill) {
								// add bar coordinates and params
								$bars[] = array($pos, $barcode_padding, $pos - 1 + $w,
									$barcode_height - $barcode_padding - 1, $fill);

								// move position pointer
								$pos += $w;
							}

							// reset params
							$f2 = $fill = null;
							$w = 0;
						}
					}
				}
				$i++;
			// invalid code, remove character from code
			} else {
				unset($_code[$k]);
			}
		}

		// check for valid bar coordinates and params
		if(!count($bars)) {
			// no valid bar coordinates and params
			return false;
		}

		// set barcode width
		$bc_w = $barcode_use_dynamic_width ? $pos + $barcode_padding : $barcode_width;

		// if not dynamic width check if barcode wider than barcode image width
		if(!$barcode_use_dynamic_width && $pos > $barcode_width) {
			return false;
		}

		// initialize image
		$img = imagecreate($bc_w, $barcode_height);
		$_000 = imagecolorallocate($img, 0, 0, 0);
		$_fff = imagecolorallocate($img, 255, 255, 255);
		$_bg = imagecolorallocate($img, $barcode_bg_rgb[0], $barcode_bg_rgb[1], $barcode_bg_rgb[2]);

		// fill background
		imagefilledrectangle($img, 0, 0, $bc_w, $barcode_height, $_bg);

		// add bars to barcode
		for($i = 0; $i < count($bars); $i++) {
			imagefilledrectangle($img, $bars[$i][0], $bars[$i][1], $bars[$i][2], $bars[$i][3], $$bars[$i][4]);
		}

		// check if using barcode text
		if($barcode_text) {
			// set barcode text box
			$barcode_text_h = 10 + $barcode_padding;
			imagefilledrectangle($img, $barcode_padding, $barcode_height - $barcode_padding - $barcode_text_h,
				$bc_w - $barcode_padding, $barcode_height - $barcode_padding, $_fff);

			// set barcode text font params
			$font_size = $barcode_text_size;
			$font_w = imagefontwidth($font_size);
			$font_h = imagefontheight($font_size);

			// set text position
			$txt_w = $font_w * strlen($barcode_string);
			$pos_center = ceil((($bc_w - $barcode_padding) - $txt_w) / 2);

			// set text color
			$txt_color = imagecolorallocate($img, 0, 255, 255);

			// draw barcod text
			imagestring($img, $font_size, $pos_center, $barcode_height - $barcode_text_h - 2,
				$barcode_string, $_000);
		}

		// check if writing image
		if($filename) {
			imagegif($img, $filename);
		// display image
		} else {
			header("Content-type: image/gif");
			imagegif($img);
		}
		
		imagedestroy($img);

		// valid barcode
		return true;
}	
if(! function_exists("delete_n_level_file")) {
	function delete_n_level_file($dir) {
		if ($handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				if($entry != "." && $entry != "..") {
					if(is_dir($entry)) {
						delete_n_level_file($dir.DIRECTORY_SEPARATOR.$entry);
					} else {
						unlink($dir.DIRECTORY_SEPARATOR.$entry);
					}
				}
			}
			closedir($handle);
		}
		return false;
	}
}

function requestBlocker() {
	$dir = './requestBlocker/'; 
	$rules   = array(
			array(
					'requests' => 10,
					'sek' => 3,
					'blockTime' => 60 * 30
			),
			array(
					'requests' => 20,
					'sek' => 5,
					'blockTime' => 60 * 60
			),
			array(
					'requests' => 50,
					'sek' => 10,
					'blockTime' => 60 * 60
			)
	);
	$time    = time();
	$blockIt = array();
	$user    = array();

	#Set Unique Name for each Client-File 
	$user[] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'IP_unknown';
	$user[] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	$user[] = strtolower(gethostbyaddr($user[0]));

	# Notice that i use files because bots does not accept Sessions
	$botFile = $dir . substr($user[0], 0, 8) . '_' . substr(md5(join('', $user)), 0, 5) . '.txt';
	if (file_exists($botFile)) {
			$file   = file_get_contents($botFile);
			$client = unserialize($file);
	} else {
			$client                = array();
			$client['time'][$time] = 0;
	}
	
	# Set/Unset Blocktime for blocked Clients
	if (isset($client['block'])) {
		foreach ($client['block'] as $ruleNr => $timestampPast) {
			$left = $time - $timestampPast;
			if (($left) > $rules[$ruleNr]['blockTime']) {
					unset($client['block'][$ruleNr]);
					continue;
			}
			$blockIt[] = $ruleNr . ' - unblocked in ' . ($left - $rules[$ruleNr]['blockTime']) . ' Sec.';
		}
		if (!empty($blockIt)) {
				return $blockIt;
		}
	}

	# log/count each access
	if (!isset($client['time'][$time])) {
		$client['time'][$time] = 1;
	} else {
		$client['time'][$time]++;
	}

	#check the Rules for Client
	$min = array(
			0
	);
	foreach ($rules as $ruleNr => $v) {
		$i            = 0;
		$tr           = false;
		$sum[$ruleNr] = '';
		$requests     = $v['requests'];
		$sek          = $v['sek'];
		foreach ($client['time'] as $timestampPast => $count) {
			if (($time - $timestampPast) < $sek) {
				$sum[$ruleNr] += $count;
				if ($tr == false) {
					#register non-use Timestamps for File 
					$min[] = $i;
					unset($min[0]);
					$tr = true;
				}
			}
			$i++;
		}

		if ($sum[$ruleNr] > $requests) {
			$blockIt[]                = 'Limit : ' . $ruleNr . '=' . $requests . ' requests in ' . $sek . ' seconds!';
			$client['block'][$ruleNr] = $time;
		}
	}
	$min = min($min) - 1;
	#drop non-use Timestamps in File 
	foreach ($client['time'] as $k => $v) {
		if (!($min <= $i)) {
			unset($client['time'][$k]);
		}
	}
	$file = file_put_contents($botFile, serialize($client));
	return $blockIt;
}

function detectBot($USER_AGENT) {
	$crawlers_agents = strtolower('Bloglines subscriber|Dumbot|Sosoimagespider|QihooBot|FAST-WebCrawler|Superdownloads Spiderman|LinkWalker|msnbot|ASPSeek|WebAlta Crawler|Lycos|FeedFetcher-Google|Yahoo|YoudaoBot|AdsBot-Google|Googlebot|Scooter|Gigabot|Charlotte|eStyle|AcioRobot|GeonaBot|msnbot-media|Baidu|CocoCrawler|Google|Charlotte t|Yahoo! Slurp China|Sogou web spider|YodaoBot|MSRBOT|AbachoBOT|Sogou head spider|AltaVista|IDBot|Sosospider|Yahoo! Slurp|Java VM|DotBot|LiteFinder|Yeti|Rambler|Scrubby|Baiduspider|accoona');
	$crawlers = explode("|", $crawlers_agents);
	if(is_array($crawlers)) {
		foreach($crawlers as $crawler) {
			if (strpos(strtolower($USER_AGENT), trim($crawler)) !== false) {
				return true;
			}
		}
	}
	return false;
}

function giantFirewall(){
	if($_SERVER['HTTP_HOST'] != 'localhost') {
		$master_firewall = 'off';
			if(strtolower($master_firewall) == 'on') {
				//TRACK IP
				$track_ip = false;
				
				// FIREWALL SETTINGS
				$firewall = false;
				$max_request_time = 50;
				
				if ($firewall == true) {
					$current_ip_address = $_SERVER['REMOTE_ADDR'];
					if ($current_ip_address != '') {
						$file_block = 'block.txt';
						$file_black = 'black.txt';
						
						$content_black = file_get_contents($file_black);
						$is_black = substr_count($content_black, $current_ip_address);
						
						if($is_black > 0) {
							die ('<h1 style="color:red">You\'re blacklisted</h1>');
						} elseif($track_ip == true) {
						
							$current = file_get_contents($file_block);
							$current .= $current_ip_address.",";
							file_put_contents($file_block, $current);
							
							$file_block_content = file_get_contents($file_block);
							$is_block = substr_count($file_block_content, $current_ip_address);
							if($is_block > $max_request_time) {
								$block_content = file_get_contents($file_black);
								// Append a new person to the file
								$block_content .= $current_ip_address.",";
								// Write the contents back to the file
								file_put_contents($file_black, $block_content);
							}
						}
					} else {
						die ('<h1 style="color:red">Bot is n\'t allowed !!</h1>');
					}
				}
				
				// FIREWALL CODE END;
				if ($t = requestBlocker()) {
					die('Slow down you are moving too fast, you are blocked, '.$t[0]) or die();
				}
				if(detectBot($_SERVER['HTTP_USER_AGENT'])) {
					die();
				}
				
				if(!setcookie('_s', '1', time()+36000)) {
					die();
				}
				
				$ip = $_SERVER['REMOTE_ADDR'];
				$blkip = "a.".$ip;
				$pos = strpos($blkip, '.8.37', 0);
				
				if($pos==1){
					 die();
				}
			}
	}
}

function session_remain_time($date, $ip){
	
	if ($ip != $_SERVER['REMOTE_ADDR']) {
		return false;	
	}
	
	if ( isset ($date)) {
		$loginTime = (strtotime($date)) + (24 * 3600);
		$curTime = strtotime(date('Y-m-d H:i:s'));
		
		if ($loginTime <= $curTime) {
			return false;
		} else {
			return '(Relogin schedule at '.date('d-M-Y h:i:s A', $loginTime).')';
		}
	}
	return false;
}

function timeDiff($time, $timeBase = false) {
    
    if ($time <= time()) {
        $dif = $timeBase - $time;

        if ($dif < 60) {
            if ($dif < 2) {
                return "1 second ago";
            }

            return $dif." seconds ago";
        }

        if ($dif < 3600) {
            if (floor($dif / 60) < 2) {
                return "A minute ago";
            }

            return floor($dif / 60)." minutes ago";
        }

        if (date("d n Y", $timeBase) == date("d n Y", $time)) {
            return "Today, ".date("g:i A", $time);
        }

        if (date("n Y", $timeBase) == date("n Y", $time) && date("d", $timeBase) - date("d", $time) == 1) {
            return "Yesterday, ".date("g:i A", $time);
        }

        if (date("Y", $time) == date("Y", time())) {
            return date("F, jS g:i A", $time);
        }
    } else {
        echo $dif = $time - $timeBase;

        if ($dif < 60) {
            if ($dif < 2) {
                return "1 second";
            }

            return $dif." seconds";
        }

        if ($dif < 3600) {
            if (floor($dif / 60) < 2) {
                return "Less than a minute";
            }

            return floor($dif / 60)." minutes";
        }

        if (date("d n Y", ($timeBase + 86400)) == date("d n Y", ($time))) {
            return "Tomorrow, at ".date("g:i A", $time);
        }
    }

    return date("F, jS g:i A Y", $time);
}



function check_mysql_inject($input_text = ""){
 
	if( $input_text != ""){
		$mysql_vars   = array("select", "delete", "update", "insert", "groupby", "orderby", "=", ";", "*", ";");
		$txt_valid    = strtolower($input_text);
  
	    foreach($mysql_vars as $v ){ 
		   if (strpos($txt_valid, $v) !== false) {
			  return true; 
		   }
	   }
  	return false;
 	}
}


/*CRYPTO */
function encrypt($plainText,$key)
{
	$secretKey = hextobin(md5($key));
	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
	$plainPad = pkcs5_pad($plainText, $blockSize);
	
	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
	{		
		  $encryptedText = mcrypt_generic($openMode, $plainPad);
			  mcrypt_generic_deinit($openMode);
					
	} 
	return bin2hex($encryptedText);
}

function decrypt($encryptedText,$key)
{
	$secretKey = hextobin(md5($key));
	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	$encryptedText=hextobin($encryptedText);
	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	mcrypt_generic_init($openMode, $secretKey, $initVector);
	$decryptedText = mdecrypt_generic($openMode, $encryptedText);
	$decryptedText = rtrim($decryptedText, "\0");
	mcrypt_generic_deinit($openMode);
	return $decryptedText;
	
}
//*********** Padding Function *********************

 function pkcs5_pad ($plainText, $blockSize)
{
	$pad = $blockSize - (strlen($plainText) % $blockSize);
	return $plainText . str_repeat(chr($pad), $pad);
}

//********** Hexadecimal to Binary function for php 4.0 version ********

function hextobin($hexString) 
 { 
	$length = strlen($hexString); 
	$binString="";   
	$count=0; 
	while($count<$length) 
	{       
		$subString =substr($hexString,$count,2);           
		$packedString = pack("H*",$subString); 
		if ($count==0)
	{
		$binString=$packedString;
	} 
		
	else 
	{
		$binString.=$packedString;
	} 
		
	$count+=2; 
	} 
	return $binString; 
  } 
  
  /*CRYPTO */
  
  
  function _sendsms($mobile = '', $message = '') {
  // CHECK MOBILE NO IS VALID OR NOT
	  if (isset($mobile) && trim($mobile) > 0 && strlen($mobile) == 10 && isset($message) && trim($message) != '') {
	   
	   // IS CURL INSTALLED YET
	   if (!function_exists('curl_init')){
	    die('Sorry cURL is not installed!');
	   }
	  
	    //$url = 'http://text.adcanopus.com/php-smpp/sms_send.php?phone='.$mobile.'&msg='.urlencode($message).'&username=paisa&password=fatafat&sendid=PAISFT';
	   
	   $url = 'http://smsalerts.adcanopus.com/api/v4/?api_key=Ac37abb04bc63b0c63f02539905422b45&method=sms&message='.urlencode($message).'&to=91'.$mobile.'&sender= NTIMES&format=xml';
	   
	   // OK cool - then let's create a new cURL resource handle
	   $ch = curl_init();
	     
	   // Set URL to download
	   curl_setopt($ch, CURLOPT_URL, $url);
	   
	   // Should cURL return or print out the data? (true = return, false = print)
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   
	   // Timeout in seconds
	   curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	   
	   // Download the given URL, and return output
	   $output = curl_exec($ch);
	   
	   return $output;
	   // Close the cURL resource, and free system resources
	   curl_close($ch);
	   
	   if (strpos(strtolower($output), 'error') !== false) {
	    // STORE SUCCESS LOG FOR THE REFERENCE
	    return false;
	    // SEND MAIL IF ERROR COMES FMOR SMSGUPSUP
	   } else {
	    // STORE SUCCESS LOG FOR THE REFERENCE
	    return true;
	   }
	   
	  }
 
 }


function quick_msg($order_val){
  	


    $merchant_data='';
    $working_code = '5F0EB58B427620DF36F2BA92920FC6FF';
    $access_code  = 'AVYR80FI61BZ18RYZB';
	  
    $order_val = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <Generate_Invoice_Query>
            <customer_name>'.$order_val['name'].'</customer_name>
            <bill_delivery_type>SMS</bill_delivery_type>
            <customer_mobile_no>'.$order_val['mobile'].'</customer_mobile_no>
            <customer_email_id>'.$order_val['email'].'</customer_email_id>
            <customer_email_subject>Invoice Pay Nutratimes</customer_email_subject>
            <invoice_description>Weight Loss </invoice_description>
            <currency>INR</currency>
            <valid_for>2</valid_for>
            <valid_type>days</valid_type>
            <amount>'.$order_val['amount'].'</amount>
            <merchant_reference_no>181436</merchant_reference_no>
             <terms_and_conditions>terms and condition</terms_and_conditions>
             <sms_content>Pls pay your LegalEntity_Name bill # Invoice_ID for
            Invoice_Currency Invoice_Amount online at Pay_Link.</sms_content>
            </Generate_Invoice_Query>';
	
	
	$encrypt = encrypt($order_val, $working_code);

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.ccavenue.com/apis/servlet/DoWebTrans",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "command=generateQuickInvoice&request_type=XML&response_type=JSON&enc_request=".$encrypt."&access_code=AVYR80FI61BZ18RYZB",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    "content-type: application/x-www-form-urlencoded",
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  return $err;
	} else {
	  return $response;
	}

  }

    function calculate_bmi($height, $weight){

	  	$data1['BMIStatus'] = $BMIStatus = "";
	  	
	  	$height = $height;
		$weight = $weight;
		$BMIScore = $weight / ($height / 100 * $height / 100);
		$val =round($BMIScore, 2);
		$data1['BMIScore'] = $val;
		$BMIStatus = "";
		
		if ($BMIScore < 18.5) {
		 	
		 	$data1['BMIStatus'] = "Thin";
		}
		
		if ($BMIScore > 18.5 && $BMIScore < 25) {
		  	
		  	$data1['BMIStatus'] = "Healthy";
		}

		if ($BMIScore > 25) {
			
			$data1['BMIStatus'] = "Overweight";
		}

	  	return $data1['BMIStatus']; 
    }