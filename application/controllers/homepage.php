<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class homePage extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		//$this->general->get_top_level_category(3);
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
	}
	
	public function index($optin_val= ""){
	
		$cur_controller 		= strtolower(__CLASS__);
		
		$this->general->email_verify($optin_val);

		// FETCH CATEGORY
		$cat['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$cat['cur_controller']	= $cur_controller;
		$brand['brands'] 		= $this->general->getBrand(); 
		$header['meta_title']   = "Nutratimes | The best Garcinia Cambogia brand in India"; 
		$header['meta_desc']    = "Buy Garcinia Cambogia from Nutratimes the best Garcinia Cambogia brand in India for weight loss  Get upto 50% Discount on Organic Fat loss Order now";
		$header['meta_keyword'] = "Nutratimes the best Garcinia Cambogia brand in India, best garcinia cambogia for weight loss in india, best brand for garcinia cambogia, best garcinia cambogia brand";

		//$deal_prods = $this->_get_deal_products();
		$whr_deal_prods = "";//($deal_prods)?" and p.prod_id NOT IN(".$deal_prods.")":'';
			
		// LATEST PRODUCTS - START
		$data['latest_products'] = $this->memcache_driver->get_cache('latest_products');
		if (!$data['latest_products']) { 
		
			$data['latest_products'] = $this->db->query("select 
												p.cat_id,
												p.sub_cat_id,
												p.seourl,
												p.cod_availability,
												p.brand,
												p.prod_id, 
												i.image, 
												p.name, 
												p.mrp,
												p.sale_price
											from 
												".PRODUCT." p 
												left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id) 
											where 
												p.status = 1 and 
												p.oos = 0 and 
												p.is_verified = 1 and 
												p.contest = 0 
												".$whr_deal_prods."
											group by 
												p.prod_id,
												p.sub_cat_id 
											order by 
												p.datecreated desc
											limit 0,5");
			
			$data['latest_products'] = $data['latest_products']->result();
			$this->memcache_driver->set_cache('latest_products', $data['latest_products'], true, CACHE_LONGTIME);
		} else {
			$data['latest_products'] = $data['latest_products'];
		}
		// FIRST SLIDE - END								
		
		
		// SELECT SETTINGS
		$data['setting_data'] = $this->memcache_driver->get_cache('homepage_setting_data');
		if (!$data['setting_data']) { 
			$this->db->cache_off();
			$setting_homepage = $this->db->query("SELECT attr_name, attr_value FROM ".SETTINGS." where attr_name in ('banner_top_wide', 'top_small_banner', 'down_first', 'down_second', 'down_third', 'down_four', 'banner_top_wide_link', 'top_small_banner_link', 'down_first_link', 'down_second_link', 'down_third_link', 'down_four_link', 'top_text_note')");
			if($setting_homepage->num_rows() > 0) {
				foreach ($setting_homepage->result() as $val_setting_homepage) {
					$data['setting_data'][$val_setting_homepage->attr_name] = $val_setting_homepage->attr_value;
				}
				$this->memcache_driver->set_cache('homepage_setting_data', $data['setting_data'], true, CACHE_LONGTIME);	
			}		
		} else {
			$data['setting_data'] = $data['setting_data'];
		}
		
			
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common', $header);
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-homepage',$data);
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	//--//Koushik 31-Dec-2014. hide super super deal attached items from search
	private function _get_deal_products()
	{
		$sql_deal_prods = "SELECT combo_product FROM ".PRODUCT." WHERE is_combo = 1 AND combo_product IS NOT NULL AND combo_product !='' AND status = 1";
		$query = $this->db->query($sql_deal_prods);
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			$arr_data = array();
			foreach ($result as $row) {
				$arr_data[] = $row['combo_product'];
			}
			return implode(',',$arr_data);
		}
		return false;
	}
}
