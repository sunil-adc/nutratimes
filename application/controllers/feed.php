<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class feed extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	public function generate() {
		$res_product_feed_xml = $this->db->query("select 
														p.cat_id,
														p.sub_cat_id,
														p.prod_id,
														p.name, 
														p.mrp, 
														p.seourl,
														pi.image, 
														p.sale_price,
														p.cod_price, 
														p.contest, 
														p.description,
														c.name category_name,
														c1.name sub_category_name 
												 from ".PRODUCT." p
														inner join ".CATEGORY." c on (c.cat_id = p.cat_id) 
														left join ".PRODUCT_IMAGE." pi on (pi.prod_id = p.prod_id) 
														left join ".CATEGORY." c1 on (c1.cat_id = p.sub_cat_id)
												 where 
														pi.isdefault = 1 and 
														p.status = 1 and 
														p.oos != 1");
		if ($res_product_feed_xml->num_rows() > 0) {
			
			header("Content-type: text/xml");
			$product_xml_feed = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			$product_xml_feed .= '<data>';
			
			foreach ($res_product_feed_xml->result() as $val_product_feed_xml) {
				$is_contest = $val_product_feed_xml->contest == 1 ? 'Yes' : 'No';
				$product_xml_feed .= "<product>";
				$product_xml_feed .= "<productid>".trim($val_product_feed_xml->prod_id)."</productid>";
				$product_xml_feed .= "<productname>".trim(str_replace('&','&amp;',$val_product_feed_xml->name))."</productname>";
				$product_xml_feed .= "<categoryname>".trim(str_replace('&','&amp;',$val_product_feed_xml->category_name))."</categoryname>";
				$product_xml_feed .= "<subcategoryname>".trim(str_replace('&','&amp;',$val_product_feed_xml->sub_category_name))."</subcategoryname>";
				$product_xml_feed .= "<url>".SITE_URL.PD_PAGE."/".PD_FUNC."/".$val_product_feed_xml->seourl."/?net=skom</url>";
				$product_xml_feed .= "<catid>".trim($val_product_feed_xml->cat_id)."</catid>";
				$product_xml_feed .= "<subcatid>".trim($val_product_feed_xml->sub_cat_id)."</subcatid>";
				$product_xml_feed .= "<mrp>".trim($val_product_feed_xml->mrp)."</mrp>";
				$product_xml_feed .= "<sale_price>".trim($val_product_feed_xml->sale_price)."</sale_price>";
				$product_xml_feed .= "<image>".S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$val_product_feed_xml->image)."</image>";
				//$product_xml_feed .= "<description>".trim($val_product_feed_xml->description)."</description>";
				$product_xml_feed .= "</product>";
			}
			
			$product_xml_feed .= '</data>';
			
			echo $product_xml_feed;
		}	
		exit;
	}
	
	public function smad() {
		$res_product_feed_xml = $this->db->query("select 
														p.cat_id,
														p.sub_cat_id,
														p.prod_id,
														p.name, 
														p.mrp,
														b.name brand_name,  
														p.seourl,
														pi.image, 
														p.sale_price,
														p.cod_price, 
														p.contest, 
														c.name category_name,
														c1.name sub_category_name 
												 from ".PRODUCT." p
														inner join ".CATEGORY." c on (c.cat_id = p.cat_id) 
														left join ".PRODUCT_IMAGE." pi on (pi.prod_id = p.prod_id) 
														left join ".CATEGORY." c1 on (c1.cat_id = p.sub_cat_id)
												 		left join ".BRANDS." b on (p.brand = b.brand_id) 
												 where 
														pi.isdefault = 1 and 
														p.status = 1 and 
														p.oos != 1");
		if ($res_product_feed_xml->num_rows() > 0) {
			
			header("Content-type: text/xml");
			$product_xml_feed = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			$product_xml_feed .= '<data>';
			
			foreach ($res_product_feed_xml->result() as $val_product_feed_xml) {
				$is_contest = $val_product_feed_xml->contest == 1 ? 'Yes' : 'No';
				$product_xml_feed .= "<item>";
				$product_xml_feed .= "<id>".trim($val_product_feed_xml->prod_id)."</id>";
				$product_xml_feed .= "<brand>".trim(str_replace('&','&amp;',$val_product_feed_xml->brand_name))."</brand>";
				$product_xml_feed .= "<name>".trim(str_replace('&','&amp;',$val_product_feed_xml->name))."</name>";
				$product_xml_feed .= "<ppc>".SITE_URL.PD_PAGE."/".PD_FUNC."/".$val_product_feed_xml->seourl."/?net=smad</ppc>";
				$product_xml_feed .= "<img>".S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$val_product_feed_xml->image)."</img>";
				$product_xml_feed .= "<rate>".trim($val_product_feed_xml->sale_price)."</rate>";
				$product_xml_feed .= "<oldrate>".trim($val_product_feed_xml->mrp)."</oldrate>";
				$product_xml_feed .= "</item>";
			}
			
			$product_xml_feed .= '</data>';
			
			echo $product_xml_feed;
		}	
		exit;
	}
}