<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {
	
	protected $all_array;
	
	function __construct(){
		parent::__construct();
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// CHECK USER IS LOGIN OR NOT
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			redirect(SITE_URL."login");
		}
		$this->all_array = all_arrays();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
	}
	 
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('login-register',$data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
	
	public function login_register() {
		if($this->input->post('mobile') != "" && $this->input->post('action')) {
			if($this->input->post('action') == "login" && $this->input->post('password') != "" ) {
				$this->db->cache_off();
				//--//Koushik 18-Dec-2014. Added address field to display in cart page
				$user_login = $this->db->query("select user_id, name, mobile, email, city, state, pincode, address from ".USERS." where mobile = '".$this->input->post('mobile')."' and passwd = '".md5($this->input->post('password'))."'");
				
				if ($user_login->num_rows() > 0) {
					foreach ($user_login->result() as $user_data) {
						// SET USER LOGIN SESSION - START
						$_SESSION[SVAIZA_USER_ID] 	= $user_data->user_id;
						$_SESSION[SVAIZA_MOBILE] 	= $user_data->mobile;
						$_SESSION[SVAIZA_EMAIL] 	= $user_data->email;
						$this->session->set_userdata('svaiza_name', $user_data->name);
						$this->session->set_userdata('svaiza_city', $user_data->city);
						$this->session->set_userdata('svaiza_state', $user_data->state);
						$this->session->set_userdata('svaiza_pincode', $user_data->pincode);
						$this->session->set_userdata('svaiza_address', $user_data->address);
						// SET USER LOGIN SESSION - END
						echo "loggedin";
					}
				} else {
					echo "user_na";
				}
			} else if($this->input->post('action') == "register") {
				?>
                <form method="post" action="javascript:void(0);" onsubmit="javascript:ajaxReg('<?php echo SITE_URL.PD_PAGE?>/user_register', '<?php echo SITE_URL?>');" id="user_signup" 
                	name="user_signup">
                  <div class="sign-up-wraper-left">
                    <div class="email-address"><strong>New Account Registration </strong></div>
                    <div class="email-address">
                      <div class="form_label left">Name : </div>
                      <div class="left">
                        <input type="text" placeholder="Full Name" class="txtbx alpha_space" id="name" name="name">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">Mobile : </div>
                      <div class="left">
                        <input type="text" placeholder="Mobile Number" class="txtbx" id="mobile" name="mobile" value="<?php echo $this->input->post('mobile');?>">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">Email : </div>
                      <div class="left">
                        <input type="text" placeholder="Valid Email Address" class="txtbx" id="email" value="" name="email">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">Address : </div>
                      <div class="left">
                        <input type="text" placeholder="Full Address" class="txtbx" id="address" name="address">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">City : </div>
                      <div class="left">
                        <input type="text" placeholder="City" class="txtbx" id="city" name="city">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">State : </div>
                      <div class="left">
                        <select name="state" id="state" class="txtbx" style="width:155px !important;">
                            <option value="">Select Option</option>
							<?php
                                if (count($this->all_array['ARR_STATE']) > 0) {
                                    asort($this->all_array['ARR_STATE']);
                                    foreach ($this->all_array['ARR_STATE'] as $k => $v) {
                                        echo "<option value='".$k."'>".$v."</option>";
                                    }
                                } else {
                                    echo "<option>Select Option</option>";
                                }
                            ?>
                        </select>
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="email-address">
                      <div class="form_label left">Pincode : </div>
                      <div class="left">
                        <input type="text" placeholder="Pincode" class="txtbx" id="pincode" name="pincode">
                      </div>
                      <div class="clr"></div>
                    </div>
                    <div class="sign-in-btn">
                      <input type="submit" value="Sign Up" id="register_btn" class="btn" />
                      <div class="forgot-text"><a class="forgot-text" href="<?php echo SITE_URL?>recover">Recover Password</a></div>
                    </div>
                  </div>
                </form>
                <?php
			} else {
				echo "require_blank";
			}
		} else {
			echo "require_blank";
		}
		// PUT EXIT FOR AJAX CALL - START
		exit;
		// PUT EXIT FOR AJAX CALL - END
	}
	
	//--//Koushik 18-Dec-2014. added for internal purpose
	function load_signup_cache()
	{
		ini_set('memory_limit', '1024M');
		$user_login = $this->db->query("select * from ".USERS." order by user_id LIMIT 30000,60000");
		echo "select * from ".USERS." order by ID LIMIT 0,1000";
		//echo $this->db->last_query();
		//$arr_mobiles = array();
		if ($user_login->num_rows() > 0) {
			foreach ($user_login->result_array() as $user_data) {
				//echo $user_data['email'],' == ',$user_data['mobile'],'<br>';
				$var_mem_cache = 'sh_user_details_'.$user_data['mobile'];
				$this->memcache_driver->set_cache($var_mem_cache,$user_data,$compress = true,$expiry_time = 0);
				//$arr_mobiles[$user_data['mobile']] = 1;
			}
		}
		//pr($arr_mobiles);*/
		$var_mem_cache = 'sh_user_details_5658584737';
		$ok = $this->memcache_driver->get_cache($var_mem_cache);
		if(!empty($ok))
		pr($ok);
		//$this->memcache_driver->set_cache($var_mem_cache,$arr_mobiles,$compress = true,$expiry_time = 0);
		exit(0);
	}
	
	//--//Koushik 18-Dec-2014. added for internal purpose
	function store_sample_data()
	{
		for($j = 0;$j<1000000;$j++)
		{
			$s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
			$i = substr(str_shuffle(str_repeat("0123456789", 9)), 0, 9);
			$data = array(
						'name'			=> $s,
						'email'			=> $s.'@seajintech.com',
						'gender' 		=> '1',
						'mobile' 		=> $i,
						'passwd' 		=> $s,
						'address'		=> $s,
						'city'			=> $s,
						'state'			=> '2',
						'status'		=> '1',
						'datecreated'	=> date('Y-m-d H:i:s') 
					);
			
			// INSERT QUERY
			$this->db->insert(USERS,$data);
		}
		exit(0);
	}
}

