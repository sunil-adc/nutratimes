<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		
		
		// SEND NETWORK AND PUB ID - START
		$this->load->model('cart_driver');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	
	public function details ($seourl = NULL){
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;

		$header['meta_title']   = ""; 
		$header['meta_desc']    = "";
		$header['meta_keyword'] = "";

		
		// SELECT PRODUCT DETAILS
		$this->replica_db->query('SET SQL_BIG_SELECTS=1');
		$this->replica_db->cache_off();
		$query = $this->replica_db->query("SELECT
										i.`image`,				i.`prod_image_id`,		i.`isdefault`,
										p.`prod_id`,			p.`cat_id`,				p.`sub_cat_id`,
										p.`brand`,				p.`name`,				p.`seourl`,
										p.`cod_price`,			p.`sale_price`,			p.`shipping`,
										p.`discount`,			p.`description`,		p.`small_description`,
										p.`model_no`,			p.`warranty_msg`,		p.`is_size`,
										p.`contest`,			p.`contest_description`,p.is_verified,
										p.`title`,				p.`meta_description`,	p.`meta_keywords`,		
										p.`free_prod`,					
										p.`video_url`,			p.`oos`,				p.`is_color`,			
										p.check_inventory,		p.cod_availability, 			
										p.`color_list`,			
										p.`total_stock`,		p.`status`,				p.`datecreated`,			
										p.`mrp`,				p.`cod_availability`,	
										b.description as b_desc,b.image brand_image, 	p.`contest_description`,
										p.free_offer_descr, p.dateupdated
									FROM
									 	".PRODUCT." p 
										left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id) 
										left join ".BRANDS." b on (p.brand = b.brand_id)
									WHERE
										p.status = 1 and 
										p.oos != 1 and 
										p.is_verified = 1 and 
										p.seourl = '".$seourl."'
									group by 
										p.prod_id");
		
		// GET THE PRODUCT DETAILS
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['prod_detail'] = $p;
				
				$data['brand_description'] = $p->b_desc;
				$data['brand_image']  = $p->brand_image;
				$header['meta_title']   = $p->title;
				$header['meta_desc'] 	= $p->meta_description;
				$header['meta_keyword'] = $p->meta_keywords;
				
				// SELECT SIZE ARRAY FOR THIS PERTICULAR PRODUCT
				if($p->is_size != "") {
					$this->replica_db->cache_off();
					$size_query = $this->replica_db->query("select pg.prod_genreid, pg.genre_quantity, g.genre_id, g.genre_name from ".PRODUCT_GENRE." pg inner join ".GENRE." g on (pg.genreid = g.genre_id) where pg.prodid = ".$p->prod_id." order by g.genre_name asc");
					if ($size_query->num_rows() > 0) {
						$data['size_genre'] = $size_query;
					} else {
						$data['size_genre'] = NULL;
					}
				} else {
					$data['size_genre'] = NULL;
				}
			
				// PRODUCT HISTORY - START
				$this->session->set_userdata('product_history', history_manager($p->prod_id, $this->session->userdata('product_history')));
				// PRODUCT HISTORY - END
			}
		} else {
			$this->session->set_flashdata('error', 'Looks like the page you requested does not exist or has moved. So automated system has moved you to homepage.');
			redirect(SITE_URL);
		}
		
		// PRODUCT IMAGE - START
		$prod_image = $this->replica_db->query("SELECT	
											i.`image`,		i.`prod_image_id`, 	i.`prod_id`
									  FROM
											".PRODUCT." p left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id)
									  WHERE
										p.status = 1 and
										i.status = 1 and 
										p.seourl = '".$seourl."'");
		// PRODUCT IMAGE - END
		
		if ($prod_image->num_rows() > 0) {
			foreach ($prod_image->result() as $pi) {
				$data['prod_image'][] = $pi;
			}
		}
		
		
		
		$this->replica_db->cache_off();
		$data['pixel_data'] = $this->replica_db->query('select pixel from '.PIXELS.' where id = 11');
		
		
		//RELATED PRODUCTS
		$data['related_products'] = $this->memcache_driver->get_cache(related_products);
		if (!$data['related_products']) { 
		
			$data['related_products'] = $this->db->query("select 
													p.cat_id,
													p.sub_cat_id,
													p.seourl,
													p.cod_availability,
													p.brand,
													p.prod_id, 
													i.image, 
													p.name, 
													p.mrp,
													p.sale_price
												from 
													".PRODUCT." p 
													left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id) 
												where 
													p.status = 1 and 
													p.oos = 0 and 
													p.is_verified = 1 and 
													p.contest = 0 and 
													p.seourl != '".$seourl."' 
												group by 
													p.prod_id,
													p.sub_cat_id 
												order by 
													p.datecreated desc
												limit 0,3");
				
			$data['related_products'] = $data['related_products']->result();
			$this->memcache_driver->set_cache('related_products', $data['related_products'], true, CACHE_LONGTIME);
		}else{
			
			$data['related_products'] = $data['related_products'];
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common', $header);
		//$this->load->view(FRONT_INC.'zn-zoom');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-product-detail',$data);
		//$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'zn-footer');
	}
	public function _alpha_space($str_in = '') {
		if (! preg_match("/^([a-zA-Z ])+$/i", $str_in)) {
			return false;
		}
		return true;
	}
	
	public function quick_register() {
		
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
				
				if ($this->session->userdata('temp_svaiza_mobile') != trim($this->input->post('mobile'))) {
				
					// CHECK FOR USER REQUEST OF REGISTRATION - START
					if ($this->input->post('quickform') != "") {
						// SET THE VALIDATION FOR FORM SUBMITTION - START
						$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|xss_clean');
						$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|exact_length[10]|numeric');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
						// SET THE VALIDATION FOR FORM SUBMITTION - END
						
						if ($this->form_validation->run() != false) {
							
							if($this->general->is_duplicate_add(USERS, "mobile", $this->input->post('mobile'), true, false)) {
								$password	=	get_random_chracter(8, 15, true, true, false);
								$arr_post 	= $this->input->post();
								// SET THE DATA FOR INSERTION
								$data = array(
												'passwd'		=> md5($password),
												'name' 			=> trim($arr_post['name']),
												'mobile' 		=> trim($arr_post['mobile']),
												'email' 		=> trim($arr_post['email']),
												'datecreated'	=> date('Y-m-d H:i:s')
											);
								// INSERT QUERY
								$this->db->insert(USERS,$data);
								$user_id = $this->db->insert_id();
								
								// SET USER LOGIN SESSION - START
								$this->session->set_userdata('temp_svaiza_user_id', $user_id);
								$this->session->set_userdata('temp_svaiza_name', trim($arr_post['name']));
								$this->session->set_userdata('temp_svaiza_mobile', trim($arr_post['mobile']));
								$this->session->set_userdata('temp_svaiza_email', trim($arr_post['email']));
								
								// SET USER LOGIN SESSION - END
								if ($this->input->post('redirectcart') == '1') {
									$this->cart_handler();
								} else {
									echo "done";
								}
							} else {
								echo "duplicate_mobile";
							}
						} else {
							echo 'validation_err';
						}
					} else {
						echo 'refresh';
					}
				}
		}
		exit;
		// CHECK FOR USER REQUEST OF REGISTRATION - END
	}
	
	public function user_register() {
		
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
				
				if ($this->session->userdata('temp_svaiza_user_id') != '' && $this->session->userdata('temp_svaiza_user_id') > 0 && 
					$this->session->userdata('temp_svaiza_mobile') != '' && $this->session->userdata('temp_svaiza_email')) {
						
						// SET THE VALIDATION FOR FORM SUBMITTION - START
						$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|xss_clean');
						$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|exact_length[10]|numeric');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('city', 'city', 'trim|required');
						$this->form_validation->set_rules('state', 'State', 'trim|required|greater_than[0]');
						$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|exact_length[6]|numeric');
						
						// SET THE VALIDATION FOR FORM SUBMITTION - END
						if ($this->form_validation->run() != false) {
							$password	=	get_random_chracter(8, 15, true, true, false);							
							$arr_post 	= $this->input->post();
							
							foreach($arr_post as $r){
								if(check_mysql_inject($r)){
									echo "validation_err";
									exit;
								}
							}
							
							// SET THE DATA FOR INSERTION
							$data = array(
											'passwd'		=> md5($password),
											'name' 			=> trim($arr_post['name']),
											'address'		=> trim($arr_post['address']),
											'city'			=> trim($arr_post['city']),
											'state'			=> trim($arr_post['state']),
											'pincode'		=> trim($arr_post['pincode']),
											'datecreated'	=> date('Y-m-d H:i:s')
										);
							// INSERT QUERY
							$this->db->where('user_id', $this->session->userdata('temp_svaiza_user_id'));
							$this->db->update(USERS, $data);
							$user_id = $this->session->userdata('temp_svaiza_user_id');
							
							// SEND EMAIL TO USER FOR REGISTER
							$this->svaiza_email->email_signup($user_id, $password);	
								
							// SET USER LOGIN SESSION - START
							$_SESSION[SVAIZA_USER_ID] 	= $user_id;
							$_SESSION[SVAIZA_MOBILE]	= trim($arr_post['mobile']);
							$_SESSION[SVAIZA_EMAIL]		= trim($arr_post['email']);
							
							$this->session->set_userdata('svaiza_name', trim($arr_post['name']));
							$this->session->set_userdata('svaiza_city', trim($arr_post['city']));
							$this->session->set_userdata('svaiza_state', trim($arr_post['state']));
							$this->session->set_userdata('svaiza_pincode', trim($arr_post['pincode']));
							
							$this->session->unset_userdata('temp_svaiza_user_id');
							$this->session->unset_userdata('temp_svaiza_name');
							$this->session->unset_userdata('temp_svaiza_mobile');
							$this->session->unset_userdata('temp_svaiza_email');
								
							
							// SET USER LOGIN SESSION - END
							if ($this->input->post('redirectcart') == '1') {
								$this->cart_handler();
							} else {
								echo "done";
							}
							
						} else {
							echo 'validation_err';
						}
				} else {
					// CHECK FOR USER REQUEST OF REGISTRATION - START
					if ($this->input->post('form') != "") {
						
							
						// SET THE VALIDATION FOR FORM SUBMITTION - START
						$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|xss_clean');
						$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|exact_length[10]|numeric');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('city', 'city', 'trim|required');
						$this->form_validation->set_rules('state', 'State', 'trim|required|greater_than[0]');
						$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|exact_length[6]|numeric');
						// SET THE VALIDATION FOR FORM SUBMITTION - END
						
						if ($this->form_validation->run() != false) {
							
							$user_login = $this->db->query("select user_id, name, mobile, email, city, state, pincode from ".USERS." where mobile = '".$this->input->post('mobile')."'");
				
								if ($user_login->num_rows() > 0) {
									foreach ($user_login->result() as $user_data) {
									
										// SET USER LOGIN SESSION - START
										$_SESSION[SVAIZA_USER_ID] 	= $user_data->user_id;
										$_SESSION[SVAIZA_MOBILE] 	= $user_data->mobile;
										$_SESSION[SVAIZA_EMAIL] 	= $user_data->email;
										$this->session->set_userdata('svaiza_name', $user_data->name);
										$this->session->set_userdata('svaiza_city', $user_data->city);
										$this->session->set_userdata('svaiza_state', $user_data->state);
										$this->session->set_userdata('svaiza_pincode', $user_data->pincode);
										// SET USER LOGIN SESSION - END
										
									}
								

									if ($this->input->post('redirectcart') == '1' ) {
										$this->cart_handler();
									} else {
										echo "done";
									}
									
							}else {	

							//if($this->general->is_duplicate_add(USERS, "mobile", $this->input->post('mobile'), true, false)) {
								$password	=	get_random_chracter(8, 15, true, true, false);
								$arr_post 	= $this->input->post();
								
								foreach($arr_post as $r){
									if(check_mysql_inject($r)){
										echo "validation_err";
										exit;
									}
								}
								// SET THE DATA FOR INSERTION
								$data = array(
												'passwd'		=> md5($password),
												'name' 			=> trim($arr_post['name']),
												'mobile' 		=> trim($arr_post['mobile']),
												'email' 		=> trim($arr_post['email']),
												'address'		=> trim($arr_post['address']),
												'city'			=> trim($arr_post['city']),
												'state'			=> trim($arr_post['state']),
												'pincode'		=> trim($arr_post['pincode']),
												'datecreated'	=> date('Y-m-d H:i:s')
											);
								// INSERT QUERY
								$this->db->insert(USERS,$data);
								$user_id = $this->db->insert_id();
								
								// SEND EMAIL TO USER FOR REGISTER
								$this->svaiza_email->email_signup($user_id, $password);
								
								// SET USER LOGIN SESSION - START
								$_SESSION[SVAIZA_USER_ID] 	= $user_id;
								$_SESSION[SVAIZA_MOBILE]	= trim($arr_post['mobile']);
								$_SESSION[SVAIZA_EMAIL]		= trim($arr_post['email']);
								
								$this->session->set_userdata('svaiza_name', trim($arr_post['name']));
								$this->session->set_userdata('svaiza_city', trim($arr_post['city']));
								$this->session->set_userdata('svaiza_state', trim($arr_post['state']));
								$this->session->set_userdata('svaiza_pincode', trim($arr_post['pincode']));
								
								$this->session->unset_userdata('temp_svaiza_user_id');
								$this->session->unset_userdata('temp_svaiza_name');
								$this->session->unset_userdata('temp_svaiza_mobile');
								$this->session->unset_userdata('temp_svaiza_email');							
								
								// SET USER LOGIN SESSION - END
								if ($this->input->post('redirectcart') == '1') {
									$this->cart_handler();
								} else {
									echo "done";
								}
							} 
						} else {
							echo 'validation_err';
						}
					
					}
				}
		} else if(
			is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			$this->cart_handler();					
		}
		exit;
		// CHECK FOR USER REQUEST OF REGISTRATION - END 
	}
	 
	public function cart_handler() {
		if(strlen( trim($this->input->post('mode'))) > 0) {
			$mode = strtolower($this->input->post('mode'));
			if($mode == "add") {
				if(is_user_login($_SESSION[SVAIZA_USER_ID],
				 				 $_SESSION[SVAIZA_MOBILE],
								 $_SESSION[SVAIZA_EMAIL])) {
					
					if ($this->input->post('oos') != 1) {
						$pid_arr = @unserialize($this->input->post('pid'));
						
						// CHECK FOR THE ARRAY IF REQUEST IS NOT IN ARRAY THEN STORE SINGE PRODUCT ELSE MULTIPLE
						if ($pid_arr === false){
							$order_id = $this->cart_driver->_add_cart($this->input->post('pid'), $this->input->post('size_id'));
							if (isset($order_id) && $order_id > 0 && $_SESSION[USER_ORDER_ID] != '') {
								echo "added";
							} else {
								echo "refresh";
							}
						} else { 
						  // CHECK FOR THE ARRAY IF REQUEST IS NOT IN ARRAY THEN STORE SINGE PRODUCT ELSE MULTIPLE
						  $cart_added = false;
						  foreach($pid_arr as $pid) {
								if ($this->cart_driver->_add_cart($pid, $this->input->post('size_id'))) {
									$cart_added = true;
								}					
						   }
						   
						   if ($cart_added == true) {
								echo "added";
						   } else {
								echo "refresh";
						   }
						}
					} else {
						echo "oos";
					}
				} else{
					echo "not_login";
				}
			}else {
				echo "err";
			}
		} else {
			echo "refresh";
		}
		// EXIT FOR AJAX CALL - START
		exit;
		// EXIT FOR AJAX CALL - END
	}
	
}