<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class paymentConfirm extends CI_Controller {	
	protected $data;	
	function __construct(){
		parent::__construct();
		
		$this->load->model(array('order_driver', 'cart_driver'));
		$this->load->library(array('crypt_rc4'));
		
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			redirect(SITE_URL."login");
		}		
	}
	
	public function index() {
		
		// UNLOCK THE TABLE
		if($_SESSION[USER_ORDER_ID] != "") {
			$this->cart_driver->unlock_order($_SESSION[USER_ORDER_ID]);
		}
		
		$this->data['is_redeem'] = 0;
		if(isset($_GET['pg'])) {
			if (strtolower($_GET['pg']) == 'payu') {
				/* CODE START FOR PAYU */
				$payu_response = array();
				if (!empty($_POST)) {
					foreach($_POST as $key => $value) { 
						$payu_response[$key] = htmlentities($value, ENT_QUOTES);
					}
				}
				
				if($payu_response['udf1'] == '1') {
					$this->data['is_redeem'] = '1';
					
					// UNSET CPS FIRED PIXELS CODE
					$this->session->unset_userdata('cps_fired_pixels');
				} else {
					$this->cart_driver->_cps_pixel_fire();
				}
				
				if(isset($payu_response['status']) && $payu_response['status'] == 'success') {
					$order_id_pg = $payu_response['txnid'];
					$this->order_driver->set_order_paid($order_id_pg);	
					
					// MINUS STOCK IF REQUIRE
					$this->cart_driver->product_stock_minus();
				}
				
				// CONFIRM THE ORDER AND SEND THE MAIL AND REDIRECT PAGE
				$this->_confirm_order_redirect($order_id_pg);
		
				/* CODE END FOR PAYU */
			} else if (strtolower($_GET['pg']) == 'ebs') {
				/* CODE START FOR EBS */
				$this->_get_ebs_details();
				$secret_key = $this->data['ebs_secret_key'];
				if(isset($_GET['DR'])) {
					 $DR = preg_replace("/\s/","+",$_GET['DR']);
				
					 $rc4 = new crypt_rc4($secret_key);
					 $QueryString = base64_decode($DR);
					 $rc4->decrypt($QueryString);
					 $QueryString = explode('&',$QueryString);
					 
					 $response = array();
					 foreach($QueryString as $param){
						$param = explode('=',$param);
						$response[$param[0]] = urldecode($param[1]);
					 }
					 
					if ($_GET['use_redeem'] == '1') {
						$this->data['is_redeem'] = '1';
						
						// UNSET CPS FIRED PIXELS CODE
						$this->session->unset_userdata('cps_fired_pixels');
					} else {
						$this->cart_driver->_cps_pixel_fire();
					}
					
					foreach( $response as $key => $value) {
						if(strtolower($key) == 'responsemessage' && strtolower($value) == 'transaction successful') {
							$order_place = '1';
						}
						if(strtolower($key) == 'merchantrefno') {
							$order_id_pg = $value;
						}
					}
					
					if($order_place == '1') {
						// UPDATE THE ORDER STATUS TO PAID
						$this->order_driver->set_order_paid($order_id_pg);	
						
						// MINUS STOCK IF REQUIRE
						$this->cart_driver->product_stock_minus();
			
						// CONFIRM THE ORDER AND SEND THE MAIL AND REDIRECT PAGE
						$this->_confirm_order_redirect($order_id_pg);
					} else {
						redirect(SITE_URL.'paymentfailure');
					}
				}
				/* CODE END FOR EBS */
			} else if (strtolower($_GET['pg']) == 'direcpay') {
				
				if(is_array($_REQUEST)) {
					$arr_directpay = explode("|", $_REQUEST['responseparams']);
					if(strtolower($arr_directpay[1]) == 'success') {
						$order_id_pg = $arr_directpay[5];
						
						if ($_GET['use_redeem'] == '1') {
							$this->data['is_redeem'] = '1';
							
							// UNSET CPS FIRED PIXELS CODE
							$this->session->unset_userdata('cps_fired_pixels');
						} else {
							$this->cart_driver->_cps_pixel_fire();
						}
						
						// UPDATE THE ORDER STATUS TO PAID
						$this->order_driver->set_order_paid($order_id_pg);	
						
						// MINUS STOCK IF REQUIRE
						$this->cart_driver->product_stock_minus();
			
						// CONFIRM THE ORDER AND SEND THE MAIL AND REDIRECT PAGE
						$this->_confirm_order_redirect($order_id_pg);
					} else {
						redirect(SITE_URL.'paymentfailure');
					}
				} else {
					redirect(SITE_URL.'paymentfailure');
				}
			} else if (strtolower($_GET['pg']) == 'citruspay') {
				
				if (is_array($_POST)) {
					if (strtolower($_POST['TxStatus']) == 'success') {
						
						$order_id_pg = $_POST['TxId'];
						
						if ($_GET['use_redeem'] == '1') {
							$this->data['is_redeem'] = '1';
							
							// UNSET CPS FIRED PIXELS CODE
							$this->session->unset_userdata('cps_fired_pixels');
						} else {
							$this->cart_driver->_cps_pixel_fire();
						}
						
						// UPDATE THE ORDER STATUS TO PAID
						$this->order_driver->set_order_paid($order_id_pg);	
						
						// MINUS STOCK IF REQUIRE
						$this->cart_driver->product_stock_minus();
				
						// CONFIRM THE ORDER AND SEND THE MAIL AND REDIRECT PAGE
						$this->_confirm_order_redirect($order_id_pg);						
					} else {
						redirect(SITE_URL.'paymentfailure');
					}
				} else {
					redirect(SITE_URL.'paymentfailure');
				}
			} else if (strtolower($_GET['pg']) == 'ccavenue') {
				
				$workingKey='D545825E1E34030BF9696002214E3BB3';		//Working Key should be provided here.
				$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
				
				if ($encResponse != "") {
					
					$rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
					$order_status="";
					$decryptValues=explode('&', $rcvdString);
					$dataSize=sizeof($decryptValues);
					

					for($i = 0; $i < $dataSize; $i++) {
						
						$information=explode('=',$decryptValues[$i]);
						if($i==3){	
							$order_status=$information[1];
						}
						
						if( $i==0){	
							$order_id = $information[1];
						}
					}
					
					if (strtolower($order_status) == 'success') {
						
						$order_id_pg = $order_id;
						
						if ($_GET['use_redeem'] == '1') {
							$this->data['is_redeem'] = '1';
							
							// UNSET CPS FIRED PIXELS CODE
							$this->session->unset_userdata('cps_fired_pixels');
						} else {
							$this->cart_driver->_cps_pixel_fire();
						}
						
						// UPDATE THE ORDER STATUS TO PAID
						$this->order_driver->set_order_paid($order_id_pg);	
						
						// MINUS STOCK IF REQUIRE
						$this->cart_driver->product_stock_minus();
				
						// CONFIRM THE ORDER AND SEND THE MAIL AND REDIRECT PAGE
						$this->_confirm_order_redirect($order_id_pg);						
					} else {
						redirect(SITE_URL.'paymentfailure');
					}
				} else {
					redirect(SITE_URL.'paymentfailure');
				}
			}
		} else {
			redirect(SITE_URL.'paymentfailure');
		}
		// PUT EXIT FOR OUTPUT VIEW
		exit;
	}
	
	public function _confirm_order_redirect($order_id_pg) {
		
		$cashback_amount = $this->cart_driver->get_cashback_points();
		if (isset($cashback_amount) && $cashback_amount != false && $cashback_amount > 0) { 
			
			$data = array(
				'credit_user_id' => $_SESSION[SVAIZA_USER_ID],
				'points' => $cashback_amount,
				'action' => 'Cashback Points '.$cashback_amount.' added for the order id '.$_SESSION[USER_ORDER_ID],
				'log_date' => date('Y-m-d H:i:s'));
			
			$this->db->insert(CREDIT_LOGS, $data);
			
			$this->db->query("update tbl_users set points = points + ".$cashback_amount." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
			 
		}
		
		// GET THE ORDER ID USING ORDER IF PG
		$order_id = $this->get_ord_from_ordpg($order_id_pg);
		
		// SELECT THE PROVIDER AS PER PINCODE
		$available_provider = $this->general->_get_provider_from_pincode($_SESSION[SVAIZA_USER_ID], '1');
		if ($available_provider != false && is_numeric($available_provider)) {
			$this->general->_set_provider_order($available_provider, $order_id);
		}
		
		// UPDATE CREDIT POINT IF PRODUCT IS IN COTEST
		$is_update_credit = $this->_put_credit_point($order_id, $_SESSION[SVAIZA_USER_ID]);
		
		if(is_array($is_update_credit)) {
			if ($is_update_credit['is_contest'] == true && $is_update_credit['is_product'] == true) {
			
				// SEND EMAIL TO USER FOR CONTEST ORDER CONFIRMATION
				$this->svaiza_email->_order_contest_confirmation($_SESSION[SVAIZA_USER_ID], $order_id);
				
				// SEND EMAIL TO USER FOR PRODUCT ORDER CONFIRMATION
				$this->svaiza_email->email_order_confirmation($_SESSION[SVAIZA_USER_ID], $order_id);
			
			} else if ($is_update_credit['is_contest'] == false && $is_update_credit['is_product'] == true) {
				
				if($this->data['is_redeem'] == '1') {
					// GET USER CURRENT CREDIT POINTS	
					$this->data['user_points'] = $this->common_model->_get_user_credit_points();
					
					// CALCULATE THE TOTAL AMOUNT OF PRODUCT
					$arr_cart_amount = $this->order_driver->_calculate_cart_amount();
					$total_amount = $arr_cart_amount['final_amount'] + $arr_cart_amount['final_shipping'];
						
					// COUNT MAXIMUM REDEMPTION POINTS
					$product_max_redemption = $this->order_driver->_count_redeemable_credit_point();
						
					// COUNT REDEEMABLE CREDIT POINT FROM CURRENT CART PRODUCT
					$final_deduction = $this->order_driver->count_deduction($total_amount, $arr_cart_amount['final_shipping'], 
														$product_max_redemption, $this->data['user_points']);
						
					$total_deduction = $final_deduction['final_redemption'];
					$final_payable_amount = $final_deduction['final_price'];
					
					if ($total_deduction < $total_amount && $this->data['user_points'] > 0) {
						$upd_credit_points = $this->db->query("update ".USERS." set points = (points - ".$total_deduction.") 
																	where user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
							
						// INSERT INTO POINT HISTORY TABLE
						$this->order_driver->_insert_point_history($total_deduction);
					}
				}
				
				// SEND EMAIL TO USER FOR PRODUCT ORDER CONFIRMATION
				$this->svaiza_email->email_order_confirmation($_SESSION[SVAIZA_USER_ID], $order_id);
				
			} else if ($is_update_credit['is_contest'] == true && $is_update_credit['is_product'] == false) {
			
				// SEND EMAIL TO USER FOR CONTEST ORDER CONFIRMATION
				$this->svaiza_email->_order_contest_confirmation($_SESSION[SVAIZA_USER_ID], $order_id);
			
			}
		} else if(!is_array($is_update_credit)) {
			// SEND EMAIL TO USER
			$this->svaiza_email->email_order_confirmation($_SESSION[SVAIZA_USER_ID], $order_id);
		}
		
		// REDIRECT TO CONFIRMATION PAGE 
		redirect(SITE_URL.'paidconfirm/index/'.$order_id);
	}
	
	public function _put_credit_point($order_id, $user_id){
		if(is_numeric($order_id) && $order_id != "" && is_numeric($user_id) && $user_id != "") {
			$is_contest = $this->db->query("select id, quantity, order_id, contest, cash_price from ".USER_PRODUCTS." where status = 1 and order_id = ".$order_id);
			if($is_contest->num_rows() > 0) {
				$credit_point = 0;
				$arr_return['is_contest'] = false;
				$arr_return['is_product'] = false;
				foreach ($is_contest->result() as $row) {
					if($row->contest == '1') {
						$arr_return['is_contest'] = true;
						$credit_point += ($row->cash_price * $row->quantity);
					} else if($row->contest == '0'){
						$arr_return['is_product'] = true;
					}
				}
				if($credit_point > 0) {
					$this->db->query("update ".USERS." set points = points + ".$credit_point.", cod_flag = 0 where user_id = ".$user_id);
					
					// ADD IN LOGS
					$this->common_model->credit_logs(0, 0, $credit_point, 'Added '.$credit_point.' Credit Points for Order Id '.$order_id, $user_id);
					
					return $arr_return;
				} else {
					return $arr_return;	
				}
			}
		}
		return false;
	}
	
	public function get_ord_from_ordpg($order_id_pg){
		$this->db->cache_off();
		$order_pg = $this->db->query('select order_id from '.ORDER_PG.' where id = '.$order_id_pg);
		if($order_pg->num_rows() >0) {
			foreach ($order_pg->result() as $row) {
				return $row->order_id;
			}
		}
	}
	
	public function _get_ebs_details() {
		// GET THE PAYMENT GATEWAY DETAILS - START
		$settings = $this->db->query("select attr_name, attr_value from ".SETTINGS." where status = '1' and attr_name in ('ebs_secret_key','ebs_account_id')");
		if($settings->num_rows() > 0) {
			foreach($settings->result() as $row) {
				if($row->attr_name == 'ebs_secret_key') {
					$this->data['ebs_secret_key'] = $row->attr_value;
				}
				if($row->attr_name == 'ebs_account_id') {
					$this->data['ebs_account_id'] = $row->attr_value;
				}
			}
		}
		// GET THE PAYMENT GATEWAY DETAILS - START
	}
}