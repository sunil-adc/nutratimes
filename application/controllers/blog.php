<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class blog extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->db->cache_off();
					
	}
     
	public function index(){

		
	    $data = "";
	    $blogid="";
	    $right['cur_controller'] 	= strtolower(__CLASS__);
	    $header['meta_title']       = "The best blog sites for weight loss 2019 | Nutratimes"; 
		$header['meta_desc']        = "Nutratimes blog the best Weight loss blog in India.. Know more about the weight loss, fat burner tips naturally and organically. Click now to read more. ";
		$header['meta_keyword']     = "best blog sites for weight loss, weight loss blog, weight loss blog india";


	    //$this->db->cache_on();
        $query=$this->db->query("SELECT
										blogid, title,url, blogcontent, blog_image, date_created
								FROM
										".BLOG."
								WHERE 
										status = 1 order by date_created desc ");
	   
		if ( $query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				 
				 $data['blogdetail'][] = $p;
			}
				//print_r($data['blogdetail']);die();
			
		}else{
		   $data['blogdetail'] = NULL;
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common', $header);
		$this->load->view(FRONT_INC.'zn-header');
		//$this->load->view(FRONT_INC.'sidebar',$cat);
		$this->load->view('blog',$data);
		//$this->load->view(FRONT_INC.'history');
		//$this->load->view(FRONT_INC.'brand',$brand);
		$this->load->view(FRONT_INC.'zn-footer');
		
	
	}
   
  	public function details($url=''){
	    
	    $data = "";
		$right['cur_controller'] 	= strtolower(__CLASS__);
		//$this->db->cache_on();
	    $query=$this->db->query("SELECT
									blogid, title, url, blogcontent,
									blog_image, date_created, pagetitle,
									meta_keyword, metadescription
								FROM
									".BLOG."
								WHERE 
									status = 1 and url='".$url."'");
		   
		if ( $query->num_rows() > 0) {
			
			foreach ($query->result() as $p) {
		    	 $data['blogdetails'][]=$p;
		    	 $meta['title']   = $p->pagetitle;
		    	 $meta['keyword'] = $p->meta_keyword;
		    	 $meta['desc']    = $p->metadescription;
		 	}
			 //print_r($data['blogdetails']);die();
		}else{
		   
		    redirect(SITE_URL."/blog");
		    $data['blogdetails'] = NULL;  
		}
		
		$header['meta_title']       = $meta['title']; 
		$header['meta_desc']        = $meta['desc'];
		$header['meta_keyword']     = $meta['keyword'];
		    
		
		$this->load->view(FRONT_INC.'zn-common', $header);
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('blog-details',$data);
		$this->load->view(FRONT_INC.'zn-footer');
	  
 	
  	}
  

	  
}
?>
