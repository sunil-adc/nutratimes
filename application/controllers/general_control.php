<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class general_control extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
	}
	
	public function send_confirm_mail() {
		$this->svaiza_email->email_order_confirmation($_POST['user_id'], $_POST['order_id'], 2);
	}
	
	public function send_cancel_mail() {
		$this->svaiza_email->cancel_order($_POST['order_id']);
	}
	
}
	