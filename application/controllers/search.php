<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class search extends CI_Controller {
		
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		$this->load->model( array('scrollpagination', 'search_driver'));
		
	}
	
	public function index($keyword = NULL){
		// CALL DEFAULT FUNCTION0
		$this->page(0, 36, 0, 0, $this->input->get('q') != "" ? $this->input->get('q') : '');
	
	}
	
	public function page($offset = 0, $perpage = 12, $pageno = 0, $isAjax = 0, $keyword="") {
		
		// GET CURRENT CONTROLLER NAME
		$cur_controller 		= strtolower(__CLASS__);
		$data['whr']			= "";
		$whr					= "";
		$whr_color				= "";
		$whr_price				= "";
		$extra_tbl				= "";
		$order_by 				= " p.priority asc ";
		$header['meta_title']   = "Buy weight loss product in Bangalore (India) | Nutratimes";  
		$header['meta_desc']    = "Buy Nutrarimes Garcinia Cambogia in India .India's best weight loss supplements . Loss upto 5 kg in a month without any side effect, Check now";
		$header['meta_keyword'] = "Buy Nutratimes Garcinia Cambogia, Buy Nutratimes Garcinia Cambogia in India,  Buy Nutratimes Garcinia Cambogia in Bangalore";


		// FETCH CATEGORY
		$cat['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$cat['cur_controller']	= $cur_controller;
	
		if($this->input->get('q') != "" || trim($keyword) != "") {
			$search_keyword = "";
			if(trim($keyword) != "") {
				$search_keyword = $keyword;
			} else if(strtolower($this->input->get('q')) != "") {
				$search_keyword = $this->input->get('q');
			}
			
			if($search_keyword != "") {
				if(check_mysql_inject($search_keyword)){
					header('Location:'.SITE_URL);   
				}
				$data['whr'] = str_replace('-', ' ', $search_keyword);
				$whr .= ' and (p.name like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%" or p.search_keywords like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%") ';
				$whr_price .= ' and (p.name like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%" or p.search_keywords like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%") ';
				$whr_color .= ' and (p.name like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%" or p.search_keywords like "%'.mysql_real_escape_string(addslashes($search_keyword)).'%") ';
			}
		}
		
		// SELECT PAGE TOP BANNER AND RELATED LINKS - START
		$setting_homepage = $this->db->query("SELECT attr_name, attr_value FROM ".SETTINGS." where attr_name in ('search_top_banner', 'search_top_banner_link')");
		if($setting_homepage->num_rows() > 0) {
			foreach ($setting_homepage->result() as $val_setting_homepage) {
				$data['setting_data'][$val_setting_homepage->attr_name] = $val_setting_homepage->attr_value;
			}
		}
		// SELECT PAGE TOP BANNER AND RELATED LINKS - END
		
		
		// IF BRAND SET THEN SEARCH FOR BRAND
		if ((isset($_REQUEST['b']) && trim($_REQUEST['b']) != "") || (isset($_GET['b']) && trim($_GET['b']) != "")) {
			$arr_args_brand = explode('|', isset($_REQUEST['b']) ? $_REQUEST['b'] : $_GET['b']);
			
			$is_brand_skip = false;
			if (count($arr_args_brand) > 0) {
				foreach ($arr_args_brand as $bk => $bv) {
					if (!preg_match('/^[1-9][0-9]*$/', $bv)) {
						header('Location:'.SITE_URL);
						die();
					}
					if (!is_numeric($bv)) {
						$is_brand_skip = true;			
					}
				}
			}
			
			if ($is_brand_skip == false) {
				$search_brand = str_replace('|', ',' , isset($_REQUEST['b']) ? $_REQUEST['b'] : $_GET['b']);
				if(check_mysql_inject($search_brand)){
					header('Location:'.SITE_URL);   
				}
				$whr .= " and p.brand in (".mysql_real_escape_string($search_brand).")";	
				$whr_color .= " and p.brand in (".mysql_real_escape_string($search_brand).")";
				$whr_price .= " and p.brand in (".mysql_real_escape_string($search_brand).")";	
			}
		}
		
		// CATEGORY
		if ((isset($_REQUEST['c']) && trim($_REQUEST['c']) != "") || (isset($_GET['c']) && trim($_GET['c']) != "")) {
			
			$arr_args_category = explode('|', isset($_REQUEST['c']) ? $_REQUEST['c'] : $_GET['c']);
			
			$is_category_skip = false;
			if (count($arr_args_category) > 0) {
				foreach ($arr_args_category as $ck => $cv) {
					
					if (!preg_match('/^[1-9][0-9]*$/', $cv)) {
						header('Location:'.SITE_URL);
					    die();
					}
					
					if (!is_numeric($cv)) {
						$is_category_skip = true;			
					}
				}
			}
			
			if ($is_category_skip == false) {
				$search_category = str_replace('|', ',' , isset($_REQUEST['c']) ? $_REQUEST['c'] : $_GET['c']);
				
				if(check_mysql_inject($search_category)){
					header('Location:'.SITE_URL);   
				}
				$whr .= " and p.cat_id in (".mysql_real_escape_string($search_category).") ";
				$whr_price .= " and p.cat_id in (".mysql_real_escape_string($search_category).") ";	
				$whr_color .= " and p.cat_id in (".mysql_real_escape_string($search_category).") ";
			}
		}
		
		// SUB CATEGORY
		if ((isset($_REQUEST['sc']) && trim($_REQUEST['sc']) != "") || (isset($_GET['sc']) && trim($_GET['sc']) != "")) {
						
			$arr_args_sub_category = explode('|', isset($_REQUEST['sc']) ? $_REQUEST['sc'] : $_GET['sc']);
			
			$is_subcategory_skip = false;
			if (count($arr_args_sub_category) > 0) {
				foreach ($arr_args_sub_category as $sck => $scv) {
					if (!preg_match('/^[1-9][0-9]*$/', $scv)) {
						header('Location:'.SITE_URL);
						die();
					}
					if (!is_numeric($scv)) {
						$is_subcategory_skip = true;			
					}
				}
			}
			
			if ($is_subcategory_skip == false) {
				$search_sub_category = str_replace('|', ',' , isset($_REQUEST['sc']) ? $_REQUEST['sc'] : $_GET['sc']);
				if(check_mysql_inject($search_sub_category)){
					header('Location:'.SITE_URL);   
				}
				
				$whr .= " and p.sub_cat_id in (".mysql_real_escape_string($search_sub_category).") ";
				$whr_price .= " and p.sub_cat_id in (".mysql_real_escape_string($search_sub_category).") ";
				$whr_color .= " and p.sub_cat_id in (".mysql_real_escape_string($search_sub_category).") ";	
			}
		}
		
		// SIZE
		if ((isset($_REQUEST['sz']) && trim($_REQUEST['sz']) != "") || (isset($_GET['sz']) && trim($_GET['sz']) != "")) {
			$arr_args_size = explode('|', isset($_REQUEST['sz']) ? $_REQUEST['sz'] : $_GET['sz']);
			$is_size_skip = false;
			if (count($arr_args_size) > 0) {
				foreach ($arr_args_size as $sck => $scv) {
					if (!is_numeric($scv)) {
						$is_size_skip = true;			
					}
				}
			}
			
			if ($is_size_skip == false) {
				$search_size = str_replace('|', ',' , isset($_REQUEST['sz']) ? $_REQUEST['sz'] : $_GET['sz']);
				if(check_mysql_inject($search_size)){
					header('Location:'.SITE_URL);   
				}
				$whr .= " and pg.genreid in (".mysql_real_escape_string($search_size).") and p.is_size = 1 ";	
				$extra_tbl .= " inner join ".PRODUCT_GENRE." pg on (p.prod_id = pg.prodid) "; 
			}
		}
		
		// IF PRICE SET THEN SEARCH FOR CATEGORY
		if ((isset($_REQUEST['p']) && trim($_REQUEST['p']) != "") || (isset($_GET['p']) && trim($_GET['p']) != "")) {
			$req_price = isset($_REQUEST['p']) ? $_REQUEST['p'] : $_GET['p'];
			$arr_price = explode("|", $req_price);
			$whr .= " and ( ";
			$whr_price .= " and ( ";
			$whr_color .= " and ( ";
			$total_param = count($arr_price);
			$k = 1;
			foreach ($arr_price as $pk => $pv) {
				$arr_inner_price = explode("-", $pv);
				
				if (is_numeric($arr_inner_price[0]) && is_numeric($arr_inner_price[1])) {
		$whr .= "p.sale_price between ".mysql_real_escape_string($arr_inner_price[0])." and ".mysql_real_escape_string($arr_inner_price[1])." ";
		$whr .= ($total_param > $k) ? " or " : "";
		
		$whr_price .= "p.sale_price between ".mysql_real_escape_string($arr_inner_price[0])." and ".mysql_real_escape_string($arr_inner_price[1])." ";
		$whr_price .= ($total_param > $k) ? " or " : "";
			
		$whr_color .= "p.sale_price between ".mysql_real_escape_string($arr_inner_price[0])." and ".mysql_real_escape_string($arr_inner_price[1])." ";
		$whr_color .= ($total_param > $k) ? " or " : "";
	
				}
				$k++;
			}
			$whr .= " ) ";
			$whr_price .= " ) ";
			$whr_color .= " ) ";	 
		}
		
		
		// COLOR ID
		if ((isset($_REQUEST['clr']) && trim($_REQUEST['clr']) != "") || (isset($_GET['clr']) && trim($_GET['clr']) != "")) {
			
			$arr_args_color = explode('|', isset($_REQUEST['clr']) ? $_REQUEST['clr'] : $_GET['clr']);
			
			$is_color_skip = false;
			if (count($arr_args_color) > 0) {
				foreach ($arr_args_color as $ck => $cv) {
					if (!is_numeric($cv)) {
						$is_color_skip = true;			
					}
				}
			}
			
			if ($is_color_skip == false) {
				$search_color = str_replace('|', ',' , isset($_REQUEST['clr']) ? $_REQUEST['clr'] : $_GET['clr']);
				if(check_mysql_inject($search_color)){
					header('Location:'.SITE_URL);
				}
				$whr .= " and p.clr_id in (".mysql_real_escape_string($search_color).") ";	
				$whr_price .= " and p.clr_id in (".mysql_real_escape_string($search_color).") ";
			}
		}
		
		// IF SORT SET THEN SEARCH FOR SORT
		if ((isset($_REQUEST['_sort']) && trim($_REQUEST['_sort']) != "") || (isset($_GET['_sort']) && trim($_GET['_sort']) != "")) {
			$req_sort = isset($_REQUEST['_sort']) ? $_REQUEST['_sort'] : $_GET['_sort'];
			
			if (!preg_match('/^[1-9][0-9]*$/', $req_sort)) {
				header('Location:'.SITE_URL);
				die();
			}
			
			if(check_mysql_inject($req_sort)){
				header('Location:'.SITE_URL);
			}
			
			switch($req_sort){
				case 1:	$order_by = " p.priority asc ";
						break;
				case 2: $order_by = " p.sale_price desc ";
						break;
				case 3: $order_by = " p.sale_price asc ";
						break;
				case 4: $order_by = " p.prod_id desc ";
						break;
				default:$order_by = "";
						break;
			}			
		}
		
		//--//Koushik 22-Dec-2014. Dont display deal products under search
		$deal_prods = $this->_get_deal_products();
		$whr_deal_prods = ($deal_prods)?" and p.prod_id NOT IN(".$deal_prods.")":'';
		
		// PREPARE THE SQL STATEMENT FOR SEARCHING
		$sql = "select 
					p.cat_id,
					p.sub_cat_id,
					p.seourl,
					p.brand,
					p.prod_id, 
					p.cod_availability, 
					i.image, 
					p.name, 
					p.mrp,
					p.sale_price
				from 
					".PRODUCT." p 
					left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id) 
					".$extra_tbl."  
				where 
					p.status = '1' and 
					p.oos != '1' and 
					p.is_verified = '1'
					".$whr."
					".$whr_deal_prods."
				group by 
					p.prod_id
				order by 
					$order_by ";
		
		// GET THE PAGINATION DATA 
		
		$data['pagination'] 	= $this->scrollpagination->getNextPage($sql, $offset, $perpage, false, true);
		$data['pageno']			= $pageno + 1;
		$data['cur_controller'] = $cat['cur_controller'];
		$data['sql']			= $sql;
		$data['whr']			= $data['whr'];
		
		
		if($isAjax == 1) {
			
			// SENT THE FORMATTED DATA TO VIEW - START
			$this->formatData($data);
			// SENT THE FORMATTED DATA TO VIEW - END
			
		} else if($isAjax == 0) {
			
			$data['category_banner'] = S3_URL.CONTENT_DIR.'/images/'.$data['setting_data']['search_top_banner'];
			$data['category_banner_link'] = urldecode($data['setting_data']['search_top_banner_link']);
				
			if (((isset($_REQUEST['c']) && trim($_REQUEST['c']) != "") || (isset($_GET['c']) && trim($_GET['c']) != "")) && isset($search_category) && $search_category != "") {
				// BRAND FOR SIDEBAR
				$category_banner = $this->replica_db->query("select cat_id, image from ".CATEGORY." where status = 1 and image != '' and cat_id in (".mysql_real_escape_string($search_category).") order by rand() limit 0, 1");
				if ($category_banner->num_rows() > 0) {
					foreach ($category_banner->result() as $val_category_banner) {
						$data['category_banner'] 		= S3_URL.CATEGORY_DIR_PATH.$val_category_banner->image;
						$data['category_banner_link'] 	= SITE_URL.'search/?&c='.$val_category_banner->cat_id;
					}
				}
			}
			
			if (((isset($_REQUEST['sc']) && trim($_REQUEST['sc']) != "") || (isset($_GET['sc']) && trim($_GET['sc']) != "")) && isset($search_sub_category) && $search_sub_category != "") {
				
				$sub_category_banner = $this->replica_db->query("select cat_id, image from ".CATEGORY." where status = 1 and image != '' and cat_id in (".mysql_real_escape_string($search_sub_category).") order by rand() limit 0, 1");
				if ($sub_category_banner->num_rows() > 0) {
					foreach ($sub_category_banner->result() as $val_category_banner) {
						$data['category_banner'] 		= S3_URL.CATEGORY_DIR_PATH.$val_category_banner->image;
						$data['category_banner_link'] 	= SITE_URL.'search/?&c='.$val_category_banner->cat_id;
					}
				}
			}
			
			
			// BRAND FOR SIDEBAR
			$cat['search_brand'] = $this->replica_db->query("select brand_id, seourl, name from ".BRANDS." where status = 1 and name != ''");
			// CATEGORY FOR SIDEBAR
			$cat['search_category'] = $this->replica_db->query("select cat_id, seourl, name from ".CATEGORY." where status = 1 and parent_cat_id = 0 and name != ''");
			// SEARCH COLOR
			$cat['search_color'] = $this->replica_db->query("select c.color_id, c.color_name, c.color_code from ".COLORS." c inner join ".PRODUCT." p on (c.color_id = p.clr_id) where c.status = 1 and p.status = 1 ".$whr_color." group by c.color_id having count(p.prod_id) > 0");
		
			$cat['search_size'] = $this->replica_db->query("select 
														g.genre_name,
														pg.prodid,
														pg.genreid
													from 
														".PRODUCT." p 
														inner join ".PRODUCT_GENRE." pg on (p.prod_id = pg.prodid)
														inner join ".GENRE." g on (g.genre_id = pg.genreid) 
													where
														p.is_size = 1 and 
														p.status = 1 and 
														g.status = 1 and 
														pg.status = '1' 
													group by 
														pg.genreid");			
			
			
			if (isset($_GET['c']) && trim($_GET['c']) != "") {
            	$arr_sub_cat = explode('|', trim($_GET['c']));
				$str_sub_cat = '';
				if (is_array($arr_sub_cat)) {
					foreach ($arr_sub_cat as $sck => $scv) {
						$str_sub_cat .= '"'.$scv.'",';
					}
				}
				$str_sub_cat = trim ($str_sub_cat,",");
				
				if (isset($str_sub_cat) && trim($str_sub_cat) != '') {
					$cat['search_sub_category'] = $this->replica_db->query("select cat_id, seourl, name from ".CATEGORY." where status = 1 and parent_cat_id != 0 and parent_cat_id in (".$str_sub_cat.")");
				}
				
			}
				
			// GET THE MAX - MIN AVAILABLE IN PRODUCT - START
			$search_price = $this->replica_db->query(" select 
													min(p.mrp) as min_mrp, 
													min(p.sale_price) as min_sale_price, 
													max(p.mrp) as max_mrp, 
													max(p.sale_price) as max_sale_price	
												from  
													".PRODUCT." p 
												where 
													p.status = 1 ".$whr_price);
			$cat['price_range']		=	$search_price;
			
			// LOAD VIEW
			$this->loadView($data, $cat, $header);
		}
	}
	
	public function formatData($data) {
		if(isset($data) && is_array($data) && $data['pagination']['available'] == true) {
			echo "<div class='page-number'>Page - ".$data['pageno']."</div>";
			echo '<ul class="wrap-products products-search">';
			$i = 1;
			$total_result = count($data['pagination']['result']);
			
			foreach($data['pagination']['result'] as $k => $v) {
				?>
                <li> 
                    <a class="photo-link" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" title="<?php echo $v->name?>">
                    <?php echo disc_percent($v->mrp, $v->sale_price); ?>
                    <img border="0" alt="<?php echo $v->name?>" 
                    src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$v->image)?>"> </a>
                    <div class="clr"></div>
                    <a class="item-name" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" title="<?php echo $v->name?>">
					<?php echo sub_string($v->name,45)?></a><div class="clr"></div>
                    <?php echo sel_price($v->mrp, $v->sale_price);?>
                    <div class="clr"></div>
                    <a class="buy-button" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" data-id="<?php echo $v->prod_id;?>">Buy Now</a>
                </li>
                <?php
				if($i == 4 && $total_result > 1) {
					$i = 0;
					echo '</ul><ul class="wrap-products products-search">';	
				}
				$i++;
				$total_result--;
			}
			?>
            </ul>
			<div class="clr"></div>
            <?php
				// SET REQUEST VARIABLES - START
				$req_brand 	= isset($_GET['b']) ? $_GET['b'] : (isset($_REQUEST['b']) ? $_REQUEST['b'] : '');
				$req_cat 	= isset($_GET['c']) ? $_GET['c'] : (isset($_REQUEST['c']) ? $_REQUEST['c'] : '');
				$req_subcat = isset($_GET['sc']) ? $_GET['sc'] : (isset($_REQUEST['sc']) ? $_REQUEST['sc'] : '');
				$req_price 	= isset($_GET['p']) ? $_GET['p'] : (isset($_REQUEST['p']) ? $_REQUEST['p'] : '');
				$req_sort 	= isset($_GET['_sort']) ? $_GET['_sort'] : (isset($_REQUEST['_sort']) ? $_REQUEST['_sort'] : '');
				$req_color 	= isset($_GET['clr']) ? $_GET['clr'] : (isset($_REQUEST['clr']) ? $_REQUEST['clr'] : '');
				$req_size = isset($_GET['sz']) ? $_GET['sz'] : (isset($_REQUEST['sz']) ? $_REQUEST['sz'] : '');
				// SET REQUEST VARIABLES - END
			?>
            <div id="pageLoaderParent" class="ajax-page-loader"><a href="javascript:page_loader()" id="pageLoader" page-redirecturl="<?php echo FULL_SITE_URL.$data['cur_controller']?>/" page-datadiv="wholedata" page-func="page" page-offset="<?php echo ($data['pagination']['page']['offset'] + $data['pagination']['page']['perpage']); ?>" page-perpage="<?php echo $data['pagination']['page']['perpage']; ?>" page-total="<?php echo $data['pagination']['page']['total']; ?>" page-cat="" page-parentdiv="pageLoaderParent" page-loading="ajax-page-loader" page-whr="<?php echo (isset($data['whr'])) ? preg_replace('![^a-z0-9]+!i', '-', $data['whr']) : '';?>" page-brand="<?php echo $req_brand; ?>" page-sort="<?php echo $req_sort;?>" page-category="<?php echo $req_cat; ?>" page-sub-category="<?php echo $req_subcat; ?>" page-price="<?php echo $req_price; ?>" page-number="<?php echo $data['pageno'];?>" page-color="<?php echo $req_color;?>" page-size="<?php echo $req_size;?>">Click here to load more...</a></div>
			<?php
		}
		exit;
	}
	
	public function loadView($data="", $cat="", $header = "" ){
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common', $header);
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-search',$data);
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	//--//Koushik 22-Dec-2014. hide super super deal attached items from search
	private function _get_deal_products()
	{
		$sql_deal_prods = "SELECT combo_product FROM ".PRODUCT." WHERE is_combo = 1 AND combo_product IS NOT NULL AND combo_product !='' AND status = 1";
		$query = $this->db->query($sql_deal_prods);
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			$arr_data = array();
			foreach ($result as $row) {
				$arr_data[] = $row['combo_product'];
			}
			return implode(',',$arr_data);
		}
		return false;
	}
}
