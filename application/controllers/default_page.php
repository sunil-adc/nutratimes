<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class default_page extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		//$this->general->get_top_level_category(3);
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		
		
	}



	public function index(){

		redirect(SITE_URL);

		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		//$this->load->view(FRONT_INC.'sidebar',$cat);
		$this->load->view('default-page');
		//$this->load->view(FRONT_INC.'history');
		//$this->load->view(FRONT_INC.'brand',$brand);
		$this->load->view(FRONT_INC.'zn-footer');

	}

}