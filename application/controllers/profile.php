<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profile extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
										  
		// SEND NETWORK AND PUB ID - START
	
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'/logout');
		}
		// CHECK USER IS LOGIN OR NOT - END
	}
	
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		$query = $this->db->query("SELECT
										name, email, mobile, address, city, state, pincode, points, gender
									FROM
									 	".USERS."
									WHERE
										status = 1 and 
										user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
		
		// GET THE PRODUCT DETAILS
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['user_details'] = $p;
			}
		} else {
			redirect(SITE_URL);
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-profile',$data);
		//$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	public function save() {
		
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('full_name', 'full_name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		
		if ($this->input->post('submit')) {
			if ($this->form_validation->run() != false) {
				// SET THE WHERE CLAUSE
				$this->db->where("user_id",$_SESSION[SVAIZA_USER_ID]);
				// SET THE DATA
				$data = array(
								'name'   => $this->input->post('full_name'),
								'email'  => $this->input->post('email'),
								'gender' => $this->input->post('gender'),
								'dateupdated' => date('Y-m-d H:i:s')
							);
				// UPDATE QUERY
				// SET THE SESSION VALUE WITH NEW ONE
				$this->session->set_userdata('svaiza_name', $this->input->post('name'));
												
				$this->db->update(USERS,$data);
				

				echo "done";
				
			} else {
				echo "validation_err";
			}
		}else{
			echo "error";
		}
	exit;
	}
}
