<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class managecolor extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
				
	}
	
	public function index($perpage = 10, $offset = 0) {
		
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= COLORS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "Manage Color";
		$data['add_page_title']	= "Add Color";
		$data['page_name']	 	= "Color";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "addcolor";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "color_id";
		$data['seach_form']		= $full_path;
		
		// CHECK FOR SEARCH REQUEST		
		if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	= ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			
			// PREPARE QUERY STRING
			$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt']."/";
			
			// CREATE THE WHERE CLAUSE
			if($data['search_field'] == 'color_id' || $data['search_field'] == 'color_code') {
				$whr = " lower(".$data['search_field'].") = '".strtolower($data['search_txt'])."'";
			} else {
				$whr = " lower(".$data['search_field'].") like '%".strtolower($data['search_txt'])."%'";
			}
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$query_string = $perpage."/";
		}
		
		// MODIFY FULL PATH
		$full_path .= $query_string;
		
		// ORDER BY CLAUSE
		$order_by = " order by datecreated desc";
		
		// CREATE THE SQL QUERY
		$sel_query = "SELECT 
						color_id,  
						color_name,
						color_code,
						status,
						datecreated 
					FROM
						".$tablename.$where.$order_by; 
		
		// PAGINATION PARAMETER VALUES START
		if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 8;
		}
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
		
		// GET THE DATA FROM PAGINATION
		$data["total_rows"] = $config['total_rows'];
		$data["results"] = $this->db_function->get_data($sql);
		$data["links"] = $this->pagination->create_links();
		
		// LOAD ALL REQUIRE VIEWS
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/manage-color',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
