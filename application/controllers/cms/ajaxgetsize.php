<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajaxgetsize extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index() {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($this->input->post('catid')) != NULL && trim ($this->input->post('catid')) != "") { 
			
			if(is_numeric($_POST['prodid']) && $_POST['prodid'] > 0) {
				$this->db->cache_off();
				$genre_prod = $this->db->query("select genreid, genre_quantity from ".PRODUCT_GENRE." where prodid = ".$_POST['prodid']);
				if ($genre_prod->num_rows() > 0) {
					foreach ($genre_prod->result() as $grow) {
						$genre_selected[] = $grow->genreid;
						$genre_quantity[$grow->genreid] = $grow->genre_quantity > 0 ? $grow->genre_quantity : 0;
					}
				}
			}
			
			$where = "";
			if(is_numeric($this->input->post('catid'))) {
				$where_cat = "cat_id = ".$this->input->post('catid')." and ";
				$where .= $where_cat;
			}
			if(is_numeric($this->input->post('subcatid'))) {
				$where .= "sub_cat_id = ".$this->input->post('subcatid')." and ";
			}
			
			$res_genre = $this->db->query("select genre_id, genre_name, parent_genre_id from ".GENRE." where ".$where." status='1'");
			if($res_genre->num_rows() > 0) {
				foreach($res_genre->result() as $row) {
					if($row->parent_genre_id == "0"){
						//echo $row->genre_name." Genre";
					} else {
					 	$checked = "";
						$quantity = 0;
						if(isset($genre_selected) && is_array($genre_selected)) {
							if(in_array($row->genre_id, $genre_selected)){
								$checked = "checked";
								$quantity = $genre_quantity[$row->genre_id];
							}
						}
						echo '<div class="role_element_size align-left">
                                <div class="chkbx align-left">
                                    <input type="checkbox" name="genre['.$row->genre_id.']" value="'.$row->genre_id.'" '.$checked.' />
                                </div>
                                <div class="chk_desc align-left">'.$row->genre_name.'</div>
								<div class="genre_txt align-left">
									<input type="text" name="genre_size['.$row->genre_id.']" class="text-input small-input" value="'.$quantity.'" />
								</div>
								<div class="clear"></div>
                             </div>';
					}
				}
			}else {
				$res_genre = $this->db->query("select genre_id, genre_name, parent_genre_id from ".GENRE." where ".$where_cat." status='1'");
				if($res_genre->num_rows() > 0) {
					foreach($res_genre->result() as $row) {
						if($row->parent_genre_id == "0"){
							//echo $row->genre_name." Genre";
						} else {
							$checked = "";
							$quantity = 0;
							if(isset($genre_selected) && is_array($genre_selected)) {
								if(in_array($row->genre_id, $genre_selected)){
									$checked = "checked";
									$quantity = $genre_quantity[$row->genre_id];
								}
							}
							echo '<div class="role_element_size align-left">
                                <div class="chkbx align-left">
                                    <input type="checkbox" name="genre['.$row->genre_id.']" value="'.$row->genre_id.'" '.$checked.' />
                                </div>
                                <div class="chk_desc align-left">'.$row->genre_name.'</div>
								<div class="genre_txt align-left">
									<input type="text" name="genre_size['.$row->genre_id.']" class="text-input small-input" value="'.$quantity.'" />
								</div>
								<div class="clear"></div>
                             </div>';
						}
					}
				} else { 
					echo "Genre not available";
				}
			}
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		exit;
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
	}
}