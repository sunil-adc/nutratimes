<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AjaxGetPubs extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index($net = "") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($net) != "") { 
			
			$this->db->cache_off();
			$res = $this->db->query("select pubs from ".PUBS." where status = 1 and net_id in (select id from ".PIXELS." where net = '".$net."')");
			echo '<select name="pubid" id="pubid">';
			if ($res->num_rows() > 0) {
				echo '<option value="">Select Option</option>';
				foreach ($res->result() as $val) {
					echo '<option value="'.$val->pubs.'">'.$val->pubs.'</option>';
				}
			} else {
				echo '<option value="0">Not Available</option>';
			}
			echo '</select>';
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}