<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addCombo extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CACHE OFF
		$this->db->cache_off();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PRODUCT;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Super-duper deal";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= "managecombo";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "prod_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['status']					= NULL;
		$data['result_data']['combo_products']			= NULL;
		$data['result_data']['prod_id']					= NULL;
		$data['result_data']['combo_name']				= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		
		
		$val_products = $this->db->query("SELECT DISTINCT prod_id, name, total_stock, is_size, pg.genre_quantity, pg.status, p.check_inventory
								FROM tbl_product p
								LEFT JOIN tbl_product_genre pg ON (p.prod_id = pg.prodid)
								WHERE p.status = 1 AND p.oos = 0 AND p.is_verified = 1 AND p.contest != 1");
		if ($val_products != false) {
			foreach ($val_products->result() as $val_products) {
				if ($val_products->total_stock > 0 && $val_products->is_size != 1) {
					$data['all_products'][$val_products->prod_id] = $val_products;
				}
				elseif ($val_products->check_inventory == '1' && $val_products->genre_quantity > 1) {
					$data['all_products'][$val_products->prod_id] = $val_products;
				}
				elseif($val_products->check_inventory == '0'){
					$data['all_products'][$val_products->prod_id] = $val_products;
				}
			}
		}
		
		if ($mode == 'edit' && is_numeric($id)) {
			
			$val = $this->db_function->get_single_row($tablename, '	prod_id,
																	name,
																	is_combo,
																	combo_product,
																	seourl
																	', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-combo',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function removecombo($prod_id = 0) {
		if (isset($prod_id) && $prod_id > 0 && is_numeric($prod_id)) {
			$this->db->query("update ".PRODUCT." set is_combo = 0, combo_product = '' where prod_id = ".$prod_id);
			$this->db->cache_delete(CMS_FOLDER_NAME, 'addcombo');
			$this->db->cache_delete(CMS_FOLDER_NAME, 'managecombo');
			$this->session->set_flashdata('success', 'Combo Deleted successfully');
			redirect(FULL_CMS_URL."/managecombo/#mtab");
			exit;
		} else {
			$this->session->set_flashdata('error', 'Please try after sometime.');
			redirect(FULL_CMS_URL."/managecombo/#mtab");
			exit;
		}
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('prod_id', 'Product Id', 'required');
		$this->form_validation->set_rules('combo_product', 'Combo Product', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				
				if (($key = array_search($_POST['prod_id'], $_POST['combo_product'])) !== false) {
    				unset($_POST['combo_product'][$key]);
				}
				
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['prod_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where('prod_id', $_POST['prod_id']);
					
					// SET THE DATA
					$data = array(
									'combo_product' => implode(",", $_POST['combo_product']),
									'is_combo' => '1',
									'status' => 1,
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(PRODUCT,$data);
					$this->session->set_flashdata('success', 'Combo edited successfully');
				
				} else if ( trim ($_POST['mode']) == 'add') { 			
					
					// SET THE WHERE CLAUSE
					$this->db->where('prod_id', $_POST['prod_id']);
					
					// SET THE DATA
					$data = array(
									'combo_product' => implode(",", $_POST['combo_product']),
									'is_combo' => '1',
									'status' => 1,
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(PRODUCT,$data);
					
					$prod_id = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Combo added successfully');
				}
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
