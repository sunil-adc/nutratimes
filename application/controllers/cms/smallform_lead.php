<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class smallform_lead extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		//$this->otherdb = $this->load->database('nutra_small_form', TRUE); 
		// CHECK ADMIN IS LOGIN - START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);*/
		// CHECK ADMIN IS LOGIN - END				
	}
	public function index($perpage = 20, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 20;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 20, $btn_search = NULL, $from_date, $to_date, $net = NULL, $status = NULL, $offset = 0) {
		
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['net'] 	    = "";
		$data['status'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= "tbl_prelander_user";
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "SmallForm Leads -  http://amazepromos.com/nutratimes";
		$data['add_page_title']	= "SmallForm Leads";
		$data['page_name']	 	= "SmallForm Leads";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "smallform_lead";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['download_form']	= FULL_CMS_URL."/".$cur_controller.'/download';
		$data['seach_form']	    = $full_path;
		$data['net']			= "";
		$data['phone']			= "";
		$today_date 			= date('Y-m-d'); 

		unset($data['results']);
		
		if($this->input->post('btn_search') != "" || $btn_search != "" && $this->input->post('from_date') != "" || $from_date != "" && $this->input->post('to_date') != "" || $to_date != "") {
			$total_lead = $total_conversion = $condition = 0;
			
				$data['phone'] 		    = $this->input->post('phone');
				$data['net'] 		    = ($net != "" )         ? $net          : $this->input->post('net');
				$data['status'] 	    = ($status != "" )      ? $status       : $this->input->post('status');
				$data['from_date']	    = ($from_date!= "")     ? $from_date    : $this->input->post("from_date");				
				$data['to_date']	    = ($to_date!= "")       ? $to_date      : $this->input->post("to_date");
				$data['btn_search']	    = ($btn_search!= "")    ? $btn_search   : $this->input->post("btn_search");						
				
				
				$whr = ' 1 = 1 and ';

				if(trim($data['status']) != '' && $data['status'] != "0" && trim($data['net']) != '' && $data['net'] != "0"){

					$whr .= ' status = "'.$data['status'].'" and utm_source = "'.$data['net'].'" and ';
					$condition = 3;
					$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['net']."/".$data['status'];
				
				}else if (trim($data['net']) != '' && $data['net'] != "0") {
					$whr .= ' utm_source = "'.$data['net'].'" and ';
					$condition = 1;
					$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['net']."/0";

				}else if (trim($data['phone']) != '' && $data['phone'] != "0") {
					$whr .= ' phone = "'.$data['phone'].'" and ';
					$condition = 1;
					//$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['net']."/0";

				}else if(trim($data['status']) != '' && $data['status'] != "0" ){

					$whr .= ' status = '.$data['status'].' and ';
					$condition = 2;
					$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/0/".$data['status'];
				
				}else{
					$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/0/0";
				}
				
				
				$this->replica_db->cache_off();
				
				if ($condition > 0) {
					
					$sel_query = "select 
										name, email, phone, address, utm_source, status, utm_campaign, date_created 
								  from 
										tbl_prelander_user
								  where 
										left(date_created, 10) between '".$data['from_date']."' and '".$data['to_date']."' and 
										".$whr." 1 = 1 order by date_created desc ";
				} else {
					$sel_query = "select 
										name, email, phone, address, utm_source,status,  utm_campaign, date_created 
								  from 
										tbl_prelander_user
								  where 
										left(date_created, 10) between '".$data['from_date']."' and '".$data['to_date']."' order by date_created desc";
				
				}
				
				
		
		}else{

			$sel_query = "select 
								id, name, email, phone, address, status, utm_source, utm_campaign , date_created
						  from 
								tbl_prelander_user
						  where 
								left(date_created, 10) between '".$today_date."' and '".$today_date."' order by date_created desc ";

			$query_string = $perpage."/GO/".$today_date."/".$today_date."/0/0";						

		}


		$full_path .= $query_string;

		// PAGINATION PARAMETER VALUES START
		if($this->uri->segment(10) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 10;
		}
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		$data['total_rows']         = $config['total_rows'];
		// PAGINATION PARAMETER VALUES END 
		
		
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;

		// GET THE DATA FROM PAGINATION
		$data["total_rows"] = $config['total_rows'];
		$data["results"] = $this->db_function->get_data($sql);
		$data["links"] = $this->pagination->create_links();
		$data['sel_query'] = $sel_query;

		//$net_query = $this->otherdb->query("select utm_source from tbl_utm_source where status = 1 "); 
 
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/smallform-lead',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}

	public function update_lead_status($lead_id = "" , $status= ""){


		if($lead_id != "" && $status != ""){

			if($this->db->query("Update tbl_prelander_user set status =". $status." where id = ".$lead_id )){
				echo "done";
			}else{
				echo "error";
			}

		}else{
			echo "error1";
		}
		exit;

	}



	public function download(){

		$arr_all = all_arrays(); 
			
		if($this->input->post('sel_query') != ""){

			$download_query = $this->db->query($this->input->post('sel_query'));

			if($download_query->num_rows() > 0){

				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=smallform_download.xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			?>		
				<table border="1">
			        <thead>
			        <tr>
			        <th width="5%">S no</th>
			        <th width="10%">Name</th>
			        <th width="10%">Phone</th>
			        <th width="10%">City</th>
			        <th width="10%">State</th>
			        <th width="10%">Utm source</th>
			        <th width="10%">status</th>
			        <th width="10%">Date</th>
			      </tr>
			        </thead>
		        <tbody>
            <?php
      
          			
          		$arr_status = array("1"=> "New Lead", "2"=>"Follow Up", "3"=> "Reject", "4"=>"Order Created", "5"=> "Reviewed");		
	            $srlno = 1;
	            foreach ($download_query->result() as $k ) {
	              ?>
	                 <tr valign="middle">
	                 <td><?php echo $srlno ?></td>
	                 <td><?php echo $k->name ?></td>
	                 <td><?php echo $k->phone ?></td>
	                 <td><?php echo $k->city ?></td>
	                 <td><?php echo $arr_all['ARR_STATE'][$k->state] ?></td>
	                 <td><?php echo $k->utm_source ?></td>
	                 <td><?php echo $arr_status[$k->status] ?></td>
	                 <td><?php echo $k->date_created ?></td>
	                 </tr>
	                  <?php
	          	 $srlno++;
	           
	           } 
             ?>
        	</tbody>
        	</table>	

		   <?php 
			}

		}else{
			echo "NO Page Found";
		}	

	}
}