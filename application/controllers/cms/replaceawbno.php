<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class replaceawbno extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= BLUDART_CODE;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Replace AWB No";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "replaceawbno";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= 'Edit';
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/replace-awbno',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('tracknums', 'New and Old Tracking is require to process', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				$blank_found = false;
				
				$arr_provider[2] = AWB_BLUEDART;
				$arr_provider[3] = AWB_DTDC;
				$arr_provider[7] = AWB_SEAJIN;
				$arr_provider[5] = AWB_FIRSTFLIGHT;
				$arr_provider[8] = AWB_RED_EXPRESS;
				$arr_provider[9] = AWB_ECOM_EXPRESS;
				
				if (array_key_exists($_POST['provider'], $arr_provider)) {
					$trk_arr = explode("\n",$_POST['tracknums']);
					
					foreach($trk_arr as $key => $val){
						if(trim($val) != "") {
							$validarr = explode("	",$val);
							$order_id = trim($validarr[0]);
							$new_awbno = isset($validarr[1]) ? trim($validarr[1]) : '';
							
							if (trim($new_awbno) != "") {
								$this->db->query("update ".ORDER." set tracking_number = '".str_replace(' ', '', $new_awbno)."' where id = '".$order_id."'");							
							} else {
								$blank_found = true;		
							}
						}
					}					
					
					if($blank_found == true) {
						$this->session->set_flashdata('success', 'System found some tracking number as blank that number is not replaced, Rest of the numbers are updated successfully');
					} else {
						$this->session->set_flashdata('success', 'Tracking Number successfully updated.');
					}
					$this->common_model->delete_order_cache();
					redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
					exit;
				} else {
					$this->session->set_flashdata('error', 'We are not using the provider which you have selected.');
					redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
					exit;	
				}
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
			exit;
		}
	}
}
