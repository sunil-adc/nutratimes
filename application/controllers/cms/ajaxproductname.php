<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AjaxProductName extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index() {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($this->input->post('product_name')) != NULL && trim ($this->input->post('product_name')) != "" && 
			trim ($this->input->post('search_param')) != NULL && trim ($this->input->post('search_param')) != "") { 
			
			$sql_query = false;
			
			$product_name = $this->input->post('product_name');
			$search_param = $this->input->post('search_param');
			
			if($search_param == 'prod_id') {
				if(is_numeric($product_name)) {
					$sql_query = 'select prod_id, name, status from '.PRODUCT.' where lower('.$this->input->post('search_param').') = '.strtolower(urldecode($product_name)).' order by status desc, prod_id desc LIMIT 10';
				}
			} else {
				$sql_query = 'select prod_id, name, status from '.PRODUCT.' where lower('.$this->input->post('search_param').') like "%'.strtolower(urldecode($product_name)).'%" order by status desc, prod_id desc LIMIT 10';
			}
			
			if($sql_query != false) {
				$query = $this->db->query($sql_query);
				if ($query->num_rows() > 0) {
					$p = 1;
					foreach ($query->result() as $row) {
						//if($row->status == 1) {
							$zera_class = ($p % 2 == 0 ) ? 'zebra_class' : '';
							echo "<div class='suggested_entity'><div class='color_blue suggested_entity_link ".$zera_class."' entity-data='".$row->name."' entity-dataid='".$row->prod_id."'>".$row->name."</div></div>";
							$p++;
						/*} else {
							echo "<div class='suggested_entity_error'><font class='color_red'>".$row->name." (It's disable, enable that product first.)</font></div>";
						}*/
					}
				} else {
					echo "<div class='suggested_entity_error'><font class='color_red'>\"".urldecode($product_name)."\"</font> not available, try another keyword.</div>";
				}
			} else {
				echo "<div class='suggested_entity_error'><font class='color_red'>Look at Search by option.</font></div>";
			}
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		exit;
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
	}
	
	public function getsize() {
		if($this->input->post('prodid') && is_numeric($this->input->post('prodid'))) {
			$res_product_genre = $this->db->query("select g.genre_id, g.genre_name from ".PRODUCT_GENRE." pg inner join ".GENRE." g on (pg.genreid = g.genre_id) where pg.prodid = ".$this->input->post('prodid'));
			if($res_product_genre->num_rows() > 0) {
				foreach ($res_product_genre->result() as $var) {
					echo '<option value="'.$var->genre_id.'">'.$var->genre_name.'</option>';
				}
			} else {
				echo '<option value="0">Not Available</option>';
			}
		}
		exit;
	}
}