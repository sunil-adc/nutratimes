<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportAgentWise extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END				
	}

	public function index($perpage = 10, $offset = 0) {
		
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "Agent Wise Report";
		$data['add_page_title']	= "Agent Wise Report";
		$data['page_name']	 	= "Agent Wise Report";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportagent";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		unset($data['results']);
		
		if($this->input->post('btn_search') != "") {
			
			$this->form_validation->set_rules('from_date', 'From Date', 'required');
			$this->form_validation->set_rules('to_date', 'To Date', 'required');
			$this->form_validation->set_rules('cms_user', 'Users', 'required|numeric');
		
			if ($this->form_validation->run() != false) {
				
				$data['from_date'] 	= $this->input->post('from_date');
				$data['to_date'] 	= $this->input->post('to_date');
				$data['cms_user'] 	= $this->input->post('cms_user');
				
				// CREATE Total Leads COUNT
				$total_leads = "select 
									count(1) as total_leads
								from
									".ORDER." o
								where 
									o.qa_user = '".$this->input->post('cms_user')."' and 
									date_format(o.dt_c, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['total_leads'] = $this->db_function->count_record_using_count($total_leads, 'total_leads',  false, true, $this->replica_db, true);
				

				// CREATE Total Pending COUNT
				$call_pending = "select 
									count(1) as call_pending
								from
									".ORDER." o
								where 
									o.caller_status = 1 and
									o.qa_user = '".$this->input->post('cms_user')."' and 
									date_format(o.dt_c, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
								
				$data['call_pending'] = $this->db_function->count_record_using_count($call_pending, 'call_pending',  false, true, $this->replica_db, true);


				// CREATE Total Review COUNT
				$call_review = "select 
									count(1) as call_review
								from
									".ORDER." o
								where 
									o.caller_status = 7 and
									o.qa_user = '".$this->input->post('cms_user')."' and 
									date_format(o.dt_c, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
								
				$data['call_review'] = $this->db_function->count_record_using_count($call_review, 'call_review',  false, true, $this->replica_db, true);

				
				// CREATE Total FollowUp COUNT
				$call_follow = "select 
									count(1) as call_follow
								from
									".ORDER." o
								where 
									o.caller_status = 8 and
									o.qa_user = '".$this->input->post('cms_user')."' and 
									date_format(o.dt_c, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
								
				$data['call_follow'] = $this->db_function->count_record_using_count($call_follow, 'call_follow',  false, true, $this->replica_db, true);


				// CREATE SENT COUNT
				$total_sent = "select 
									count(1) as total_sent
								from
									".ORDER." o
								where 
									o.payment_status = 0 and 
									o.caller_status = 3 and 
									o.payment_mode = 2 and 
									o.qa_user = '".$this->input->post('cms_user')."' and 
									date_format(o.sent_date, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['total_sent'] = $this->db_function->count_record_using_count($total_sent, 'total_sent',  false, true, $this->replica_db, true);
				
				// TOTAL DELIVERED
				$total_delivered = "select 
									count(1) as total_delivered
								from
									".ORDER." o
								where 
									o.caller_status = 3 and 
									o.payment_mode = 2 and 
									o.delivery_status = 2 and 
									o.qa_user = '".$this->input->post('cms_user')."' and 
									left(o.sent_date, 10) between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['total_delivered'] = $this->db_function->count_record_using_count($total_delivered, 'total_delivered',  false, true, $this->replica_db, true);
				
				// TOTAL RECEIVED
				$total_recieved = "select 
									count(1) as total_recieved
								from
									".ORDER." o
								where  
									o.caller_status = 3 and 
									o.payment_mode = 2 and 
									o.delivery_status = 4 and 
									o.qa_user = '".$this->input->post('cms_user')."' and 
									left(o.sent_date, 10) between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['total_recieved'] = $this->db_function->count_record_using_count($total_recieved, 'total_recieved',  false, true, $this->replica_db, true);
				
				// TOTAL PENDING
				$cod_delivery_pending = "select 
									count(1) as total_pending
								from
									".ORDER." o
								where 
									o.payment_status != 1 and 
									o.caller_status = 3 and 
									o.payment_mode = 2 and 
									(o.delivery_status = 0 or o.delivery_status = 1) and 
									o.qa_user = '".$this->input->post('cms_user')."' and 
									left(o.sent_date, 10) between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['cod_delivery_pending'] = $this->db_function->count_record_using_count($cod_delivery_pending, 'total_pending',  false, true, $this->replica_db, true);
				
				// TOTAL COUNT PRODUCT
				$cntprd = " select 
								count(1) as cntprd
							from
								".ORDER." o 
								LEFT JOIN ".USER_PRODUCTS." up on (o.id = up.order_id) 
							where 
								up.contest != 1 and 
								o.payment_status = 0 and 
								o.caller_status = 3 and 
								o.payment_mode = 2 and 
								o.delivery_status = 2 and 
								o.qa_user = '".$this->input->post('cms_user')."' and 
								o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['cntprd'] = $this->db_function->count_record_using_count($cntprd, 'cntprd',  false, true, $this->replica_db, true);
				
				// TOTAL Quick Invoice
				$quick_invoice = " select 
								count(1) as quick_invoice
							from
								".ORDER." o 
								LEFT JOIN ".USER_PRODUCTS." up on (o.id = up.order_id) 
							where 
								o.offer_percentage != 0 and
								o.payment_status = 1 and 
								o.caller_status = 3 and 
								o.payment_mode = 1 and 
								o.qa_user = '".$this->input->post('cms_user')."' and 
								o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['quick_invoice'] = $this->db_function->count_record_using_count($quick_invoice, 'quick_invoice',  false, true, $this->replica_db, true);
				

				// TOTAL COUNT PRODUCT
				$sum_val = " select 
								sum(up.cod_price + up.shipping) as sum_val
							from
								".ORDER." o 
								LEFT JOIN ".USER_PRODUCTS." up on (o.id = up.order_id) 
							where 
								up.contest != 1 and 
								o.caller_status = 3 and 
								o.payment_mode = 2 and 
								o.delivery_status = 2 and 
								o.qa_user = '".$this->input->post('cms_user')."' and 
								o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'";
									
				$data['sum_val'] = $this->db_function->count_record_using_count($sum_val, 'sum_val',  false, true, $this->replica_db, true);
				
				$data['total_cost'] = ($data['cntprd'] * PRODUCT_COST) + 
									  ($data['total_delivered'] * SHIPMENT_COST_DELIVERED) + 
									  ($data['total_recieved'] * RETURN_COST) + 
									  (1 * MANPOWER_COST);
									  
				$data['total_revenue'] 			= $data['sum_val'];
				$data['total_margin'] 			= $data['total_revenue'] - $data['total_cost'];
				$data['delivery_percentage'] 	= ($data['total_sent'] > 0) ? ($data['total_delivered'] * 100) / $data['total_sent'] : 0;
				$data['cms_user_selected']		= $this->db_function->get_single_value(ADMIN, 'email', "id=".$this->input->post('cms_user'),false,true);
				$data['bgcolor'] 				= (strpos ("123".$data['total_margin'], "-") > 0) ? "rgb(255, 181, 181)" : '';
				$data['results']				= 1;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(FULL_CMS_URL.'/'.$data['cur_controller'].'/');
				exit;
			}
		}
		 
		$cmsuser_where['status'] = 1;
		$cmsuser_where['adm_role_id'] = 3;
		$data['cms_user'] = $this->common_model->create_combo("cms_user", "cms_user", ADMIN, $cmsuser_where, "email", "id", $this->input->post('cms_user'), "javascript:void(0);", "3"); 
		
		// LOAD ALL REQUIRE VIEWS
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/report-agentwise',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}