<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class searchDownload extends CI_Controller {
	
	protected $data;
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		
		if(!array_admin_file_download_role($this->session->userdata('admin_role_id')) &&   
		   !array_excel_file_download_role($this->session->userdata('admin_role_id'))) {
			@session_start();
			$_SESSION['permission_warning'] = 'You are not permitted to view that page, Consider this message as a warning !!';
			redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);
		}
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index () {

	  if(isset($_POST['select_fields']) && isset($_POST['table_fields']) != "" && isset($_POST['where_fields'])) {
		if(trim($_POST['select_fields']) != "" && trim($_POST['table_fields']) != "" && trim($_POST['where_fields'])) {
			
			if(array_admin_user_view($this->session->userdata('admin_role_id'))) {
				$res_admin_users = $this->replica_db->query("select id, username, email from ".ADMIN);
				if($res_admin_users->num_rows() > 0) {
					foreach ($res_admin_users->result() as $val_admin_users) {
						$array_val_admin_users[$val_admin_users->id][] = $val_admin_users;
					}
				}
			}
			
			$_POST['full_sql'] = " SELECT
										".stripslashes($_POST['select_fields']).", 
										o.payment_mode,
										o.provider,
										o.cms_user,
										o.clkid,
										o.zoneid,
										o.qa_user,
										o.state,
										o.ip,
										o.tracking_number,
										o.address,
										o.city,
										o.pincode,
										o.sent_date 
								  FROM
										".stripslashes($_POST['table_fields'])."
								  WHERE 
										".stripslashes($_POST['where_fields']) ;
		
			$sql_order_id = "select o.id as order_id, o.pincode  FROM ".stripslashes($_POST['table_fields'])." WHERE ".stripslashes($_POST['where_fields']);
			$this->replica_db->cache_off();
			$res_order_id = $this->replica_db->query($sql_order_id);
			if(!$res_order_id) {
				echo '<br>Order Id : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
			} else {
				if($res_order_id->num_rows() > 0) {
					$all_order_id = $all_pincode = "";
					foreach($res_order_id->result() as $val) {
						$all_pincode .= "'".$val->pincode."',";
						$all_order_id .= $val->order_id.",";
					} 
					$all_pincode = trim($all_pincode, ',');
					$all_order_id = trim($all_order_id, ",");
				}
			}
			
			if ($all_order_id != "") {
				$this->replica_db->cache_off();	
				$order_products = "
							 SELECT 
								p.name, 		up.product, 
								up.cash_price, 	up.size, 
								up.cod_price, 	up.shipping,
								g.genre_name,   up.order_id,
								up.quantity 
							 FROM 
								".USER_PRODUCTS." up left join ".PRODUCT." p on (up.product = p.prod_id)
								left join ".GENRE." g on (up.size = g.genre_id) 
							 WHERE 
								up.order_id in (".$all_order_id.") and up.status = '1'";
								
				$res_order_products = $this->replica_db->query($order_products);
				if(!$res_order_products) {
					echo '<br>Order Products : Error Occured While download !! Try again, this error will come when you try to download huge data';
				} else {
					if($res_order_products->num_rows() > 0) {
						$arr_all_order_products = "";
						foreach($res_order_products->result() as $val_products) {
							$arr_all_order_products[$val_products->order_id][] = $val_products;
						}
					}
				}
				
			}
		}
		
		$this->data['is_address'] = NULL;
		if ( isset($_POST['firstflight']) && trim($_POST['firstflight']) != "") {
				
			$this->data['full_sql'] = $_POST['full_sql']. " and o.provider = 5 ";
			$this->data['filename'] = 'firstflight-'.date('Y-m-d-h-m-s-A').time();
		} else if ( isset($_POST['bluedart']) && trim($_POST['bluedart']) != "") {
			/*if ($all_pincode != "") {
				$res_pincode = $this->replica_db->query("select code, pincode from ".BLUDART_CODE." where pincode in (".$all_pincode.")");
				if(!$res_pincode) {
						echo '<br>Pincode : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
				} else {
					if($res_pincode->num_rows() > 0) {
						$arr_all_order_pincode = "";
						foreach($res_pincode->result() as $val_pincode) {
							$arr_all_order_pincode[$val_pincode->pincode][] = $val_pincode;
						}
					}	
				}
			}*/
				
			$this->data['full_sql'] = $_POST['full_sql']. " and o.provider = 2 ";
			$this->data['filename'] = 'bluedart-'.date('Y-m-d-h-m-s-A').time();
		} else if ( isset($_POST['xpressbee']) && trim($_POST['xpressbee']) != "") {

			
			$this->data['full_sql'] = $_POST['full_sql']. " and o.provider = 1 ";
			$this->data['filename'] = 'xpressbee-'.date('Y-m-d-h-m-s-A').time();
		} else if ( isset($_POST['address']) && trim($_POST['address']) != "") {
			
			$this->data['full_sql'] = $_POST['full_sql'];
			$this->data['filename'] = 'ShipmentAddress-'.date('Y-m-d-h-m-s-A').time();
		} else if ( isset($_POST['excel']) && trim($_POST['excel']) != "") {
			
			$this->data['full_sql'] = $_POST['full_sql'];
			$this->data['filename'] = 'excel-'.date('Y-m-d-h-m-s-A').time();
		}
		
		$this->data['data_download'] = 'No data available bad request captured !!!';
		
		if(isset($this->data['full_sql']) && trim($this->data['full_sql']) != "") {
			
			$all_array = all_arrays();
			
			if( isset($_POST['address']) && trim($_POST['address']) != "") {
				$this->data['select_fields'] 	= $_POST['select_fields'];
				$this->data['table_fields'] 	= $_POST['table_fields'];
				$this->data['where_fields'] 	= $_POST['where_fields'];
				$this->data['is_address'] 		= true;
			} else {
				$this->data['full_sql']." group by o.id";
				$res = $this->replica_db->query($this->data['full_sql']." group by o.id");
				if($res->num_rows() > 0) {
					$this->data['data_download'] = '<table border="1">';
					
					// IF BLUEDART PREPARE DIFFRENT SHEET FOR THAT
					if(isset($_POST['xpressbee']) && trim($_POST['xpressbee']) != "") {
						$this->data['data_download'] .= '<tr>
														<th>ShippingID</th>
														<th>Order Type</th>
														<th>ShippingReferenceNo</th>
														<th>SellerName</th>
														<th>ShipName</th>
														<th>ShipAddress</th>
														<th>ShipCity</th>
														<th>ShipState</th>
														<th>ShipPinCode</th>
														<th>ShipMobileNo</th>
														<th>ProductCategory</th>
														<th>RTOAddress</th>
														<th>InvoiceNumber</th>
														<th>InvoiceDate</th>
														<th>NetPayment</th>
													</tr>';
					}else if(isset($_POST['bluedart']) && trim($_POST['bluedart']) != "") {
						$this->data['data_download'] .= '<tr>
														<th>AwbNo</th>
														<th>Type</th>
														<th>Ref No.</th>
														<th>Sender / Store Name</th>
														<th>Attention</th>
														<th>Add1</th>
														<th>City</th>
														<th>State</th>
														<th>pincode</th>
														<th>Tel Number</th>
														<th>Mobile No</th>
														<th>Prod/SKU code</th>
														<th>Contents</th>
														<th>Weight</th>
														<th>Declared Value</th>
														<th>Collectable Value</th>
														<th>Vendor Code</th>
														<th>Shipper Name</th>
														<th>Return Address 1</th>
														<th>Return Address 2</th>
														<th>Return Address 3</th>
														<th>Return Pin</th>
														<th>Length (Cms)</th>
														<th>Breadth (Cms)</th>
														<th>Height (Cms)</th>
														<th>Pieces (Cms)</th>
														<th>Area_customer_code</th>
													</tr>';
					} else if (isset($_POST['ecomexpress']) && trim($_POST['ecomexpress']) != "") {
					    // FOR ECOM EXPRESS PREPARE DIFFRENT SHEET
					   $this->data['data_download'] .= '<tr>
														<th>Air Waybill number</th>
														<th>Order Number</th>
														<th>Product</th>
														<th>Shipper</th>
														<th>Consignee</th>
														<th>Consignee Address1</th>
														<th>Destination City</th>
														<th>Pincode</th>
														<th>State</th>
														<th>Mobile</th>
														<th>Telephone</th>
														<th>Item Description</th>
														<th>Pieces</th>
														<th>Collectable Value</th>
														<th>Declared value</th>
														<th>Actual Weight(g)</th>
														<th>Volumetric Weight(g)</th>
														<th>Length(cms)</th>
														<th>Breadth(cms)</th>
														<th>Height(cms)</th>
													</tr>';
					} else if (isset($_POST['firstflight']) && trim($_POST['firstflight']) != "") {
					    // FOR FIRST FLIGHT PREPARE DIFFRENT SHEET
					   $this->data['data_download'] .= '<tr>
														<th>Sl No.</th>
														<th>AwbNo</th>
														<th>suborderno</th>
														<th>Merchant</th>
														<th>Name</th>
														<th>Address</th>
														<th>Address 2</th>
														<th>City</th>
														<th>State</th>
														<th>Zip</th>
														<th>teleno</th>
														<th>Mobile</th>
														<th>teleno</th>
														<th>total_amt</th>
														<th>Mode</th>
														<th>Courier</th>
														<th>Collectable</th>
														<th>Weight</th>
														<th>Courier Cd</th>
														<th>Product Cd</th>
														<th>Item_Name</th>
													</tr>';
					} else if (isset($_POST['redexpress']) && trim($_POST['redexpress']) != "") {
					    // FOR RED EXPRESS PREPARE DIFFRENT SHEET
					   $this->data['data_download'] .= '<tr>
														<th>Sl No.</th>
														<th>Temp AWB No. /  Ref No.</th>
														<th>Order Number/ Invoice No</th>
														<th>COD/PP Shipment</th>
														<th>COD VALUE</th>
														<th>Consignee NAME </th>
														<th>CITY</th>
														<th>PINCODE</th>
														<th>WEIGHT</th>
														<th>PCS</th>
														<th>GOODS DESCRIPTION</th>
														<th>ADDRESS</th>
														<th>AREA</th>
														<th>MOBILE NO</th>
													</tr>';
					} else {
						// FOR OTHERS PREPARE DIFFRENT SHEET
					    $this->data['data_download'] .= '<tr>
														<th>Sl No.</th>
														<th>AwbNo</th>
														<th>Ref No.</th>';
													
						if ( isset($_POST['excel']) && trim($_POST['excel']) != "" && array_constant_check($this->session->userdata('admin_role_id'), SEARCH_ORDER_NETWORK)) {
							$this->data['data_download'] .= '<th>net</th>
															 <th>pubid</th>
															 <th>Click Id</th>';								
						}
						
						if ( isset($_POST['excel']) && trim($_POST['excel']) != "") {
							$this->data['data_download'] .= '<th>Order Id Pg</th>
															 <th>User Id</th>';								
						}	
				
						if ( isset($_POST['excel']) && trim($_POST['excel']) != "" && array_admin_user_view($this->session->userdata('admin_role_id'))) {
							$this->data['data_download'] .= '<th>QA User</th>
															 <th>COD User</th>';
						}
						
						if ( (isset($_POST['excel']) && trim($_POST['excel']) != "" && 
							 array_admin_marketing_bnr($this->session->userdata('admin_role_id'))) || 
							 array_constant_check($this->session->userdata('admin_role_id'), SEARCH_ORDER_NETWORK)) {
							$this->data['data_download'] .= '<th>BNR</th>
															 <th>Zone Id</th>
															 <th>Ip Address</th>';
						}
			
					   $this->data['data_download'] .= '<th>Merchant</th>
														<th>Name</th>
														<th>Email</th>
														<th>Address</th>
														<th>Address 2</th>
														<th>City</th>
														<th>State</th>
														<th>Zip</th>
														<th>Mobile</th>
														<th>Total Amount</th>
														<th>Mode</th>
														<th>Date Placed</th>
														<th>Courier</th>
														<th>Collectable</th>
														<th>Weight</th>
														<th>Courier Cd</th>
														<th>Product_Cd</th>
														<th>ItemName</th>
														<th>Comment</th>
														<th>Sent Date</th>
													</tr>';
					}
					
					$i = 1;
					foreach ($res->result() as $val) {

						$total_price = $total_price_is_zero = $total_product_qty = 0;
						$product_name = $area_customer_code = $bluedart_vendor_code = "";
						$pincode_specific_code = "";
						
						
						/*******************FOR BLUEDART PREPAID CODE *****************************/
						if ( isset($_POST['bluedart']) && trim($_POST['bluedart']) != "" && $all_pincode != "" && 
							is_array($arr_all_order_pincode) && count($arr_all_order_pincode) > 0) {
							if (isset($arr_all_order_pincode[$val->pincode]) && is_array($arr_all_order_pincode[$val->pincode])) {
								foreach ($arr_all_order_pincode[$val->pincode] as $ppk => $ppv) {
									$pincode_specific_code = "<td>".$ppv->code."</td>";
								}
							}
						}
						/******************FOR BLUEDART PREPAID CODE **************************/
			
						$payment_mode = ($val->payment_mode) ? $all_array['ARR_PAYMENT'][$val->payment_mode] : 'N/A'; 
						$provider = ($val->provider) ? $all_array['ARR_PROVIDER'][$val->provider] : 'N/A'; 
						
						if (isset ($all_array['ARR_STATE'][$val->state])) {
							$state = ($val->state) ? $all_array['ARR_STATE'][$val->state] : 'N/A'; 
						} else {
							$state = 'N/A'; 
						}
						
						if ( isset($_POST['bluedart']) && trim($_POST['bluedart'])) { 
							if($val->payment_mode == '2') {
								$bluedart_vendor_code 	= "<td>120864</td>";
								$area_customer_code 	= "<td>BLR120864</td>";
							} else {
								$area_customer_code = $bluedart_vendor_code = "<td></td>";
							}
						}
						
						if (isset($arr_all_order_products[$val->id]) && is_array($arr_all_order_products[$val->id]) && 
							count($arr_all_order_products[$val->id]) > 0) {
							$arr_order_shipping = array();
							foreach ($arr_all_order_products[$val->id] as $key_product => $val_product) {
								
								$genre_name = (isset($val_product->genre_name)) ? $val_product->genre_name : 'N/A';
								$product_name .= $val_product->name."(Qty: ".$val_product->quantity.", Size : ".$genre_name."), ";
								$total_product_qty += (isset($val_product->quantity) && trim($val_product->quantity) > 0) ? $val_product->quantity : 0;
								
								if($val->payment_mode == '1') {
									$total_price += ($val_product->cash_price * $val_product->quantity);
									$total_price_is_zero += 0;
									$arr_order_shipping[] = $val_product->shipping;
								} else if($val->payment_mode == '2') {
									$total_price += ($val_product->cod_price * $val_product->quantity);
									$total_price_is_zero += ($val_product->cod_price * $val_product->quantity);
								}
							}
							if($val->payment_mode == '1') {
								$total_price += max($arr_order_shipping);
								$total_price_is_zero += 0;
							}
						}
						
						// IF BLUEDART PREPARE DIFFRENT SHEET FOR THAT
						if(isset($_POST['xpressbee']) && trim($_POST['xpressbee']) != "") {
							$this->data['data_download'] .= "<tr>
																<td>".$val->tracking_number."</td>
																<td>".$payment_mode."</td>
																<td>".$val->id."</td>
																<td>".SITE_NAME."</td>
																<td>".$val->name."</td>
																<td>".$val->address."</td>
																<td>".$val->city."</td>
																<td>".$state."</td>
																<td>".$val->pincode."</td>
																<td>".$val->mobile."</td>
																<td>".trim($product_name, ', ')."</td>
																<td>".SITE_ADDR_1."</td>
																<td>".$val->id."</td>
																<td>".$val->dt_c."</td>
																<td>".$total_price."</td>
																 <td></td>
																 <td></td>
																 <td></td>
																 <td></td>"
																 .$area_customer_code."
															</tr>";
						} else if(isset($_POST['bluedart']) && trim($_POST['bluedart']) != "") {
							$this->data['data_download'] .= "<tr>
																<td>".$val->tracking_number."</td>
																<td>".$payment_mode."</td>
																<td>".$val->id."</td>
																<td>".SITE_NAME."</td>
																<td>".$val->name."</td>
																<td>".$val->address."</td>
																<td>".$val->city."</td>
																<td>".$state."</td>
																<td>".$val->pincode."</td>
																<td></td>
																<td>".$val->mobile."</td>
																<td></td>
																<td>".trim($product_name, ', ')."</td>
																<td></td>
																<td>".$total_price."</td>
																<td>".$total_price."</td>
																".$bluedart_vendor_code.
																"<td>".SITE_NAME."</td>
																 <td>".SITE_ADDR_1."</td>
																 <td>".SITE_ADDR_1."</td>
																 <td>".SITE_ADDR_1."</td>
																 <td>".RETURN_PINCODE."</td>
																 <td></td>
																 <td></td>
																 <td></td>
																 <td></td>"
																 .$area_customer_code."
															</tr>";
						} else if ( isset($_POST['ecomexpress']) && trim($_POST['ecomexpress']) != "") {
							
							/******************************* FOR ECOM EXPRESS ****************************************/
							$this->data['data_download'] .= "<tr>
																<td>".$val->tracking_number."</td>
																<td>".$val->id."</td>
																<td>".trim($product_name, ', ')."</td>
																<td>".$provider."</td>
																<td>".$val->name."</td>
																<td>".$val->address."</td>
																<td>".$val->city."</td>
																<td>".$val->pincode."</td>
																<td>".$state."</td>
																<td>".$val->mobile."</td>
																<td>".$val->mobile."</td>
																<td>".trim($product_name, ', ')."</td>
																<td>".$total_product_qty."</td>
																<td>".$total_price_is_zero."</td>
																<td>".$total_price."</td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>";
							/******************************* FOR ECOM EXPRESS ****************************************/
						
						} else if ( isset($_POST['firstflight']) && trim($_POST['firstflight']) != "") {
							
							/******************************* FOR FIRST FLIGHT ****************************************/
							$this->data['data_download'] .= "<tr>
																<td>".$i."</td>
																<td>".$val->tracking_number."</td>
																<td>".$val->id."</td>
																<td>SEAJIN</td>
																<td>".$val->name."</td>
																<td>".$val->address."</td>
																<td></td>
																<td>".$val->city."</td>
																<td>".$state."</td>
																<td>".$val->pincode."</td>
																<td></td>
																<td>".$val->mobile."</td>
																<td></td>
																<td>".$total_price."</td>
																<td>".$payment_mode."</td>
																<td>".$provider."</td>
																<td>".$total_price."</td>
																<td></td>
																<td></td>
																<td></td>
																<td>".trim($product_name, ', ')."</td>
															</tr>";
							/******************************* FOR FIRST FLIGHT ****************************************/
						
						} else if ( isset($_POST['redexpress']) && trim($_POST['redexpress']) != "") {
							
							/******************************* FOR RED EXPRESS ****************************************/
							$this->data['data_download'] .= "<tr>
																<td>".$i."</td>
																<td>".$val->tracking_number."</td>
																<td>".$val->id."</td>
																<td>COD</td>
																<td>".$total_price."</td>
																<td>".$val->name."</td>
																<td>".$val->city."</td>
																<td>".$val->pincode."</td>
																<td></td>
																<td>".$total_product_qty."</td>
																<td>".trim($product_name, ', ')."</td>
																<td>".$val->address."</td>
																<td>".$state."</td>
																<td>".$val->mobile."</td>
															</tr>";
							/******************************* FOR FIRST FLIGHT ****************************************/
						
						} else {
							$this->data['data_download'] .= "<tr>
																<td>".$i."</td>
																<td>".$val->tracking_number."</td>
																<td>".$val->id."</td>";
																
							if ( isset($_POST['excel']) && trim($_POST['excel']) != "" && array_constant_check($this->session->userdata('admin_role_id'), SEARCH_ORDER_NETWORK)) {
							   $this->data['data_download'] .= "<td>".$val->net."</td>
							   									<td>".$val->pubid."</td>
																<td>".$val->clkid."</td>";
							} 
							
							if ( isset($_POST['excel']) && trim($_POST['excel']) != "") {
							   $this->data['data_download'] .= "<td>".$val->order_id_pg."</td>
							   									<td>".$val->user_id."</td>";
							} 
							if ( isset($_POST['excel']) && trim($_POST['excel']) != "" && 
								array_admin_user_view($this->session->userdata('admin_role_id'))) {
								
								if($val->qa_user != "" && array_key_exists($val->qa_user, $array_val_admin_users)) {
									$this->data['data_download'] .= '<td>'.$array_val_admin_users[$val->qa_user][0]->email.'</td>';
								} else {
									$this->data['data_download'] .= '<td></td>';
								}
								
								if($val->cms_user != "" && array_key_exists($val->cms_user, $array_val_admin_users)) {
									$this->data['data_download'] .= '<td>'.$array_val_admin_users[$val->cms_user][0]->email.'</td>';
								} else {
									$this->data['data_download'] .= '<td></td>';
								}
							}
							if ( (isset($_POST['excel']) && trim($_POST['excel']) != "" && 
								 array_admin_marketing_bnr($this->session->userdata('admin_role_id'))) || 
								 array_constant_check($this->session->userdata('admin_role_id'), SEARCH_ORDER_NETWORK)) {
								$this->data['data_download'] .= '<td>'.$val->bnr.'</td>
																 <td>'.$val->zoneid.'</td>
																 <td>'.$val->ip.'</td>';
							}
						
							   $this->data['data_download'] .= "<td>Nutratimes</td>
																<td>".$val->name."</td>
																<td>".$val->email."</td>
																<td>".$val->address."</td>
																<td></td>
																<td>".$val->city."</td>
																<td>".$state."</td>
																<td>".$val->pincode."</td>
																<td>".$val->mobile."</td>
																<td>".$total_price."</td>
																<td>".$payment_mode."</td>
																<td>".$val->order_date."</td><br />
																<td>".$provider."</td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td>".trim($product_name, ', ')."</td>
																<td>".$val->comments."</td>
																<td>".$val->sent_date."</td>
															</tr>";
						}						
						$i++;
					}
					$this->data['data_download'] .= '</table>';
				}
				
				if (is_ip_download_allowed() && isset($i) && $i > 0) {
					$email_body = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" 
							style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
							<tr style="background: #6C9;color: #FFF;"><td colspan="2">Data downloaded</td></tr>
							<tr><td>Search Criteria</td><td>'.stripslashes(urldecode($_POST['search_criteria'])).'</td></tr>
							<tr><td>Number of Data</td><td>'.$i.'</td></tr>
							<tr><td>Username</td><td>'.$this->session->userdata('username').'</td></tr>
							<tr><td>Role Name</td><td>'.$this->session->userdata('admin_role').'</td></tr>
							<tr><td>time</td><td>'.date('dS F Y g:i:s A').' (India Time)</td></tr>
							<tr><td>IP Address</td><td>'.$_SERVER['REMOTE_ADDR'].' (Proxy Possibility)</td></tr>
							</table>';
					//$this->svaiza_email->data_download('noreply@'.SITE_NAME.'com', SITE_NAME.' Tracker', EMAIL_DOWNLOAD_EMAIL, $i.' - Data downloaded', $email_body);
				}
			}
		}
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/search-download', $this->data);
	  } else {
	  		$this->session->set_flashdata('success', 'Some temporary problem occure try again');
	  		redirect(FULL_CMS_URL."/searchorders");
	  }
	}
}
