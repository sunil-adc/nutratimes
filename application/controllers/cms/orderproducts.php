<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orderProducts extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");
		// CHECK ADMIN IS LOGIN END
	}
	
	public function index($id = NULL) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= USER_PRODUCTS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		$cur_controller = strtolower(__CLASS__);
		
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		$data['menu'] = $menu;
		$data['page_title'] = "Order Products";
		$data['page_name'] = $data['page_title'];
		$data['manage_page_title'] = "Order Products";
		$data['manage_page'] = $cur_controller;
		$data['add_file'] = $cur_controller;
		$data['form_submit'] = CMS_FOLDER_NAME."/".$cur_controller."/".strtolower(__FUNCTION__).'/';	
		$data['menunum'] = 3;
		$data['leftmenu'] = 1;
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		
		
		if ($this->input->post('submit') != "") {
			
			$cur_url =  $this->input->post('cur_url');
			
			// AGAIN SET RULE FOR FORM VALIDATION 
			$this->form_validation->set_rules('product_name', 'Product Name', 'required');
			$this->form_validation->set_rules('quantity', 'Quantity', 'required');
			if ($this->form_validation->run() != false) {
				
				// AGAIN SET RULE FOR FORM VALIDATION 
				$this->form_validation->set_rules('pid', 'Product Id', 'required');
				$this->form_validation->set_rules('ord_id', 'Order Id', 'required');
				if ($this->form_validation->run() != false) {
						
					$val_product = $this->db_function->get_single_row(PRODUCT, "mrp, sale_price, contest, free_prod, cod_availability, cod_price, shipping", "prod_id=".$this->input->post('pid'));
					$val_ord = $this->db_function->get_single_row(ORDER, "user", "offer_percentage", "id=".$this->input->post('ord_id'), false, true);
					
					if($val_product != false){
						$arr_ins['free_prod'] 		= (isset($val_product['free_prod']) && $val_product['free_prod'] != '') ? $val_product['free_prod'] : '';
						$arr_ins['cod_price'] 		= $val_product['cod_price'];
						$arr_ins['cash_price'] 		= $val_product['sale_price'];
						$arr_ins['shipping'] 		= $val_product['shipping'];
						$arr_ins['contest'] 		= $val_product['contest'];
					}
					
					if($val_ord != false) {
						$arr_ins['session_user_id']	= $val_ord['user'];
					}
		
					
					$arr_ins['status']			= 1;
					$arr_ins['order_id']		= $this->input->post('ord_id');
					$arr_ins['product']			= $this->input->post('pid');
					$arr_ins['size']			= $this->input->post('product_size');
					$arr_ins['quantity']		= $this->input->post('quantity');
					
					// ADD IN LOG
					$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Product added by cms user(Prod Id : '.$this->input->post('pid').', Qty : '.$this->input->post('quantity').')', '0', $this->input->post('ord_id'));
			
					$this->db->insert(USER_PRODUCTS, $arr_ins);
					$this->db->cache_delete(CMS_FOLDER_NAME, $this->input->post('manage_page'));
					$this->session->set_flashdata('success', 'Product added successfully in order');
				} else {
					$this->session->set_flashdata('error', 'Product You selected is not available or order id mismatch');
				}
			} else {
				$this->session->set_flashdata('error', '* Denotes require fields');
			}
			redirect($cur_url."/#addproduct");
			exit;
		}
		
		
		$data['result_data']= NULL;
		
		$val_user = $this->db->query('SELECT 
											u.name,				u.email,			u.mobile,
											u.address,			u.city,				u.state,
											u.pincode,			u.dnd_status,		u.points
									  FROM 
									  		'.USERS.' u 
											INNER JOIN '.ORDER.' o ON (u.user_id = o.user)
									  WHERE
									  		o.id = '.$id);
		$data['user_data'] = $val_user;
		
		// TURN OFF THE CACHE FOR SINGLE QUERY
		$this->db->cache_off();
		$val = $this->db->query('SELECT
									up.id as ord_prod_id,	up.order_id, o.offer_percentage,
									up.product,				up.shipping, o.caller_status,			
									up.quantity, 			up.size,
									up.contest, 			up.cod_price,
									up.cash_price,			p.name as product_name,			
									up.datecreated,			up.dateupdated,
									o.user,					o.caller_status,
									up.combo_product,		up.is_combo,
									o.payment_mode,			o.mobile,
									o.name as user_name,	o.email,
								 	o.payment_check_status,	o.payment_mode,
									o.cms_user
								 FROM 
								 	'.$tablename.' up
									LEFT JOIN '.PRODUCT.' p ON (up.product = p.prod_id)
									LEFT JOIN '.ORDER.' o ON (up.order_id = o.id)
									LEFT JOIN '.USERS.' u ON (o.user = u.user_id) 
								 WHERE
								 	o.id = '.$id.' and 
									o.status = 1 and 
									up.status = 1
								 ORDER BY 
								 	up.dateupdated desc, 
									up.datecreated desc');
		
		
        if ($val->num_rows() > 0) {
			foreach ($val->result() as  $usr_val1) {
				$data['result_data1'] = $usr_val1;					
			}
		}

		$data['result_data'] = $val;
		$data['order_id']	 = $id;
		
		// CHECK IF FORM IS SUBMITTED END
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/order-products',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	public function qualify_codlead() {
		
		if ($this->session->userdata('uid') != '') {
			$order_id 		= $_POST['order_id'];
			$payment_mode 	= $_POST['payment_mode'];
			$total_amount	= 0;
			
			$cart_data = $this->db->query("SELECT
											o.pincode, 
											o.user, 
											o.mobile, 
											sum(up.quantity * up.cod_price) as total_amount
										  FROM
											".ORDER." o inner join ".USER_PRODUCTS." up on (up.order_id = o.id)   
										  WHERE
											o.id = ".$order_id." and 
											up.order_id = ".$order_id." and 
											up.status = 1");
			if($cart_data->num_rows() > 0) {
				foreach($cart_data->result() as $row) {
					$total_amount = $row->total_amount;
					$user_pincode = $row->pincode;
					$user_mobile = $row->mobile;
					$user_id = $row->user;
				}
			}
			
			
			if ($user_id != '' && $user_id > 0 && $order_id != '' && $order_id > 0 && $payment_mode != '' && $payment_mode > 0) {
				// UPDATE ORDERS
				$this->db->query("update tbl_orders set caller_status = 2, cms_user = ".$this->session->userdata('uid')." where id = ".$order_id);
				
				// ZIPDIAL by JAYDIP
				$this->general->zipdial_cod_confirm($order_id, $user_mobile, $total_amount, $user_pincode);
						
				// SEND MAILS
				$this->svaiza_email->email_waiting_order_confirmation($user_id, $order_id, $payment_mode);
				
				echo 'done';
			} else {
				echo 'err';
			}
		} else {
			echo 'err';	
		}
		exit;
	}
	
	public function delete ($id = "", $order_id = ""){
		if($id != "" && $order_id != "") {
			
			$data = array(
               'status' => 0
            );
			
			$this->db->where(array('id' => $id, 'order_id' => $order_id));
			$this->db->update(USER_PRODUCTS, $data); 
			
			// ADD IN LOG
			$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Product deleted (Prod Id : '.$id.')', '0', $order_id);
			
			$this->db->cache_delete(CMS_FOLDER_NAME, strtolower(__CLASS__));
			$this->session->set_flashdata('success', 'Product deleted successfully from order !!');
		} else {
			$this->session->set_flashdata('error', 'Error Occure refresh the page and try again');
		}
		redirect(FULL_CMS_URL.'/'.strtolower(__CLASS__).'/index/'.$order_id.'/#addproduct');
	}
	
	public function changePaymentStatus() {
		
		if (trim($_POST['id']) != '' && trim($_POST['id']) > 0 ) {
		
			$this->db->cache_off();
			$status = abs($this->db_function->get_single_value(ORDER, 'payment_check_status', "id=".$_POST['id']) - 1);
			
			$data = array(
               'payment_check_status' => $status
            );
			
			$this->db->where(array ('id' => $_POST['id']));
			$this->db->update(ORDER, $data); 
			
			?><a href="javascript:void(0)" data-siteurl="<?php echo CMS_URL;?>" data-id="<?php echo $_POST['id'];?>" class="change_payment_status">
			<?php echo ($status == '1') ? '<img src="'.S3_URL.'cms/images/icons/tick_circle.png" />&nbsp;&nbsp;' : '<img src="'.S3_URL.'cms/images/icons/exclamation.png" />&nbsp;&nbsp;'; echo $status == 1 ? 'Payment Checked' : 'Payment Not Checked';?></a><?php
			
			// DELETE ALL ORDER RELATED CACHE
			$this->common_model->delete_order_cache();
		} else {
			echo 'Refresh your page and try again';
		}
		exit;
	}
	
	public function update($ord_id="", $prod_id="", $qty="", $price="", $payment_mode="") {
		if ($ord_id != "" && $prod_id != "" && $qty != "" && $price != "" && $payment_mode != "") {
			
			// UPDATE DATE
			$date_upd = date('Y-m-d H:i:s');
			
			// SET DATA IN ARRAY
			$data = array(
							'quantity' 		=> $qty,
							'dateupdated'	=> $date_upd
 						  );
						  
			//SETTING WHERE CLAUSE
			$this->db->where('order_id', $ord_id);
			$this->db->where('id', $prod_id);
    		
			// FINALLY FIRE THE QUERY
			$this->db->update(USER_PRODUCTS, $data);
			
			// ADD IN LOG
			$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Product Modified (Prod Id : '.$prod_id.', Qty : '.$qty.')', '0', $ord_id);
			
			// DELETE ALL ORDER RELATED CACHE
			$this->common_model->delete_order_cache();
			
			$ttl_price = ($price * $qty);
			$upd_details = (strtotime($date_upd) > 0) ? time_diff ($date_upd) : "Not Updated";
			echo '{"price":"'.$price.'","quantity":'.$qty.',"total_price":'.$ttl_price.',"dateupdated":"'.$upd_details.'"}';
		}else {
			echo "refresh";
		}
		exit;
	}


	public function update_offer_percentage(){

		if($_POST['offer_percentage'] != "" && $_POST['order_id'] != "" && $_POST['order_id'] != 0 ){
			
			$offer_percentage = $_POST['offer_percentage'];
			$order_id  = $_POST['order_id'];

			if($this->db->query("update ".ORDER." set offer_percentage=".$offer_percentage." where id =".$order_id." limit 1" )){
				
				$this->offer_per($order_id); 
				
				redirect(FULL_CMS_URL."/orderproducts/index/".$order_id."/#atab"); 					
			}
		}

	}

	public function offer_per($order_id = 0) {

		
		$catid = '';
		$up_id = array();
		$new_cash_price = "";
		$all_array = all_arrays();

		$cart_data = $this->db->query("SELECT 
											o.id , o.offer_percentage, up.id as up_id, up.cash_price, p.sale_price as prod_sale_price, p.cod_price
										FROM
											".ORDER." o left join ".USER_PRODUCTS." up on  (o.id = up.order_id) 
											left join ".PRODUCT." p on (up.product = p.prod_id) 	 
										WHERE
											o.payment_status != 1 and  
											o.id = ".$order_id." and 
											up.status = 1");											

		if($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $v2) {
				
				if( $v2->offer_percentage != 0 ){
					
					$percentage = $all_array['ARR_OFFER_PER'][$v2->offer_percentage] ; 
					$p = explode("%",$percentage);	

					if($v2->up_id !="" && $v2->up_id > 0){

						$new_cash_price = ($v2->cod_price - ($v2->cod_price*$p[0])/100);

						$new_cod_price  = ($v2->cod_price - ($v2->cod_price*$p[0])/100);
						
						if($new_cash_price != "" && $new_cash_price ){
							
							$this->db->query("update ".USER_PRODUCTS." set cash_price = ".$new_cash_price.", cod_price = ".$new_cod_price." where id = ".$v2->up_id." and order_id = ".$v2->id);

							return true;
						}

					}
				}
				
			}

		}

		return false;	
		
	}


	public function send_quick_invoice($ord_id){

		$arr_ins = "";
		$total_final_price = "";

		if($ord_id != ""){


			$cart_data = $this->db->query("SELECT 
												o.id , up.id, o.email, o.name,o.mobile , up.cash_price, o.payment_mode, up.cod_price
											FROM
												".ORDER." o left join ".USER_PRODUCTS." up on  (o.id = up.order_id) 
											WHERE
												o.payment_status != 1 and 
												o.id = ".$ord_id." and 
												up.status = 1");											
			
			if($cart_data->num_rows() > 0) {
				foreach ($cart_data->result() as $val_ord) {
											
					$arr_ins['name']	= $val_ord->name;
					$arr_ins['email']	= $val_ord->email;
					$arr_ins['mobile']	= $val_ord->mobile;  
				 	$total_final_price += $val_ord->cod_price;	  
				}


				//CHANGE PAYMENT MODE COD TO PREPAID				
				$this->db->query("update ".ORDER." set payment_mode = 1 where id = ".$ord_id);

				$arr_ins['amount'] = $total_final_price;
				
				$res = quick_msg($arr_ins);
echo $res;
die();

				if($res != "" ){

					$rcvdString = $res;
					$order_status="";
					$decryptValues=explode('&', $rcvdString);
					$dataSize=sizeof($decryptValues);
					

					for($i = 0; $i < $dataSize; $i++) {
						
						$information=explode('=',$decryptValues[$i]);
						if(strtolower($information[0]) == "status" && $information[1] ==0){

							echo "Invoice sent successfully. Please confirm with User";
						}
			 
					}	

				}

			}
		}

		die();
				
	}

}