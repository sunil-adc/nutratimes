<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changeVerify extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index($tablename = "", $field = "", $cur_controller = "", $value = "", $status_column = "", $divid = "") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($tablename) != "" && trim ($field) != "" && trim ($cur_controller) != "" &&  
			trim ($value) != "" && trim ($status_column) != "" && trim ($divid) != "") { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$this->db->cache_off();
			$this->db->cache_delete(CMS_FOLDER_NAME, $cur_controller);
			$this->db->cache_delete(CMS_FOLDER_NAME, 'addproducts');
			$is_verified = abs($this->db_function->get_single_value($tablename, $status_column, $field."=".$value) - 1);
			
			// UPDATE STATUS COLUMN			
			$data = array(
               $status_column 	=> 	$is_verified,
			   'verified_by'	=>	($is_verified > 0) ? $this->session->userdata('uid') : 0
            );
			
			$this->db->where(array ($field => $value));
			$this->db->update($tablename, $data); 
			
			if(strtolower($tablename) == 'tbl_product') {
				if($is_verified == '0') {
					$msg = 'Unverified';
				} else {
					$msg = 'Verified';
				}
				// ADD TO LOG TABLE
				$this->common_model->product_logs($value, $this->session->userdata('uid'), $this->session->userdata('admin_role_id'), $msg);	
			}
			
			// SEND THE RESPONCE IN CALLBACK
			?><a href="javascript:change_verifiy('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $field;?>','<?php echo $value;?>','<?php echo $status_column?>','<?php echo $divid?>');"><?php echo ($is_verified == '0') ? "<span class='color_red'>Not Verified</span>" : "<span class='color_green'>Verified</span>"; ?></a><?php
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}