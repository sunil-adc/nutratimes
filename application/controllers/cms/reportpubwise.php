<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportPubWise extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN - END				
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page(NULL, NULL, NULL);
	}
	
	public function page($from_date = NULL, $to_date = NULL, $net_id = NULL, $timezone = 0, $pubid = NULL) {
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		$data['label_from_to_date'] = '';
		
		$pubs_condition = " and 1 = 1";
		
		if (isset ($_POST['from_date']) && $_POST['from_date'] != '') {
			$from_date = $_POST['from_date'];
		}
		if (isset ($_POST['to_date']) && $_POST['to_date'] != '') {
			$to_date = $_POST['to_date'];
		}
		if (isset ($_POST['net']) && $_POST['net'] != '') {
			$net_id = $_POST['net'];
		}
		if (isset ($_POST['timezone']) && $_POST['timezone'] != '') {
			$timezone = $_POST['timezone'];
		}
		if ((isset ($_POST['pubs']) && $_POST['pubs'] != '' && strtolower($_POST['pubs']) != 'all') ||  
			(isset ($pubid) && $pubid != '' && strtolower($pubid) != 'all')) {
			$pubid = $data['current_pub'] = (isset($_POST['pubs']) && $_POST['pubs'] != '') ? $_POST['pubs'] : $pubid;
			$pubs_condition = " and p.id = ".$pubid;
		}
		
		$data['to_date'] = $to_date;
		$data['from_date'] = $from_date;
		$data['timezone'] = $timezone;
		
		$today_time = getdate(time());
				
		if ($from_date == date('Y-m-d') && $to_date == date('Y-m-d') && $timezone != '0') {
		
			$from_date	= date('Y-m-d 00:00:00', strtotime($from_date));	
			$to_date	= date('Y-m-d '.$today_time['hours'].':'.$today_time['minutes'].':'.$today_time['seconds'], strtotime($to_date));
			
			if ($timezone != '') {
				$min_from_date  = date('Y-m-d 00:00:00', strtotime($from_date));
				$min_to_date  	= date('Y-m-d 00:00:00', strtotime($to_date));
				
				$timezone_from_date 	= (strtotime($from_date) + $timezone);
				$timezone_to_date 		= (strtotime($to_date) + $timezone);
				
				if ($timezone_from_date < strtotime($min_from_date) || $timezone_to_date > strtotime($min_to_date)) {
						
					// FIND TODAY SECONDS
					$today_seconds = strtotime(date('Y-m-d H:i:s')) - strtotime(date('Y-m-d 00:00:00'));
					
					if ($today_seconds > abs($timezone)) {
						$add_seconds = $today_seconds - abs($timezone);
					} else {
						$add_seconds = $today_seconds;
					}
					
					$str_from_date 	= strtotime($min_from_date);
					$str_to_date 	= strtotime($min_to_date) + $add_seconds;	
				}
				
				$diff_timedate = $str_to_date - $str_from_date;
				$from_time = strtotime(date('Y-m-d H:i:s')) - $diff_timedate;
					
				$from_date = date('Y-m-d H:i:s', $from_time);
				$to_date = date('Y-m-d H:i:s');
		
				$data['label_from_to_date'] = '<div class="notification information png_bg"><div>Converted time <span class="bg_red">'.date('Y-m-d H:i A', $from_time).'</span> to <span class="bg_red">'.date('Y-m-d H:i A').'</span></div></div>';
				
			}
			$dateFormat = '%Y-%m-%d %H:%s:%i';
		} else {
			$dateFormat = '%Y-%m-%d';
			$from_date 	= $from_date;
			$to_date 	= $to_date;					
		}
		
		$net_where['status'] = 1;
		$net_where['net_id'] = $net_id;
		$data['pub'] = $this->common_model->create_combo("pubs", "pubs", PUBS, $net_where, "pubs", "id", $pubid, "javascript:void(0);", "3");
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "PubWise Report";
		$data['add_page_title']	= "PubWise Report";
		$data['page_name']	 	= "PubWise Report";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportpubwise";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		unset($data['results']);
		
		if ($net_id != NULL && $net_id > 0) {
				
			if(	$from_date != "" && $to_date != "" && $net_id > 0) {
					
					// CACHE OFF
					$this->replica_db->cache_off();
					
					$sel_pp_query = $this->replica_db->query("select id from ".PARENT_PINCODE." where status = '1'");
					if($sel_pp_query->num_rows() > 0) {
						$data['sel_pp'] 	= $sel_pp_query;
					} else {
						$data['sel_pp'] 	= false;
					}
					
					
				
					// FETCH ALL PUBS INSIDE NETWORD
					$sel_query = $this->replica_db->query("select p.id, p.pubs, p.fire_type, p.pincode_type, p.handle_type, p.goal, p.cpl, p.pubs, p.net_id, n.net from ".PUBS." p inner join ".PIXELS." n on (n.id = p.net_id) where p.net_id = ".$net_id.$pubs_condition);
					if($sel_query->num_rows() > 0) {
						$i = 0;
						foreach ($sel_query->result() as $row) {
							
							$lead_to_conversion = '';
							$data['sale'] = $data['leads'] = $data['lead_fired'] = $data['revenue'] = 0;
							$pub_name 					= $row->pubs;
							$pubid 						= $row->id;
							$cpl 						= $row->cpl;
							$net_name 					= $row->net;
							$net_id 					= $row->net_id;
							$data['id'][$i] 			= $pubid;
							$data['net_name'][$i] 		= $row->pubs;
							$data['fire_type'][$i] 		= $row->fire_type;
							$data['pincode_type'][$i] 	= $row->pincode_type;
							$data['handle_type'][$i] 	= $row->handle_type;
							$data['goal'][$i] 			= $row->goal;
		
							// COUNT THE LEADS
							$leads = "	select 
											COUNT(1) AS leads
										from
											".ORDER." o
										where 
											lower(o.pubid) = '".$pub_name."' and 
											lower(o.net) = '".strtolower($net_name)."' and 
											DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$from_date."' and '".$to_date."'";
							
							$data['leads'] = $this->db_function->count_record_using_count($leads, 'leads',  false, true, $this->replica_db, true);
							
							// COUNT THE LEADS FIRED
							$lead_fired = "	select 
											COUNT(1) AS lead_fired
										from
											".ORDER." o
										where 
											o.fire_status = 1 and 
											lower(o.pubid) = '".$pub_name."' and 
											lower(o.net) = '".strtolower($net_name)."' and 
											DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$from_date."' and '".$to_date."'";
												
							$data['lead_fired'] = $this->db_function->count_record_using_count($lead_fired, 'lead_fired',  false, true, $this->replica_db, true);
							
							// COUNT THE LEADS FIRED
							$sale = "	select 
											COUNT(1) AS sale
										from
											".ORDER." o
										where 
											o.payment_status = 1 and 
											lower(o.pubid) = '".$pub_name."' and 
											lower(o.net) = '".strtolower($net_name)."' and 
											DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$from_date."' and '".$to_date."'";
												
							$data['sale'] = $this->db_function->count_record_using_count($sale, 'sale',  false, true, $this->replica_db, true);
							
							// COUNT THE LEADS FIRED
							$revenue = "select 
											SUM(up.cash_price * up.quantity + up.shipping) AS revenue
										from
											".ORDER." o 
											LEFT JOIN ".USER_PRODUCTS." up ON (o.id = up.order_id)
										where 
											up.contest = 1 and 
											o.payment_status = 1 and 
											up.status = 1 and 
											lower(o.pubid) = '".$pub_name."' and 
											lower(o.net) = '".strtolower($net_name)."' and 
											DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$from_date."' and '".$to_date."'";
							
							$data['revenue'] = $this->db_function->count_record_using_count($revenue, 'revenue',  false, true, $this->replica_db, true);
							
							$data['final_revenue'][$i] 		= ($data['revenue'] > 0) ? $data['revenue'] : 0;
							$data['final_spent'][$i]	 	= $data['lead_fired'] * $cpl;
							$data['final_margin'][$i]  		= $data['revenue'] - $data['final_spent'][$i];
							$data['final_pubname'][$i]  	= trim($pub_name);
							$data['final_pubid'][$i]  	= trim($pubid);
							$data['final_sale'][$i] 		= $data['sale'];
							
							if ($data['sale'] > 0) {
								$data['final_cps'][$i] = $data['final_spent'][$i] / $data['final_sale'][$i];
							} else {
								$data['final_cps'][$i] = 0;
							}
							
							if ($data['leads'] > 0) {
								$data['final_lead_to_conversion'][$i] = number_format($data['final_sale'][$i] * 100 / $data['leads'], 2);
							} else {
								$data['final_lead_to_conversion'][$i] = 0;
							}
							$data['final_leads'][$i] = $data['leads'];
							$data['final_lead_fired'][$i] = $data['lead_fired'];
							$i++;
						}
					} else {
						$this->session->set_flashdata('error', 'Publisher is not available for this network.');
						redirect(FULL_CMS_URL.'/reportnetwise/');
						exit;
					}
					
				$data['network']				= $net_id;
				$data['results']				= 1;
					
			}	
		}	
		
		if ($this->input->post('net') != '') {
			$net_current = $this->input->post('net'); 
		} else if (isset($_GET['net']) && $_GET['net'] != '') {
			$net_current = $_GET['net']; 
		} else if (isset($net_id) && $net_id != ''){
			$net_current = $net_id;
		} else {
			$net_current = 0;
		}
		
		$data['net_current'] = $net_current;
		
		if (isset($net_current) && $net_current != '') {
			$net_current = $this->db_function->get_single_value(PIXELS, 'id', "net = '".$net_current."'", false, false);
		}
		
		$net_combo_where['status'] = 1;
		$net_combo_where['is_cpl_cps'] = 1;
		
		$data['net'] = $this->common_model->create_combo("net", "netGetPub", PIXELS, $net_combo_where, "net", "id", (!is_numeric($data['net_current'])) ? $net_current : $data['net_current'], "", "3", "", false, false, 'style="width:10% !important" data-cmsurl="'.FULL_CMS_URL.'"');
	
		$data['net_id'] = $net_id;
	
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/report-pubwise',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}