<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportcitywise extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
		
				
	}
	public function index($perpage = 30, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 30, $btn_search = NULL, $from_date = NULL, $to_date = NULL, $offset = 0) {
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['btn_search'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "Report Citywise";
		$data['add_page_title']	= "Report Citywise";
		$data['page_name']	 	= "Report Citywise";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportcitywise";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		// CHECK FOR SEARCH REQUEST		
		if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['from_date'] 	= ($from_date != "") ? $from_date : $this->input->post("from_date");
			$data['to_date'] 	= ($to_date != "") ? $to_date : $this->input->post("to_date");
			
			// PREPARE QUERY STRING
			$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/";
			
			$full_path .= $query_string;
			
			// CREATE THE SQL QUERY
			$sel_query = "Select 
								count(id) as total_lead, city
							From
								".ORDER."  
							Where  
								DATE_FORMAT(dt_c, '%Y-%m-%d')  between '".$data['from_date']."' and '".$data['to_date']."' 
							group by
							    city 
							ORDER BY
							    count(id) desc";
			

			
			$sel_delivery = "Select 
								count(id) as total_lead, city
							From
								".ORDER."  
							Where  
								DATE_FORMAT(dt_c, '%Y-%m-%d')  between '".$data['from_date']."' and '".$data['to_date']."' 
								and delivery_status = 2  
							group by
							    city 
							ORDER BY
							    count(id) desc limit 20";
			
			$data['delivery_query'] = $this->db->query($sel_delivery);


			//STATEWISE LEAD

			$state_tot_lead = "Select 
								count(id) as total_lead, state
							From
								".ORDER."  
							Where  
								DATE_FORMAT(dt_c, '%Y-%m-%d')  between '".$data['from_date']."' and '".$data['to_date']."' 
							group by
							    state
							ORDER BY
							    count(id) desc";
			
			$data['state_lead_query'] = $this->db->query($state_tot_lead);

			$statewise_delivery = "Select 
								count(id) as total_lead, state
							From
								".ORDER."  
							Where  
								DATE_FORMAT(dt_c, '%Y-%m-%d')  between '".$data['from_date']."' and '".$data['to_date']."' 
								and delivery_status = 2  
							group by
							    state 
							ORDER BY
							    count(id) desc limit 20";
			
			$data['state_delivery_query'] = $this->db->query($statewise_delivery);




			// PAGINATION PARAMETER VALUES START
			if($this->uri->segment(8) == "") {
				$config['uri_segment'] 	= 5;
			} else {
				$config['uri_segment'] 	= 8;
			}
			
			$config['total_rows'] 		= $this->db_function->count_record($sel_query, false, true, $this->replica_db, true);
			$config['per_page'] 		= $perpage;
			$config['base_url'] 		= $full_path;
			$choice 					= $config['total_rows'] / $config["per_page"];
			$config['num_links'] 		= 2;
			$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
			$config['full_tag_close'] 	= '</ul></div>';
			$config['anchor_class'] 	= 'class="btn" ';
			$config['first_tag_open'] 	= '<li class="num_off">';
			$config['first_tag_close'] 	= '</li>';
			$config['last_tag_open'] 	= '<li class="num_off">';
			$config['last_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="num_on">';
			$config['cur_tag_close'] 	= '</li>';
			$config['num_tag_open'] 	= '<li class="num_off">';
			$config['num_tag_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="num_off">';
			$config['prev_tag_close'] 	= '</li>';
			$config['next_tag_open'] 	= '<li class="num_off">';
			$config['next_tag_close'] 	= '</li>';
			$config['prev_link'] 		= 'PREVIOUS';
			$config['next_link'] 		= 'NEXT';
			$config['use_page_numbers'] = FALSE;
			$data['total_rows']			= $config['total_rows'];
			// PAGINATION PARAMETER VALUES END 
			
			// PAGINATION PARAMETER INITIALIZE 
			$this->pagination->initialize($config);
			
			$data['only_sql'] = $sel_query;
			
			// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
			$sql = $sel_query." limit ".$offset.", ".$perpage;
			
			// GET THE DATA FROM PAGINATION
			$data['sql'] = $sql;
			$data["total_rows"] = $config['total_rows'];
			$data["results"] = $this->db_function->get_data($sql, false, $this->replica_db, true);
			$data["links"] = $this->pagination->create_links();
			
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/report-citywise',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}