<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxNetworkUpdate extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function nupdate() {
		if (strtolower($_POST['data']) == 'send') {
			
			$pincode_type 		= $_POST['pincode_type'];
			$search_form		= $_POST['search_form'];
			$handle_type 		= $_POST['handle_type'];
			$fire_type 			= $_POST['fire_type'];
			$goal 				= $_POST['goal'];
			$primary_id 		= $_POST['primary_id'];
			$timezone			= $_POST['timezone'];
			$primary_id_value 	= $_POST['primary_id_value'];
			$from_date 			= $_POST['from_date'];
			$to_date 			= $_POST['to_date'];
			$net_id				= $_POST['net_id'];
		
			if($handle_type != "" && $pincode_type != "" && $goal != "") {
				$data = array(	'handle_type' => $handle_type,
								'fire_type' => $fire_type, 
								'pincode_type' => $pincode_type,
								'goal' => $goal);
				
				$this->db->where("id", $net_id);
				$this->db->update(PIXELS, $data);
				?>
                <style>
                	.notification div{
						padding:10px 10px 0px 10px !important;
					}
                </style>
                <div class="notification success png_bg">
                	<a href="#" class="close">
                    	<img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    	<div class="align-left">Network updated with new details want to refresh report ? 	</div>
                        <div class="align-left">
                        <form action="<?php echo $search_form?>" id="frm_net_report_upd" method="post">
                            <input type="hidden" name="from_date" value="<?php echo $from_date;?>" />
                            <input type="hidden" name="timezone" value="<?php echo $timezone;?>" />
                            <input type="hidden" name="to_date" value="<?php echo $to_date;?>" />
                            <input type="hidden" name="net" value="<?php echo $net_id;?>" />
                            <input type="submit" id="btn_search" name="btn_search" class="align-left color_red_bg" value="Yes" />
                            <input type="button" class="align-left color_yellow_bg" value="Ignore" />
                        </form>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
				<?php
			} else {
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>You can\'t left blank any fields</div></div>';
			}
		}
		exit;
	}
	
	public function pupdate() {
		if (strtolower($_POST['data']) == 'send') {
			
			$pincode_type 		= $_POST['pincode_type'];
			$search_form		= $_POST['search_form'];
			$handle_type 		= $_POST['handle_type'];
			$fire_type 			= $_POST['fire_type'];
			$goal 				= $_POST['goal'];
			$pubname	 		= $_POST['pubname'];
			$primary_id 		= $_POST['primary_id'];
			$primary_id_value 	= $_POST['primary_id_value'];
			$from_date 			= $_POST['from_date'];
			$to_date 			= $_POST['to_date'];
			$timezone			= $_POST['timezone'];
			$net_id				= $_POST['net_id'];
		
			if($handle_type != "" && $pincode_type != "" && $goal != "") {
				$data = array(	'handle_type' => $handle_type,
								'fire_type' => $fire_type, 
								'pincode_type' => $pincode_type,
								'goal' => $goal);
								
				$this->db->where("id", $primary_id_value);
				$this->db->update(PUBS, $data);
				?><div class="notification success png_bg">
                	<a href="#" class="close">
                    	<img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    	<div>Publisher updated with new details want to refresh report ? <a class="color_red_bg" href="<?php echo $search_form.$from_date.'/'.$to_date.'/'.$net_id.'/'.$timezone.'/'.$pubname;?>">Yes</a>
                        <a class="color_yellow_bg" href="javascript:void(0)">Ignore</a>
                    </form>
                    </div>
                </div>
				<?php
			} else {
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>You can\'t left blank any fields</div></div>';
			}
		}
		exit;
	}
}