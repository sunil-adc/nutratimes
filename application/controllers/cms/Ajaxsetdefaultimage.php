<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AjaxSetDefaultImage extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index($prodid = "", $imageid = "") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($prodid) != "" && trim ($imageid) != "") { 
			
			// REMOVE ALL DEFAULT IMAGE FROM CURRENT PRODUCT ID 
			$this->db->where(array('prod_id' => $prodid));
			$this->db->update(PRODUCT_IMAGE, array('isdefault' => 0)); 
			
			// ADD CURRNET IMAGE AS DEFAULT
			$this->db->where(array('prod_image_id' => $imageid, 'prod_id' => $prodid));
			$this->db->update(PRODUCT_IMAGE, array('isdefault' => 1)); 
			
			// DELETE CACHE AFTER ADD OR EDIT 
			$this->db->cache_delete(CMS_FOLDER_NAME, 'manageproducts');
			$this->db->cache_delete(CMS_FOLDER_NAME, 'addproducts');
			
			echo "1";
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}