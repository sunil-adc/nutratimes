<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addPubs extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CACHE OFF
		$this->db->cache_off();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PUBS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Publisher";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= "managepubs";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['pubs'] 					= NULL;
		$data['result_data']['pixels']					= NULL;
		$data['result_data']['handle_type']				= NULL;
		$data['result_data']['cpl']						= NULL;
		$data['result_data']['pincode_type']			= NULL;
		$data['result_data']['goal']					= NULL;
		$data['result_data']['status']					= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	id, 			pubs,			pixels,
																	handle_type,	cpl, 			pincode_type, 	
																	goal,			status,			net_id,
																	net_type,		pixels,			datecreated,	
																	fire_type,		dateupdated', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
		
		$data['pixel_data'] = $this->db->query("select id from ".PARENT_PINCODE." where status = '1'");
		$data['net_data'] = $this->db->query("select id, net from ".PIXELS." where status = '1' order by net asc");
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-pubs',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('net_id', 'Network', 'required');
		$this->form_validation->set_rules('pubs', 'Publisher Name', 'required');
		$this->form_validation->set_rules('pincode_type', 'Pincode Type', 'required');
		$this->form_validation->set_rules('handle_type', 'Handle Type', 'required');
		$this->form_validation->set_rules('cpl', 'CPL', 'required');
		$this->form_validation->set_rules('goal', 'Goal', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("id",$_POST['id']);
					// SET THE DATA
					$data = array(
									'pubs' => $_POST['pubs'],
									'handle_type' => $_POST['handle_type'],
									'cpl' => $_POST['cpl'],
									'pincode_type' => $_POST['pincode_type'],
									'fire_type' => $_POST['fire_type'],
									'goal' => $_POST['goal'],
									'pixels' => $_POST['pixels'],
									'net_id' => $_POST['net_id'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
								
					if ($_POST['fire_type'] == 1 && $_POST['goal'] > 100) {
						$this->session->set_flashdata('error', 'Goal Percentage must be less than or equals to 100.');						
					} else {
						// UPDATE QUERY
						$this->db->update(PUBS,$data);
						$this->session->set_flashdata('success', 'Publisher edited successfully');
					}
					
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'pubs' => $_POST['pubs'],
									'handle_type' => $_POST['handle_type'],
									'cpl' => $_POST['cpl'],
									'pincode_type' => $_POST['pincode_type'],
									'fire_type' => $_POST['fire_type'],
									'goal' => $_POST['goal'],
									'pixels' => $_POST['pixels'],
									'net_id' => $_POST['net_id'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					
					if ($_POST['fire_type'] == 1 && $_POST['goal'] > 100) {
						$this->session->set_flashdata('error', 'Goal Percentage must be less than or equals to 100.');						
					} else {
						// INSERT QUERY
						$this->db->insert(PUBS,$data);
						$this->session->set_flashdata('success', 'Publisher added successfully');
					}
				}
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
