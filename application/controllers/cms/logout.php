<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	public function index(){
		// UNSET ALL SESSION DATA FOR LOGOUT FUNCTION
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('uid');
		$this->session->unset_userdata('admin_role');
		$this->session->unset_userdata('admin_role_id');
		$this->session->unset_userdata('admin_role_details');
		
		redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);
	}
}
