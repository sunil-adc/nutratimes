<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class updateProfile extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN END
	}
	
	public function index() {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ADMIN;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		$cur_controller = strtolower(__CLASS__);
		
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		$data['menu'] = $menu;
		$data['page_title'] = "Update Profile";
		$data['manage_page_title'] = "Update Profile";
		$data['manage_page'] = $cur_controller;
		$data['add_file'] = $cur_controller;
		$data['form_submit'] = CMS_FOLDER_NAME."/".$cur_controller;	
		$data['menunum'] = 3;
		$data['leftmenu'] = 1;
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_dash');
		$this->form_validation->set_rules('fname', 'First Name', 'required|alpha');
		$this->form_validation->set_rules('lname', 'Last Name', 'required|alpha');
		
		// CHECK IF FORM IS SUBMITTED START
		if(isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// CHECK FOR UNIQUE RECORD EMAILS AND USERNAME
				// FOR EMAIL ADDRESS
				$this->db->cache_off();
				$email_query = $this->db->query("select email from ".$tablename." where 
												id != ".$this->session->userdata('uid')." and email = '".$_POST['email']."'");
				if ($email_query->num_rows() > 0){
					$this->session->set_flashdata('error', '"'.$_POST['email'].'" email address is already exists. try another email address');
					redirect(FULL_CMS_URL.'/'.$cur_controller.'/');	
				}
				
				
				// FOR USERNAME 
				$this->db->cache_off();
				$username_query = $this->db->query("select username from ".$tablename." where 
												id != ".$this->session->userdata('uid')." and username = '".$_POST['username']."'");
				
				if ($username_query->num_rows() > 0){
					$this->session->set_flashdata('error', '"'.$_POST['username'].'" username is already exists. try another username');
					redirect(FULL_CMS_URL.'/'.$cur_controller.'/');	
				}
				// CHECK FOR UNIQUE RECORD EMAILS AND USERNAME
				
				$data = array(
				   'email' => $_POST['email'],
				   'username' => $_POST['username'],
				   'fname' => $_POST['fname'],
				   'lname' => $_POST['lname']
				);
						
				$this->db->where('id', $this->session->userdata('uid'));
				$this->db->update($tablename, $data); 
				
				// DELETE THE RELATED CACHE 
				$this->db->cache_delete(CMS_FOLDER_NAME, 'manageadminuser');
				$this->db->cache_delete(CMS_FOLDER_NAME, 'addadminuser');
				$this->db->cache_delete(CMS_FOLDER_NAME, 'updateprofile');
				// DELETE THE RELATED CACHE
				 
				$this->session->set_flashdata('success', 'Your profile Detail has been changed.');
				redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
			}
		}
	
	
		$data['result_data']['usename']		= NULL;
		$data['result_data']['fname']		= NULL;
		$data['result_data']['lname']		= NULL;
		$data['result_data']['email']		= NULL;
		$data['result_data']['fb_url']		= NULL;
		$data['result_data']['twt_url']		= NULL;
		$data['result_data']['last_psw_cng']= NULL;
		$data['result_data']['status']		= NULL;
		$data['result_data']['datecreated']	= NULL;
		$data['result_data']['dateupdated']	= NULL;
		
		
		// TURN OFF THE CACHE FOR SINGLE QUERY
		$this->db->cache_off();
		$val = $this->db_function->get_single_row($tablename, '	username, 		fname, 
																lname, 			email,
																fb_url, 		twt_url,
																last_psw_cng, 	status,
																datecreated, 	dateupdated', 'id='.$this->session->userdata('uid'));
		$data['result_data'] = $val;
		
		// CHECK IF FORM IS SUBMITTED END
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/update-profile',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
