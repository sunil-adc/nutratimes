<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sendUserPassword extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= BRANDS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['manage_page_title']	= "Send User Password";
		$data['page_name']	 		= "Send User Password";
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= strtolower(__CLASS__);
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "user_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/send_password";
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/ckeditor');
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/send-user-password',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function send_password () {
		
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// SET THE WHERE CLAUSE
				$this->db->cache_off();
				$res_user = $this->db->query("select name, user_id, email from ".USERS." where mobile = '".$this->input->post('mobile')."'");
				if($res_user->num_rows() > 0) {
					
					// GET THE DATA FROM DATABASE
					foreach ($res_user->result() as $row) {
						$user_id = $row->user_id;
						$email = $row->email;
						$full_name = $row->name;
					}
					
					// GENERATE RANDOM PASSWORD
					$password = get_random_chracter(8, 15, true, true, false);	
						
					// SET THE ARRAY FOR UPDATION
					$data = array('passwd' => md5($password));
					$this->db->where('user_id', $user_id);
					$this->db->update(USERS, $data);
					
					// SEND THE NEW PASSWORD MAIL
					$this->svaiza_email->email_recover($full_name, $email, $password, $this->input->post('mobile'));
					
					// SET FLASH MESSAGE
					$this->session->set_flashdata('success', 'Password sent at <strong>'.$email.'</strong> email for mobile number <strong><strong>'.$this->input->post('mobile').'</strong></strong>.');
					redirect($_POST['cur_url']);
					exit;	
				} else {
					$this->session->set_flashdata('error', 'Mobile number is not registered till now.');
					redirect($_POST['cur_url']);
					exit;
				}
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
