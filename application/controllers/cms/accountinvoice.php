<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class accountinvoice extends CI_Controller {
	protected $data;
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Account Invoice";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "accountinvoice";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/generate_invoice";
		$data['mode'] 				= 'Edit';
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/account-invoice',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function generate_invoice () {
		$val_orders = array();
		$all_arrays = all_arrays();
		$arr_order_products = $arr_order_products = array();
		$comma_orders = $comma_mobile = "";
					
		$this->form_validation->set_rules('orderids', 'Order Id', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				if (isset($_POST['orderids'])) {
					// GET ORDER IDS
					$val_arr = explode("\n", trim($_POST['orderids']));
					
					foreach ($val_arr as $key => $val){
						if(trim($val) != "") {
							$validarr = explode("	",$val);
							$comma_orders .= '"'.trim($validarr[0]).'",';
							$comma_mobile .= '"'.trim($validarr[1]).'",';
						}
					}
					
					$comma_orders = trim($comma_orders,",");
					$comma_mobile = trim($comma_mobile,",");
						
					if (trim($comma_orders) != "") {
						$this->replica_db->cache_off();
						$res_orders = $this->replica_db->query("SELECT 
															id,					address,			mobile,
															email,				pincode,			name,
															provider,			dt_c,				payment_mode,
															tracking_number,	city,     state,	pincode
														FROM
															".ORDER."
														WHERE 
															id in (".$comma_orders.")");
						if (!$res_orders) {
							echo '<br>Order : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
						} else {
							foreach ($res_orders->result() as $orders_details) {
								$val_orders[$orders_details->id] = $orders_details;
							}
						}
						
						$res_order_details = $this->replica_db->query("SELECT 
																	up.order_id,
																	up.quantity, 
																	up.cash_price, 
																	up.cod_price,
																	up.shipping,
																	up.product,
																	p.name as prod_name,
																	p.name_acc productname
																	
															  FROM 
																	".USER_PRODUCTS." up  
																	INNER JOIN ".PRODUCT." p ON (p.prod_id = up.product) 
															  WHERE 
																	up.product = p.prod_id and
																	up.status = 1 and 
																	up.order_id in (".$comma_orders.")");
						if(!$res_order_details) {
							echo '<br>Order : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
						} else {
							foreach ($res_order_details->result() as $val_orders_details) {
								$arr_order_products[$val_orders_details->order_id][] = $val_orders_details;
							}
						}
					
						// START PREPARING DATA FOR INVOICE
						$this->data['data_download'] = '<table border="1">
														  <tr>
															<th>Voucher Number</th>
															<th>Date</th>
															<th>Voucher Type</th>
															<th>PARTY A/c Name</th>
															<th>Customer Name</th>
															<th>Address 1</th>
															<th>Address 2</th>
															<th>Address 3</th>
															<th>State</th>
															<th>GSTIN</th>
															<th>Mode /Term of Payment</th>
															<th>Dispatch Through</th>
															<th>Destination</th>
															<th>Order No</th>
															<th>Due On</th>
															<th>Item Name</th>
															<th>Tax Rate</th>
															<th>Batch No</th>
															<th>QTY</th>
															<th>UOM</th>
															<th>Rate</th>
															<th>Discount %</th>
															<th>Amount</th>
															<th>Sales Ledger</th>
															<th>Other Charges_1 Amount</th>
															<th>Other Charges_1 Ledger</th>
															<th>Other Charges_2 Amount</th>
															<th>Other Charges_2 Ledger</th>
															<th>SGST Amount</th>
															<th>SGST Ledger</th>
															<th>CGST Amount</th>
															<th>CGST Ledger</th>
															<th>IGST Amount</th>
															<th>IGST Ledger</th>
															<th>Godown</th>
															<th>Narration</th>
														  </tr>';
						// GET IT IN LOOP
						$srl = 1;
						foreach($val_arr as $key => $val){										
							if(trim($val) != "") {
								$validarr = explode("	",$val);
								$refid 	= trim($validarr[0]);
								$mobile = trim($validarr[1]);
								
								if(trim($refid) != "") {
									// GENERATE INVOICE 
									//$invoice_no = $this->assign_invoice_no($refid);
									
									if (isset($arr_order_products[$refid]) && count($arr_order_products[$refid]) > 0) {
										
										foreach ($arr_order_products[$refid] as $key_product => $val_product) {
											

											$shipping  = ($val_orders[$refid]->payment_mode == '2') ? 0 : $val_product->shipping;

											$party_acc = ($val_orders[$refid]->payment_mode == '1') ? 'Infibeam Avenues Limited-CC Avenue' : $all_arrays['ARR_PROVIDER'][$val_orders[$refid]->provider];
											
											// GET THE AMOUNT OF PRODUCT
											$prod_cost = ($val_orders[$refid]->payment_mode == '2') ? $val_product->cod_price : $val_product->cash_price;
											
											// COUNT TOTAL COST FOR THE PRODUCT
											$total_cost = $prod_cost * $val_product->quantity;

											$number_of_bottle = $all_arrays["BOTTLE_ARR"][$val_product->product]; 
											
											/*if ($total_cost > $val_product->min_vat_rate) {
												if($val_orders[$refid]->payment_mode == '2') {
													$vat_price = (($val_product->vat * $total_cost) / (100 + $val_product->vat));
												} else {
													$vat_price = ($total_cost * $val_product->vat) / 100;
												}
											} else {
												$vat_price = 0;
											}*/
											
											$base_price = (($total_cost * $val_product->quantity) /112)*100;
											$igst       = ($total_cost  * $val_product->quantity) - $base_price ;
											$cgst       = round($igst/2, 2);
											$sgst       = round($cgst, 2);
											$gst_details= "";
											
											$final_cost_exclu_tax = ($total_cost - $igst);  

											if($val_orders[$refid]->state == 2){

												$gst_details = "<td>".$sgst." </td>
																<td>Output SGST-6%</td>
																<td>".$cgst." </td>
																<td>Output CGST-6%</td>
																<td> </td>
																<td> </td>"; 

											}else{	

												 $gst_details = "<td> </td>
																 <td></td>
																 <td> </td>
																 <td></td>
																 <td>".round($igst,2)."</td>
																 <td>Output IGST-12%</td>"; 	
											}	

											$date_format = str_replace("/", "-", date("d-m-Y", $val_orders[$refid]->dt_c));

											$this->data['data_download'] .= '<tr>
																				<td>'.$srl.'</td>
																				<td>'.$val_orders[$refid]->dt_c.'</td>
																				<td>Sales Order</td>
																				<td>'.$party_acc.'</td>
																				<td>'.$val_orders[$refid]->name.'</td>
																				<td>'.$val_orders[$refid]->address.'</td>
																				<td>'.$val_orders[$refid]->area.'</td>
																				<td></td>
																				<td>'.$all_arrays["ARR_STATE"][$val_orders[$refid]->state].'</td>
																				<td></td>
																				<td>'.$all_arrays["ARR_PAYMENT"][$val_orders[$refid]->payment_mode].'</td>
																				<td>'.$all_arrays["ARR_PROVIDER"][$val_orders[$refid]->provider].'</td>
																				<td>'.$val_orders[$refid]->city.'</td>
																				<td>'.$val_orders[$refid]->id.'</td>
																				<td>'.$val_orders[$refid]->dt_c.'</td>
																				<td>'.$val_product->prod_name.'</td>
																				<td>12</td>
																				<td> </td>
																				<td> '.$number_of_bottle.' </td>
																				<td>BTL</td>
																				<td>'.round($final_cost_exclu_tax/$number_of_bottle,2).'</td>	
																				<td></td>
																				<td>'.$final_cost_exclu_tax.'</td>
																				<td>Sales-Interstate </td>
																				<td> </td>
																				<td> </td>
																				<td> </td>
																				<td> </td>
																				'.$gst_details.'
																				<td>J P Nagar</td>
																				<td>Being Order Booked </td>
																			  </tr>';
															
											
										}
								}
								}
							}
						$srl++;	
						}
					
						$this->data['data_download'] .= '</table>';
						$this->data['filename'] = 'invoice-'.date('Y-m-d-H-i-s').time();
						
						header("Content-Type:  application/vnd.ms-excel");
						header("Content-Disposition: attachment; filename=".$this->data['filename'].".xls");
						header("Expires: 0");
						header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
						
						echo $this->data['data_download'];
						exit;
					}
				} else {
					$this->session->set_flashdata('error', 'Fields are required which denotes *.');
					redirect($_POST['cur_url']);
					exit;
				}
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		}
	}
	
	function assign_invoice_no($order_id){
		$res_invoice = $this->db->query("SELECT id FROM ".INVOICE_HISTORY." WHERE order_id = '".$order_id."'");
		if ($res_invoice->num_rows() > 0) {
			foreach ($res_invoice->result() as $val_invoice) {
				return $val_invoice->id;
			}
		} else {
			$this->db->insert(INVOICE_HISTORY, array('order_id' => $order_id));
			return $this->db->insert_id();	
		}
	}
}
