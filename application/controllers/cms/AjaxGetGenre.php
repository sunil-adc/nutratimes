<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AjaxGetGenre extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index($catid = "0") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($catid) != "" ) { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$this->db->cache_off();
			$cat_where['parent_cat_id'] = $catid;	
			echo $this->common_model->create_combo("sub_cat_id", "sub_cat_id", CATEGORY, $cat_where, "name", "cat_id", "", "", "2");
		
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}