<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changeFraudStatus extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index($tablename = "", $field = "", $cur_controller = "", $value = "", $status_column = "", $divid = "") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($tablename) != "" && trim ($field) != "" && trim ($cur_controller) != "" && 
			trim ($value) != "" && trim ($status_column) != "" && trim ($divid) != "") { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$this->db->cache_off();
			$this->db->cache_delete(CMS_FOLDER_NAME, $cur_controller);
			
			$val_category = $this->db_function->get_single_row($tablename, "*", $field."=".$value, false, false);
			if($val_category != false) {
				
				$status = abs($val_category[$status_column] - 1);
				
				// UPDATE STATUS COLUMN			
				$this->db->or_where(array ($field => $value));
				$this->db->update($tablename, array ($status_column => $status)); 
				
				
				// UPDATE PRODUCT 
				/*$this->db->or_where(array ('sub_cat_id' => $val_category['cat_id'], 'cat_id' => $val_category['cat_id']));
				$this->db->update(PRODUCT, array ('status' => $status)); */
					
				// SEND THE RESPONCE IN CALLBACK
				?>
                <a href="javascript:change_fraud_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $field;?>','<?php echo $value;?>','<?php echo $status_column?>','<?php echo $divid?>');"><?php echo ($status == '1') ? "<span class='color_green_bg'>Yes</span>" : "<span class='color_red_bg'>No</span>"; ?></a><?php
			
			} else {
				echo "refresh";
			}
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}