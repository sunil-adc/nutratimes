<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manageProductsLogs extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PRODUCT;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_name']	 	= "Products";
		$data['page_title']	 	= "Manage ".$data['page_name'];
		$data['add_page_title']	= "Add ".$data['page_name'];
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "manageproductlogs";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "prod_id";
		$data['seach_form']		= $full_path;
		
		$data['prod_id'] = '';
		$data['results'] = '';
		
		
		if (isset($_POST['btn_search']) && $_POST['btn_search'] != '') {
			$this->db->cache_off();
			$res = $this->db->query("SELECT
									p.name,
									u.username,
									r.role_name,
									l.action,
									l.log_date
							 FROM 
									".PRODUCT_LOGS." l 
									inner join ".PRODUCT." p on (p.prod_id = l.prod_id) 
									inner join ".ADMIN." u on (u.id = l.user_id) 
									inner join ".ADMIN_ROLE." r on (r.adm_role_id = l.role_id) 
							 WHERE
									l.prod_id = ".$_POST['prod_id']."
							 ORDER BY 
							 		l.log_date desc");
			$data['results'] = $res->result();
			$data['prod_id'] = $_POST['prod_id'];
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/manage-products-logs',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}