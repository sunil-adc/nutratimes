<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model(CMS_FOLDER_NAME.'/common_model');
		
		// CHECK ADMIN IS LOGIN START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);*/
		// CHECK ADMIN IS LOGIN END
		
	}
	
	public function index(){
		$menu = $this->common_model->Menu_Array();
		$data['menu'] = $menu;
		$data['cur_controller'] = strtolower(__CLASS__);
		
		
		//$data['total_country'] = $this->db->count_all(COUNTRY);
		//$data['total_product'] = $this->db->count_all(PRODUCT);
		//$data['total_users']   = $this->db->count_all(USERS);
		
		//PRODUCT UPLOAD STATISTICS CODE - START
		//$data['prod_stats'] = $this->common_model->product_upload_statistics($days = 15);
		//PRODUCT UPLOAD STATISTICS CODE - END
		
		//ORDER STATISTICS CODE - START
		//$data['order_stats'] = $this->common_model->order_statistics($days = 15);
		//ORDER STATISTICS CODE - END
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
				
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$data);
		$this->load->view(CMS_FOLDER_NAME.'/dashboard1',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
