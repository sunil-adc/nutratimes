<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class zohocrwal extends CI_Controller {
	
	public $memcache;
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// CHECK ADMIN IS LOGIN START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");*/
		// CHECK ADMIN IS LOGIN END
		
	}
	
	public function get_data(){
		
		/*if($_SERVER['REMOTE_ADDR'] == "182.75.91.58" || $_SERVER['REMOTE_ADDR'] == "106.51.76.101"  || $_SERVER['REMOTE_ADDR'] == "204.141.42.194" || $_SERVER['REMOTE_ADDR'] == "136.143.190.194" || $_SERVER['REMOTE_ADDR'] == "8.39.54.194" ||  $_SERVER['REMOTE_ADDR'] == '8.39.54.194' || $_SERVER['REMOTE_ADDR'] == '204.141.42.44'){
			
		}else{
			echo "You are not Allowed. <a href = '".SITE_URL."'>CLICK HERE </a>";
			die();
		}*/			   
		

		$today_sale = $today_users = $expected_revenue = $lead_to_conversion = $total_conversion = $total_prepaid_sale = $total_cod_sale = 0;
		$json_data = NULL;
				
		$from_date = date('Y-m-d');   
		$to_date   = date('Y-m-d');		       		

		$last_month_startdate =  date('Y-m-d',strtotime('first day of last month'));
		$last_month_lastdate  =  date('Y-m-d',strtotime('last day of last month'));

		$this_month_startdate  = date("Y-m-01");
		$this_month_lastdate   = date("Y-m-d"); 
		
		$yesterday_date = date('d.m.Y',strtotime("-1 days"));

		// COUNT REVENUE DETAILS FOR TODAY
		$revenue_last_month = $this->common_model->count_revenue($this->replica_db, $last_month_startdate, $last_month_lastdate);

		$revenue_this_month = $this->common_model->count_revenue($this->replica_db, $this_month_startdate, $this_month_lastdate);

		$yesterday_revenue = $this->common_model->count_revenue($this->replica_db, $yesterday_date, $yesterday_date);

		// COUNT TODAY LEADS / USERS
		$total_leads = $this->common_model->count_user($this->replica_db, $from_date, $to_date);

		//Delivered Count this MONTH
		$whr_delivery_status = " o.delivery_status = 2 and ";
		$dlvd_this_month = $this->common_model->count_send_order($this->replica_db, $this_month_startdate, $this_month_lastdate, $whr_delivery_status);
		
		 
		$stats_json = '{';
		$stats_json .= '"revenue_last_month":'.$revenue_last_month.',';
		$stats_json .= '"revenue_this_month":'.$revenue_this_month.',';
		$stats_json .= '"revenue_yesterday":'.$yesterday_revenue.',';
		$stats_json .= '"total_leads":'.$total_leads.',';
		$stats_json .= '"dlvrd_this_month":'.$dlvd_this_month;
		$stats_json .= '}';
		
		echo $stats_json;
		
		// EXIT FOR STOP EXECUTION 
		exit;
	}
}
