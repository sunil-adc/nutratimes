<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addGenre extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= GENRE;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Genre";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "managegenre";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "genre_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// 
		if($mode == "edit" && ($id == NULL || $id < 1)) {
			$this->session->set_flashdata('error', 'Don\'t try to cheat the system, one or more require parameters are missing !!');
			redirect(FULL_CMS_URL."/dashboard");
			exit;
		}
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['cat_id']			= NULL;
		$data['result_data']['genre_id']		= NULL;
		$data['result_data']['parent_genre_id']	= NULL;
		$data['result_data']['genre_name']		= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		
		$data['arr_genre'] 						= NULL;
		$data['arr_genre']['cat_id']			= NULL;
		$data['arr_genre']['genre_id']			= NULL;
		$data['arr_genre']['parent_genre_id']	= NULL;
		$data['arr_genre']['genre_name']		= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	cat_id, genre_id, parent_genre_id, genre_name, status, datecreated, dateupdated ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		$cat_where['parent_cat_id'] = '0';
		$cat_where['status'] = '1';
		$data['categories'] = $this->common_model->create_combo("cat_id", "cat_id", CATEGORY, $cat_where, "name", "cat_id", $data['result_data']['cat_id'], "javascript:get_subcat('".FULL_CMS_URL."',this.value,'','ajax_subcat');",'1');
		
		$data['arr_category'] = $this->db->query("SELECT genre_id, parent_genre_id, genre_name from ".$tablename);
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-genre',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('cat_id', 'Category', 'required');
		$this->form_validation->set_rules('genre_name', 'Genre Name', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['genre_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where('genre_id',$_POST['genre_id']);
					// SET THE DATA
					$data = array(
									'cat_id' => ($_POST['cat_id']) ? $_POST['cat_id'] : '',
									'sub_cat_id' => ($_POST['sub_cat_id']) ? $_POST['sub_cat_id'] : '0',
									'parent_genre_id' => ($_POST['parent_genre_id']) ? $_POST['parent_genre_id'] : '',
									'genre_name' => $_POST['genre_name'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(GENRE,$data);
					$this->session->set_flashdata('success', 'Genre edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'cat_id' => ($_POST['cat_id']) ? $_POST['cat_id'] : '',
									'sub_cat_id' => ($_POST['sub_cat_id']) ? $_POST['sub_cat_id'] : '0',
									'parent_genre_id' => ($_POST['parent_genre_id']) ? $_POST['parent_genre_id'] : '',
									'genre_name' => $_POST['genre_name'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(GENRE,$data);
					$this->session->set_flashdata('success', 'Genre added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
