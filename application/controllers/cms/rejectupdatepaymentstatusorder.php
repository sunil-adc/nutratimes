<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rejectUpdatePaymentStatusOrder extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Reject Orders & Update Payment Status";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "rejectupdatepaymentstatusorder";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= 'Edit';
		
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/reject-update-payment-status-order',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('orderids', 'Reject Order Id', 'required');
		$this->form_validation->set_rules('order_action', 'Action', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				$order_action = $this->input->post('order_action');
				$action_arr = explode("\n", $this->input->post('orderids'));
				  foreach ($action_arr AS $key=>$val){
						$id = $order_id = '';
						$order_id = trim($val);
			
					if($order_id != '' && $order_id > 0){
						switch ($order_action) {
							case 'reject_order' :
								// REJECT ORDER START
								$this->db->where('id',$order_id); 
								$data = array(
									'id' => $order_id,
									'status' => '1',
									'caller_status' => 4
								);
								$this->db->update(ORDER,$data);	
								// REJECT ORDER END
								break;
							case 'payment_status_paid' : 
								// PAYMENT STATUS PAID UPDATE START
								$this->db->query("update ".ORDER." set payment_status = 1 where id = ".$order_id);
								// PAYMENT STATUS PAID UPDATE END
								break;
							case 'payment_status_notpaid' : 
								// PAYMENT STATUS NOT PAID UPDATE START
								$this->db->query("update ".ORDER." set payment_status = 0 where id = ".$order_id);
								// PAYMENT STATUS NOT PAID UPDATE END
								break;
						}
						
					}
			
				}  
  
			  	// DELETE ALL ORDER RELATED CACHE
				$this->common_model->delete_order_cache();
				
				$this->session->set_flashdata('success', 'Order Updated to Rejected.');
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
