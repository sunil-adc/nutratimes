<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cmsLogin extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array ('form'));
		$this->load->library('form_validation');
    	$this->load->model(CMS_FOLDER_NAME.'/login_model');
		$this->load->model(CMS_FOLDER_NAME.'/common_model');
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login_Redirect($this->session->userdata('username'), $this->session->userdata('uid'), 
							   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
							   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN END
	}
	   
	public function index() {
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/cmsLogin');
	}
	
	public function otp() {
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/otp');
		
		if (isset($_GET['status']) && $_GET['status'] == 'resend') {
			$email_body = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
						<tr style="background: #6C9;color: #FFF;">
						<td colspan="2">One time password for Unknown ip login</td></tr>
						<tr><td>OTP</td><td>'.$this->session->userdata('temp_otp').'</td></tr>
						<tr><td>Username</td><td>'.$this->session->userdata('temp_username').'</td></tr>
						<tr><td>Role Name</td><td>'.$this->session->userdata('temp_admin_role').'</td></tr>
						<tr><td>time</td><td>'.date('dS F Y g:i:s A').' (India Time)</td></tr>
						<tr><td>IP Address</td><td>'.$_SERVER['REMOTE_ADDR'].' (Proxy Possibility)</td></tr>
						</table>';
			$this->svaiza_email->unknownLogin('noreply@shophunk.com', SITE_NAME.' Admin Login OTP ', EMAIL_DOWNLOAD_EMAIL, SITE_NAME.' Login Tracker', $email_body);
			$this->session->set_flashdata('error', 'OTP resend to administrator.');	
			redirect(CMS_FOLDER_NAME."/cmslogin/otp");
		}
		
		if (isset ($_POST['submit'])) {
			if ($_POST['otp'] == $this->session->userdata('temp_otp')) {
				$this->session->set_userdata('username', $this->session->userdata('temp_username'));
				$this->session->set_userdata('uid', $this->session->userdata('temp_uid'));
				$this->session->set_userdata('admin_role', $this->session->userdata('temp_admin_role'));
				$this->session->set_userdata('admin_role_id', $this->session->userdata('temp_admin_role_id'));
				$this->session->set_userdata('admin_role_details', $this->session->userdata('temp_admin_role_details'));
				$this->session->set_userdata('login_time', date('Y-m-d H:i:s'));
				$this->session->set_userdata('login_ip', $_SERVER['REMOTE_ADDR']);
				
				$email_body = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" 
						style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
						<tr style="background: #6C9;color: #FFF;"><td colspan="2">Unknown IP Login</td></tr>
						<tr><td>Username</td><td>'.$this->session->userdata('username').'</td></tr>
						<tr><td>Role Name</td><td>'.$this->session->userdata('admin_role').'</td></tr>
						<tr><td>time</td><td>'.date('dS F Y g:i:s A').' (India Time)</td></tr>
						<tr><td>IP Address</td><td>'.$_SERVER['REMOTE_ADDR'].' (Proxy Possibility)</td></tr>
						</table>';
				$this->svaiza_email->unknownLogin('noreply@shophunk.com', SITE_NAME.' Login Tracker', EMAIL_DOWNLOAD_EMAIL, SITE_NAME.' Login Tracker', $email_body);
				
				$this->session->unset_userdata('temp_username');
				$this->session->unset_userdata('temp_uid');
				$this->session->unset_userdata('temp_admin_role');
				$this->session->unset_userdata('temp_admin_role_id');
				$this->session->unset_userdata('temp_admin_role_details');
				$this->session->unset_userdata('temp_otp');
				
				redirect(CMS_FOLDER_NAME."/dashboard");
			} else {
				$this->session->set_flashdata('error', 'OTP not matched.');	
				redirect(CMS_FOLDER_NAME."/cmslogin/otp");
			}
		}
	}
	
	 public function validatelogin(){
		$logindata = $_POST;
		
		// SET RULES FOR VALIDATION
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		// SET RULES FOR VALIDATION
		
		
		if ( $this->form_validation->run() ) {
			echo $validatelogin = $this->login_model->validatelogin($logindata);
			if($validatelogin == 1) {
				if($this->session->userdata('uid') > 0 && trim($this->session->userdata('uid')) != "") {
					
					redirect(CMS_FOLDER_NAME."/dashboard");
				} else if ($this->session->userdata('temp_uid') > 0 && trim($this->session->userdata('temp_uid')) != ""){
					$email_body = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
								<tr style="background: #6C9;color: #FFF;">
								<td colspan="2">One time password for Unknown ip login</td></tr>
								<tr><td>OTP</td><td>'.$this->session->userdata('temp_otp').'</td></tr>
								<tr><td>Username</td><td>'.$this->session->userdata('temp_username').'</td></tr>
								<tr><td>Role Name</td><td>'.$this->session->userdata('temp_admin_role').'</td></tr>
								<tr><td>time</td><td>'.date('dS F Y g:i:s A').' (India Time)</td></tr>
								<tr><td>IP Address</td><td>'.$_SERVER['REMOTE_ADDR'].' (Proxy Possibility)</td></tr>
								</table>';
					$this->svaiza_email->unknownLogin('noreply@shophunk.com', SITE_NAME.' Admin Login OTP ', EMAIL_DOWNLOAD_EMAIL, SITE_NAME.' Login Tracker', $email_body);
					redirect(CMS_FOLDER_NAME."/cmslogin/otp");
				} else {
					$this->session->set_flashdata('error', 'Username Password invalid, Please try again.');
					redirect(CMS_FOLDER_NAME."/".strtolower(__CLASS__));
				}
			} else if ($validatelogin == 2) {
				// FOR USER BLOCK THROW ERROR
				$this->session->set_flashdata('error', 'You are blocked, Please contact Super Admin.');
				redirect(CMS_FOLDER_NAME."/cmslogin");				
			
			} else if ($validatelogin == 3) {
				// FOR DEPARTMENT BLOCK THROW ERROR
				$this->session->set_flashdata('error', 'Your department is blocked.');
				redirect(CMS_FOLDER_NAME."/".strtolower(__CLASS__));
			
			} else {	
				// FOR INVALID LOGIN THROW ERROR
				$this->session->set_flashdata('error', 'Username Password invalid, Please try again.');
				redirect(CMS_FOLDER_NAME."/".strtolower(__CLASS__));
			}
		}else{
			$this->session->set_flashdata('error', 'Please enter username password to login.');
			redirect(CMS_FOLDER_NAME."/".strtolower(__CLASS__));
		}
	}
}