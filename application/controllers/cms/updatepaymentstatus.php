<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class updatePaymentStatus extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");
		// CHECK ADMIN IS LOGIN - END
		
		if (!array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) && !array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
			@session_start();
			$_SESSION['permission_warning'] = 'You are not permitted to view that page, Consider this message as a warning !!';
			redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);
			exit;
		}
	}
	
	public function index ( $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		if( $id == "" || !is_numeric($id)) {
			$this->session->set_flashdata('error', 'One or more require parameter is not set, please try again');
			redirect(FULL_CMS_URL."/searchorders/index");
			exit;
		}
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Update Payment Status";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "updatepaymentstatus";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/update_credits";
		
		// TURN OFF THE CACHE FOR SINGLE QUERY
		$this->db->cache_off();
		$val = $this->db_function->get_single_row($tablename, '	id,
																name, 		email,		payment_check_status,
																mobile,		payment_status', 'id='.$id);
		$data['result_data'] = $val;
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');
		$this->load->view(CMS_FOLDER_NAME.'/update-payment-status',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function update_credits() {
		
		$this->form_validation->set_rules('payment_status', 'Payment Status', 'required');
		$this->form_validation->set_rules('payment_check_status', 'Payment Check Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
			
				if ($this->input->post('id') > 0){
					$this->db->query("UPDATE ".ORDER." SET PAYMENT_STATUS = ".$this->input->post('payment_status').", payment_check_status = ".$this->input->post('payment_check_status')." WHERE ID = ".$this->input->post('id'));
					
					// NOTE THE LOGS
					$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Order payment status has been changed from CMS', 0, $this->input->post('id'));
					
					// DELETE ALL ORDER RELATED CACHE
					$this->common_model->delete_order_cache();
				
					// DISPLAY THE FLASH MESSAGE AFTER SUCCESSFULL UPDATION
					$this->session->set_flashdata('success', 'Order Payment Status has been updated.');
					redirect($_POST['cur_url']."/#mtab");
					exit;
				} else {
					$this->session->set_flashdata('error', 'Something went wrong please try again');
					redirect($_POST['cur_url']);	
				}
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
