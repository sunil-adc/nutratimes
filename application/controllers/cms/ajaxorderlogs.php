<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxOrderLogs extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index() {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($this->input->post('ordid')) != "" && $this->input->post('ordid') > 0 && is_numeric($this->input->post('ordid'))) { 
			
			$res = $this->db->query("select * from ".ORDER_LOGS." where order_id = ".$this->input->post('ordid')." order by log_date desc");
			if($res->num_rows() > 0) {
				echo '<div class="color_blue">Order Logs</div>';
				echo '<div class="order-detailed-stats scroll">';
				foreach ($res->result() as $val) {
					$action = $val->action;
					if($val->user_id > 0) {
						$cms_user = $this->db_function->get_single_value(ADMIN, 'username', "id=".$val->user_id);
						$action .= '(CMS User : '.$cms_user.')';
					}
					if($val->role_id > 0) {
						$cms_role = $this->db_function->get_single_value(ADMIN_ROLE, 'role_name', "adm_role_id=".$val->role_id);
						$action .= '(Role : '.$cms_role.')';
					}
					?>
                    <div class="order-stats">
						<div class="order-action"><?php echo $action;?></div>
                        <div class="order-date"><?php echo date('dS-M-Y h:i:s A', strtotime($val->log_date));?></div>
                   	</div>
                    <?php
				}
				echo '</div>'; 
			} else {
				?>Logs not available for this order<?php
			}
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "Please refresh your page and try again";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}