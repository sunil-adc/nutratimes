<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class assignAwbNo extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Assign AWB No.";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "assignawbno";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= 'Edit';
		
		$res_usage = $this->db->query("
						  select 'xpress', count(awb_xpressbee_id) total_xpress from ".AWB_XPRESSBEE." where status = 1 union all 
						  select 'red_express', count(awb_red_id) total_red from ".AWB_RED_EXPRESS."  where status = 1 union all 
						  select 'bluedart', count(awb_bd_id) total_bluedart from ".AWB_BLUEDART."  where status = 1 union all 
						  select 'dtdc', count(awb_dtdc_id) total_dtdc from ".AWB_DTDC."  where status = 1 union all 
						  select 'firstflight', count(awb_ff_id) total_ff from ".AWB_FIRSTFLIGHT."  where status = 1 union all 
						  select 'seajin', count(awb_seajin_id) total_seajin from ".AWB_SEAJIN." where status = 1 ");
		
		if ($res_usage->num_rows() > 0) {
				
			foreach ($res_usage->result() as $val_count) {
				if ($val_count->xpress == 'xpress') {
					$data['xpress'] = $val_count->total_xpress;
				} else if ($val_count->ecom == 'red_express') {
					$data['total_red'] = $val_count->total_ecom;
				} else if ($val_count->ecom == 'bluedart') {
					$data['total_bluedart'] = $val_count->total_ecom;
				} else if ($val_count->ecom == 'dtdc') {
					$data['total_dtdc'] = $val_count->total_ecom;
				} else if ($val_count->ecom == 'firstflight') {
					$data['total_ff'] = $val_count->total_ecom;
				} else if ($val_count->ecom == 'seajin') {
					$data['total_seajin'] = $val_count->total_ecom;
				}
			}
		} else {
			$data['xpress'] = 0;
			$data['total_red'] = 0;
			$data['total_bluedart'] = 0;
			$data['total_dtdc'] = 0;
			$data['total_ff'] = 0;
			$data['total_seajin'] = 0;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');
		$this->load->view(CMS_FOLDER_NAME.'/assign-awb-no',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('provider', 'Provider', 'required');
		$this->form_validation->set_rules('payment_mode', 'Payment Mode', 'required');
		$this->form_validation->set_rules('sent_date', 'Sent Date', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// SET VARIABLES 
				$payment_mode 		= $_REQUEST['payment_mode'];
				$provider 			= $_REQUEST['provider'];
				$trk_arr 			= explode("\n",$_REQUEST['tracknums']);
				$tracking_number	= NULL;
				
				foreach ($trk_arr as $key => $val) {
					$id = trim($val);
					if ($id > 0) {
						if ($payment_mode == 3){
							$this->session->set_flashdata('error', 'CBD is not available now!! please contact developers.');
						} else if ($payment_mode == 1 || $payment_mode == 2) {
							if ($provider == 1) {
								
								// ASSIGN XPRESSBEE AWB
								$tracking_number = $this->common_model->assign_awbno(AWB_XPRESSBEE, 'awb_no', $payment_mode, 'awb_xpressbee_id', $id);
								
							} else if ($provider == 2) {
								// ASSIGN BLUEDART AWB
								$tracking_number = $this->common_model->assign_awbno(AWB_BLUEDART, 'awb_no', $payment_mode, 'awb_bd_id', $id);
							} else if ($provider == 3) {
								// ASSIGN INDIAPOST AWB
								$tracking_number = $id ; //$this->common_model->assign_awbno(AWB_DTDC, 'awb_no', $payment_mode, 'awb_dtdc_id', $id);
							} else if ($provider == 4) {
								// ASSIGN DTDC AWB
								$tracking_number = $id ; //$this->common_model->assign_awbno(AWB_DTDC, 'awb_no', $payment_mode, 'awb_dtdc_id', $id);
							} else if ($provider == 5) {
								// ASSIGN SELF AWB
								$tracking_number = $id ; //$this->common_model->assign_awbno(AWB_FIRSTFLIGHT, 'awb_no', $payment_mode, 'awb_ff_id', $id);
							} else {
								$validarr = explode(" ",$val);
								$id = trim($validarr[0]);
								$tracking_number = trim($validarr[1]);
							}
						} else {
							$this->session->set_flashdata('error', 'Payment mode is not available.');
						}
					}
					
					// INSUFFICIENT AWB NOs
					if ($tracking_number == false) {
						$this->session->set_flashdata('error', 'Purchase new AWB No all are used.');
						redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
						exit;
					}
					
					// UPDATE THE ORDER DETAILS IN ORDER TABLE AND INSERT TRACKING NUMBER
					if ($id > 0 && $tracking_number) {
						
						$this->db->where('id', $id); 
						$data = array();
						$data['tracking_number'] = $tracking_number;
						$data['provider'] = $_REQUEST['provider'];
						
						if ($payment_mode == 2 || $payment_mode == 1){
							$data['caller_status']   = 3;
							$data['delivery_status'] = 1;
							$data['sent_date'] = $_REQUEST['sent_date'];
							
							
							// SEND MAIL TO USER
							//$this->svaiza_email->dispatch_order($id, $tracking_number, $payment_mode);
					
						}
						
						$this->db->update(ORDER,$data);
			
						// SEND MAIL TO USER
						if ($payment_mode != 1){
						   // TILL NOW MAIL IS NOT SENT
						   //get_contest_products ($id);
						}
					}
				}
				
				// DELETE ORDER CACHE
				$this->common_model->delete_order_cache();
				
				$this->session->set_flashdata('success', 'AWB Number assigned !!');
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
