<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class update_expressbee extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');

		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function get_order_list($oid= "") {
		
		$cond = "";
		
		$today_date = date('Y-m-d');
		$yesterday_date = date('Y-m-d',strtotime("-1 days"));
		 
		if($oid != ""){
			$cond = " o.id = ".$oid;
		}else{
			$cond = " Date_Format(o.sent_date ,'%Y-%m-%d') between '".$yesterday_date."' and '".$today_date."'";
		}
		

		$o_query = $this->db->query("Select 
								    o.id as oid,	o.name as cname, 		o.email,		o.mobile, 		
								    o.address,      o.offer_percentage,	o.status,	o.dt_u, 
									o.city,			o.area,			o.state,		o.alternate_phone,
									o.pincode,		o.payment_mode,	o.caller_status, o.delivery_status,
									o.provider,		o.tracking_number, o.sent_date,	 o.dt_c,
									up.product,		up.quantity,	
									up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
									up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
									p.name,			p.model_no,		p.seourl,       p.mrp,		
									p.cod_availability, p.is_combo,		
									p.combo_product,  up.is_combo as user_combo,	
									up.combo_product as user_combo_prouct
							FROM
									".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
									left join ".PRODUCT." p on (up.product = p.prod_id)
							 WHERE
									up.product = p.prod_id and 
									up.status = 1 and
									o.caller_status = 3 and
									o.provider = 1 and 
									".$cond
									) ;			 
		
		if($o_query->num_rows() > 0){

				foreach($o_query->result() as $k ){
					
					echo "order id-".$k->oid."<br>";
					
					$token_number = $this->premanifest($k);
					
					if($token_number != "" ) {

						$data = array(
								'token_number'   => $token_number,
								'order_id' 	     => $k->oid,
								'tracking_number'=> $k->tracking_number,
								'date_created'    => date('Y-m-d H:i:s')
							);
						// INSERT QUERY
						//$this->db->insert(XPRESSBEE_TMP,$data);
						//$xpres_id 	= $this->db->insert_id();
							
					}

				}
		
		}else{
			echo "No Details Found, Please check caller status and Provider";
		}
					

	}


	public function premanifest($payload_arr = ""){
		$all_arrays = all_arrays();
		$url =  'http://xbclientapi.xbees.in/POSTShipmentService.svc/AddManifestDetails';
		$ch = curl_init( $url );
		
		$adress =  rtrim(preg_replace('/[\s\"]/', ' ', $payload_arr->address));

		$payload = '{
						"XBkey": "uTGBTGdcGkifRdj939mtorSA",
						"VersionNumber": "V5",
						"ManifestDetails": {
								"ManifestID": "12320160729",
								"OrderType": "'.($payload_arr->payment_mode == 1 ? "Prepaid" : "COD").'",
								"OrderNo": "'.$payload_arr->oid.'",
								"SubOrderNo": "'.$payload_arr->oid.'",
								"PaymentStatus": "'.($payload_arr->payment_mode == 1 ? "Prepaid" : "COD").'",
								"PickupVendor": 	 "'.SITE_NAME.'",
								"PickVendorPhoneNo": "'.office_number.'",
								"PickVendorAddress": "'.office_address.'",
								"PickVendorCity": 	 "'.office_city.'",
								"PickVendorState": 	 "'.office_state.'",
								"PickVendorPinCode": "'.office_pincode.'",
								"CustomerName": "'.$payload_arr->cname.'",
								 "CustomerCity": "'.$payload_arr->city.'",
								"CustomerState": "'.$all_arrays['ARR_STATE'][$payload_arr->state].'",
								"ZipCode": "'.$payload_arr->pincode.'",
								"CustomerAddressDetails": [{
											"Type": "Primary",
											"Address": "'.$adress.'"
											},
											{
											"Type": "Secondary",
											"Address": "'.$adress.'"
											}],
								"CustomerMobileNumberDetails": [{
											"Type": "Primary",
											"MobileNo": "'.$payload_arr->mobile.'"
											},
											{
											"Type": "Secondary",
											"MobileNo": "'.$payload_arr->mobile.'"
											}],
								"RTOName": 	  "'.SITE_NAME.'",
								"RTOMobileNo":"'.office_number.'",
								"RTOAddress": "'.office_address.'",
								"RTOToCity":  "'.office_city.'",
								"RTOToState": "'.office_state.'",
								"RTOPinCode": "'.office_pincode.'",
								"BillableWeight": 1,
								"VolWeight": 1,
								"PhyWeight": 1,
								"ShipLength": 0,
								"ShipWidth": 0,
								"ShipHeight": 0,
								"AirWayBillNO": "'.$payload_arr->tracking_number.'",
								"ServiceType": "SD",
								"Quantity": 1,
								"PickupVendorCode": "M34",
								"PickupType": "Vendor",
								"IsDGShipmentType": "0",
								"IsOpenDelivery": "0",
								"CollectibleAmount": "'.($payload_arr->payment_mode == 2 ? $payload_arr->cod_price : "0").'",
								"DeclaredValue": "'.($payload_arr->payment_mode == 2 ? $payload_arr->cod_price : $payload_arr->cash_price).'",
								"GSTMultiSellerInfo": [{
											"ProductDesc": "Weight Loss Medicine",
											"ProductCategory": "Herbal Product",
											"SellerName": "Nutratimes",
											"SellerAddress": "'.office_address.'",
											"SupplySellerStatePlace": "'.office_state.'",
											"SellerPincode": "'.office_pincode.'",
											"InvoiceNumber": "",
											"InvoiceDate": "'.$payload_arr->dt_c.'",
											"EWayBillSrNumber": "",
											"EBNExpiryDate":"",
											"BuyerGSTRegNumber": "",
											"IsSellerRegUnderGST": "Yes",
											"SellerGSTRegNumber": "'.GST.'",
											"HSNCode": "'.HSNCODE.'",
											"TaxableValue": 0,
											"SGSTAmount": 0,
											"CGSTAmount": 0,
											"IGSTAmount": 0,
											"Discount": 0,
											"GSTTaxRateCGSTN": 0.0,
											"GSTTaxRateSGSTN": 0,
											"GSTTAXRateIGSTN": 0,
											"GSTTaxTotal": 0.0
											}]
								}
					}';
		
		
		# Setup request to send json via POST.
		//$payload = json_encode( array( "XBkey" => "uTGBTGdcGkifRdj939mtorSA", "AWBNo" => $tracking_number) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		# Return response instead of printing.	
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		# Send request.
		$result = curl_exec($ch);
		
		if (curl_error($ch)) {
		    echo $error_msg = curl_error($ch);
		}

		$json_arr = json_decode($result);
		
		# Print response.

		echo "<pre>";
		print_r($json_arr);
		if(strtolower($json_arr->AddManifestDetails[0]->ReturnMessage) == "successful"){
			
			return $json_arr->AddManifestDetails[0]->TokenNumber;
			
		}
		
	}

	
	public function update_delivery_status($o_id){
		
		$trackid_arr = array();
		$status_delv = "";

		$order_id = " and id = ".$o_id;

		$sql_q = $this->db->query("Select 
										  id, tracking_number, payment_mode, dt_c, dt_u 
									From
										  ".ORDER."
									Where
										   caller_status in (2,3) and 
										   delivery_status in (1,5)". $order_id." limit 1 ");
							
		if($sql_q->num_rows() > 0){
			foreach($sql_q->result() as $k){
				if($k->tracking_number != ""){	
					
					$status_delv = $this->get_delivery_status($k->tracking_number, "one");
					
					if(strtolower($status_delv) == "it"){
						$status_val = 5;

					}else if(strtolower($status_delv) == "dlvd"){
						$status_val = 2;
					}

					if( $k->tracking_number != "" && $k->id !=""  ){

						if($status_val == 2 ){

							if($k->payment_mode == 2 && $k->payment_status != 2 ){

								$this->db->query("Update tbl_orders set delivery_status = ".$status_val.", payment_status = 1 where id = ".$k->id);	

								echo "COD order delivered by Xpressbeed Successful";	
							}else if($k->payment_mode == 1 ){

								$this->db->query("Update tbl_orders set delivery_status = ".$status_val." where id = ".$k->id);	

								echo "Prepaid order delivered by Xpressbeed Successful";
							}

							

						}else if($status_val == 5 ){
							$this->db->query("Update tbl_orders set delivery_status = ".$status_val." where id = ".$k->id);	

							echo "Order in Transit Tracking updated";

						}
						
					}					

				}
			}
		}
	
	}


	public function get_all_delivery_status(){
		
		$trackid_arr = array();
		$status_delv = "";

		$twodays_backdate = date('Y-m-d',strtotime("-3 days"));

		$sql_q = $this->db->query("Select 
										  id, tracking_number, payment_mode, qa_user, dt_c, dt_u 
									From
										  ".ORDER."
									Where
										   caller_status in (3) and 
										   provider = 1 and
										   delivery_status in (1,5) and
										   Date_Format(sent_date ,'%Y-%m-%d') between '2018-07-01' AND '".$twodays_backdate."'" );
							

		if($sql_q->num_rows() > 0){
			foreach($sql_q->result() as $k){
				if($k->tracking_number != ""){	
					
					$status_delv = $this->get_delivery_status($k->tracking_number,"all");
					

					if( $k->tracking_number != "" && $k->id !=""  ){

						if(is_object($status_delv)){									
							
							$data = array(
								'status_code'    => $status_delv->Status,
								'comment'		 => $status_delv->Comment,
								'order_id' 	     => $k->id,
								'telecaller'     => $k->qa_user,
								'tracking_number'=> $k->tracking_number,
								'date'			 => date('Y-m-d', strtotime($status_delv->StatusDate)),
								'date_created'   => date('Y-m-d H:i:s')
							);
							
							// INSERT QUERY
							
							$this->db->insert(XPRESSBEE_TMP,$data);
							$xpres_id 	= $this->db->insert_id();

						
							//echo "Delivery Status for order id-".$k->id."- ".$status_delv->Status." <br>";  
							
							//UPDATE DELIVERY STATUS 

							if(strtolower($status_delv->Status) == "delivered"){

								if($k->payment_mode == 2){
									$this->db->query("Update tbl_orders set delivery_status = 2, payment_status = 1 where id = ".$k->id." limit 1");	

									echo "Delivery Status Updated For COD- order id ".$k->id."<br>";
								}else if($k->payment_mode == 1){

									$this->db->query("Update tbl_orders set delivery_status = 2 where id = ".$k->id." limit 1");	

									echo "Delivery Status Updated For Prepaid- order id ".$k->id."<br>";
								}

							}

						}
						
					}					

				}
			}
		}
	
	}

	public function get_delivery_status($tracking_number, $one_or_all){
		
		$url =  'http://xbclientapi.xbees.in/TrackingService.svc/GetShipmentSummaryDetails';
		$ch = curl_init( $url );
		
		# Setup request to send json via POST.
		$payload = json_encode( array( "XBkey" => "uTGBTGdcGkifRdj939mtorSA", "AWBNo" => $tracking_number) );

		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);
		
		$json_arr = json_decode($result);
		# Print response.
		
		if($one_or_all == "one"){

			echo "<pre>";
			print_r($json_arr);
		
		}

		if(strtolower($json_arr[0]->ReturnMessage) == "successful"){
			if(is_array($json_arr[0]->ShipmentSummary) ){
				if($one_or_all == "one"){

					return $json_arr[0]->ShipmentSummary[0]->StatusCode;

				}else if($one_or_all == "all"){
					return $json_arr[0]->ShipmentSummary[0];

				}
			}
		}
		
		return "error"; 
	}


	public function xpressbee_download(){			

		$tele_cond = "";
		$today_date = date('Y-m-d');

		header("Content-Type:  application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=xpressbee_delivery_".$today_date.".xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");			

		if($this->session->userdata('admin_role_id') == 3){
			$tele_cond = " and telecaller=".$this->session->userdata('uid');	
		}
		

		$sql_d = $this->db->query("Select id, status_code, comment, order_id, telecaller, tracking_number, date, date_created From ".XPRESSBEE_TMP." where 
			Date_Format(date_created ,'%Y-%m-%d') = '".$today_date."' ".$tele_cond);

		echo "<table border='1'>
			  <tr>	
			  <th> Order id</th>
			  <th> Status</th>
			  <th> Comment</th>
			  <th> Telecaller</th>	
			  <th> StatusDate </th>
			  <th> Db Date </th>
			  </tr>";

		$tele = "";	  
		if($sql_d->num_rows() > 0){

			foreach($sql_d->result() as $k ){
				if($k->telecaller == 5 ){
					$tele = "Vijoy";

				}else if($k->telecaller == 7 ){
					$tele = "Deepak";
				}

				echo "<tr>";
				echo "<td>".$k->order_id."</td>";
				echo "<td>".$k->status_code."</td>";
				echo "<td>".$k->comment."</td>";
				echo "<td>".$tele."</td>";
				echo "<td>".$k->date."</td>";
				echo "<td>".$k->date_created."</td>";

			}
		}

		echo "</table>";



	} 
	
}