<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addslider extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
	 	is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= SLIDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_title']	 		= ucfirst($mode)." Slider";
		$data['manage_page_title']	= "Manage Slider";
		$data['page_name']	 		= "Slider";
		$data['manage_page'] 		= "manageslider";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// 
		if($mode == "edit" && ($id == NULL || $id < 1)) {
			$this->session->set_flashdata('error', 'Don\'t try to cheat the system, one or more require parameters are missing !!');
			redirect(FULL_CMS_URL."/dashboard");
			exit;
		}
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['id']			    = NULL;
		$data['result_data']['image']	        = NULL;
		$data['result_data']['name']			= NULL;
		$data['result_data']['link']	    	= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	id, name, image, link, status, datecreated, dateupdated ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-slider',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// FIND THE CATEGORY LEVEL
				//$cat_level = $this->common_model->find_category_level(CATEGORY, "cat_level", $_POST['parent_cat_id']);
				
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$id_img= $_POST['id'];
					$this->db->where('id',$_POST['id']);
					// SET THE DATA
					$data = array(
									'id' => $_POST['id'],
									'name' => $_POST['name'],
									'link' => $_POST['link'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(SLIDER,$data);
					$this->session->set_flashdata('success', 'Slider edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'id' => $_POST['id'],
									'name' => $_POST['name'],
									'link' => $_POST['link'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(SLIDER,$data);
					$id_img = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Slider added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				
				if (isset ($_FILES['slider_image']['name'])) {

					// SET AND INITIAL S3 CLASS AND FOR BUCKET
					$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
					$s3->putBucket(S3_BUCKET, S3::ACL_PUBLIC_READ);
					
					$image_ext = allowed_extensions($_FILES['slider_image']['name']);
					if($image_ext != false) {
						if($_FILES['slider_image']['size'] > IMAGE_UPLOAD_MAX_SIZE) {
							// IF MORE THAN GENERATE FLASH MESSAGE
							$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																	(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
						} else {
							// CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
							if (is_dir("../".SLIDER_DIR_PATH) == false) {
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								//$slider_image = $this->db_function->get_single_value(SLIDER, 'image', "id=".$id_img, false, false);
								
								// FINALLY UPLOAD THE FILE AFTER CHECKING OF EVERYTHING 
								$new_img_name = get_rand_chracter('12','3').".".$image_ext;

								move_uploaded_file($_FILES['slider_image']['tmp_name'],"./".SLIDER_DIR_PATH.$new_img_name);						

								
								// COPY IMAGE TO AMAZON S3 SERVER
								$s3->putObjectFile("./".SLIDER_DIR_PATH.$new_img_name, S3_BUCKET, SLIDER_DIR_PATH.$new_img_name, S3::ACL_PUBLIC_READ);

								// DELETE FROM OLD IMAGE FROM S3 BUCKET
								$s3->deleteObject(S3_BUCKET, CATEGORY_DIR_PATH.$slider_image);
								
								// DELETE IMAGE FROM HDD
								unlink("./".SLIDER_DIR_PATH.$new_img_name);
								
								
								$slide_image = NULL;
								$slide_image = array(
								   'image' => $new_img_name
								);

								

								// SET THE WHERE CLAUSE
								$this->db->where("id", $id_img);
								$this->db->update(SLIDER, $slide_image);
							}
						}
					}
				}
				
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
