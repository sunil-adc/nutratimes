<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addreply extends CI_Controller {
	
	function __construct(){
		
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL , $offset = 0 ) {
		
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= COMPLAINT;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Complaint";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "managecomplaints";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "complaint_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		$data['offset']				= $offset;
		$data['complaint_id'] 		= $id;
		
		if (isset($id)) {
				$data_query = $this->db->query("select name,complaint,mobile,email,datecreated,complaint_status from ".COMPLAINT." where complaint_id='$id'");
				if($data_query->num_rows() > 0)
				{
					foreach($data_query->result() as $r1)
					{
						$data['customer_name'] = $r1->name;
						$data['customer_mobile'] = $r1->mobile;
						$data['customer_email'] = $r1->email;
						$data['customer_complaint'] = $r1->complaint;
						$data['customer_date'] = $r1->datecreated;
						$data['customer_complaint_status'] = $r1->complaint_status;
					}
				}
		
				$history_query = $this->db->query("select c.complaint_id,c.name,c.complaint,c.datecreated,c.parent_reference_id,c.replied_employee,a.username from ".COMPLAINT." c  left JOIN  ".ADMIN." a on (c.replied_employee = a.id) where c.parent_reference_id = '$id' order by c.datecreated desc");
				
				
			
				if($history_query->num_rows() > 0)
				{
					$a=0;
					foreach($history_query->result() as $a1)
					{
						$row[$a++] = $a1;
					}
					$data['parent_history'] = $row;
				}
				
				if($data['customer_complaint_status'] == 1)
				{
					$this->db->query("update ".COMPLAINT." set complaint_status=2 where complaint_id='$id'");
				}
					
					
				$this->db->cache_delete(CMS_FOLDER_NAME, $data['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, strtolower(__CLASS__));
				$this->db->cache_delete('addcomplaints', 'index');
				$this->db->cache_delete('addcomplaints', 'save');
				$this->db->cache_delete('addcomplaints', 'ajaxresolvedstatus');
		}
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		 
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-reply',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}

	
	function add_edit () {
		
			
		if($_POST['complaint_status'] != 1)	
		{
			$this->form_validation->set_rules('complaint', 'Reply', 'required');
		}
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false || $_POST['complaint_status'] == 1) {

				if( trim ($_POST['mode']) == 'edit') { 
					// SET THE WHERE CLAUSE
					if($_POST['complaint_id'])
					{
						$this->db->where('complaint_id',$_POST['complaint_id']);
						// SET THE DATA
						$data = array(
										'complaint' => $_POST['complaint'],
										'complaint_status' => 3,
										'dateupdated' => date('Y-m-d H:i:s')
									);
						// UPDATE QUERY
						$this->db->update(COMPLAINT,$data);
						$this->session->set_flashdata('success', 'Reply edited successfully');
					}
					else
					{
						$this->session->set_flashdata('error', 'There is some problem. Please try again');
					}
				
				} else if (trim($_POST['mode']) == 'add') { 	
				
					if($_POST['complaint_status'] == 1){
						$c_sts = 5;
					} else {
						$c_sts = 3;
					}
					// SET THE DATA FOR INSERTION
					
					if($_POST['complaint'] != ''){
						$data = array(
										'complaint' => $_POST['complaint'],
										'parent_reference_id' => $_POST['complaint_id'],
										'complaint_status' => $c_sts,
										'datecreated' => date('Y-m-d H:i:s'),
										'replied_employee' => $this->session->userdata('uid'),
										'name' => $_POST['name'],
										'mobile' => $_POST['mobile'],
										'email' => $_POST['email'],
										'replied_employee_role' => $this->session->userdata('admin_role_id')
									);
						// INSERT QUERY
						$this->db->insert(COMPLAINT,$data);
					
					}
					$this->db->query("update ".COMPLAINT." set complaint_status='$c_sts',dateupdated = now() where complaint_id='".$_POST['complaint_id']."' ");
					
					$this->session->set_flashdata('success', 'Reply added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				$this->db->cache_delete('addcomplaints', 'index');
				$this->db->cache_delete('addcomplaints', 'save');
				$this->db->cache_delete('addcomplaints', 'ajaxresolvedstatus');
				
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
	
	public function ajaxresolvedstatus() {
	
		
		if($_POST['checked_status'] == 'true')
		{
			
			$this->db->query("update ".COMPLAINT." set complaint_status = 5 , dateupdated = now() where complaint_id = '".$_POST['complaint_id']."' ");
		}
		else
		{
		
			$this->db->query("update ".COMPLAINT." set complaint_status = 1  , dateupdated = now() where complaint_id = '".$_POST['complaint_id']."'");
		}
		
		$this->db->cache_delete(CMS_FOLDER_NAME, 'managecomplaints');
		$this->db->cache_delete(CMS_FOLDER_NAME, 'addreply');
		$this->db->cache_delete('addcomplaints', 'index');
		$this->db->cache_delete('addcomplaints', 'save');
		$this->db->cache_delete('addcomplaints', 'ajaxresolvedstatus');
		
		redirect($_POST['cur_url']);
		
		exit;
	}
}
