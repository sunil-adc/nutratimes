<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class siteSettings extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN END
	}
	
	public function index() {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= SETTINGS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		$cur_controller = strtolower(__CLASS__);
		
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		$data['menu'] = $menu;
		$data['page_title'] = "Site Settings";
		$data['manage_page_title'] = "Site Settings";
		$data['manage_page'] = $cur_controller;
		$data['add_file'] = $cur_controller;
		$data['form_submit'] = CMS_FOLDER_NAME."/".$cur_controller;	
		$data['menunum'] = 4;
		$data['leftmenu'] = 1;
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		
		
		$this->form_validation->set_rules('phone_no', 'Phone No.', 'required');
		$this->form_validation->set_rules('site_name', 'Site Name', 'required');
		
		if($this->session->userdata('admin_role_id') == '1') {
			$this->form_validation->set_rules('admin_email', 'Admin Email', 'required');
			$this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
			//$this->form_validation->set_rules('payu_key', 'Payu Key.', 'required');
			//$this->form_validation->set_rules('salt', 'Payu Salt', 'required');
			//$this->form_validation->set_rules('ebs_secret_key', 'EBS Secret Key', 'required');
			//$this->form_validation->set_rules('ebs_account_id', 'EBS Account Id', 'required');
		}
		
		if(isset($_POST['fb_url']) && trim($_POST['fb_url']) != "") {
			$this->form_validation->set_rules('fb_url', 'Facebook URL', 'prep_url');
		}
		if(isset($_POST['twt_url']) && trim($_POST['twt_url']) != "") {
			$this->form_validation->set_rules('twt_url', 'Twitter URL', 'prep_url');
		}
		if(isset($_POST['pin_url']) && trim($_POST['pin_url']) != "") {
			$this->form_validation->set_rules('pin_url', 'Pinterest URL', 'prep_url');
		}
		if(isset($_POST['ytube_url']) && trim($_POST['ytube_url']) != "") {
			$this->form_validation->set_rules('ytube_url', 'Youtube URL', 'prep_url');
		}
		
		// CHECK IF FORM IS SUBMITTED START
		if(isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				$this->db->where('attr_name', 'phone_no');
				$this->db->update($tablename, array('attr_value' => $_POST['phone_no']));
				
				$this->db->where('attr_name', 'site_name');
				$this->db->update($tablename, array('attr_value' => $_POST['site_name']));
				
				$this->db->where('attr_name', 'admin_note');
				$this->db->update($tablename, array('attr_value' => $_POST['admin_note'])); 
				
				$this->db->where('attr_name', 'contest_winner');
				$this->db->update($tablename, array('attr_value' => $_POST['contest_winner'])); 
				
				if($this->session->userdata('admin_role_id') == '1') {
					
					$this->db->where('attr_name', 'admin_name');
					$this->db->update($tablename, array('attr_value' => $_POST['admin_name']));
					
					$this->db->where('attr_name', 'admin_email');
					$this->db->update($tablename, array('attr_value' => $_POST['admin_email']));
					
					$this->db->where('attr_name', 'payu_key');
					$this->db->update($tablename, array('attr_value' => $_POST['payu_key']));
					
					$this->db->where('attr_name', 'salt');
					$this->db->update($tablename, array('attr_value' => $_POST['salt']));
					
					$this->db->where('attr_name', 'ebs_secret_key');
					$this->db->update($tablename, array('attr_value' => $_POST['ebs_secret_key'])); 
					
					$this->db->where('attr_name', 'ebs_account_id');
					$this->db->update($tablename, array('attr_value' => $_POST['ebs_account_id']));
				}
				
				$this->db->where('attr_name', 'fb_url');
				$this->db->update($tablename, array('attr_value' => $_POST['fb_url']));
				
				$this->db->where('attr_name', 'twt_url');
				$this->db->update($tablename, array('attr_value' => $_POST['twt_url']));
				
				$this->db->where('attr_name', 'pin_url');
				$this->db->update($tablename, array('attr_value' => $_POST['pin_url']));
				
				$this->db->where('attr_name', 'ytube_url');
				$this->db->update($tablename, array('attr_value' => $_POST['ytube_url']));
				
				// DELETE THE RELATED CACHE 
				$this->db->cache_delete(CMS_FOLDER_NAME, $cur_controller);
				$this->db->cache_delete("default", "index");
				// DELETE THE RELATED CACHE
				 
				$this->session->set_flashdata('success', 'Your settings has been changed.');
				redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
			}
		}
	
		$data['result_data']['phone_no']		= NULL;
		$data['result_data']['admin_name']		= NULL;
		$data['result_data']['contest_winner']	= NULL;
		$data['result_data']['admin_note']		= NULL;
		$data['result_data']['admin_email']		= NULL;
		$data['result_data']['site_name']		= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		$data['result_data']['fb_url']			= NULL;
		$data['result_data']['twt_url']			= NULL;
		$data['result_data']['pin_url']			= NULL;
		$data['result_data']['ytube_url']		= NULL;
		
		// TURN OFF THE CACHE FOR SINGLE QUERY
		$val = $this->db->query("SELECT attr_name, attr_value from ".$tablename." where attr_name in ('phone_no', 'admin_name', 'admin_email', 'site_name', 'admin_note', 'contest_winner', 'payu_key', 'salt', 'ebs_secret_key', 'ebs_account_id', 'fb_url', 'twt_url', 'pin_url', 'ytube_url')");
		if($val->num_rows() > 0) {
			foreach ($val->result() as $row) {
				$data['result_data'][$row->attr_name] = $row->attr_value;
			}
		}
		
		// CHECK IF FORM IS SUBMITTED END
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/ckeditor');
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/site-settings',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
