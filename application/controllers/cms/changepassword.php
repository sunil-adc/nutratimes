<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changePassword extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN END
	}
	
	public function index() {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ADMIN;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		$cur_controller = strtolower(__CLASS__);
		
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		$data['menu'] = $menu;
		$data['page_title'] = "Change Password";
		$data['manage_page_title'] = "Change Password";
		$data['manage_page'] = $cur_controller;
		$data['add_file'] = $cur_controller;
		$data['form_submit'] = CMS_FOLDER_NAME."/".$cur_controller;	
		$data['menunum'] = 3;
		$data['leftmenu'] = 1;
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[new_password]');
		
		// CHECK IF FORM IS SUBMITTED START
		if(isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->cache_off();
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$old_password = $this->db_function->get_single_value($tablename, 'password', "id=".$this->session->userdata('uid'));
				
				if($old_password == md5($_POST['password'])) { 
					if($old_password == md5($_POST['new_password'])) {
					
						$this->session->set_flashdata('error', 'Your Old Password & New password must be different.');
						redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
					} else {
					
						$data = array(
							   'password' => md5($_POST['new_password']),
							   'last_psw_cng' => date('Y-m-d H:i:s')
						);
						
						$this->db->where('id', $this->session->userdata('uid'));
						$this->db->update(ADMIN, $data); 
						
						$this->session->set_flashdata('success', 'Your admin panel Password has been changed.');
						redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
					}
				} else {
					$this->session->set_flashdata('error', 'Your Current Password is wrong.');
					redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
				}
			}
		}
		// CHECK IF FORM IS SUBMITTED END
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/change-password',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
