<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class managecomplaints extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
		
				
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		if($where == "") {
			$where = " where parent_reference_id = 0 and complaint_status != 5  ";
		} else {
			$where .= " and parent_reference_id = 0 and complaint_status != 5   ";
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= COMPLAINT;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "Manage Complaints";
		$data['add_page_title']	= "Manage Complaints";
		$data['page_name']	 	= "Complaints";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "addreply";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "complaint_id";
		$data['seach_form']		= $full_path;
		$data['offset']			= $offset;
		
		
		
		// CHECK FOR SEARCH REQUEST		
		if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	= ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			
			// PREPARE QUERY STRING
			$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt']."/";
			
			// CREATE THE WHERE CLAUSE
			$whr = $data['search_field']." like '".$data['search_txt']."%'";
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
			
		} else {
			$query_string = $perpage."/";
		}
		
		
		// MODIFY FULL PATH
		$full_path .= $query_string;
		
		$group_by = " group by complaint_id ";
		
		// ORDER BY CLAUSE
		$order_by = " order by complaint_id desc, dateupdated desc";
		
		// CREATE THE SQL QUERY
		$sel_query = "SELECT 
						complaint_id,  
						complaint, 
						datecreated,
						name, 						
						mobile,
						email,
						complaint_status,
						status
					FROM
						".$tablename.$where.$group_by.$order_by; 

	
		
		// PAGINATION PARAMETER VALUES START
		if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 8;
		}
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 
		
		
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
		
		// GET THE DATA FROM PAGINATION
		$data["total_rows"] = $config['total_rows'];
		$data["results"] = $this->db_function->get_data($sql);
		

		$data["links"] = $this->pagination->create_links();
		
		// LOAD ALL REQUIRE VIEWS
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		
		
		/////////// Query for counts //////////////
		

		
		$union_qry = $this->db->query("SELECT 'active_cnt',COUNT(complaint_id) as active  from ".COMPLAINT." where status=1 and parent_reference_id = 0 and complaint_status != 5
							union all
						 SELECT 'in_active_cnt',COUNT(complaint_id) as inactive from ".COMPLAINT." where status!=1 and parent_reference_id = 0  and complaint_status != 5
						    union all
						 SELECT 'pending',COUNT(complaint_id) as pen  from ".COMPLAINT." where complaint_status=1 and parent_reference_id = 0  and complaint_status != 5
						 	union all
						 SELECT 'viewed_cnt',COUNT(complaint_id) as viewed  from ".COMPLAINT." where complaint_status=2 and parent_reference_id = 0  and complaint_status != 5
						    union all
						 SELECT 'replied_cnt',COUNT(complaint_id) as replied   from ".COMPLAINT." where complaint_status>=3	and parent_reference_id = 0  and complaint_status != 5
		");

		if($union_qry->num_rows() > 0)
		{
			$data['union_qry'] = $union_qry->result();			
		}
		
	
		
		
		//////////////END////////////////////////////
			
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/manage-complaints',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	
}