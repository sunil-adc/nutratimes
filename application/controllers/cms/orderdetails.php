<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orderDetails extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function') );
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ($id = NULL, $mode = "edit" ) {
		$menu = $this->common_model->Menu_Array();
		
		if( $id == "" || !is_numeric($id)) {
			$this->session->set_flashdata('error', 'One or more require parameter is not set, Start search again.');
			redirect(FULL_CMS_URL."/searchorders/index");
			exit;
		}
		
		$data['cod_agents'] = $this->db->query("select id, username, email from ".ADMIN." where adm_role_id=3 and status=1");

		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['manage_page_title']	= "Order Details";
		$data['page_name']	 		= "Order Details";
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "searchorders/index";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['name']					= NULL;
		$data['result_data']['email']					= NULL;
		$data['result_data']['mobile']					= NULL;
		$data['result_data']['address']					= NULL;
		$data['result_data']['city']					= NULL;
		$data['result_data']['area']					= NULL;
		$data['result_data']['state']					= NULL;
		$data['result_data']['alternate_phone']			= NULL;
		$data['result_data']['pincode']					= NULL;
		$data['result_data']['payment_mode']			= NULL;
		$data['result_data']['caller_status']			= NULL;
		$data['result_data']['delivery_status']			= NULL;
		$data['result_data']['qa_status']				= NULL;
		$data['result_data']['provider']				= NULL;
		$data['result_data']['tracking_number'] 		= NULL;
		$data['result_data']['cms_user']				= NULL;
		$data['result_data']['qa_user']					= NULL;
		$data['result_data']['qualified_date']			= NULL;
		$data['result_data']['sent_date']				= NULL;
		$data['result_data']['cms_order_user']			= NULL;
		$data['result_data']['notpaid_reason']			= NULL;
		$data['result_data']['cms_order']				= NULL;
		$data['result_data']['return_date']				= NULL;
		$data['result_data']['comments']				= NULL;
		$data['result_data']['issue_comments']			= NULL;
		$data['result_data']['dt_c']					= NULL;
		$data['result_data']['dt_u']					= NULL;
		$data['result_data']['status']					= NULL;
		$data['result_data']['qa_user']					= NULL;
		$data['result_data']['refund_status']			= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	id,			is_fraud,
												name, 			email,			mobile, 		address,  offer_percentage,
												city,			area,			state,			alternate_phone,
												pincode,		payment_mode,	caller_status, 	delivery_status,
												qa_status,		provider,		tracking_number,cms_user, qa_user,
												qa_user,		qualified_date,	sent_date,		issue_comments,
												return_date,	comments,		issue,			cms_order_user, 
												cms_order,		refund_status,	
												dt_c, 			status,			dt_u,			notpaid_reason', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
			
			if ($this->common_model->can_order_proceed($data['result_data']['mobile'])) {
				$data['result_data']['can_order_proceed'] = true;	
			} else {
				$data['result_data']['can_order_proceed'] = false;	
			}
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/order-details',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
			  
			  if ($this->common_model->can_order_proceed($_POST['hidmobile'])) {
					
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id']) && trim($_POST['id']) > 0) {
					 		
					
						// SET THE DATA
						$data = array(
										'name' => $_POST['name'],
										'email' => $_POST['email'],
										'address' => $_POST['address'],
										'city' => $_POST['city'],
										'area' => $_POST['area'],
										'state' => $_POST['state'],
										'refund_status' => $_POST['refund_status'],
										'is_fraud' => $_POST['is_fraud'],
										'alternate_phone' => $_POST['alternate_phone'],
										'pincode' => $_POST['pincode'],
										'payment_mode' => $_POST['payment_mode'],
										'offer_percentage' => $_POST['offer_percentage'],
										'notpaid_reason' => $_POST['notpaid_reason'],
										'tracking_number' => $_POST['tracking_number'],
										'provider' 		=> $_POST['provider'],
										'comments' => $_POST['comments'],
										'issue_comments' => $_POST['issue_comments'],
										'delivery_status' => $_POST['delivery_status'],
										'status' => $_POST['status'],
										'dt_u' => date('Y-m-d H:i:s')
									);
						
						


						/*if(isset($_POST['payment_mode'])) {
							$available_provider = $this->general->_payment_mode_wise_provider($_POST['pincode'], $_POST['payment_mode']);
							
							if ($available_provider != false && is_numeric($available_provider)) {
								$this->general->_set_provider_order($available_provider, $_POST['id']);
							} else if ($available_provider == '') {
								$this->general->_set_provider_order(1, $_POST['id']);
							}
						}*/
					 
					if (isset($_POST['caller_status'])) {
						$data['caller_status'] = $_POST['caller_status'];
						if ($this->session->userdata('admin_role_id') == 3) {
							if(isset($_POST['cms_user']) && trim($_POST['cms_user']) != "") {
								$data['cms_user'] = $_POST['cms_user'];
							}
						}
					}
					/*if (isset($_POST['qa_status'])) {
						$data['qa_status'] = $_POST['qa_status'];
						if ($this->session->userdata('admin_role_id') == 2) {
							if(isset($_POST['qa_user']) && trim($_POST['qa_user']) != "") {
								$data['qa_user'] = $_POST['qa_user'];
							}
						}
					}*/
					
					if ($this->session->userdata('admin_role_id') == 1) {
						
						if($_POST['cod_agents'] != ""){
								$data['qa_user'] = $_POST['cod_agents'] ;
						} 		
					}

					if($_POST['caller_status'] == '2') {
						$data['qualified_date'] = date('Y-m-d');
					} else if($_POST['caller_status'] == '3') {
						//$data['sent_date'] = date('Y-m-d');
					}
					
					if($_POST['delivery_status'] == '4') {
						$data['return_date'] = date('Y-m-d');
					}
					
					/*if(isset($_POST['send_after']) && $_POST['send_after'] > 0) {
						$data['qualified_date'] = date('Y-m-d', strtotime("+".$_POST['send_after']." day", time()));
					}*/
					
					
					// SET THE WHERE CLAUSE
					$this->db->where("id", $_POST['id']);
					
					// UPDATE QUERY
					$this->db->update(ORDER, $data);
					
					// NOTE THE LOGS
					$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Order details has been changed from CMS', 0, $_POST['id']);
					
					$this->session->set_flashdata('success', 'Order edited successfully');
				
				} else {
					$this->session->set_flashdata('error', 'Please try again later');
					redirect($_POST['cur_url']);
					exit;
				}
				// DELETE ALL ORDER RELATED CACHE
				$this->common_model->delete_order_cache();
				
			  } else {
			  	  	$this->session->set_flashdata('error', 'Can\'t proceed order customer has return more than 1 order');
			  }
				redirect($_POST['cur_url']."/#atab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}



	public function offer_per($order_id = 0) {
		
		$catid = '';
		$up_id = array();
		$new_cash_price = "";
		$all_array = all_arrays();

		$cart_data = $this->db->query("SELECT 
											o.id , o.offer_percentage, up.id as up_id, up.cash_price
										FROM
											".ORDER." o left join ".USER_PRODUCTS." up on  (o.id = up.order_id) 
										WHERE
											o.payment_status != 1 and 
											o.id = ".$order_id." and 
											up.status = 1");											

		if($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $v2) {
				
				if( $v2->offer_percentage != 0 ){
					
					$percentage = $all_array['ARR_OFFER_PER'][$v2->offer_percentage] ; 
					$p = explode("%",$percentage);	

					if($v2->up_id !="" && $v2->up_id > 0){

						$new_cash_price = ($v2->cash_price - ($v2->cash_price*$p[0])/100);
						
						if($new_cash_price != "" && $new_cash_price <  $v2->cash_price){

							$this->db->query("update ".USER_PRODUCTS." set cash_price = ".$new_cash_price." where id = ".$v2->up_id." and order_id = ".$v2->id);

							return true;
						}

					}
				}
				
			}

		}

		return false;	
		
	}


	public function dietitian_mail($order_id){

		if($order_id != ""){
			if($this->svaiza_email->dietitian_mail($order_id)){

				echo "DieEititan email sent";
			}else{
				echo "Email not sent";
			}
		}else{
			echo "null";
		}
		exit;
	}


	public function call_triger($order_id, $mobile, $cms_user){

	if (isset($mobile) && trim($mobile) > 0 && strlen($mobile) == 10 ) {
	   		
		   $url = 'http://106.51.76.48/TransVoice/Click2Call/c2c.php?Lead_ID='.$order_id.'&Cust_NO='.$mobile.'&Crm_ID='.$cms_user;
		   
		   $ch = curl_init();
		     
		   curl_setopt($ch, CURLOPT_URL, $url);
		   
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		   
		   curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		   
		   $output = curl_exec($ch);
		   
		   curl_close($ch);
		   
		   $op = json_decode($output);	
		   
		   if (strtolower($op->Status) == 'success') {
		    
		    	echo "Please wait We will connect your call to ".$mobile;
		    
		   } else {
		    
		    	echo "Sorry call will Not connect";
		   }

	
	}else{

		echo "Some issue occured Please try again";

	}

	}
}
