<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class fulladdress extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
	
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'/logout');
		}
		// CHECK USER IS LOGIN OR NOT - END
	}
	
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
        $tablename 			    = USERS;
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		$query = $this->db->query("SELECT
										name, email, mobile, address, ship_address, bill_address, city, state, pincode, points, gender
									FROM
									 	".$tablename."
									WHERE
										status = 1 and 
										user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
		
		// GET THE PRODUCT DETAILS
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['user_details'] = $p;
			}
		} else {

			redirect(SITE_URL);
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-full-address',$data);
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	public function save() {
		
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('state', 'state', 'required');
		$this->form_validation->set_rules('pincode', 'pincode', 'required');
		
		
		if(isset($_POST['submit'])) {

			if ($this->form_validation->run()) {
			
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->cache_off();
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->where("user_id",$_SESSION[SVAIZA_USER_ID]);
				
				// SET THE DATA
				$data = array(
								'address' => $this->input->post('address'),
								'city'    => $this->input->post('city'),
								'state'   => $this->input->post('state'),
								'pincode' => $this->input->post('pincode'),
								

							);
							 
			   // UPDATE QUERY
				
				$this->db->update(USERS,$data);
				
				echo "done";

			} else {

				echo "validation_err";
			}

		}else{
			echo "error";

		}
		exit;
	}
}
