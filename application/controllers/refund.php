<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class refund extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
										  
		// SEND NETWORK AND PUB ID - START
	
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'/login');
		}
		// CHECK USER IS LOGIN OR NOT - END
	}
	//--//Koushik - Removed refund policy on 9th Dec 2014 based on Rohit Tiwari's feedback
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		$query = $this->db->query("SELECT
										name, email, mobile, address, city, state, pincode, points, gender
									FROM
									 	".USERS."
									WHERE
										status = 1 and 
										user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
		
		// GET THE PRODUCT DETAILS
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['user_details'] = $p;
			}
		} else {
			redirect(SITE_URL);
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('refund-money',$data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
	
	public function save() {
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('order_id', 'Order Id', 'required');
		
		if ($this->input->post('submit')) {
			if ($this->form_validation->run() != false) {
				
				
				$this->db->cache_off();
				$res = $this->db->query("select refund_id from ".REFUND_STATUS." 
											where mobile = ".$this->input->post('mobile')." and 
												order_id = ".$this->input->post('order_id'));
				
				if ($res->num_rows() > 0) {
				 	$this->session->set_flashdata('error', 'Order id request is already taken.');	
					redirect(SITE_URL.'refund/?msg=alreq');
				} else {
					
					$res_order = $this->db->query("select id, user, mobile, name, email, payment_mode from ".ORDER." where id = ".$this->input->post('order_id'));
					if ($res_order->num_rows() > 0) {
						foreach ($res_order->result() as $val_orders) {
							$order_id = $val_orders->id;
							$mobile = $val_orders->mobile;
							$payment_mode = $val_orders->payment_mode;
							$name = $val_orders->name;
							$email = $val_orders->email;
							$order_user = $val_orders->user;
						}
						
						if ($payment_mode == 1){
							if ($_SESSION[SVAIZA_USER_ID] == $order_user) {
								 
								// SET THE DATA
								$data = array(
												'user_id' => $_SESSION[SVAIZA_USER_ID],
												'order_id' => $order_id,
												'mobile' => $mobile,
												'name' => $name,
												'isvalid' => 1,
												'email' => $email,
												'datecreated' => date('Y-m-d H:i:s')
											);
								$this->db->insert(REFUND_STATUS, $data);
								$this->db->query("update ".ORDER." set refund_status = 1, refund_date = '".date('Y-m-d H:i:s')."' where user = ".$order_user." and id = ".$order_id);
								$this->svaiza_email->email_refund_request($this->input->post('email'));
								$this->db->cache_delete('refund', 'index');
								$this->session->set_flashdata('success', 'Refund request taken successfully.');
								redirect(SITE_URL.'refund/?msg=success');	
							} else {
								$this->session->set_flashdata('error', 'Order id is invalid please provide valid details');
								redirect(SITE_URL.'refund/?msg=ierr');
							}
						} else {
							$this->session->set_flashdata('error', 'Please call to customer care and proceed for your refund');
							redirect(SITE_URL.'refund/?msg=ierr');							
						}
					} else {
						$this->session->set_flashdata('error', 'Order id is invalid please provide valid details');
						redirect(SITE_URL.'refund/?msg=ierr');
					}
				}
			} else {
				$this->session->set_flashdata('error', '(*) denotes mandatory fields');
				redirect(SITE_URL.'refund/?msg=err');
			}
		} else {
			 $this->session->set_flashdata('error', 'request not captured');
		}
		redirect(SITE_URL.'refund');
	}
}
