<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class payment extends CI_Controller {
	
	public $data;
		
	function __construct(){
		parent::__construct();
		
		$this->load->model(array('order_driver', 'cart_driver'));
		$this->load->library(array('CryptAES'));
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
										 
		// SEND NETWORK AND PUB ID - START
		
		
		if ($_SESSION[USER_ORDER_ID] == '' || $_SESSION[USER_ORDER_ID] <= 0) {
			redirect(SITE_URL."cart");
		}
		
		// CHECK USER IS LOGIN OR NOT
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
					  $_SESSION[SVAIZA_MOBILE],
					  $_SESSION[SVAIZA_EMAIL])) {
		
			redirect(SITE_URL."login");
		}
		// GET ALL ARRAY - START
		$this->data['all_array'] = all_arrays();
		// GET ALL ARRAY - END
		
		// SET DEFAULT REDEEM TO OFF
		$this->data['use_redeem'] = '0';
	}
	
	public function index() {		
		if ($this->input->post('submit') || (isset($_GET['sbt']) && $_GET['sbt'] == 'parsed')) {
			
			if ($this->input->post('redeem_point') == '1' || (isset($_GET['r']) && $_GET['r'] == '1')) {
				
				// CHANGE REDEEM STATUS
				$this->data['use_redeem'] = '1';
				
				// CHECK WHICH TYPE OF ITEMS ARE AVAILABLE IN CART PRODUCT OR CONTEST
				$product_type = $this->common_model->_decide_product_type();
				
				// AFTER CHECKING SET THE VARIABLE
				$this->data['is_product'] = $product_type['is_product'];
				$this->data['is_contest'] = $product_type['is_contest'];
					
				// GET USER CURRENT CREDIT POINTS	
				$this->data['user_points'] = $this->common_model->_get_user_credit_points();
				
				// CHECK ONLY PRODUCT IS AVAILALE {
				if ($this->data['is_product'] == true && $this->data['is_contest'] == false && $this->data['user_points'] > 0) {
					
					// CALCULATE THE TOTAL AMOUNT OF PRODUCT
					$arr_cart_amount = $this->order_driver->_calculate_cart_amount();
					$this->data['user_details'] = $arr_cart_amount['user_details'];
					
					$total_amount = $arr_cart_amount['final_amount'] + $arr_cart_amount['final_shipping'];
					
					// COUNT MAXIMUM REDEMPTION POINTS
					$product_max_redemption = $this->order_driver->_count_redeemable_credit_point();
					
					// COUNT REDEEMABLE CREDIT POINT FROM CURRENT CART PRODUCT
					$final_deduction = $this->order_driver->count_deduction($total_amount, $arr_cart_amount['final_shipping'], 
													$product_max_redemption, $this->data['user_points']);
					
					$total_deduction = $final_deduction['final_redemption'];
					$final_payable_amount = $final_deduction['final_price'];
					
					// CHECK FOR THE AMOUNT AND CREDIT POINTS
					if ($total_deduction >= $total_amount && $total_amount > 0) {
						
						// UPDATE CREDIT POINTS AS DEDUCTED
						$upd_credit_points = $this->db->query("update ".USERS." set points = (points - ".$total_deduction.") 
																where user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
						// ADD IN LOGS
						$this->common_model->credit_logs(0, 0, $total_deduction, 'Redeem for Order Id '.$_SESSION[USER_ORDER_ID], $_SESSION[SVAIZA_USER_ID]);
						
						// INSERT INTO POINT HISTORY TABLE
						$this->order_driver->_insert_point_history($total_deduction);
		
						// DO FULL PAYMENT WITH CREDIT POINTS
						$this->_do_payment_with_credit_point($total_amount);
					
					} else if ($total_deduction < $total_amount && $this->data['user_points'] > 0) {
						
						// DO PARTIAL OR AVAILABLE CREDIT POINT PAYMENT
						$final_payable_amount = $total_amount - $total_deduction;
						
						// SEND TO PAYMENT GATEWAY FOR REST AMOUNT PAYMENT
						$this->_send_to_payment_gateway($final_payable_amount);
					}
				} else {
					// IF CONDITION FAIL THEN REDIRECT TO PAYMENT GATEWAY PAGE
					$this->_send_to_payment_gateway();	
				}
			} else {
				// IF CONDITION FAIL THEN REDIRECT TO PAYMENT GATEWAY PAGE
				$this->_send_to_payment_gateway();	
			}
		} else {
			// IF CONDITION FAIL THEN REDIRECT TO PAYMENT GATEWAY PAGE
			$this->_send_to_payment_gateway();	
		}
		
	}
	
	
	public function _do_payment_with_credit_point($total_amount = 0) {
		
		// UPDATE ORDER TABLE WITH PAYMENT STATUS
		$this->db->where('id', $_SESSION[USER_ORDER_ID]);
		$this->db->update(ORDER, array('payment_status' => '1', 'caller_status'  => '2', 'qualified_date' => date('Y-m-d'),
										'order_id_pg'	=> 0));
		
		// MINUS STOCK IF REQUIRE
		$this->cart_driver->product_stock_minus();
		
		// SEND EMAIL TO USER
		$this->svaiza_email->email_order_confirmation($_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
	 
		// REDIRECT TO CONFIRMATION PAGE
		redirect(SITE_URL.'paidconfirm/index/'.$_SESSION[USER_ORDER_ID]);
	
	}
	
	public function get_payment_gateway() {
		// PAYMENT GATEWAY PRIORITY
		$is_contest = $is_product = false;
		$this->db->cache_off();
		$decide_pg = $this->db->query("SELECT up.contest FROM ".ORDER." o inner join ".USER_PRODUCTS." up on (o.id = up.order_id) WHERE up.status = 1 and o.id = '".$_SESSION[USER_ORDER_ID]."'");
		if($decide_pg->num_rows() > 0) {
			foreach ($decide_pg->result() as $row) {
				if($row->contest == '1') {
					$is_contest = true;
				}
				if($row->contest == '0') {
					$is_product = true;
				}
			}
			
			// SELECT PAYMENT GATEWAY FROM DATABASE
			$this->db->cache_off();
			$pg_info = $this->db->query("SELECT 
											pg_id, 
											pg_name, 
											product_priority, 
											contest_priority, 
											both_priority 
										FROM 
											".PAYMENT_GATEWAY." 
										WHERE 
											status = '1' 
										ORDER BY 
											pg_id desc");
			if($pg_info->num_rows() > 0) {
				foreach ($pg_info->result() as $pg_rows) {
					$arr_product_priority[$pg_rows->product_priority] = $pg_rows->pg_id;
					$arr_contest_priority[$pg_rows->contest_priority] = $pg_rows->pg_id;
					$arr_both_priority[$pg_rows->both_priority] = $pg_rows->pg_id;
				}
			}
			
			if($is_contest == true && $is_product == true) {
				return $arr_both_priority[min($arr_both_priority)];
			}else if($is_contest == false && $is_product == true) {
				return $arr_product_priority[min($arr_product_priority)];
			}else if($is_contest == true && $is_product == false) {
				return $arr_contest_priority[min($arr_contest_priority)];
			}
		}
		// RETURN DEFAULT PAYMENT GATEWAY PAYU
		return '2';	
	}
	
	public function _send_to_payment_gateway($amount = '0') {
		
		// CHECK FOR REQUESTED AMOUNT 
		if($amount == '0') {
			// CALCULATE AMOUNT AND SET VARIABLE
			$arr_amount = $this->order_driver->_calculate_cart_amount();
			$this->data['user_details'] = $arr_amount['user_details'];
			
			$final_amount = $arr_amount['final_amount']; 
			$final_shipping = $arr_amount['final_shipping'];
		
			// GET THE FINAL AMOUNT
			$this->data['amount'] = $final_amount + $final_shipping;
		} else {
			// GET THE FINAL AMOUNT
			$this->data['amount'] = $amount;
		}
		
		// GET THE DEFAULT PAYMENT GATEWAY
		$payment_gateway = $this->get_payment_gateway();
		
		//TEMP
		$payment_gateway = 5;
		if ($_SERVER['REMOTE_ADDR'] == '202.191.245.14') {
			//$payment_gateway = 4;	
		}
		//TEMP
		
		// GET NEWLY INSERTED ORDERPG ID
		$order_pg_id = $this->order_driver->insert_order_pg($_SESSION[USER_ORDER_ID], $this->data['amount'], $payment_gateway, date('Y-m-d H:i:s')); 
		
		// UPDATE ORDER-PG ID IN ORDER TABLE 
		$this->order_driver->update_orderpg_id($order_pg_id, $_SESSION[USER_ORDER_ID]);
		
		// PROCESS FOR PAYU PAYMENT GATEWAY
		$productinfo 				= "products";
		
		
		// SET USER DETAILS IN VARIABLE FOR VIEW
		$this->data['uname'] 		= htmlentities($this->data['user_details']['uname'], ENT_QUOTES);
		$this->data['uemail'] 		= htmlentities($this->data['user_details']['uemail'], ENT_QUOTES);
		$this->data['umobile']		= $this->data['user_details']['umobile'];
		$this->data['ucity']		= $this->data['user_details']['ucity'];
		$this->data['ustate']		= $this->data['user_details']['ustate'];
		$this->data['upincode']		= $this->data['user_details']['upincode'];
		$this->data['uaddress']		= $this->data['user_details']['uaddress'];
		
		// SET THE PAYMENT GATEWAY IN VARIABLE FOR VIEW
		$this->data['payment_gateway'] = $payment_gateway;
		
		// REDEEM STRING
		$this->data['redeem_string'] = '&use_redeem='.$this->data['use_redeem'];
			
		
		// CHECK WHICH PAYMENT GATEWAY IS REQUESTED
		if($payment_gateway == 1) {
			$key = "qcAHa6tt8s0l5NN7UWPVAQ==";
			
			$this->data['requestparameter'] = "201210311000005|DOM|IND|INR|".$this->data['amount']."|".$order_pg_id."|others|".SITE_URL."paymentconfirm/index/?pg=direcpay&use_redeem=".$this->data['use_redeem']."|".SITE_URL."paymentfailure/index/|DirecPay";
			
			$this->data['billingparameter'] = $this->data['uname']."|".$this->data['uaddress']."|".$this->data['ucity']."|".$this->data['ustate']."|".$this->data['upincode']."|IN|91|||".$this->data['umobile']."|".$this->data['uemail']."|Direc Pay Shophunk Transaction";
			
			$this->data['shippingparameter'] = $this->data['uname']."|".$this->data['uaddress']."|".$this->data['ucity']."|".$this->data['ustate']."|".$this->data['upincode']."|IN||||".$this->data['umobile'];
			
			$AES = new CryptAES();
			$AES->set_key(base64_decode($key));
			$AES->require_pkcs5();
			
			$this->data['requestparameter'] = $AES->encrypt($this->data['requestparameter']);
			$this->data['billingparameter']  = $AES->encrypt($this->data['billingparameter']);
			$this->data['shippingparameter']  = $AES->encrypt($this->data['shippingparameter']);
			
		} else if($payment_gateway == 2) {
			
			// SET THE PAYU KEY IN DATA VARIABLE
			$this->_get_payu_details();
			
			/* PAYU CODE START */
			$this->data['txnid']	= $order_pg_id;
			$oidpg 					= substr(hash('sha256', $order_pg_id . microtime()), 0, 5);
			$ordidpg 				= $oidpg."sss".$order_pg_id;
			$hash_string 			= $this->data['payu_key']."|".$this->data['txnid']."|".$this->data['amount']."|".$productinfo."|".$this->data['uname']."|".$this->data['uemail']."|".$this->data['use_redeem']."||||||||||";
			
			$hash_string			.= $this->data['salt'];
			$this->data['hash'] 	= strtolower(hash('sha512', $hash_string));
			/* PAYU CODE END */
		}else if($payment_gateway == 3) {
			
			// SET THE EBS KEY IN DATA VARIABLE
			$this->_get_ebs_details();
			
			// PAYMENT MODE
			$this->data['mode'] = 'LIVE';
			
			// GENERATE HASH VALUE 
			$hash = $this->data['ebs_secret_key']."|".$this->data['ebs_account_id']."|".$this->data['amount']."|".$order_pg_id."|".SITE_URL."paymentconfirm/index/?pg=ebs".$this->data['redeem_string']."&DR={DR}|".$this->data['mode'];
			$this->data['secure_hash'] 	= md5($hash);
			$this->data['reference_no'] = $order_pg_id;
		} else if ($payment_gateway == 4) {
			
			$this->data['returnUrl']	= SITE_URL.'paymentconfirm/index/?pg=citruspay';
            $this->data['txnid']	= $order_pg_id;
		/* CCAVENUE CODE END */
		}else if ($payment_gateway == 5) {
			
			
			$merchant_data='';
			$this->data['working_key']='D545825E1E34030BF9696002214E3BB3';//Shared by CCAVENUES
			$this->data['access_code']='AVCO78FF83CC01OCCC';//Shared by CCAVENUES
			
			
			/*foreach ($_POST as $key => $value){
				$merchant_data.=$key.'='.$value.'&';
			}*/
			
			$merchant_data = "tid=".$order_pg_id."&merchant_id=181436&order_id=".$order_pg_id."&amount=".$this->data['amount']."&currency=INR&redirect_url=".SITE_URL."paymentconfirm/index/?pg=ccavenue&cancel_url=".SITE_URL."paymentcancel/index&language=EN
			&billing_name=".$this->data['uname']."&billing_address=".$this->data['uaddress']."&billing_city=".$this->data['ucity']."&billing_state=".$this->data['ustate']."&billing_zip=".$this->data['upincode']."&billing_country=INDIA&billing_tel=".$this->data['umobile']."&billing_email=".$this->data['uemail'];
			
			$this->data['encrypted_data'] = encrypt($merchant_data,$this->data['working_key']); // Method for encrypting the data.

		}
		
		// LOCK THE TABLE
		if($_SESSION[USER_ORDER_ID] != "") {
			$this->cart_driver->lock_order($_SESSION[USER_ORDER_ID]);
		}
		
		// LOAD THE VIEW METHOD
		$this->load_views();
	}
	
	
	public function load_views() {
		if($this->data['payment_gateway'] == 1) {
		//	$this->load->view(FRONT_INC.'directpay');
		}
		$this->load->view('payment', $this->data);
	}
	
	public function _get_payu_details() {
		// GET THE PAYMENT GATEWAY DETAILS - START
		$this->db->cache_off();
		$settings = $this->db->query("select attr_name, attr_value from ".SETTINGS." where status = '1' and attr_name in ('payu_key','salt')");
		if($settings->num_rows() > 0) {
			foreach($settings->result() as $row) {
				if($row->attr_name == 'payu_key') {
					$this->data['payu_key'] = $row->attr_value;
				}
				if($row->attr_name == 'salt') {
					$this->data['salt'] = $row->attr_value;
				}
			}
		} else {
			$this->session->set_flashdata('error', 'We are down for maintainance !! please check back later');
			redirect(SITE_URL);
		}
		// GET THE PAYMENT GATEWAY DETAILS - START
	}
	
	public function _get_ebs_details() {
		// GET THE PAYMENT GATEWAY DETAILS - START
		$this->db->cache_off();
		$settings = $this->db->query("select attr_name, attr_value from ".SETTINGS." where status = '1' and attr_name in ('ebs_secret_key','ebs_account_id')");
		if($settings->num_rows() > 0) {
			foreach($settings->result() as $row) {
				if($row->attr_name == 'ebs_secret_key') {
					$this->data['ebs_secret_key'] = $row->attr_value;
				}
				if($row->attr_name == 'ebs_account_id') {
					$this->data['ebs_account_id'] = $row->attr_value;
				}
			}
		} else {
			$this->session->set_flashdata('error', 'We are down for maintainance !! please check back later');
			redirect(SITE_URL);
		}
		// GET THE PAYMENT GATEWAY DETAILS - START
	}
}
