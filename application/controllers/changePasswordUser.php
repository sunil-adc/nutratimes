<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changePasswordUser extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
	
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'/logout');
		}
		// CHECK USER IS LOGIN OR NOT - END
	}
	
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
        $tablename 			    = USERS;
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		$query = $this->db->query("SELECT
										name, email, mobile, address, city, state, pincode, points, gender
									FROM
									 	".$tablename."
									WHERE
										status = 1 and 
										user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
		
		// GET THE PRODUCT DETAILS
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['user_details'] = $p;
			}
		} else {

			redirect(SITE_URL);
		}
		 

		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-change-password',$data);
		//$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	public function save() {
		
		$this->form_validation->set_rules('old_password', 'Old Password', 'required');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[new_password]');
	
		
		if(isset($_POST['submit'])) {

			if ($this->form_validation->run()) {
			
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->cache_off();
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$old_passwd_usr = "";
				$user_name	    = "";
				$users = $this->db->query("SELECT name, email, mobile, passwd FROM ".USERS." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
				if ($users->num_rows() > 0) {
				    foreach ($users->result() as $user_data) {
				    	$old_passwd_usr = $user_data->passwd;
				    	$user_name = $user_data->name;
				 	}
				 }



				if($old_passwd_usr == md5($_POST['old_password'])) { 
				
					if($old_passwd_usr == md5($_POST['new_password'])) {
					
						echo "newpass_notmatch";

					} else {
					
						$data = array(
							   'passwd' => md5($_POST['new_password'])
						);
						
						$this->db->where('user_id', $_SESSION[SVAIZA_USER_ID]);
						$this->db->update(USERS, $data); 
						
						$user_name 	 = $user_name;
						$user_email  = $_SESSION[SVAIZA_EMAIL];
						$user_mobile = $_SESSION[SVAIZA_MOBILE];

						$this->svaiza_email->email_ChangePassword($user_name, $user_email, $_POST['new_password'], $user_mobile);
						
						echo "done";
						exit;						
					}
				} else {
					echo "old_password";
					exit;
				}
			} else {
				echo "form_validation_err";
				exit;
			}

		}else{
			echo "error";
			exit;	
		}
		
	}
}