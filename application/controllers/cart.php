<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cart extends CI_Controller {
	protected $data = array();
	protected $cacheID;
	
	function __construct(){
		parent::__construct();
		
		$this->load->model(array('order_driver', 'cart_driver'));
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		// CHECK USER IS LOGIN OR NOT
		
		$this->db->cache_off();
	
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		//giantFirewall();
	}
	
	public function cartupd($is_ajax=0) {		
		if($is_ajax == 1) {
			$order_id = $this->input->post('oid');
			$cart_product_id = $this->input->post('cpid');
			$quantity = $this->input->post('qty');
			if($quantity >= 0) {
				if($quantity <= 5) {
					$is_locked = 0;
					
					$this->db->cache_off();
					$lock_res = $this->db->query("select is_locked from ".ORDER." where id = ".$order_id);
					if($lock_res != false) {
						foreach ($lock_res->result() as $val_locked){
							$is_locked = $val_locked->is_locked;
						}
					}
					
					$real_size = 0;
					$res_user_product_p = $this->db->query("select product, quantity, size from ".USER_PRODUCTS." where id = ".$cart_product_id);
					if($res_user_product_p != false) {
						foreach ($res_user_product_p->result() as $val_user_product_p){
							$real_prod_id = $val_user_product_p->product;
							$real_quantity = $val_user_product_p->quantity;
							$real_size = $val_user_product_p->size;
						}
					}
					
					$prod_availability = $this->cart_driver->_product_qty_instock_check($real_prod_id, $real_size, $quantity);
					if ($prod_availability == 'prod_na') {
						$quantity = 0;
						$prod_availability == 'yes';
					}
					
					if ($prod_availability == 'yes') {		
						if($is_locked == 0) {
							if($quantity == 0) {
								// NOTE THE LOGS
								$this->common_model->order_logs(0, 0, 'Product deleted by user it self (Prod Id :'.$real_prod_id.')', $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
								
								$this->db->query("update ".USER_PRODUCTS." set status = 0 where id = ".$cart_product_id." and order_id = ".$order_id);
							} else if ($quantity > 0) {
								
								if ($real_quantity > $quantity) {
								
									$final_qty = $real_quantity - $quantity;
									// NOTE THE LOGS
									$this->common_model->order_logs(0, 0, $final_qty.' item(s) remove and final quantity is '.$quantity.' for product id : '.$real_prod_id, $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
								
								} else if ($real_quantity <= $quantity) {
								
									$final_qty = $quantity - $real_quantity;
									// NOTE THE LOGS
									$this->common_model->order_logs(0, 0, $final_qty.' item(s) added and final quantity is '.$quantity.' for product id : '.$real_prod_id, $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
								
								} else {
								
									// NOTE THE LOGS
									$this->common_model->order_logs(0, 0, 'Quantity Modified to '.$quantity.' for product id : '.$real_prod_id, $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
								
								}
								
							$this->db->query("update ".USER_PRODUCTS." set quantity = ".$quantity." where id = ".$cart_product_id." and order_id = ".$order_id);
							}
							//REMOVE THE CACHE ON UPDATION OR DELETION
							$this->db->cache_delete(__CLASS__, __FUNCTION__);
							$this->db->cache_delete(__CLASS__, "index");
							// CALL THE INDEX FUNCTION FOR RENDERING NEW DATA IN CART
							echo "upd";	
						} else {
							echo "lkd";
						}
					} else {
						echo "stock_err";
					}
				} else {
					echo "qnty_err";
				}
			} else {
				echo "err";
			}
		}
		// PUT EXIT FOR AJAX CALL - START
		exit;
		// PUT EXIT FOR AJAX CALL - END
	}
	
	public function getSize() {
		$data_order_id = $_POST['data_order_id'];
		$data_prodid = $_POST['data_prodid'];
		$data_cartprodid = $_POST['data_cartprodid'];
		$data_size_id = $_POST['data_size_id'];
		$data_size_name = $_POST['data_size_name'];
		
		
		if ($data_order_id != '' && $data_prodid != '' && $data_cartprodid != '' && $data_size_id != '' && $data_size_name != '') {
			$this->db->cache_off();
			$res_product_genre = $this->db->query("select g.genre_id, g.genre_name from ".PRODUCT_GENRE." pg inner join ".GENRE." g on (pg.genreid = g.genre_id) where pg.genre_quantity > 0 and pg.prodid = ".$data_prodid);
			if($res_product_genre->num_rows() > 0) {
				echo '<select class="chageSizeAjax" onchange="chageSizeAjax('.$data_prodid.', '.$data_cartprodid.', this.value, \''.SITE_URL.'\');">';
				foreach ($res_product_genre->result() as $var) {
					$selected = ($data_size_name == $var->genre_name) ? 'selected' : '';
					echo '<option '.$selected.' value="'.$var->genre_id.'">'.$var->genre_name.'</option>';
				}
				echo '</select>';
			} else {
				echo '<option value="0">Not Available</option>';
			}
		} else {
			echo 'refresh';
		}
		exit;
	}
	
	public function sizeUpdate() {
		$prodid = $_POST['prodid'];
		$cartprodid = $_POST['cartprodid'];
		$currentsize = $_POST['currentsize'];
		
		if ($prodid > 0 && $cartprodid > 0) {
			$this->db->query("update ".USER_PRODUCTS." set size = ".$currentsize." where id = ".$cartprodid);
			
			//REMOVE THE CACHE ON UPDATION OR DELETION
			$this->db->cache_delete(__CLASS__, __FUNCTION__);
			$this->db->cache_delete(__CLASS__, "index");
		}
		exit;
	}
	

	public function prdupdate() {
		
		$cart_data  = "";
		
		$order_id = $_SESSION[USER_ORDER_ID];
		
		if ($order_id !=  "" && $order_id > 0) {

			$cart_data = $this->db->query("SELECT
												p.prod_id,
												up.product,		up.quantity,	
												p.cod_price,	p.sale_price,	up.shipping,
												up.contest,	    up.id, p.name
											  FROM
												".USER_PRODUCTS." up inner join ".PRODUCT."	p  on (up.product = p.prod_id)
 											  WHERE
												up.order_id = ".$_SESSION[USER_ORDER_ID]." and 
												up.product = p.prod_id and 
												up.status = 1 and 
												p.is_verified = 1 and 
												p.status = 1");
				
			if($cart_data->num_rows() > 0){

				foreach ($cart_data->result() as $cartvalue) {
						if($cartvalue->prod_id != 6 ){
						 	
						 	$sale_price_new = ($cartvalue->sale_price - ($cartvalue->sale_price *DISCOUNT_PERCENTAGE)/100); 
				
							$this->db->query("update ".USER_PRODUCTS." set cash_price=".$sale_price_new.", cod_price=".$cartvalue->cod_price.", shipping=".$cartvalue->shipping."  where id = ".$cartvalue->id);
							
							//REMOVE THE CACHE ON UPDATION OR DELETION
							$this->db->cache_delete(__CLASS__, __FUNCTION__);
							$this->db->cache_delete(__CLASS__, "index");
						}else if($cartvalue->prod_id == 6 ){

							//$sale_price_new = ($cartvalue->sale_price - 300); 
							
							$this->session->set_userdata('promocode', 'ntrhr300');

							//$this->db->query("update ".USER_PRODUCTS." set cash_price=".$sale_price_new.", cod_price=".$cartvalue->cod_price.", shipping=".$cartvalue->shipping."  where id = ".$cartvalue->id);
							
							//REMOVE THE CACHE ON UPDATION OR DELETION
							//$this->db->cache_delete(__CLASS__, __FUNCTION__);
							//$this->db->cache_delete(__CLASS__, "index");


						}
					
					
				}
			}
			

		}
			
			
		exit;
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		$this->data['payment_mode'] = 0;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		// CHECK USER IS LOGIN OR NOT
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
		
			// CHECK ORDER IS SET OR NOT
			if($_SESSION[USER_ORDER_ID] != "") {
				$cart_data = $this->db->query("SELECT
												o.user,			o.name,			o.email,		o.mobile,
												o.address,		o.city,			o.state,		o.alternate_phone,
												o.pincode,		o.id order_id,	up.product,		up.quantity,	
												up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
												up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
												p.name,			p.model_no,		p.seourl,		pi.image, p.mrp, 		
												g.genre_id,		g.genre_name,	p.cod_availability, o.payment_mode,
												p.is_combo,		p.combo_product,  up.is_combo as user_combo,	
												up.combo_product as user_combo_prouct
											  FROM
												".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
												left join ".GENRE." g on (up.size = g.genre_id),
												".PRODUCT."	p left join ".PRODUCT_IMAGE." pi on (p.prod_id = pi.prod_id)
 											  WHERE
												o.id = ".$_SESSION[USER_ORDER_ID]." and 
												up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
												up.product = p.prod_id and 
												up.status = 1 and 
												p.is_verified = 1 and 
												p.status = 1");
												
				if($cart_data->num_rows() > 0) {
					
					// SET VARIABLE FALSE
					$this->data['is_contest'] = $this->data['is_product'] = false;
					
					// CHECK WHICH TYPE OF ITEMS ARE AVAILABLE IN CART PRODUCT OR CONTEST
					$product_type = $this->common_model->_decide_product_type();
					
					// AFTER CHECKING SET THE VARIABLE
					$this->data['is_product'] = $product_type['is_product'];
					$this->data['is_contest'] = $product_type['is_contest'];
					
					// IF REDEMPTION IS ON THEN COUNT AND SET REDEMPTION FLOW
					if(isset($_POST['redeem_credit']) && $_POST['redeem_credit'] == '1') {
						// SET REDEEMPTION IS ON
						$this->data['redeem_credit'] = '1';
						
						// CATEGORY WISE REDEMPTION LISTING
						$this->data['redeem_category'] = $this->getCategoryRedeem();
						
						// TOTAL REDEEMABLE AMOUNT
						$this->data['product_max_redemption'] = $this->order_driver->_count_redeemable_credit_point();
					} else {
						// SET THAT REDEEMPTION IS OFF
						$this->data['redeem_credit'] = '0';
					}
					
					// GET USER CURRENT CREDIT POINTS	
					$this->data['user_points'] = $this->common_model->_get_user_credit_points();
					
					// DECIDE 3RD PARTY PIXEL FIRE STATUS
					
					if ($this->session->userdata('pixel_3rd_party_fire_status') == "") {

						$pixel_3rd_party = $this->cart_driver->fire_pixel_3rd_party($_SESSION[USER_ORDER_ID]);

						if(is_array($pixel_3rd_party) && $pixel_3rd_party != false) {
							 
							 // SET SCRIPT IN VARIABLE
							 $this->data['pixel_3rd_party'] = $pixel_3rd_party['net_pub_script'];
							 $this->data['shopaaj_script'] = $pixel_3rd_party['shopaaj_script'];
							 
							 $this->session->set_userdata('pixel_3rd_party_fire_status', '1');
						}
					} else {
						$this->data['shopaaj_script'] = false;
						$this->data['pixel_3rd_party'] = false;
					}
					
					$this->data['cod_availability'] = true;
					
					// GET CART DATA FROM DATABASE
					foreach($cart_data->result() as $row) {
						
						$this->data['cart'][] = $row;
						
						$this->data['up_id'][]  = $row->cp_id;
						
						if ($row->cod_availability == '0') {
							$this->data['cod_availability'] = false;
							$cart_cod_availability = false;

						}
						$this->data['payment_mode'] = $row->payment_mode;
					}
					
					$val_users = $this->db->query("select * from ".USERS." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
					if($val_users->num_rows() > 0) {
						foreach ($val_users->result() as $val) {
							$this->data['user_details'] = $val;
						}
					}
					
				}
			} else {
				$this->data['cart'] = "order_not_available";
			}
		} else {
			$this->data['cart'] = "not_login";
		}
		
		$this->load_views();
	}
	 
	public function getCategoryRedeem() {
		$this->db->cache_off();
		
		$second_slab = '0';
		$res_user = $this->db->query("select cod_flag from ".USERS." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
		if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $val_user) {
				if ($val_user->cod_flag == '1') {
					$second_slab = '1';	
				}
			}
		}
		
		$max_redeem = $this->db->query("select name, max_redemption, cod_max_redemption from ".CATEGORY." where status = '1' and parent_cat_id = 0 and max_redemption > 0");
		if($max_redeem->num_rows() > 0) {
			$str = '<div class="cart-category-redeem">';
			foreach ($max_redeem->result() as $row) {
				
				if ($second_slab == '1') {
					$str .= '<div class="category_container left">
								<div class="category_name">'.$row->name.'</div>
								<div class="category_redeem">'.$row->cod_max_redemption.'</div>
							</div>';
				} else {
					$str .= '<div class="category_container left">
								<div class="category_name">'.$row->name.'</div>
								<div class="category_redeem">'.$row->max_redemption.'</div>
							</div>';
				}
			}
			$str .= '<div class="clr"></div></div>';
		}
		return $str;
	}
	
	public function convertOrd($chk = 0) {
		if ($_SESSION[USER_ORDER_ID] > 0) {
			if ($chk == 1) {
				$cod_availability = true;
				$this->db->cache_off();
				$cart_data = $this->db->query("SELECT
												o.id order_id,	up.product,		up.quantity,	
												p.cod_availability, o.payment_mode
											  FROM
												".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
												left join ".GENRE." g on (up.size = g.genre_id),
												".PRODUCT."	p left join ".PRODUCT_IMAGE." pi on (p.prod_id = pi.prod_id)
 											  WHERE
												o.id = ".$_SESSION[USER_ORDER_ID]." and 
												up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
												up.product = p.prod_id and 
												up.status = 1 and 
												p.is_verified = 1 and 
												p.status = 1 ");
				if($cart_data->num_rows() > 0) {
					foreach($cart_data->result() as $row) {
						if ($row->cod_availability == '0') {
							$cod_availability = false;
						}
					}
					
					if ($cod_availability == true) {
						$user_data = $this->db->query("update ".ORDER." set payment_mode = 2 where id = ".$_SESSION[USER_ORDER_ID]);		
						
						$this->db->cache_delete(__CLASS__, __FUNCTION__);
						$this->db->cache_delete(__CLASS__, "index");
						
						// SELECT THE PROVIDER AS PER PINCODE
						$available_provider = $this->general->_get_provider_from_pincode($_SESSION[SVAIZA_USER_ID], '2');
						if ($available_provider != false && is_numeric($available_provider)) {
							$this->general->_set_provider_order($available_provider, $_SESSION[USER_ORDER_ID]);
						}
						
						echo 'convrtd';
					} else {
						$user_data = $this->db->query("update ".ORDER." set payment_mode = 1 where id = ".$_SESSION[USER_ORDER_ID]);		
						
						// SELECT THE PROVIDER AS PER PINCODE
						$available_provider = $this->general->_get_provider_from_pincode($_SESSION[SVAIZA_USER_ID], '1');
						if ($available_provider != false && is_numeric($available_provider)) {
							$this->general->_set_provider_order($available_provider, $_SESSION[USER_ORDER_ID]);
						}
						
						echo 'onmrnoncod';	
					}
					
					
				} else {
					echo 'ordblank';
				}
			} else {
				$user_data = $this->db->query("update ".ORDER." set payment_mode = 1 where id = ".$_SESSION[USER_ORDER_ID]);
				echo 'onmrnoncod';
			}
		} else {
			echo 'ordblank';
		}
		exit;
	}
	
	public function set_cart_details() {
		if (isset($_POST['address']) && $_POST['address'] != '' && 
			isset($_POST['pincode']) && $_POST['pincode'] != '' && 
			isset($_POST['city']) 	 && $_POST['city'] 	  != '' && 
			isset($_POST['state']) 	 && $_POST['state']   != '' ) {
			if ($_SESSION[USER_ORDER_ID] > 0) {
				$res = $this->db->query("update ".ORDER." set 
														address = '".$_POST['address']."',
														pincode = '".$_POST['pincode']."',
														city = '".$_POST['city']."',
														state = '".$_POST['state']."'		
										 where id = ".$_SESSION[USER_ORDER_ID]);
										 
				
				if ($_SESSION[SVAIZA_USER_ID] > 0) {
					$res = $this->db->query("update ".USERS." set 
															address = '".$_POST['address']."',
															pincode = '".$_POST['pincode']."',
															city = '".$_POST['city']."',
															state = '".$_POST['state']."'		
											 where user_id = ".$_SESSION[SVAIZA_USER_ID]);
				}
				
				if ($_POST['redeem_point'] == 1) {
					echo SITE_URL.'payment/index/?r='.$_POST['redeem_point'];
				} else {
					echo SITE_URL.'payment/index/';
				}
			} else {
				echo 'refresh';
			}
		} else {
			echo 'leftblank';
		}
		exit;
	}
	
	public function get_cart_details() {
		
		if($this->_validate_sd_deal() == false)
		{
			echo '{"msg":"deal_error"}';
			exit(0);
		}
		
		$all_array = all_arrays();
		
		if ($_SESSION[USER_ORDER_ID] > 0) {
			$flag = false;
			$this->db->cache_off();
			$res = $this->db->query("select address, city, pincode, state from ".ORDER." where id = ".$_SESSION[USER_ORDER_ID]);
			if ($res->num_rows() > 0) {
				$address = $city = $pincode = $state = '';
				foreach ($res->result() as $val) {
					$address = $val->address;
					$city = $val->city;
					$pincode = $val->pincode;
					$state = $val->state;
					
					if (trim($val->address) == '') {
						$flag = true;
					}
					if (trim($val->city) == '') {
						$flag = true;
					}
					if (trim($val->pincode) == '') {
						$flag = true;
					}
					if (trim($val->state) == 0) {
						$flag = true;
					}
				}
			}
			if ($flag == true) {
				?>
                {"form":
				<form action="javascript:cart_payment_redirect('<?php echo $_POST['payurl'];?>', '<?php echo SITE_URL;?>', this)" method="post">
                <div class="form_cart_popup">
                    <div class="popup_header">Complete your details for the checkout</div>
                    <div class="popup_text">
                        <div class="left popup_text_title">Address</div>
                        <div class="right popup_text_textbox"><input type="text" class="quick-address" name="address" id="address" value="<?php echo $address;?>" placeholder="Address" /></div>
                        <div class="clr"></div>
                    </div>
                    <div class="popup_text">
                        <div class="left popup_text_title">Pincode</div>
                        <div class="right popup_text_textbox"><input placeholder="Pincode" class="quick-pincode"  maxlength="6" type="text" value="<?php echo $pincode;?>" id="pincode" name="pincode"  onblur="get_details_pincode('<?php echo SITE_URL."ajax/getpincode/index";?>')"  /></div>
                        <div class="clr"></div>
                    </div>
                    <div class="popup_text">
                        <div class="left popup_text_title">City</div>
                        <div class="right popup_text_textbox"><input placeholder="City" value="<?php echo $city;?>" class="quick-city"  type="text" id="city" name="city"></div>
                        <div class="clr"></div>
                    </div>
                    <div class="popup_text">
                        <div class="left popup_text_title">State</div>
                        <div class="right popup_text_textbox">
                            <select name="state" id="state" class="quick-state" >
                                <option value="">Select Option</option>
                            <?php
                                if (count($all_array['ARR_STATE']) > 0) {
                                    asort($all_array['ARR_STATE']);
                                    foreach ($all_array['ARR_STATE'] as $k => $v) {
                                        $selected = ($state == 1) ? 'selected' : '';
                                        echo "<option value='".$k."' ".$selected.">".$v."</option>";
                                    }
                                } else {
                                    echo "<option>Select Option</option>";
                                }
                            ?>
                            </select>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="popup_text">
                        <div class="left popup_text_title">&nbsp;</div>
                        <div class="right popup_text_textbox">
                            <input type="hidden" name="redeem_point" id="quick_redeem_point" value="<?php echo $_POST['redeem_point']; ?>" />
                            <input type="submit" name="submit" id="cartpopupsubmit" value="Save Details" /></div>
                        <div class="clr"></div>
                    </div>
                </div>
                </form>
                }
                <?php
			} else {
				if (isset($_POST['redeem_point']) && $_POST['redeem_point'] == '1') {
					echo '{"url":"'.$_POST['payurl'].'/?r=1&sbt=parsed"}';
				} else {
					echo '{"url":"'.$_POST['payurl'].'"}';	
				}
			}
		} else {
			echo '{"msg":"notlogin"}';
		}
		exit;
	}
	
		
	public function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-cart',$this->data);
		//$this->load->view(FRONT_INC.'zn-history');
		$this->load->view(FRONT_INC.'zn-footer');
	}
	
	private function _validate_sd_deal()
	{
		$this->db->cache_off();
		$res = $this->db->query("select p.prod_id, p.combo_product, p.is_combo
							from ".USER_PRODUCTS." up
								LEFT JOIN ".PRODUCT." p ON (p.prod_id = up.product) where up.order_id = ".$_SESSION[USER_ORDER_ID]." and up.status = 1");
		$arr_deal_prod_ids = array();
		$arr_prod_ids = array();
		$combo_count = 0;
		foreach ($res->result() as $val)
		{
			if($val->is_combo == 1)
			{
				if (isset($_POST['redeem_point']) && $_POST['redeem_point'] == 1) {
					return false;
				}
				$arr_deal_prod_ids[$val->prod_id] = explode(',',$val->combo_product);
				$combo_count++;
			}
			else
			{
				$arr_prod_ids[] = $val->prod_id;
			}
		}
		$validate_this = array();
		if(!empty($arr_prod_ids))
		{
			foreach($arr_prod_ids as $prod_id)
			{
				foreach($arr_deal_prod_ids as $deal_prod_ids)
				{
					if(in_array($prod_id,$deal_prod_ids))
					{
						$validate_this[$prod_id] = true;
					}
				}
			}
		}

		if (count($validate_this)!= $combo_count) { 
			return false;
		}
		return true;
	}
	
	
	public function paypending_order($order_id = 0) {
		
		$catid = '';
		$up_id = array();
		$new_cash_price = "";
		$all_array = all_arrays();

		$cart_data = $this->db->query("SELECT 
											o.id , o.offer_percentage, up.id as up_id, up.cash_price
										FROM
											".ORDER." o left join ".USER_PRODUCTS." up on  (o.id = up.order_id) 
										WHERE
											o.payment_status != 1 and 
											o.id = ".$order_id." and 
											o.user= ".$_SESSION[SVAIZA_USER_ID]." and
											up.status = 1");											

		if($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $v2) {
				
				if( $v2->offer_percentage != 0 ){
					
					$percentage = $all_array['ARR_OFFER_PER'][$v2->offer_percentage] ; 
					$p = explode("%",$percentage);	

					if($v2->up_id !="" && $v2->up_id > 0){

						$new_cash_price = ($v2->cash_price - ($v2->cash_price*$p[0])/100);
						
						if($new_cash_price != "" && $new_cash_price <  $v2->cash_price){

							$this->db->query("update ".USER_PRODUCTS." set cash_price = ".$new_cash_price." where id = ".$v2->up_id." and order_id = ".$v2->id);
						}

					}
				}
				
				$_SESSION[USER_ORDER_ID] = $order_id;	
			}

			
			// SET ORDER ID IN SESSION FOR FURTHER OPERATION
			
			
			redirect(SITE_URL.'cart');
		}
		
	}
}
