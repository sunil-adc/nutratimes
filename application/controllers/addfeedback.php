<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addfeedback extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
	
			
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		

		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'login');
		}

		// CHECK USER IS LOGIN OR NOT - END
	}
	
	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		$query = $this->db->query("SELECT
										name, email, mobile, address, city, state, pincode, points, gender
									FROM
									 	".USERS."
									WHERE
										status = 1 and 
										user_id = '".$_SESSION[SVAIZA_USER_ID]."'");
		
		// GET THE PRODUCT DETAILS
	
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p) {
				$data['user_details'] = $p;
			}
		} else {
			redirect(SITE_URL."login");
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('add-feedback',$data);
		$this->load->view(FRONT_INC.'footer');
	}
	
	public function save() {
		
		$this->form_validation->set_rules('feedback_suggestion', 'Feedback', 'required');
		$this->form_validation->set_rules('comments', 'Comments', 'required');
		$this->form_validation->set_rules('order_id', 'Order ID', 'required|numeric');
	
		
		
		if ($this->input->post('submit')) {
			if ($this->form_validation->run() != false) {
				// SET THE WHERE CLAUSE
				$this->db->where("user_id",$_SESSION[SVAIZA_USER_ID]);
				// SET THE DATA
				$data = array(
								'suggestion' => $this->input->post('feedback_suggestion'),
								'comments' => $this->input->post('comments'),
								'order_id' => $this->input->post('order_id'),
								'user_id' => $_SESSION[SVAIZA_USER_ID],
								'mobile' => $_SESSION[SVAIZA_MOBILE],
								'email' => $_SESSION[SVAIZA_EMAIL]								
							);
				// Insert QUERY
				
							
				$this->db->insert(FEEDBACK,$data);
				
				$this->session->set_flashdata('success', 'Feedback added successfully');
				
				$this->db->cache_delete(CMS_FOLDER_NAME, 'managefeedback');
				$this->db->cache_delete('addfeedback', 'addfeedback');

			
						
				
			} else {
				$this->session->set_flashdata('error', '(*) denotes mandatory fields');
			}
		}
		redirect(SITE_URL.'addfeedback');
	}
}