<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class invitefriends extends CI_Controller {
	protected $data = array();
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		$this->load->model(array(CMS_FOLDER_NAME.'/db_function'));
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		//LOAD LIBERARY
		$this->load->library(array('GmailOath','GmailGetContacts'));
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'login');
		
		}

	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		//INVITE FRIENDS API
		$sClientId = '248727174394.apps.googleusercontent.com';
		$sClientSecret = 'aPRL92a9U89Y3lZP5rcgA7ll';
		$sCallback = 'http://www.shophunk.com/invitefriends'; // callback url, don't forget to change it to your!
		$iMaxResults = 100; // max results
		$this->data['sStep'] = 'auth'; // current step
		$argarray = '' ; 		
		
		// prepare new instances of GmailOath  and GmailGetContacts
		$oAuth = new GmailOath($sClientId, $sClientSecret, $argarray, false, $sCallback);
		
		$oGetContacts = new GmailGetContacts();
		
		if ($_GET && $_GET['oauth_token']) {
		    
			$this->data['sStep'] = 'fetch_contacts'; // fetch contacts step
		
			// decode request token and secret
			$sDecodedToken = $oAuth->rfc3986_decode($_GET['oauth_token']);
			$sDecodedTokenSecret = $oAuth->rfc3986_decode($_SESSION['oauth_token_secret']);
		
			// get 'oauth_verifier'
			$oAuthVerifier = $oAuth->rfc3986_decode($_GET['oauth_verifier']);
		
			// prepare access token, decode it, and obtain contact list
			$oAccessToken = $oGetContacts->get_access_token($oAuth, $sDecodedToken, $sDecodedTokenSecret, $oAuthVerifier, false, true, true);
			$sAccessToken = $oAuth->rfc3986_decode($oAccessToken['oauth_token']);
			$sAccessTokenSecret = $oAuth->rfc3986_decode($oAccessToken['oauth_token_secret']);
			$aContacts = $oGetContacts->GetContacts($oAuth, $sAccessToken, $sAccessTokenSecret, false, true, $iMaxResults);
		    // turn array with contacts into html string
			 $sContactName = '';
			 $sContacts = array();
			foreach($aContacts as $k => $aInfo) {
				$aLast = end($aContacts[$k]);
				foreach($aLast as $aEmail) {
				$this->data['sContacts'][]=  $aEmail;
				//echo $contacts = $aEmail['address'];
				}
			}
		} else {
			
			// prepare access token and set it into session
			$oRequestToken = $oGetContacts->get_request_token($oAuth, false, true, true);
			$_SESSION['oauth_token'] = $oRequestToken['oauth_token'];
			$_SESSION['oauth_token_secret'] = $oRequestToken['oauth_token_secret'];
			$this->data['oRequestToken'] = $oRequestToken;
		    
		}
        
		$this->data['oAuth'] = $oAuth;
        //INVITE FRIENDS API END
		
		// LOAD ALL VIEWS
		$this->load_views();
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('invite-friends',$this->data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
}
