<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class brand extends CI_Controller {
	protected $data;
		
	function __construct(){
		parent::__construct();
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		$brand = $this->db->query("select seourl, image, name, brand_id from ".BRANDS." where status = '1'");
		if($brand->num_rows() > 0) {
			foreach($brand->result() as $row) {
				$this->data['brand'][] = $row;
			}
		} else {
			$this->data['brand'] = false;
		}

		$this->load_views();
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('brand',$this->data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
}
