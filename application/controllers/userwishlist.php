<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class userwishlist extends CI_Controller {
	protected $data = array();
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		// CHECK USER IS LOGIN OR NOT
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		// FETCH ORDER HISTORY
		$this->replica_db->cache_off();
		$order_history = $this->replica_db->query("SELECT 
												pi.image,
												pi.prod_id,
												w.product_id,
												w.user_prod_id,
												w.size,
												w.status,
												w.id,
												p.name,
												p.sale_price,
												p.seourl
										   FROM 
												".WISHLIST." w 
												inner join ".PRODUCT_IMAGE." pi on(w.product_id = pi.prod_id)
												inner join ".PRODUCT." p on(p.prod_id = w.product_id) 
										   WHERE  
												w.status != '0' AND
												w.user_id = ".$_SESSION[SVAIZA_USER_ID]." 
										   GROUP BY 
												w.id");
	
		if($order_history->num_rows() > 0) {
			foreach ($order_history->result() as $row) {
				$this->data['order_history'][] = $row;
			}
		} else {
			$this->data['order_history'] = false;
		}
		
		// LOAD ALL VIEWS
		$this->load_views();
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('user-wishlist',$this->data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
}
