<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hairoil extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'), $this->input->get('utm_source'),
										  $this->input->get('utm_campaign'), $this->input->get('pub_id') );
	

	}
	
	public function lp1(){
	
		$this->load->view('hairoil-lp1',$data);
		
	}

	public function lp2(){
	
		$this->load->view('hairoil-lp2',$data);
		
	}

	public function lp3(){
		
		$this->load->view('hairoil-lp3',$data);
		
	}
}