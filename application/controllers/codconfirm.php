<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class codConfirm extends CI_Controller {
	protected $data = array();
	protected $cacheID;
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('order_driver', 'cart_driver', 'general'));
		$this->load->helper('form');
		$this->load->library(array('recaptcha', 'form_validation'));
		
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
		
		if ($_SESSION[USER_ORDER_ID] == '' || $_SESSION[USER_ORDER_ID] <= 0) {
			redirect(SITE_URL."cart");
		}
		
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
	}
	
	public function index($is_ajax = 0) {
		
		// GENERATE CAPTCHA
		$this->data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
		
		// CHECK USER IS LOGIN OR NOT
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			// CHECK ORDER IS SET OR NOT
			if($_SESSION[USER_ORDER_ID] != "") {
				$cart_data = $this->db->query("SELECT
												o.user,			o.name,			o.email,		o.mobile,
												o.address,		o.city,			o.state,		o.alternate_phone,
												o.pincode,		o.id order_id,	up.product,		up.quantity,	
												up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
												up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
												p.name,			p.model_no,		p.seourl,		pi.image, 
												g.genre_name,	p.cod_availability
											  FROM
												".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
												left join ".GENRE." g on (up.size = g.genre_id),
												".PRODUCT."	p left join ".PRODUCT_IMAGE." pi on (p.prod_id = pi.prod_id)
 											  WHERE
												o.id = ".$_SESSION[USER_ORDER_ID]." and 
												up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
												up.product = p.prod_id and 
												up.status = 1 and 
												p.is_verified = 1 and 
												p.status = 1 and 
												p.cod_availability = '1' and 
												pi.isdefault = 1");
				if($cart_data->num_rows() > 0) {
					
					// SET VARIABLE FALSE
					$this->data['is_contest'] = $this->data['is_product'] = false;
					
					// CHECK WHICH TYPE OF ITEMS ARE AVAILABLE IN CART PRODUCT OR CONTEST
					$product_type = $this->common_model->_decide_product_type();
					
					// AFTER CHECKING SET THE VARIABLE
					$this->data['is_product'] = $product_type['is_product'];
					$this->data['is_contest'] = $product_type['is_contest'];
					
					/*// IF REDEMPTION IS ON THEN COUNT AND SET REDEMPTION FLOW
					if(isset($_POST['redeem_credit']) && $_POST['redeem_credit'] == '1') {
						// SET REDEEMPTION IS ON
						$this->data['redeem_credit'] = '1';
						
						// CATEGORY WISE REDEMPTION LISTING
						$this->data['redeem_category'] = $this->getCategoryRedeem();
						
						// TOTAL REDEEMABLE AMOUNT
						$this->data['product_max_redemption'] = $this->order_driver->_count_redeemable_credit_point();
					} else {
						// SET THAT REDEEMPTION IS OFF
						$this->data['redeem_credit'] = '0';
					}
					
					// GET USER CURRENT CREDIT POINTS	
					$this->data['user_points'] = $this->common_model->_get_user_credit_points();
					*/
					// GET CART DATA FROM DATABASE
					foreach($cart_data->result() as $row) {
						$this->data['cart'][] = $row;
					}
					$val_users = $this->db->query("select * from ".USERS." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
					if($val_users->num_rows() > 0) {
						foreach ($val_users->result() as $val) {
							$this->data['user_details'] = $val;
						}
					}
					
				}
			} else {
				$this->data['cart'] = "order_not_available";
			}
		} else {
			$this->data['cart'] = "not_login";
		}
		
		//$this->load_views();
	}
	
	public function load_views() {
		$this->load->view('codconfirm', $this->data);
	}
	
	public function confirmorder() {
		
		// CHECK USER IS LOGIN OR NOT
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL]) && $_SESSION[USER_ORDER_ID] != '') {
		
		$cod_availability = true;
		$cart_data = $this->db->query("SELECT
												o.user,			o.name,			o.email,		o.mobile,
												o.address,		o.city,			o.state,		o.alternate_phone,
												o.pincode,		o.id order_id,	up.product,		up.quantity,	
												up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
												up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
												p.name,			p.model_no,		p.seourl,		pi.image, 
												g.genre_name,	p.cod_availability
											  FROM
												".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
												left join ".GENRE." g on (up.size = g.genre_id),
												".PRODUCT."	p left join ".PRODUCT_IMAGE." pi on (p.prod_id = pi.prod_id)
 											  WHERE
												o.id = ".$_SESSION[USER_ORDER_ID]." and 
												up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
												up.product = p.prod_id and 
												up.status = 1 and 
												p.is_verified = 1 and 
												p.status = 1");
			if($cart_data->num_rows() > 0) {
				
				
				if($this->validate_sd_deal() == false)
				{
					$this->svaiza_email->_send_error_cart_messsage();
					$this->session->set_flashdata('error', 'Super duper deal can\'t be proceed without selecting product from the list.');
					redirect(SITE_URL.'cart');
					exit;
				}
				
				$total_amount = 0;
				
				foreach($cart_data->result() as $row) {
					$total_amount = ($row->cod_price * $row->quantity);
					if ($row->cod_availability == '0') {
						$cod_availability = false;
					}
					if ($row->pincode != '') { 
						$user_pincode = $row->pincode;
					}
				}
				
				if ($cod_availability == true) {
					
					$this->db->query("update ".ORDER." set payment_status = 0, payment_mode = 2, caller_status = 9 where id = ".$_SESSION[USER_ORDER_ID]);
					
					// MINUS STOCK IF REQUIRE
					$this->cart_driver->product_stock_minus();
					
					// SEND EMAIL TO USER
					$this->svaiza_email->email_waiting_order_confirmation($_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID], 2);
					
					if ($user_pincode != '') {
						$cod_provider = $this->general->_payment_mode_wise_provider($user_pincode, 2);
						$this->general->_set_provider_order($cod_provider, $_SESSION[USER_ORDER_ID]);
					}
					
					redirect(SITE_URL.'paidconfirm/index/'.$_SESSION[USER_ORDER_ID]);
				} else {
					$this->db->query("update ".ORDER." set payment_status = 0, payment_mode = 1 where id = ".$_SESSION[USER_ORDER_ID]);
					$this->session->set_flashdata('error', 'You dont have cod products in your cart');
					redirect(SITE_URL.'cart');
				}
			} else {
				$this->session->set_flashdata('error', 'You dont have cod products in your cart');
				redirect(SITE_URL.'cart');
			}
		}
	}
	
	public function validate_sd_deal()
	{
		$this->db->cache_off();
		$res = $this->db->query("select p.prod_id, p.combo_product, p.is_combo
							from ".USER_PRODUCTS." up
								LEFT JOIN ".PRODUCT." p ON (p.prod_id = up.product) where up.order_id = ".$_SESSION[USER_ORDER_ID]." and up.status = 1");
		$arr_deal_prod_ids = array();
		$arr_prod_ids = array();
		$combo_count = 0;
		foreach ($res->result() as $val)
		{
			if($val->is_combo == 1)
			{
				if ($this->input->post('redeem_point') == 1) {
					return false;
				}
				$arr_deal_prod_ids[$val->prod_id] = explode(',',$val->combo_product);
				$combo_count++;
			}
			else
			{
				$arr_prod_ids[] = $val->prod_id;
			}
		}
		$validate_this = array();
		if(!empty($arr_prod_ids))
		{
			foreach($arr_prod_ids as $prod_id)
			{
				foreach($arr_deal_prod_ids as $deal_prod_ids)
				{
					if(in_array($prod_id,$deal_prod_ids))
					{
						$validate_this[$prod_id] = true;
					}
				}
			}
		}

		if (count($validate_this)!= $combo_count) { 
			return false;
		}
		return true;
	}
}