<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class returnorders extends CI_Controller {
	protected $data = array();
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		// CHECK USER IS LOGIN OR NOT
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		// FETCH ORDER HISTORY
		$this->replica_db->cache_off();
		$order_returns = $this->replica_db->query("SELECT 
												up.order_id, 
												sum(up.quantity * up.cash_price + up.shipping) order_total,
												count(up.id) total_items,
												o.dt_c 
										   FROM 
												".ORDER." o inner join ".USER_PRODUCTS." up on(o.id = up.order_id) 
										   WHERE 
												o.user = '".$_SESSION[SVAIZA_USER_ID]."' and 
												o.delivery_status = '4'
										   GROUP BY 
												up.order_id");
	
		if($order_returns->num_rows() > 0) {
			foreach ($order_returns->result() as $row) {
				$this->data['order_returns'][] = $row;
			}
		} else {
			$this->data['order_returns'] = false;
		}
		
		// LOAD ALL VIEWS
		$this->load_views();
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-return-orders',$this->data);
		$this->load->view(FRONT_INC.'zn-footer');
	}
}
