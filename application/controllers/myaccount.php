<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class myaccount extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
										  
		// SEND NETWORK AND PUB ID - START
	
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'/logout');
		}
		// CHECK USER IS LOGIN OR NOT - END
	}
	
	public function index() {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		
		// SELECT PRODUCT DETAILS
		$this->db->cache_off();
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-myaccount',$data);
		//$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'zn-footer');
	}
}