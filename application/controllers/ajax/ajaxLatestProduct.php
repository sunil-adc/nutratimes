<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxLatestProduct extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);
	}
	
	public function index($cat_id = 0) {
				
		if ($cat_id > 0) {
			// LATEST PRODUCTS - START
			$latest_products = $this->replica_db->query("select 
												p.seourl,
												p.brand,
												p.cod_availability, 
												p.prod_id, 
												i.image, 
												p.name, 
												p.mrp,
												p.sale_price
											from 
												".PRODUCT." p 
												left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id) 
											where 
												p.status = 1 and 
												p.oos = 0 and 
												i.isdefault = 1 and 
												p.cat_id = ".$cat_id."  
											group by 
												p.prod_id,
												p.sub_cat_id 
											order by 
												p.datecreated desc
											limit 0,2");
											
			$latest_product_ads = '';
			if($latest_products != false) {
				
				foreach ($latest_products->result() as $r) {
					$latest_product_ads .= $r->prod_id.",";
				 ?>
				 <div class="u-mayalso-product-wrap">
				   <div class="img">
					  <a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$r->seourl;?>">
						<img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$r->image);?>" title="<?php echo $r->name;?>" alt="<?php echo $r->name;?>" />
					  </a>
				   </div>
				   <div class="recommand-desc">
					   <a class="img-text" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$r->seourl;?>"><?php echo $r->name;?></a>
					   <div class="clr"></div>
					   <a class="rs" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$r->seourl;?>"><?php echo _r($r->sale_price);?></a>
				   </div>
				 </div>
				 <?php
				}
				$latest_product_ads = trim($latest_product_ads,",");
			
			}
		}
		exit;
	}
}