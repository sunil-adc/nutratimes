<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class getpincode extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		$this->load->helper('form');
		$this->load->library(array('form_validation'));

	}
	public function index() {
		
		if (isset($_POST['pincode']) && trim($_POST['pincode'])) {
		
			$pincode = $_POST['pincode'];
	
			$this->db->cache_off();
			$res = $this->db->query("select pincode, state, city from ".DB_PINCODE." where pincode = ".$pincode);	
			if ($res->num_rows() == 0) {
			
					$ch = curl_init("http://maps.google.com/maps/api/geocode/json?address=$pincode&components=country:in&sensor=false");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					$result = curl_exec($ch);
					
					$city = $state = $country = '';
					if($result){
						$curr_json=json_decode($result,true);
						
						if ($curr_json['status'] == 'OK') {
							
							foreach($curr_json as $val){
								if(is_array($val)){						
									foreach($val as $in_val){
										foreach($in_val['address_components'] as $out_val){
							
											if(isset($out_val['types'][0])){
												if($out_val['types'][0] == 'administrative_area_level_2') {
													if(stripos($out_val['long_name']," ") > 0) {
														$city = substr($out_val['long_name'],0,stripos($out_val['long_name']," ")); 
													} else {
														$city = $out_val['long_name'];
													}
												} else if ($out_val['types'][0] == 'administrative_area_level_1') {
													$state = $out_val['long_name'];
												} else if ($out_val['types'][0] == 'country'){
													$country = $out_val['long_name'];
												}
											}
										}							
									}
								}
							}
							
							if (trim($city) != '' || trim($country) != '') {
								$data = array(
									'pincode' => $pincode,
									'city' => $city,
									'state' => $state,
									'country' => $country,
									'formatted_address' => $curr_json['results'][0]['formatted_address'],
									'northeast_lat' => $curr_json['results'][0]['geometry']['bounds']['northeast']['lat'],
									'northeast_long' => $curr_json['results'][0]['geometry']['bounds']['northeast']['lng'],
									'southwest_lat' => $curr_json['results'][0]['geometry']['bounds']['southwest']['lat'],
									'southwest_long' => $curr_json['results'][0]['geometry']['bounds']['southwest']['lng'],
									'lat' => $curr_json['results'][0]['geometry']['location']['lat'],
									'long' => $curr_json['results'][0]['geometry']['location']['lng'],
									'datecreated' => date('Y-m-d H:i:s')
								);
								
								$this->db->insert(DB_PINCODE, $data);
							}
						}
					}
					$all_arrays = all_arrays();
					$state_id = array_search($state ,$all_arrays['ARR_STATE']);		
					echo $city.'|'.$state_id.'|'.$country;
				} else {
					$all_arrays = all_arrays();
					foreach ($res->result() as $val) {
						$state_id = array_search($val->state,$all_arrays['ARR_STATE']);		
						echo $val->city.'|'.$state_id.'|';
					}
				}
		}
		exit;
	}
}