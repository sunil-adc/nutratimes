<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class wishList extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
	}
	public function wishlist($is_ajax=0) {
		
		if($is_ajax=1) {
			
			$prod_id = $this->input->post('prod');
            $usr_id = $this->input->post('u_id');
			$usr_pro_id = $this->input->post('up_id');
			$size = $this->input->post('size');		
			$status =0;
			$data_arr ="";
			
			
			if($usr_pro_id >0){
				
				$data =$this->db->query("update ".USER_PRODUCTS." set status = ".$status." where id = ".$usr_pro_id);
				//--//Koushik 18-Dec-2014. Removing product ID from session
				$arr_cart_urls = $this->memcache_driver->get_cache('cart_'.$_SESSION[SVAIZA_USER_ID]);
				unset($arr_cart_urls[$prod_id]);
				$this->memcache_driver->set_cache('cart_'.$_SESSION[SVAIZA_USER_ID],$arr_cart_urls);
				
				$check_for_duplicates = $this->db->query("select id,status from ".WISHLIST." where user_id = '$usr_id' and product_id = '$prod_id' and user_prod_id = '$usr_pro_id'");
				
				if($check_for_duplicates->num_rows() == 0)
				{
					$data_arr = array(
								'user_id'      => $usr_id,
								'product_id'   => $prod_id,
								'user_prod_id' => $usr_pro_id,
								'size'         => $size,
								'status'         => '1'
							);
							
					$data_result = $this->db->insert(WISHLIST,$data_arr);	
					echo "inserted";			
		
				} else {
					foreach($check_for_duplicates->result() as $val){
						if($val->status == 0){
							$this->db->query("update ".WISHLIST." set status = 1 where id='".$val->id."'");
						}
					}
				}
			}

		}
		
		exit;		
		}
}