<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxHistory extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index() {
		
		$arr_product_history = unserialize($this->session->userdata('product_history'));
		if(isset($arr_product_history)) {
			if(is_array($arr_product_history)) {
				
				$arr_product_history = array_reverse($arr_product_history);
				$history_string = "";
				
				// TAKE 5 ELEMENT AND REMOVE REST
				foreach ($arr_product_history as $khk => $vhv) {
					if ($khk > 4)
					   unset($arr_product_history[$khk]);
				}
				
				foreach ($arr_product_history as $hk => $hv) {
					if($hv != "")
						$history_string .= "'".$hv."',";
				}
				
				 $history_query = $this->db->query("SELECT
														i.`image`,				p.`prod_id`,			p.`name`,				
														p.`seourl`,				p.`cod_price`,			p.`sale_price`,			
														p.`contest`,			p.`oos`,				p.`status`,				
														p.`mrp`,				p.`cod_availability`
													FROM
														".PRODUCT." p left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id)
													WHERE
														p.status = 1 and 
														p.oos != 1 and 
														i.isdefault = 1 and 
														p.prod_id in (".trim($history_string,',').")
													group by 
														p.prod_id");
				if($history_query->num_rows() > 0) {	
				?>
				<div class="full-wrapper">
				  <h1 class="recent-history-head">Recent History</h1>
				  <ul class="wrap-products-recent">
					<?php
						$i = 1;
						foreach ($history_query->result() as $v) {
						?>
						<li <?php echo ($i%5 == 0) ? "class='last-li'" : "";?>> 
							<a class="photo-link" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" title="<?php echo $v->name?>">
							<?php echo disc_percent($v->mrp, $v->sale_price); ?>
							<img border="0" alt="<?php echo $v->name?>" src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_200_300_PATH.$v->image)?>"> </a>
							<div class="clr"></div>
							<a class="item-name" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" title="<?php echo $v->name?>"><?php echo sub_string($v->name,45)?></a>
							<div class="clr"></div>
							<?php echo sel_price($v->mrp, $v->sale_price);?>
							<?php
								if (isset($v->cod_availability) && $v->cod_availability == '1') {
									?>
									<label class="cash-delivery-icon"><a title="COD Available on this item." href="javascript:void(0)">
									<img src="https://shophunk.s3.amazonaws.com/images/cod-serch.png"></a></label>
									<?php
								}
							?>
							<div class="clr"></div>
							<a class="buy-button" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" data-id="<?php echo $v->prod_id;?>">Buy Now</a>
						</li>
						<?php
							$i++;
						}
					?>
				  </ul>
				</div>
				<div class="clr"></div>
				<?php
				}
			}
		}
		exit;
	}
}