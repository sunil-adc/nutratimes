<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxFooter extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);

	}
	
	public function index() {
		?>
		<div class="footer">
              <div class="testimonial">
                    <ul class="cntrWrp950 bgBlue Testimonial">
                        <li>
                            <label class="heading1">Testimonials</label>
                            <img class="userImg" src="https://shophunk.s3.amazonaws.com/images/user-1.png">
                            <div class="details">
                                <label class="custName">Rohit</label>
                                <label class="city">Mumbai</label>
                                <p>"hassle free and breakage free delivery of my consignment in time and in perfect packing speaks volumes about the working in shophunk.it is no doubt a perfect and trustworthy shopping site."</p>
                                <!-- <a href="#">More Testimonails</a>-->
                            </div>
                        </li>
                        <li>
                            <label class="heading1"><!-- Winner of the month --></label>
                            <img class="userImg" src="https://shophunk.s3.amazonaws.com/images/user-1.png">
                            <div class="details">
                                <label class="custName">Vikram Singh</label>
                                <label class="city">Bangalore</label>
                                <p>"Got the Apple Iphone in less than a week,packing was excellent.shophunk.com Rocks.."</p>
                               <!--  <a href="#">More Winners</a> -->
                            </div>
                        </li>
                    </ul>
              </div>
           <div class="news-letter-pay">
                <div class="news-letter">
                    <p class="new-head"><b>Sign up for our newsletter</b><br /></p>
                   
                    <input type="text" placeholder="Your E-mail Address" class="new-src" id="nws_emailaddress" data-siteurl="<?php echo SITE_URL?>" name="email" />
                    <div>
                        <input class="sub-btn-ne newsletter-two-submit" type="button" data-value="0" value="For Female" gender="female" />
                        <input class="sub-btn-ne newsletter-two-submit" type="button" data-value="1" value="For Male" gender="male" />
                    </div>
                </div>
                <div class="pay-method">
                        <dl class="wrp-pay-nws">
                            <div class="dt-payship"><b>Payment Methods</b></div>
                            <dd>
                                <a class="payMethod visa" href="#" style="cursor:default;"></a>
                                <a class="payMethod master" href="#" style="cursor:default;"></a>
                                <a class="payMethod netBank" href="#" style="cursor:default;"></a>
                                <a class="payMethod cod" href="#" style="cursor:default;"></a>
                                <a class="payMethod ruppay" href="#" style="cursor:default;"></a>
                            </dd>
                        </dl>
                            <dl class="wrp-pay-nws">
                            <div class="dt-payship"><b>Shipping Partner</b></div>
                            <dd>
                                <a class="shipping armx" href="#" style="cursor:default;"></a>
                                <a class="shipping ff" href="#" style="cursor:default;"></a>
                                <a class="shipping dtdc" href="#" style="cursor:default;"></a>
                                <a class="shipping bludrt" href="#" style="cursor:default;"></a>
                            </dd> 
                        </dl>
                </div>
             </div>
        <div class="footer-linkn" style="background:none;">
          <div class="footer-morelink-wrp">
            <ul class="footer-data-links-wrp">
              <li class="list-1-3">
                <dl>
                  <dt><?php echo SITE_NAME;?></dt>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/aboutus">About Us</a></dd>
                  <!--<dd><a class="font_12px" href="<?php echo SITE_URL?>search?&c=4">Best Deals</a></dd>-->
                  <dd><a class="font_12px" href="<?php echo SITE_URL?>info/winners">winner list</a></dd>
                 <!-- <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/refundpolicy">How to refund my money?</a></dd>-->
                  <!--<dd><a href="<?php echo SITE_URL;?>info/feedback">Feedback</a></dd>
                  <dd><a href="<?php echo SITE_URL;?>info/sitemap">Sitemap</a></dd>-->
                  
                </dl>
              </li>
              <li class="list-1-3">
                <dl>
                  <dt>Contact Us</dt>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/returnpolicy">Shipping and cancellations</a></dd>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/contactus">Contact Us</a></dd>
                  <dd><a class="font_12px" href="#login_form" id="login_pop">Refunds</a></dd>                  
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>addfeedback">Feedback</a></dd>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>addcomplaints">Complaints</a></dd>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/affilate">Affilate</a></dd>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/faq">FAQ</a></dd>
                </dl>
              </li>
              <li class="list-last">
                <dl class="wrp-pay-nwsl">
                  <dt>Our Policies</dt>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/returnpolicy">Return Policy</a></dd>
                   <!--<dd><a class="font_12px" href="<?php echo SITE_URL;?>info/refundpolicy">Refund Policy</a></dd>-->
                   <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/privacypolicy">Privacy Policy</a></dd>
                  <dd><a class="font_12px" href="<?php echo SITE_URL;?>info/terms_of_service">Terms of use</a></dd>
                 
                </dl>
              </li>
              
              <li class="list-1-3">
                <dl>
                  <dt>Categories</dt>
                  <?php
                    $is_popular = $this->replica_db->query("select cat_id, name, seourl from ".CATEGORY." where is_popular = 1 limit 0, 6");
                    if($is_popular->num_rows() > 0) {
                        foreach($is_popular->result() as $cat_footer) {
                            echo '<dd><a class="font_12px" href="'.SITE_URL.'search?&c='.$cat_footer->cat_id.'">'.$cat_footer->name.'</a></dd>';		
                        }
                    }
                  ?>
                </dl>
              </li>
              
            </ul>
                
          </div>
        </div>
        <div class="social">
          <div class="social_netwrk_wrp">
            <div class="social_lhs"> © 2011-<?php echo date('Y');?>, <?php echo SITE_NAME?>.com, All Rights Reserved </div>
            <!--<div class="social_rhs">
              <ul>
                <li>
                  <p>Join Us on :</p>
                </li>
                <?php
                   $val = $this->replica_db->query("SELECT attr_name, attr_value from ".SETTINGS." where attr_name in ('fb_url', 'twt_url', 'pin_url', 'ytube_url')");
                   if($val->num_rows() > 0) {
                        foreach ($val->result() as $row) {
                            $footer_settings[$row->attr_name] = $row->attr_value;
                        }
                   }
                   if($footer_settings['fb_url'] != "") {
                        echo '<li><a class="b-fb" href="'.$footer_settings['fb_url'].'" target="blank"></a></li>';   	
                   }
                   if($footer_settings['twt_url'] != "") {
                        echo '<li><a class="b-twt" href="'.$footer_settings['twt_url'].'" target="blank"></a></li>';   	
                   }
                   if($footer_settings['pin_url'] != "") {
                        echo '<li><a class="b-pt" href="'.$footer_settings['pin_url'].'" target="black"></a></li>';   	
                   }
                   if($footer_settings['ytube_url'] != "") {
                        echo '<li><a class="b-ytb" href="'.$footer_settings['ytube_url'].'" target="black"></a></li>';   	
                   }
                ?>
              </ul>
            </div>-->
          </div>
        </div>
    <div class="clr"><!--social ::end--></div>
<a href="#x" class="overlay1" id="login_form"></a>
<div class="popup">
<img src="https://shophunk.s3.amazonaws.com/images/popup.jpg">
<a class="close1" href="#close1"></a>
</div>
  </div>
  		<?php
		exit;
	}
}