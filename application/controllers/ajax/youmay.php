<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class youmay extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);

	}
	
	public function index($sub_cat_id = 0, $contest = 0) {
    	
		if ($sub_cat_id > 0) {
		
		$arr_combo_products = false;
			
		// COMBO PRODUCTS - START
		$combo_products = $this->replica_db->query("SELECT	
											i.`image`,		p.`prod_id`,
											p.`name`,		p.`cod_price`,
											p.`sale_price`,	p.seourl,   
											p.mrp,			p.is_size,
											p.check_inventory
											
									  FROM
											".PRODUCT." p left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id)
									  WHERE
											p.sub_cat_id = ".$sub_cat_id." and 
											i.isdefault = 1 and 
											p.status = '1' and 
											p.oos != '1' and 
											p.is_verified = 1 and 
									  		p.is_size != 1 
									  group by 
									  		p.prod_id
									  order by
									  		rand()
									  limit 
									  		0, 2");
		
		if ($combo_products->num_rows() > 1) {
			$ick = 0;
			foreach ($combo_products->result() as $r) {
				$arr_combo_products[] = $r;
				$ick++;
			}
			
			if ($ick < 1) {
				$arr_combo_products = false;	
			}
		}
		
		if ($contest == 0 && is_array($arr_combo_products) && $arr_combo_products != false) {
			?>
			<div class="clr"></div>
			<div class="ui-box-full-w">
				<h1 class="boughttogether"><label>Bought <span>Together</span></label></h1>
				<ul class="combo-product">   
			  <?php 
				  $saleprice =0; 
				  $mrp_price =0;
				  $i = 1;
				  foreach($arr_combo_products as $v){
						  $combo_prod_id[] = $v->prod_id; 
						  $mrp_price += $v->mrp; 
						  $saleprice += $v->sale_price;
			   ?> 
				<li>
				<a class="photo-link-combo" href="<?php echo SITE_URL;?>/product/details/<?php echo $v->seourl;?>"><img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$v->image);?>" title="Women pink Leggings" alt="Women pink Leggings">
				</a>
				 <div class="clr"></div>
				 <a class="item-name-combo" href="<?php echo SITE_URL;?>/product/details/<?php echo $v->seourl;?>" title="Women pink Leggings"><?php echo $v->name;?></a>
				 <div class="clr">
				 </div>
				 <label class="item-price-combo"><?php echo _r($v->sale_price);?></label>
				<div class="clr"></div>
			 </li>
			   <?php if($i == 1) {
					 echo "<li class='plus'>+</li>";
					   }
					 $i++;
			   } ?> 
			  
			   
		 <li class="combo-price">
			  <div class="offer-btn">Combo Offer</div>
				 <div class="price-box">
					 <div class="mrp-price-combo">MRP <?php echo $mrp_price;?>/-</div>
				   <div class="rs-price"><?php echo _r($saleprice);?>/-</div>
						<form>
						 <?php  
						  $combo_prod_arr = serialize($combo_prod_id);
						  $combo_encode = urlencode($combo_prod_arr);
						  
						  if(is_user_login($_SESSION[SVAIZA_USER_ID],
								  $_SESSION[SVAIZA_MOBILE],
								  $_SESSION[SVAIZA_EMAIL])) {
							?><a href="javascript:add_cart('add','<?php echo $combo_encode?>','<?php echo SITE_URL?>');scroll_div('header')" class="buynow-btn" data-sizeid="" id="product-buy-now">Add 2 Item</a><?php 
							}else{
								?><a href="javascript:add_cart('add','<?php echo $combo_encode?>','<?php echo SITE_URL?>');scroll_div('header')" class="buynow-btn" data-sizeid="" id="product-buy-now">Buy 2 Items</a><?php 
							}
							?>
					 </form>
				 </div> 
			   </li>
			 </ul>
			</div>
			<?php
		}
		// COMBO PRODUCTS - END
		   
		    $recommand = $this->replica_db->query("SELECT	
                                                p.cod_availability, 
                                                i.`image`,		p.`prod_id`,
                                                p.`name`,		p.`cod_price`,
                                                p.`sale_price`,	p.seourl
                                          FROM
                                                ".PRODUCT." p left join ".PRODUCT_IMAGE." i on (p.prod_id = i.prod_id)
                                          WHERE
                                                p.sub_cat_id = ".$sub_cat_id." and 
                                                i.isdefault = 1 and 
                                                p.status = '1' and 
                                                p.oos != '1' and 
                                                p.is_verified = 1 
                                          group by 
                                                p.prod_id
                                          order by
                                                rand()
                                          limit 
                                                0, 4");
            // RECOMMANDED PRODUCTS - END
            
            if ($recommand->num_rows() > 0) {
				echo '<ul class="crocell-products">';
				echo '<div class="head-you">You may like</div>';
                foreach ($recommand->result() as $vl) {
                    ?>
                    <li><a class="photo-link" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$vl->seourl;?>"><img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$vl->image);?>" title="<?php echo $vl->name;?>" alt="<?php echo $vl->name;?>" /></a>
					<div class="clr"></div>
                    <a class="item-name" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$vl->seourl;?>" title="<?php echo $vl->name;?>"><?php echo $vl->name;?></a><div class="clr"></div><label class="item-price">Our price <?php echo _r($vl->sale_price);?></label><div class="clr"></div>
                    </li>
					<?php
				}
				echo '</ul>';
				echo '<div class="clr"></div>';
			}
		}
	}
}