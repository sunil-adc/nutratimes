<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class searchSuggestion extends CI_Controller {
		
	public function __construct(){
		parent::__construct();
		$this->load->model( array('general'));
	}
	public function index($product_name = NULL) {
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($product_name) != NULL && trim ($product_name) != "" ) { 
			
			// SEARCH FOR BRANDS USING FULL TEXT QUERY START
			$quick_search_brand = $this->memcache_driver->get_cache('brandsearch_'.md5(strtolower(urldecode($product_name))));
			if (!$quick_search_brand) { 
				$sql_query = ' select 
									brand_id, name 
								from 
									'.BRANDS.' 
								where 
									name like "%'.strtolower(urldecode($product_name)).'%" and status = 1 
								order by 
									name asc LIMIT 5';
				$query = $this->db->query($sql_query);
				if ($query->num_rows() > 0) {
					$query_result = $query->result();
					echo '<div class="searchlabel">BRAND</div>';
					foreach ($query_result as $row) {
						echo '<div class="search_result" data-entity="'.$row->name.'">'.wordHighLight($row->name, $product_name).'</div>';
					}
					$this->memcache_driver->set_cache('brandsearch_'.md5(strtolower(urldecode($product_name))), $query_result, true, CACHE_LONGTIME);	
				}
			} else {
				echo '<div class="searchlabel">BRAND</div>';
				foreach ($quick_search_brand as $row) {
					echo '<div class="search_result" data-entity="'.$row->name.'">'.wordHighLight($row->name, $product_name).'</div>';
				}
			}
			
			//--//Koushik 22-Dec-2014. Dont display deal products under search
			$deal_prods = $this->_get_deal_products();
			$whr_deal_prods = ($deal_prods)?" and prod_id NOT IN(".$deal_prods.")":'';
			//$this->memcache_driver->del_cache('productsearch_'.md5(strtolower(urldecode($product_name))));
			
			// SEARCH FOR CATEGORY USING FULL TEXT QUERY START
			$quick_search_product = $this->memcache_driver->get_cache('productsearch_'.md5(strtolower(urldecode($product_name))));
			if (!$quick_search_product) { 
				$sql_query = '  select 
									prod_id, name, seourl
								from 
									'.PRODUCT.' 
								where 
									(name like "%'.strtolower(urldecode($product_name)).'%" or
									search_keywords like "%'.strtolower(urldecode($product_name)).'%")  and 
									status = "1" and is_verified = 1 
									'.$whr_deal_prods.'
								order by 
									prod_id desc LIMIT 10';
				$query = $this->db->query($sql_query);
				if ($query->num_rows() > 0) {
					$result = $query->result();
					echo '<div class="searchlabel">OTHERS</div>';
					foreach ($result as $row) {
						echo '<div class="search_result" data-entity="'.$row->name.'"><a href="'.SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl.'">'.wordHighLight($row->name, $product_name).'</a></div>';
					}
					$this->memcache_driver->set_cache('productsearch_'.md5(strtolower(urldecode($product_name))), $result, true, CACHE_LONGTIME);
				} else {
					// SEARCH FOR CATEGORY FULL TEXT QUERY END
					
					// SEARCH FOR CATEGORY SINGLE CHARACTER OR IF FULL TEXT NOT RETURN RESULT THAN FIRE THIS QUERY
					$sql_query = '  select 
										prod_id, name, seourl
									from 
										'.PRODUCT.' 
									where 
										(lower(name) like "'.strtolower(urldecode($product_name)).'%" or 
										lower(search_keywords) like "'.strtolower(urldecode($product_name)).'%") and 
										status = "1" and is_verified = 1 
										'.$whr_deal_prods.'
									order by 
										prod_id desc LIMIT 10';
					$query = $this->db->query($sql_query);
					if ($query->num_rows() > 0) {
						$result = $query->result();
						echo '<div class="searchlabel">OTHERS</div>';
						foreach ($result as $row) {
							echo '<div class="search_result" data-entity="'.$row->name.'"><a href="'.SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl.'">'.wordHighLight($row->name, $product_name).'</a></div>';
						}
						$this->memcache_driver->set_cache('productsearch_'.md5(strtolower(urldecode($product_name))), $result, true, CACHE_LONGTIME);
					} else {
						$sql_query = '  select 
											prod_id, name, seourl 
										from 
											'.PRODUCT.' 
										where 
											(lower(name) like "%'.strtolower(urldecode($product_name)).'%" or 
											lower(search_keywords) like "%'.strtolower(urldecode($product_name)).'%") and 
											status = "1" and is_verified = 1 
											'.$whr_deal_prods.'
										order by 
											prod_id desc LIMIT 10';
						$query = $this->db->query($sql_query);
						if ($query->num_rows() > 0) {
							$result = $query->result();
							echo '<div class="searchlabel">OTHERS</div>';
							foreach ($result as $row) {
								echo '<div class="search_result" data-entity="'.$row->name.'"><a href="'.SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl.'">'.wordHighLight($row->name, $product_name).'</a></div>';
							}
							$this->memcache_driver->set_cache('productsearch_'.md5(strtolower(urldecode($product_name))), $result, true, CACHE_LONGTIME);
						}
					}
				}
			} else {
				echo '<div class="searchlabel">OTHERS</div>';
				foreach ($quick_search_product as $row) {
					echo '<div class="search_result" data-entity="'.$row->name.'"><a href="'.SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl.'">'.wordHighLight($row->name, $product_name).'</a></div>';
				}
			}
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
	
	//--//Koushik 22-Dec-2014. hide super super deal attached items from search
	private function _get_deal_products()
	{
		$sql_deal_prods = "SELECT combo_product FROM ".PRODUCT." WHERE is_combo = 1 AND combo_product IS NOT NULL AND combo_product !='' AND status = 1";
		$query = $this->db->query($sql_deal_prods);
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			$arr_data = array();
			foreach ($result as $row) {
				$arr_data[] = $row['combo_product'];
			}
			return implode(',',$arr_data);
		}
		return false;
	}
}