<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class userNewsletter extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
		$this->load->model( array('general'));
	}
	public function index() {
		if ( isset($_POST['form'])) {
			if ( isset($_POST['email']) && trim($_POST['email']) != "") {
				if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_POST['email'])) {
					$this->db->cache_off();
					$res = $this->db->query("select email from ".NEWSLETTER." where email = '".$_POST['email']."'");
					if($res->num_rows() == 0) {
						$data = array(
								'email'		=>	$_POST['email'],
								'gender'	=>	$_POST['gender']);
						
						$this->db->insert(NEWSLETTER, $data);
						$this->svaiza_email->footerNewsletter($_POST['email']);
						echo "done";
					} else {
						echo "email_exist";
					}
				} else {
					echo "email_invalid";
				}
			} else {
				echo "email_invalid";
			}
		} else {
			echo "refresh";
		}
		exit;
	}
}