<?php
class svaiza_email extends CI_MODEL
{
	protected $setting_phone_no;
	protected $setting_admin_name;
	protected $setting_site_name;
	protected $all_array;
	
	function __construct(){
		parent::__construct();	
		
		$this->load->library(array('email', 'smtp', 'phpmailer'));
		
		$this->all_array = all_arrays();
	}
	
	
	
	function data_download($from_email, $from_name, $to, $subject, $message) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
			return true;
		} else {
			return false;
		}
	}
	
	function unknownLogin($from_email, $from_name, $to, $subject, $message) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
			return true;
		} else {
			return false;
		}
	}
	
	function ticket_mail($to, $message) {
			// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		if($this->mail_send("info@".SITE_NAME.".com", SITE_NAME, $to, SITE_NAME.' Support Ticket', $message)) {
			return true;
		} else {
			return false;
		}
	}
	/*
	function footerNewsletter($to) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		$message = '<table width="100%" border="0">
					  <tr>
						<td>
							<table cellpadding="0" cellspacing="0" align="center" style="width:600px; border:0px none; font-family:\'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; font-size:12px; color:#333; border:1px solid #ffa200;">
							<tr style="background-color:#f6f6f6;">
								<td height="40" style="padding:10px 0px 10px 10px ;"><img src="'.SITE_URL.'/images/shp_logo.png" width="129" height="35" alt="'.$this->setting_site_name.'" title="'.$this->setting_site_name.'" /></td>
							</tr>
							<tr style="background-color:#006FDB; ">
								<td height="40" style="padding:0px 10px 0px 10px; font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; color:#FFF;">Subscription Complete</td>
							</tr>
							<tr>
								<td height="10" style="font:bold 14px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; padding:10px 15px 10px 15px; ">Dear user,</td>
							</tr>
							<tr>
								<td height="10" style="padding:10px 15px 10px 15px;">Thank you for registering with our newsletter.<br />
								  we will send you the updates every week for new arrivals</td>
							</tr>
							<tr>
							  <td height="10" style="padding:0px 15px 0px 15px;">
									<br />
									Regards,<br />
									'.$this->setting_site_name.' Team
							  </td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							</table>
						</td>
					  </tr>
					</table>';
						
		if($this->mail_send("info@".SITE_NAME.".com", SITE_NAME, $to, SITE_NAME.' Newsletter Subscription', $message)) {
			return true;
		} else {
			return false;
		}
	}*/
	
	/*function mail_send($from_email="", $from_name="", $to="", $subject="", $message="") {
		global $dm_name;
		
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: '.$from_name.' <'.$from_email.'>' . "\r\n";
		@mail($to, $subject, $message, $headers);
		
		return true;
	}*/
	
	
	
	
	function mail_send($from_email="", $from_name="", $to="", $subject="", $message="") {
		
		global $dm_name;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			 
		$mail = new PHPMailer();
		$mail->Host = "slm.21.buynutratimes.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "admin@nutratimesmail.com"; // SMTP username
		$mail->Password = "fh9Gbocb8yivbjk"; // SMTP password
		//$mail->SMTPDebug = 2; // uncomment to print debugging info
		
		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "nutratimes"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom("notify@nutratimesmail.com", $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;
		
		
	}
	
	
	function mail_send_attachment($from_email="", $from_name="", $to="", $subject="", $message="", $path_name="",$filename="") {
		
		global $dm_name;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			 
		$mail = new PHPMailer();
		$mail->Host = "sap31.nutratimesmail.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "admin@nutratimesmail.com"; // SMTP username
		$mail->Password = "fh9Gbocb8yivbjk"; // SMTP password
		//$mail->SMTPDebug = 2; // uncomment to print debugging info
		
		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "nutratimes"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		$full_path = $path_name;
		
		$mail->AddAttachment($full_path, $filename );

		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom("notify@nutratimesmail.com", $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;		
		
	} 


	function update_settings() {
		$settings = $this->db->query("SELECT attr_name, attr_value FROM ".SETTINGS." where status = '1' and attr_name in ('phone_no','admin_name','site_name','admin_email', 'phone_no')");
		if ($settings->num_rows() > 0) {
			foreach ($settings->result() as $setting_data) {
				if($setting_data->attr_name == 'phone_no') {	
					$this->setting_phone_no = $setting_data->attr_value;
				} else if($setting_data->attr_name == 'admin_name') {	
					$this->setting_admin_name = $setting_data->attr_value;
				} else if($setting_data->attr_name == 'site_name') {	
					$this->setting_site_name = $setting_data->attr_value;
				} else if($setting_data->attr_name == 'admin_email') {	
					$this->setting_admin_email = $setting_data->attr_value;
				} else if($setting_data->attr_name == 'phone_no') {	
					$this->setting_phone_no = $setting_data->attr_value;
				}
			}
		}
	}
	
	/*
	public function cancel_order($order_id = 0) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		if ($order_id > 0) {
		  
		  // GET USER DETAILS
		  $res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = (select user from '.ORDER.' where id = '.$order_id.')');
		
		  if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email; 
				
				$subject = "Your Order has been cancelled from ".$this->setting_site_name;
				$message = '<table cellpadding="0" cellspacing="0" width="100%" bgcolor="#fff" style="margin:0;padding:0;">
		  <tr>
			<td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="700" style="margin:0;padding:0;">
				<tr>
				  <td align="left" valign="top"><table cellpadding="0" cellspacing="0" width="100%" style="margin:0;padding:0;">
					  <tr>
						<td style="font-size:0; line-height:0; height:10px;"></td>
					  </tr>
					  <tr>
						<td style="line-height:1;" valign="top"><a href="'.SITE_URL.'"><img src="'.SITE_URL.'/images/shp_logo.png" style="border:none;" /></a></td>
						<td style="font-size:10px;-webkit-text-size-adjust:none;font-family:arial;color:#999999;" align="right"><a href="'.SITE_URL.'info/aboutus" style="color:#999;text-decoration:none;padding-top:15px;">About Us</a> | <a href="'.SITE_URL.'info/contactus" style="color:#999;text-decoration:none;padding-top:15px;">Contact Us</a> | <a href="'.SITE_URL.'info/terms_of_service" style="color:#999;text-decoration:none;padding-top:15px;">Terms of use</a></td>
					  </tr>
					  <tr>
						<td style="font-size:0; line-height:0; height:5px;"></td>
					  </tr>
					</table></td>
				</tr>
			  </table></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" bgcolor="#fff" style="margin:0;padding:0;">
		  <tr>
			<td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="700">
				<tr>
				  <td><table cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #F5721B" bgcolor="#fff">
					  
					  <tr>
						<td style="padding:10px 40px 10px 40px;" align="left"><table cellpadding="0" cellspacing="0">
							<tr>
							  <td style="height:25px;font-size:0;">&nbsp;</td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px;color:#333;" align="left">Dear <b>'.$user_data->name.'</b>,</td>
							</tr>
							<tr>
							  <td style="height:15px;font-size:0; line-height:0;">&nbsp;</td>
							</tr>

							<tr>
							  <td style="height:10px;font-size:0; line-height:0;">&nbsp;</td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px;color:#333; line-height:18px;"><p>
							  You are receiving this e-mail as acknowledgement of the rejection of your order (order id '.$order_id.'). We expect you would make a complete successful shopping from our portal in the near future. Visit Shophunk.com for more exciting offers.</p></td>
							</tr>
							<tr>
							  <td style="font:0px Arial, Helvetica, sans-serif; line-height:0; height:10px;">&nbsp;</td>
							</tr>';
							
							
						 $message .= '
						  </table></td>
					  </tr>
					  <tr>
						<td style="line-height:0; font-size:0; height:30px;"></td>
					  </tr>
					  <tr>
						<td style="line-height:0; font-size:0; height:0px;"></td>
					  </tr>
					  <tr>
						<td style="padding:20px 40px 60px 40px;" align="left"><table cellpadding="0" cellspacing="0" width="580">
							<tr>
							  <td style="height:0px;font-size:0;">&nbsp;</td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px; line-height:18px; color:#333;" align="left"> If you need any assistance or have any questions,<br>
								Please feel free to call to contact us at '.$this->setting_phone_no.' or <br>
								Email us at <a style="color:#03C;" href="mailto:'.$this->setting_admin_email.'">'.$this->setting_admin_email.'</a></td>
							</tr>
							<tr>
							  <td style="height:35px;font-size:0;">&nbsp;</td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px;height:20px;color:#333;" align="left">Wish you all the best of shopping</td>
							</tr>
							<tr>
							  <td style="height:2px; font-size:0; line-height:0px;"></td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px;height:20px;color:#333;">'.$this->setting_site_name.' Team</a></td>
							</tr>
							<tr>
							  <td style="height:2px; font-size:0; line-height:0px;"></td>
							</tr>
							<tr>
							  <td style="font-family:arial;font-size:12px;height:20px;color:#666666;"><a style="color:#03C;" href="'.SITE_URL.'">'.SITE_URL.'</a></td>
							</tr>
						  </table></td>
					  </tr>
					</table></td>
				</tr>
			  </table></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" bgcolor="#fff" style="margin:0;padding:0;">
		  <tr>
			<td height="10" style="font-size:0;">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="700">
				<tr>
				  <td width="350" align="left" style="font:12px Arial, Helvetica, sans-serif; color:#999999;"> THIS IS A SYSTEM GENERATED MAIL. <br>
					REPLIES TO THIS MAIL ID WILL NOT BE RESPONDED. </td>
				  <td width="350" align="right" style="padding-top:10px; padding-bottom:10px;"><table cellpadding="0" cellspacing="0" width="140">
					  <tr>
						<td align="right" width="35"><a style="border:none;" href="https://www.facebook.com/shophunk"><img style="border:none;" src="https://shophunk.s3.amazonaws.com/cdn/images/mail_fb.png" alt="Facebook"></a></td>
						<td align="right" width="35"><a style="border:none;" href="http://www.pinterest.com/shophunk"><img style="border:none;" src="https://shophunk.s3.amazonaws.com/cdn/images/mail_pin.png" alt="Pinterest"></a></td>
						<td align="right" width="35"><a style="border:none;" href="https://twitter.com/shop_hunk"><img style="border:none;" src="https://shophunk.s3.amazonaws.com/cdn/images/mail_tw.png" alt="Twitter"></a></td>
						<td align="right" width="35"><a style="border:none;" href="https://www.youtube.com/user/shophunk"><img style="border:none;" src="https://shophunk.s3.amazonaws.com/cdn/images/mail_youtube.png" alt="Youtube"></a></td>
					  </tr>
					</table></td>
				</tr>
			  </table></td>
		  </tr>
		</table>';
			
				if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
					return true;
				} else {
					return false;
				}
			
			}
		  }
		}
	}
	*/

	public function dispatch_order($order_id = 0, $tracking_number = 0, $payment_mode = 1, $provider = '') {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		if ($order_id > 0) {
		  
		  // GET USER DETAILS
		  $res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = (select user from '.ORDER.' where id = '.$order_id.')');
		
		  if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email; 
				
				$subject = $user_data->name." Your Nutratimes order is shipped";
				
				$message = '<html><head>
					<title>'.$this->setting_site_name.'</title>
					<style type="text/css">
					body {
						-webkit-text-size-adjust: 100% !important;
						-ms-text-size-adjust: 100% !important;
						-webkit-font-smoothing: antialiased !important;
					}

					img {
						border: 0 !important;
						outline: none !important;
					}

					p {
						Margin: 0px !important;
						padding: 0px !important;
					}

					table {
						border-collapse: collapse;
						mso-table-lspace: 0px;
						mso-table-rspace: 0px;
					}

					td {
						border-collapse: collapse;
						mso-line-height-rule: exactly;
					}

					a {
						border-collapse: collapse;
						mso-line-height-rule: exactly;
					}

					span {
						border-collapse: collapse;
						mso-line-height-rule: exactly;
					}

					.ExternalClass * {
						line-height: 100%;
					}

					span.MsoHyperlink {
						mso-style-priority: 99;
						color: inherit;
					}

					span.MsoHyperlinkFollowed {
						mso-style-priority: 99;
						color: inherit;
					}

					@media and screen {
						td {
						font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
						}
						a {
						font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
						}
					}
					</style>
				</head>

				<body>
					<table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;" width="600" cellspacing="0" cellpadding="0" align="center">
					<tbody>
						<tr>
								<td style="padding: 3px 20px;">
										<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;" width="560" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;" colspan="2" align="center">Warm Greetings from Nutratimes</td>
												</tr>
												<tr style="background: #f5f5f5;">
													<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
														<a href="http://www.nutratimes.com" target="_blank">
															<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'" width="250" height="50"> </a>
													</td>
													<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
														<a style="display: inline-block;width: 135px;background: #e25c26;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 13px;font-size: 13px !important;color: #323232;padding: 6px 0px;text-align: center;border: 1px solid #e25c26;font-weight: 600;text-decoration: none; color: #fff;" href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
						</tr>

						<tr>
						<td style="padding: 0 20px;">
							<table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							 <tr>
								<td style="padding: 15px 10px 30px 0px; " align="left" valign="top" width="310">
									<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey '.$user_data->name.',
									</span><br>
									<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px !important; font-weight: 400; color: #4d4d4d;">
										Thank you for choosing Nutratimes for your health needs.
									</span>
									<br>
									
								</td>
								<td style="padding: 15px 0px 30px 10px;" align="center" valign="top">
									<span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">Tracking Id:</span>
									<span style="display: block; width: 210px;     margin-bottom: 8px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 22px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">                   
									<span>'.$tracking_number.'</span><br>
									</span>
								</td>
								</tr>
							</tbody>
							</table>
						</td>
						</tr>
						<tr>
							<td style="padding: 0 15px 25px;"><p style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px !important; font-weight: 400; color: #4d4d4d; margin-bottom: 10px!important;">This is to inform you that we have dispatched your ordered product on (date). Your order is on the way and expect to deliver anytime. </p>
								<p style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px !important; font-weight: 400; color: #4d4d4d;">To track your order kindly visit <a href=" www.xpressbees.com" style="color: #e25c26;font-weight: bold;"> www.xpressbees.com</a></p>
							</td>
						</tr>

						<tr>
								<td>
									<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;" href="http://www.nutratimes.com" target="_blank">
										<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;" align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at '.$this->setting_admin_email.'.</td>
												</tr>
											</tbody>
										</table>
									</a>
								</td>
							</tr>
					</tbody>
					</table>
					<center>


					<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
						<tbody>
						<tr>
							<td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
							<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
								<tbody>
								<tr>
									<td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
									<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, | Bengaluru-560078
									| Karnataka
									<br>
									<br>
									</td>
								</tr>
								</tbody>
							</table>
							</td>
						</tr>
						</tbody>
					</table>
					</center>




				</body></html>';
						
				if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
					return true;
				} else {
					return false;
				}
			
			}
		  }
		}
	}
	
	public function email_signup($user_id = "", $passwd = "") {
		if($user_id != "" && $passwd != "") {
			
			// UPDATE VARIABLE AND SET SETTING INFORMATION
			$this->update_settings();
			
			// GET THE DETAILS OF USER
			$users = $this->db->query("SELECT name, email, mobile, passwd FROM ".USERS." where user_id = ".$user_id);
			if ($users->num_rows() > 0) {
			    foreach ($users->result() as $user_data) {
			   	 	
					$from_email = $this->setting_admin_email;
					$from_name 	= $this->setting_admin_name;
					$to 		= $user_data->email; 
					
					$subject = $this->setting_site_name." - Thank you for registering ";
					
					
				$message = '<html>
							<head>
								<title>'.$this->setting_site_name.'</title>
								<style type="text/css">
									body {
										-webkit-text-size-adjust: 100% !important;
										-ms-text-size-adjust: 100% !important;
										-webkit-font-smoothing: antialiased !important;
									}

									img {
										border: 0 !important;
										outline: none !important;
									}

									p {
										Margin: 0px !important;
										padding: 0px !important;
									}

									table {
										border-collapse: collapse;
										mso-table-lspace: 0px;
										mso-table-rspace: 0px;
									}

									td {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									a {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									span {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									.ExternalClass * {
										line-height: 100%;
									}

									span.MsoHyperlink {
										mso-style-priority: 99;
										color: inherit;
									}

									span.MsoHyperlinkFollowed {
										mso-style-priority: 99;
										color: inherit;
									}

									@media and screen {
										td {
											font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
										}
										a {
											font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
										}
									}
								</style>
							</head>

							<body>
								<table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
								 width="600" cellspacing="0" cellpadding="0" align="center">
									<tbody>
										<tr>
											<td style="padding: 3px 20px;">
												<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;"
												 width="560" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;"
															 colspan="2" align="center">Warm Greetings from Nutratimes</td>
														</tr>
														<tr style="background: #f5f5f5;">
															<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
																<a href="http://www.nutratimes.com" target="_blank">
																	<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'"
																	 width="250" height="50"> </a>
															</td>
															<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
																<a style="
													  display: inline-block;
													  width: 135px;
													  background: #e25c26;
													  font-family: Open Sans, Arial, Helvetica, sans-serif;
													  font-size: 13px;
													  font-size: 13px !important;
													  color: #323232;
													  padding: 6px 0px;
													  text-align: center;
													  border: 1px solid #e25c26;
													  font-weight: 600;
													  text-decoration: none;
													  color: #fff;
													  " href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>

										<tr>
											<td style="padding: 0 20px;">
												<table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0"
												 cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="padding: 15px 10px 30px 0px;" align="left" valign="top" width="310">
																<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey '.$user_data->name.',
																	<br>Welcome and thank you for registering at Nutratimes!</span>
																<br>
																<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 13px !important; font-weight: 400; color: #323232; padding: 0;">
																	Your account has now been created and you can log in by using your user id and password by visiting our website or
																	<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 15px; font-size: 13px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
																	 href="http://www.nutratimes.com/login">click here</a> to Login.
																</span>
															</td>
															<td style="padding: 15px 0px 30px 10px;" align="center" valign="top">
																<span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">Login Details:</span>
																<span style="display: block; width: 250px;     margin-bottom: 8px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 14px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">

																	<span>User Id : '.$user_data->mobile.'</span>
																	<br>
																	<span>Password : '.$passwd.' </span>
																</span>

																<span style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 11px !important; color: #6f6f6f; font-weight: 400; padding-top: 15px;">(Please change/update your password after login)</span>
																<a style="display: block;
																width: 135px;
																background: #e25c26;
																font-family: Open Sans, Arial, Helvetica, sans-serif;
																font-size: 16px !important;
																color: #323232;
																padding: 12px 0px;
																text-align: center;
																border: 1px solid #e25c26;
																font-weight: 600;
																text-decoration: none;
																margin: 20px 0;
																color: #ffff;
																border-radius: 4px;" href="http://www.nutratimes.com/login" target="_blank">Login</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>


										<tr>
											<td>
												<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
												 href="http://www.nutratimes.com" target="_blank">
													<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
														<tbody>
															<tr>
																<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
																 align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at '.$this->setting_admin_email.'.</td>
															</tr>
														</tbody>
													</table>
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<center>


									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
										<tbody>
											<tr>
												<td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
													<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
														<tbody>
															<tr>
																<td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
																	<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center,
																	| Bengaluru-560078 | Karnataka
																	<br>
																	<br>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</center>

							</body>

							</html>';		
					
					
					if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function send_otp($user_id = "", $otp = "") {
		if($user_id != "" && $otp != "") {
			
			// UPDATE VARIABLE AND SET SETTING INFORMATION
			$this->update_settings();
			
			// GET THE DETAILS OF USER
			$users = $this->db->query("SELECT name, email, mobile, passwd FROM ".USERS." where user_id = ".$user_id);
			if ($users->num_rows() > 0) {
			    foreach ($users->result() as $user_data) {
			   	 	
					$from_email = $this->setting_admin_email;
					$from_name 	= $this->setting_admin_name;
					$to 		= $user_data->email; 
					
					$subject = "One-time Password (OTP) - verification code : ".$otp;
					
					
				 $message = '<html>
						<head>
						  <title>'.$this->setting_site_name.'</title>
						  <style type="text/css">
							body {
							  -webkit-text-size-adjust: 100% !important;
							  -ms-text-size-adjust: 100% !important;
							  -webkit-font-smoothing: antialiased !important;
							}

							img {
							  border: 0 !important;
							  outline: none !important;
							}

							p {
							  Margin: 0px !important;
							  padding: 0px !important;
							}

							table {
							  border-collapse: collapse;
							  mso-table-lspace: 0px;
							  mso-table-rspace: 0px;
							}

							td {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							a {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							span {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							.ExternalClass * {
							  line-height: 100%;
							}

							span.MsoHyperlink {
							  mso-style-priority: 99;
							  color: inherit;
							}

							span.MsoHyperlinkFollowed {
							  mso-style-priority: 99;
							  color: inherit;
							}

							@media and screen {
							  td {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							  a {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							}
						  </style>
						</head>

						<body>
						  <table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
							width="600" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							  <tr>
										<td style="padding: 3px 20px;">
												<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;"
												 width="560" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;"
															 colspan="2" align="center">Warm Greetings from Nutratimes</td>
														</tr>
														<tr style="background: #f5f5f5;">
															<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
																<a href="http://www.nutratimes.com" target="_blank">
																	<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'"
																	 width="250" height="50"> </a>
															</td>
															<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
																<a style="display: inline-block;width: 135px;background: #e25c26;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 13px;font-size: 13px !important;color: #323232;padding: 6px 0px;text-align: center;border: 1px solid #e25c26;font-weight: 600;text-decoration: none; color: #fff;" href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
							  </tr>

							  <tr>
								<td style="padding: 0 20px;">
								  <table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0" cellpadding="0"
									align="center">
									<tbody>
									 <tr>
										<td style="padding: 15px 10px 30px 0px; " align="left" valign="top" width="310">
										  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey '.$user_data->name.',
											</span><br>
											<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 400; color: #4d4d4d;">
													Please confirm the verification code sent to you. 
											</span>
										  <br>
										  
										</td>
										<td style="padding: 15px 0px 30px 10px;" align="center" valign="top">
										  <span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">OTP:</span>
										  <span style="display: block; width: 210px;     margin-bottom: 8px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 22px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">                   
											<span>'.$otp.'</span><br>
										  </span>
										
										<!-- <span style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 12px !important; color: #6f6f6f; font-weight: 400; padding-top: 15px;"
										 >(Valid Only For 30 Minute)</span> -->
								   
										</td>
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>


								<tr>
										<td>
											<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
											 href="http://www.nutratimes.com" target="_blank">
												<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
															 align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at '.$this->setting_admin_email.'.</td>
														</tr>
													</tbody>
												</table>
											</a>
										</td>
									</tr>
							</tbody>
						  </table>
						  <center>


							<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
							  <tbody>
								<tr>
								  <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
									<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
									  <tbody>
										<tr>
										  <td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
											<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, | Bengaluru-560078
											| Karnataka
											<br>
											<br>
										  </td>
										</tr>
									  </tbody>
									</table>
								  </td>
								</tr>
							  </tbody>
							</table>
						  </center>

						</body>

						</html>';		
					
					
					if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

			
	public function email_order_confirmation($user_id, $order_id, $payment_mode = 1) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		// GET USER DETAILS
		$res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = '.$user_id);
		if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email; 
				
				$subject = "Your ".$this->setting_site_name."  Order Confirmation" ;
				
				$message = '<html>

						<head>
						  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
						  <title>Nutratimes</title>
						  <style type="text/css">
							body {
							  -webkit-text-size-adjust: 100% !important;
							  -ms-text-size-adjust: 100% !important;
							  -webkit-font-smoothing: antialiased !important;
							}

							img {
							  border: 0 !important;
							  outline: none !important;
							}

							p {
							  Margin: 0px !important;
							  padding: 0px !important;
							}

							table {
							  border-collapse: collapse;
							  mso-table-lspace: 0px;
							  mso-table-rspace: 0px;
							}

							td {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							a {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							span {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							.ExternalClass * {
							  line-height: 100%;
							}

							span.MsoHyperlink {
							  mso-style-priority: 99;
							  color: inherit;
							}

							span.MsoHyperlinkFollowed {
							  mso-style-priority: 99;
							  color: inherit;
							}

							@media and screen {
							  td {
								font-family: Open Sans, Arial, Helvetica, sans-serif !important;
							  }
							  a {
								font-family: Open Sans, Arial, Helvetica, sans-serif !important;
							  }
							}
						  </style>
						</head>

						<body>
						  <table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
							width="600" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							  
							  <tr>
								<td style="padding: 3px 20px;">
								  <table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;" width="560"
									cellspacing="0" cellpadding="0" align="center">
									<tbody>
									  <tr>
										<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 0 0px 25px;"
										  colspan="2" align="center">Warm Greetings from Nutratimes</td>
									  </tr>
									  <tr>
										<td style="width: 260px; padding-bottom: 10px;">
										  <a href="http://www.nutratimes.com/" target="_blank">
											<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="nutratimes.com"
											  width="250" height="50"> </a>
										</td>
										
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>

							  <tr>
								<td style="padding: 0 20px;">
								  <table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0" cellpadding="0"
									align="center">
									<tbody>
									  <tr>
										<td style="padding: 15px 10px 30px 0px; border-bottom: 1px solid #d7d7d7;" align="left" valign="top" width="310">
										  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Dear '.$user_data->name.',
											<br>Your order has been received  !</span>
										  <br>
										  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 300; color: #323232; padding: 0;">Thank You for choosing Nutratimes as your health partner. We have sent your order to the concerned department
											and it will be dispatched shortly. To view your order details,
											<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
											  href="http://www.nutratimes.com/orderhistory">click here</a>
										  </span>
										</td>
										<td style="padding: 15px 0px 30px 10px; border-bottom: 1px solid #d7d7d7;" align="center" valign="top">
										  <span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">Your Order Number is:</span>
										  <span style="display: block; width: 210px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 32px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">  '.$order_id.'</span>
										</td>
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>

							  <tr>
								<td style="text-align: center;">
								  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 26px; font-size: 26px !important; color: #323232; text-align: center; padding: 30px 0 14px;">Order Details</span>
								  <span style="display: inline-block; width: 70px; height: 1px; background: #d7d7d7; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 1px; color: #323232; text-align: center; line-height: 1px; vertical-align: top;">&nbsp;</span>
								</td>
							  </tr>';
							  
									$order_product = $this->db->query('SELECT 
																		o.payment_mode, 
																		p.name, 		up.cash_price, 
																		up.cod_price, 	up.shipping, 
																		up.contest, 	up.quantity, pi.image 
																	FROM  
																		'.ORDER.' o 
																		INNER JOIN '.USER_PRODUCTS.' up on (up.order_id = o.id)  
																		INNER JOIN '.PRODUCT.' p ON (p.prod_id = up.product) 
																		left JOIN  '.PRODUCT_IMAGE.' pi on (p.prod_id = pi.prod_id)
																	WHERE 
																		up.status = 1 and 
																		up.contest = 0 and 
																		up.order_id='.$order_id);
								if($order_product->num_rows() > 0) {
									$total = 0;
									foreach ($order_product->result() as $val_order_product) {
										$shipping[] = $val_order_product->shipping;
										//$price = $val_order_product->payment_mode == 1 ? $val_order_product->cash_price : $val_order_product->cod_price;
										$price = $val_order_product->cash_price ;
								
											
							  $message .='
							  <tr>
								<td>
								  <table width="560" cellpadding="0" cellspacing="0" align="center" style="width:560px; margin:0 auto; font-family:Open Sans, Arial, Helvetica, sans-serif;">
									<tbody>
									  <tr>
										<td align="left" style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:16px; color:#323232; text-align:left; font-weight:400; padding:12px 0;">'.$val_order_product->name.'</td>
										<td align="right" width="190" style="padding:12px 0;">
										  <span style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:18px; line-height:22px; color:#323232; font-weight:bold;">'._r($price).'</span>
										</td>
									  </tr>
									  <tr>
										<td align="left" valign="top" style="padding:0 0 12px;">
										  <img src="http://nutratimes.com/cdn//prd_r325fc224q/prd_thumb_200_300/'.$val_order_product->image.'" alt="Nutratimes"
											style="display:block; width:280px;object-fit: contain; height:170px;">
										</td>
										<td width="190" align="right" valign="top" style="padding:12px 0; line-height:24px;">
										  <span style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; text-align:right; color:#6f6f6f;">Quantity:</span>
										  <span style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:14px; font-weight:100; color:#323232;">'.$val_order_product->quantity.'</span>
										  <br>
										</td>
									  </tr>

									  <tr>
										<td colspan="2" style="font-size:1px; line-height:1px; border-top:1px solid #d7d7d7; padding:0 0 24px;">&nbsp;</td>
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>';
										
									$total += ($price * $val_order_product->quantity);
									}
									$shipping = $val_order_product->payment_mode == 2 ? 0 : max($shipping);
								}
							
							$message .='	
							  <tr>
								<td style="padding: 10px 0 0;">
								  <table style="width: 560px; margin: 0 auto;" width="560px" cellspacing="0" cellpadding="0" align="center">
									<tbody>
									  <tr>
										<td style="padding: 0 50px 20px 20px; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; color: #6f6f6f; line-height: 21px;">
										  <div style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #323232; font-weight: 600; padding-bottom: 4px; margin: 0;">Shipping Address</div>
										  '.$user_data->name.'
										  <br>'.$user_data->address.'
										  <br>'.ucfirst($user_data->city).', '.$this->all_array['ARR_STATE'][$user_data->state].' '.$user_data->pincode.'
										  <br>
										  <br>
										  <br>
										  <div style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #323232; font-weight: 600; padding-bottom: 4px; margin: 0;">Billing Address</div>
										  '.$user_data->name.'
										  <br>'.$user_data->address.'
										  <br>'.ucfirst($user_data->city).', '.$this->all_array['ARR_STATE'][$user_data->state].' '.$user_data->pincode.'
										  <br>
										  <br>
										  <br>
										  <div style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #323232; font-weight: 600; padding-bottom: 4px; margin: 0;">Payment Info</div>
										  '.($payment_mode == 2 ? "Post Payment" : "Paid").'
										  <br> </td>
										<td style="vertical-align: top;" width="280">
										  <table style="border:5px solid #89b863;" width="100%" cellspacing="0" cellpadding="0" align="center">
											<tbody>
											  <tr>
												<td style="padding:15px 20px 0">
												  <table width="100%" cellpadding="0" cellspacing="0">
													<tbody>
													  <tr>
														<td style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:16px; color:#323232; text-align:left; padding:0 0 10px;">Total</td>
														<td style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:16px; color:#323232; text-align:right; padding:0 0 10px;">Rs '.$total.'</td>
													  </tr>
													  <tr>
														<td style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:16px; color:#323232; text-align:left; padding:0 0 10px;">Shipping Charges</td>
														<td style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:16px; color:#323232; text-align:right; padding:0 0 10px;">Rs '.$shipping.'</td>
													  </tr>
													  
													</tbody>
												  </table>
												</td>
											  </tr>
											  <tr>
												<td style="padding:0 20px 30px;">
												  <table width="100%" cellspacing="0" cellpadding="0">
													<tbody>
													  <tr>
														<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; color: #323232; text-align: left; padding: 15px 0; font-weight: 600; border-top: 1px solid #d7d7d7; border-bottom: 1px solid #d7d7d7;">Order Total:</td>
														<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; color: #323232; text-align: right; padding: 15px 0; font-weight: 600; border-top: 1px solid #d7d7d7; border-bottom: 1px solid #d7d7d7;">'._r($total + $shipping).'</td>
													  </tr>
													</tbody>
												  </table>
												</td>
											  </tr>
											  <!--  -->
											  
											  <tr>
												<td style="padding:0 20px 35px; text-align: centar; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; color: #6f6f6f;" align="center">
												  <div style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; color: #323232; font-weight: 600; margin: 0; padding-bottom: 3px;">Order Status:</div>

												  '.($payment_mode == 2 ? "Cash On Delivery Confirmed" : "Payment Confirmed").' <br>
													'.date('m/d/Y h:i a', time()).'
												</td>
											  </tr>
											  <!--  -->
											</tbody>
										  </table>
										</td>
									  </tr>
									  
									</tbody>
								  </table>
								</td>
							  </tr>


							    <tr>
						          <td>
						            <a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
						             href="http://www.nutratimes.com" target="_blank">
						              <table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
						                <tbody>
						                  <tr>
						                    <td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
						                     align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at support@nutratimes.com</td>
						                  </tr>
						                </tbody>
						              </table>
						            </a>
						          </td>
						        </tr>

						    </tbody>
						  </table>
						  <center>

						    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
						      <tbody>
						        <tr>
						          <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
						            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
						              <tbody>
						                <tr>
						                  <td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
						                    <br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, | Bengaluru-560078
						                    | Karnataka
						                    <br>
						                    <br>
						                  </td>
						                </tr>
						              </tbody>
						            </table>
						          </td>
						        </tr>
						      </tbody>
						    </table>
						  </center>

						</body>

						</html>';
					


							
					if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
						return true;
					} else {
						return false;
					}
			}
		}
	}

	/*
	public function email_refund_request($to = "") {
		if($to != "") {
			
			// UPDATE VARIABLE AND SET SETTING INFORMATION
			$this->update_settings();
			
			$from_email = $this->setting_admin_email;
			$from_name 	= $this->setting_admin_name;
			$to 		= $to; 
			
			$subject = "Your Refund request initiated ".$this->setting_site_name;
			$message = '
			<table width="100%" border="0">
					  <tr>
						<td>
							<table cellpadding="0" cellspacing="0" align="center" style="width:600px; border:0px none; font-family:\'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; font-size:12px; color:#333; border:1px solid #ffa200;">
							<tr style="background-color:#f6f6f6;">
								<td height="40" style="padding:10px 0px 10px 10px ;"><img src="'.SITE_URL.'/images/shp_logo.png" width="129" height="35" alt="'.$this->setting_site_name.'" title="'.$this->setting_site_name.'" /></td>
							</tr>
							<tr style="background-color:#006FDB; ">
								<td height="40" style="padding:0px 10px 0px 10px; font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; color:#FFF;">Refund request initiated</td>
							</tr>
							<tr>
								<td height="10" style="font:bold 14px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; padding:10px 15px 10px 15px; ">Dear customer,</td>
							</tr>
							<tr>
								<td height="10" style="padding:10px 15px 10px 15px;">your refund request is initiated for confirmation on this request you will get a call from one of our customer care executive shortly.<br><br>
								Note : Refund process will take 7-10 working days, after customer care call verification, if you need immediate refund then call on this number '.$this->setting_phone_no.' .</td>
							</tr>
							<tr>
								<td height="10" style="padding:0px 15px 0px 15px;"><br />
								If you need any assistance or have any questions, feel free to contact us at '.$this->setting_phone_no.' or '.$this->setting_admin_email.'</b></td>
							</tr>
							<tr>
							  <td height="10" style="padding:0px 15px 0px 15px;">
									<br />
									Regards,<br />
									'.$this->setting_site_name.' Team
							  </td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							</table>
						</td>
					  </tr>
					</table>';	
			
			if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}*/
	
	public function email_recover($user_name = "", $user_email = "", $passwd = "", $mobile="") {
		if($user_name != "" && $user_email != "" && $passwd != "") {
			
			// UPDATE VARIABLE AND SET SETTING INFORMATION
			$this->update_settings();
			
			// GET THE DETAILS OF USER
			$from_email = $this->setting_admin_email;
			$from_name = $this->setting_admin_name;
			$to = $user_email; 
			
			$subject = "Your ".$this->setting_site_name." account new password "; 
			
			 $message = '<html>
						<head>
						  <title>'.$this->setting_site_name.'</title>
						  <style type="text/css">
							body {
							  -webkit-text-size-adjust: 100% !important;
							  -ms-text-size-adjust: 100% !important;
							  -webkit-font-smoothing: antialiased !important;
							}

							img {
							  border: 0 !important;
							  outline: none !important;
							}

							p {
							  Margin: 0px !important;
							  padding: 0px !important;
							}

							table {
							  border-collapse: collapse;
							  mso-table-lspace: 0px;
							  mso-table-rspace: 0px;
							}

							td {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							a {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							span {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							.ExternalClass * {
							  line-height: 100%;
							}

							span.MsoHyperlink {
							  mso-style-priority: 99;
							  color: inherit;
							}

							span.MsoHyperlinkFollowed {
							  mso-style-priority: 99;
							  color: inherit;
							}

							@media and screen {
							  td {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							  a {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							}
						  </style>
						</head>

						<body>
						  <table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
							width="600" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							  <tr>
								<td style="padding: 3px 20px;">
										<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;"
										 width="560" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;"
													 colspan="2" align="center">Warm Greetings from Nutratimes</td>
												</tr>
												<tr style="background: #f5f5f5;">
													<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
														<a href="http://www.nutratimes.com" target="_blank">
															<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'"
															 width="250" height="50"> </a>
													</td>
													<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
														<a style="display: inline-block;width: 135px;background: #e25c26;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 13px;font-size: 13px !important;color: #323232;padding: 6px 0px;text-align: center;border: 1px solid #e25c26;font-weight: 600;text-decoration: none; color: #fff;" href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
							  </tr>

							  <tr>
								<td style="padding: 0 20px;">
								  <table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0" cellpadding="0"
									align="center">
									<tbody>
									 <tr>
										<td style="padding: 15px 10px 30px 0px; " align="left" valign="top" width="310">
										  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey '.$user_name.',
											</span><br>
											<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 400; color: #4d4d4d;">
													Your password has been restored and new password are as below.
											</span>
										  <br>
										  
										</td>
										<td style="padding: 15px 0px 30px 10px;" align="center" valign="top">
										  <span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">New Password :</span>
										  <span style="display: block; width: 210px;     margin-bottom: 8px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 22px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">                   
											<span>'.$passwd.'</span><br>
										  </span>
										
										<!-- <span style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 12px !important; color: #6f6f6f; font-weight: 400; padding-top: 15px;"
										 >(Valid Only For 30 Minute)</span> -->
								   
										</td>
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>


								<tr>
										<td>
											<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
											 href="http://www.nutratimes.com" target="_blank">
												<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
															 align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at '.$this->setting_admin_email.'.</td>
														</tr>
													</tbody>
												</table>
											</a>
										</td>
									</tr>
							</tbody>
						  </table>
						  <center>


							<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
							  <tbody>
								<tr>
								  <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
									<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
									  <tbody>
										<tr>
										  <td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
											<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, | Bengaluru-560078
											| Karnataka
											<br>
											<br>
										  </td>
										</tr>
									  </tbody>
									</table>
								  </td>
								</tr>
							  </tbody>
							</table>
						  </center>

						</body>

						</html>';		
				

			if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public function email_ChangePassword($user_name = "", $user_email = "", $passwd = "", $mobile="") {

		if($user_name != "" && $user_email != "" && $passwd != "") {
			
			// UPDATE VARIABLE AND SET SETTING INFORMATION
			$this->update_settings();
			
			// GET THE DETAILS OF USER
			$from_email = $this->setting_admin_email;
			$from_name = $this->setting_admin_name;
			$to = $user_email; 
			
			$subject = "Password Changed for ".$this->setting_site_name;
			
			 $message = '<html>
						<head>
						  <title>'.$this->setting_site_name.'</title>
						  <style type="text/css">
							body {
							  -webkit-text-size-adjust: 100% !important;
							  -ms-text-size-adjust: 100% !important;
							  -webkit-font-smoothing: antialiased !important;
							}

							img {
							  border: 0 !important;
							  outline: none !important;
							}

							p {
							  Margin: 0px !important;
							  padding: 0px !important;
							}

							table {
							  border-collapse: collapse;
							  mso-table-lspace: 0px;
							  mso-table-rspace: 0px;
							}

							td {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							a {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							span {
							  border-collapse: collapse;
							  mso-line-height-rule: exactly;
							}

							.ExternalClass * {
							  line-height: 100%;
							}

							span.MsoHyperlink {
							  mso-style-priority: 99;
							  color: inherit;
							}

							span.MsoHyperlinkFollowed {
							  mso-style-priority: 99;
							  color: inherit;
							}

							@media and screen {
							  td {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							  a {
								font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
							  }
							}
						  </style>
						</head>

						<body>
						  <table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
							width="600" cellspacing="0" cellpadding="0" align="center">
							<tbody>
							  <tr>
										<td style="padding: 3px 20px;">
												<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;"
												 width="560" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;"
															 colspan="2" align="center">Warm Greetings from Nutratimes</td>
														</tr>
														<tr style="background: #f5f5f5;">
															<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
																<a href="http://www.nutratimes.com" target="_blank">
																	<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'"
																	 width="250" height="50"> </a>
															</td>
															<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
																<a style="display: inline-block;width: 135px;background: #e25c26;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 13px;font-size: 13px !important;color: #323232;padding: 6px 0px;text-align: center;border: 1px solid #e25c26;font-weight: 600;text-decoration: none; color: #fff;" href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
							  </tr>

							  <tr>
								<td style="padding: 0 20px;">
								  <table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%" cellspacing="0" cellpadding="0"
									align="center">
									<tbody>
									 <tr>
										<td style="padding: 15px 10px 30px 0px; " align="left" valign="top" width="310">
										  <span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey '.$user_name.',
											</span><br>
											<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 400; color: #4d4d4d;">
													Your password has been changed successfully and new password are as below.
											</span>
										  <br>
										  
										</td>
										<td style="padding: 15px 0px 30px 10px;" align="center" valign="top">
										  <span style="display: block; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 600; color: #6f6f6f; padding: 0px 0 5px;">New Password :</span>
										  <span style="display: block; width: 210px;     margin-bottom: 8px; text-align: center; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 32px; font-size: 22px !important; font-weight: bold; color: #323232; padding: 9px 0px; border: 5px solid #89b863;">                   
											<span>'.$passwd.'</span><br>
										  </span>
										
										<!-- <span style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 12px !important; color: #6f6f6f; font-weight: 400; padding-top: 15px;"
										 >(Valid Only For 30 Minute)</span> -->
								   
										</td>
									  </tr>
									</tbody>
								  </table>
								</td>
							  </tr>


								<tr>
										<td>
											<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
											 href="http://www.nutratimes.com" target="_blank">
												<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
															 align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us at '.$this->setting_admin_email.'.</td>
														</tr>
													</tbody>
												</table>
											</a>
										</td>
									</tr>
							</tbody>
						  </table>
						  <center>


							<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
							  <tbody>
								<tr>
								  <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
									<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
									  <tbody>
										<tr>
										  <td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
											<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, | Bengaluru-560078
											| Karnataka
											<br>
											<br>
										  </td>
										</tr>
									  </tbody>
									</table>
								  </td>
								</tr>
							  </tbody>
							</table>
						  </center>

						</body>

						</html>';		
				
			
			if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				
				return true;
			} else {

				return false;
			}
		}
	}
	
	public function email_waiting_order_confirmation($user_id, $order_id, $payment_mode = 1) {
		// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		// GET USER DETAILS
		$res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = '.$user_id);
		if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email; 
				
				$subject = "Order Placed Successfully on ".$this->setting_site_name;
				$message = '<table width="100%" border="0">
					  <tr>
						<td><table cellpadding="0" cellspacing="0" align="center" style="width:600px; border:0px none; font-family:\'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; font-size:12px; color:#333; border:1px solid #ffa200;">
							<tr style="background-color:#f6f6f6;">
							  <td height="40" style="padding:10px 0px 10px 10px ;"><img src="'.SITE_URL.'/images/shp_logo.png" width="129" height="35" alt="'.$this->setting_site_name.'" title="'.$this->setting_site_name.'" /></td>
							</tr>
							<tr style="background-color:#006FDB; ">
							  <td height="40" style="padding:0px 10px 0px 10px; font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; color:#FFF;">Respond to our message & get confirmed your order!</td>
							</tr>
							<tr>
							  <td height="10" style="font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; padding:10px 15px 10px 15px; ">Dear '.$user_data->name.',</td>
							</tr>
							<tr>
							  <td height="10" style="padding:10px 15px 10px 15px;">Greetings from <b>'.SITE_NAME.'!</b><br />
									Thank You for placing order with us. We have received your order but to get it confirmed please reply to the message we have sent to your registered mobile number. With that your order gets confirmed and we will give you an order confirmation mail at your registered email id</td>
							</tr>';
							
							if ($payment_mode == 2) {
									$message .= '<tr>
							  <td style="padding:10px 15px 10px 15px;font-family:arial;font-size:12px;color:#333; line-height:18px;"></td>
							  <tr>
							  	<td style="padding:10px 15px 10px 15px;font-family:arial;font-size:12px;color:#333; line-height:18px;"></td>';
							}
							
							$message .= '<tr>
							  <td height="10" style="padding:10px 15px 10px 15px; border-bottom:1px solid #E4E4E4; font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; color:#333;">Item(s) Ordered :</td>
							</tr>
							<tr>
							  <td height="10"><table width="576" border="0" align="center" style="padding:10px 10px 0px 10px;">
								  <tr>
									<td width="55" height="74" valign="top"><strong>Product Details</strong></td>
									<td width="15" valign="top"><strong>Purchase Date</strong></td>
									<td width="5" valign="top"><strong>Ordered Quantity</strong></td>
									<td width="10" valign="top"><strong>Price</strong></td>
									<td width="15" valign="top"><strong>Sub Total</strong></td>
								  </tr>';
							
							$order_product = $this->db->query('SELECT 
																	o.payment_mode, 
																	p.name, 		up.cash_price, 
																	up.cod_price, 	up.shipping, 
																	up.contest, 	up.quantity
																FROM  
																	'.ORDER.' o 
																	INNER JOIN '.USER_PRODUCTS.' up on (up.order_id = o.id)  
																	INNER JOIN '.PRODUCT.' p ON (p.prod_id = up.product)
																WHERE 
																	up.status = 1 and 
																	up.contest = 0 and 
																	up.order_id='.$order_id);
							if($order_product->num_rows() > 0) {
								$total = 0;
								foreach ($order_product->result() as $val_order_product) {
									$shipping[] = $val_order_product->shipping;
									$price = $val_order_product->payment_mode == 1 ? $val_order_product->cash_price : $val_order_product->cod_price;
									
									$message .= '<tr style="background-color:#EAEAEA;">
													<td height="50">'.$val_order_product->name.'</td>
													<td align="center">'.date('M, d Y').'</td>
													<td align="center">'.$val_order_product->quantity.'</td>
													<td align="center">'._r($price).'</td>
													<td align="center">'._r($price * $val_order_product->quantity).'</td>
												  </tr>';
									$total += ($price * $val_order_product->quantity);
								}
								$shipping = $val_order_product->payment_mode == 2 ? 0 : max($shipping);
							}
							
							$message .= '
								  <tr>
									<td colspan="3"></td>
									<td colspan="1">Sub Total</td>
									<td align="center">'.$total.'</td>
								  </tr>
								  <tr>
									<td colspan="3"></td>
									<td colspan="1">Shipping Charge</td>
									<td align="center">'.$shipping.'</td>
								  </tr>
								  <tr>
									<td colspan="3"></td>
									<td colspan="1">Grand Total</td>
									<td align="center"><b>'._r($total + $shipping).'</b></td>
								  </tr>
								</table></td>
							</tr>
							<tr>
							  <td height="10"  width="100%">
                              <table>
                              <tr><td width="50%" style="padding:10px 0px 0px 15px;">
							  <b>Shipping Address:</b><br />
								'.$user_data->name.'<br />
								'.$user_data->address.'<br />
								'.ucfirst($user_data->city).' - '.$user_data->pincode.'<br />
								'.$this->all_array['ARR_STATE'][$user_data->state].'<br />
								Phone: '.$user_data->mobile.'<br /></td>';
								
								if ($payment_mode == 2) {
									$message .= '<td width="50%" align="right" style=" font:bold 18px Arial, Helvetica, sans-serif; color:#333; text-decoration:none;">Cancellation Process<br /><a href="'.SITE_URL.'orderhistorycod" style="font:12px Arial, Helvetica, sans-serif; text-decoration:none; color:#333;">If you want to cancel the order, Please <strong style="font-size:18px; text-decoration:underline;">click here</strong> </a></td>';
								}
							
							$message .= '</tr>
                            </table></td></tr>
							<tr>
							  	<td style="text-align:right">
									<table width="100%">
										<tr>
											<td style="text-align:left"></td>
											<td style="text-align:right">Contact Us : '.$this->setting_phone_no.'<br />'.$this->setting_admin_email.'</td>
										</tr>
									</table>
								</td>
							</tr>
						  </table></td>
					  </tr>
					</table>';
							
					if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
						return true;
					} else {
						return false;
					}
			}
		}
	}
	
	
	public function _order_contest_confirmation($user_id, $order_id) {
			// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		// GET USER DETAILS
		$res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = '.$user_id);
		if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email; 
				
				$subject = "Order Placed Successfully on ".$this->setting_site_name;
				$message = '';
							
					if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
						return true;
					} else {
						return false;
					}
			}
		}
							
	}


	
	public function dietitian_mail($order_id) {
		
			// UPDATE VARIABLE AND SET SETTING INFORMATION
		$this->update_settings();
		
		// GET USER DETAILS
		$res_user = $this->db->query('select name, email, mobile, address, city, state, pincode from '.ORDER.' where id = '.$order_id);
		if($res_user->num_rows() > 0) {
			foreach ($res_user->result() as $user_data) {
				
				
				// GET THE DETAILS OF USER
				$from_email = $this->setting_admin_email;
				$from_name = $this->setting_admin_name;
				$to = $user_data->email;
				$filename   = "nutratimes-dietchart.pdf";
				$path_name	= "./cdn/pdf/nutratimes-dietchart.pdf";
			 
				
				$subject = ucwords($user_data->name)." - Your Nutratimes Diet Chart to get fit soon!";
				$message = '<html>
							<head>
								<title>'.$this->setting_site_name.'</title>
								<style type="text/css">
									body {
										-webkit-text-size-adjust: 100% !important;
										-ms-text-size-adjust: 100% !important;
										-webkit-font-smoothing: antialiased !important;
									}

									img {
										border: 0 !important;
										outline: none !important;
									}

									p {
										Margin: 0px !important;
										padding: 0px !important;
									}

									table {
										border-collapse: collapse;
										mso-table-lspace: 0px;
										mso-table-rspace: 0px;
									}

									td {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									a {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									span {
										border-collapse: collapse;
										mso-line-height-rule: exactly;
									}

									.ExternalClass * {
										line-height: 100%;
									}

									span.MsoHyperlink {
										mso-style-priority: 99;
										color: inherit;
									}

									span.MsoHyperlinkFollowed {
										mso-style-priority: 99;
										color: inherit;
									}

									@media and screen {
										td {
											font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
										}

										a {
											font-family: "Open Sans", Arial, Helvetica, sans-serif !important;
										}
									}
								</style>
							</head>

							<body>
								<table style="width: 600px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; color: #323232; background: #ffffff;"
								 width="600" cellspacing="0" cellpadding="0" align="center">
									<tbody>
										<tr>
											<td style="padding: 3px 20px;">
												<table style="width: 560px; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif; border-bottom: 1px solid #d7d7d7;"
												 width="560" cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; font-size: 13px !important; color: #6f6f6f; padding: 10px 0px 10px;"
															 colspan="2" align="center">Warm Greetings from Nutratimes</td>
														</tr>
														<tr style="background: #f5f5f5;">
															<td style="width: 260px; padding-bottom: 10px;padding-top: 10px;padding-left: 10px;">
																<a href="http://www.nutratimes.com" target="_blank">
																	<img style="border: none; display: block;" src="http://www.nutratimes.com/cdn/image/catalog/logo.png" alt="'.$this->setting_site_name.'"
																	 width="250" height="50"> </a>
															</td>
															<td style="width: 300px; padding-bottom: 10px;padding-top: 10px;padding-right: 10px;" align="right">
																<a style="display: inline-block;width: 135px;background: #e25c26;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 13px;font-size: 13px !important;color: #323232;padding: 6px 0px;text-align: center;border: 1px solid #e25c26;font-weight: 600;text-decoration: none; color: #fff;"
																 href="http://www.nutratimes.com/login" target="_blank">MY ACCOUNT</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>

										<tr>
											<td style="padding: 0 20px;">
												<table style="width: 100%; margin: 0 auto; font-family: Open Sans, Arial, Helvetica, sans-serif;" width="100%"
												 cellspacing="0" cellpadding="0" align="center">
													<tbody>
														<tr>
															<td style="padding: 15px 10px 30px 0px; " align="left" valign="top" width="310">
																<span style="display: block; font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 18px; font-size: 18px !important; font-weight: 600; color: #323232;">Hey
																	'.$user_data->name.',
																</span><br>

																<span style="display: block;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 16px !important;font-weight: 400;color: #4d4d4d;margin-bottom: 14px;line-height: 22px;">We
																	are overwhelmed with happiness to provide your Nutratimes Diet Chart which is personally prepared to
																	effectively aid in weight loss.</span><span style="display: block;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 16px !important;font-weight: 400;color: #4d4d4d;margin-bottom: 14px;line-height: 22px;">Following
																	the Diet chart consistently have helped thousands to shed off weight and get fit in short span of time.</span><span
																 style="display: block;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 16px !important;font-weight: 400;color: #4d4d4d;margin-bottom: 14px;line-height: 22px;">Attached
																	your Diet Chart below.</span><span style="display: block;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 16px !important;font-weight: 400;color: #4d4d4d;margin-bottom: 14px;line-height: 22px;">Get
																	Fit Soon. All the best.</span><span style="display: block;font-family: Open Sans, Arial, Helvetica, sans-serif;font-size: 16px !important;font-weight: 400;color: #4d4d4d;text-align: right;line-height: 25px;font-weight: bold;font-style: italic;">Your
																	Dietitian,<br>
																	Nutratimes</span>
																<br>

															</td>

														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>

										</tr>

										<tr>
											<td>
												<a style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 14px; font-size: 14px !important; font-weight: 600; text-decoration: none; color: #89b863; line-height: 1.7em; letter-spacing: 1px;"
												 href="http://www.nutratimes.com" target="_blank">
													<table style="width: 100%;background: #70a943;color: #FFF;" cellspacing="0" cellpadding="0" align="center">
														<tbody>
															<tr>
																<td style="font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 16px; font-size: 16px !important; color: #ffffff; padding: 15px 10px 15px; line-height: 1.7em;"
																 align="center">For further assistance, feel free to call us at +91-'.$this->setting_phone_no.' or mail us
																	at '.$this->setting_admin_email.'.</td>
															</tr>
														</tbody>
													</table>
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<center>


									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
										<tbody>
											<tr>
												<td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
													<table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
														<tbody>
															<tr>
																<td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
																	<br> Nutratimes | #555, Axis Padegal, 1st Floor, JP Nagar 3rd Phase, 9th Cross, Near Sony Center, |
																	Bengaluru-560078
																	| Karnataka
																	<br>
																	<br>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</center>




							</body>

							</html>';
							
					if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message, $path_name, $filename)) {
						return true;
					} else {
						return false;
					}
			}
		}
							
	}
	
	/*public function _send_error_cart_messsage() {
		$message = 'Following user is trying to direct access payment gateway <br>';
		$message .= '<div>Browser Details  : '.$_SERVER[HTTP_USER_AGENT].'</div>';
		$message .= '<div>Customer Mobile : '.$_SESSION[SVAIZA_MOBILE].'</div>';
		$message .= '<div>Customer Email : '.$_SESSION[SVAIZA_EMAIL].'</div>';
		$message .= '<div>Order Id  : '.$_SESSION[USER_ORDER_ID].'</div>';
		
		if($this->mail_send('admin@shophunk.com', 'shophunk error', 'sukhi@seajintech.com, jaydip@seajintech.com, anil@seajintech.com, rohit.tiwari@seajintech.com', 'cart page direct access', $message)) {
			return true;
		} else {
			return false;
		}
	}*/
	
	public function email_contactus($email,$name) {
		
			// UPDATE VARIABLE AND SET SETTING INFORMATION
		    $this->update_settings();
		
			// GET THE DETAILS OF USER
			$from_email = $this->setting_admin_email;
			$from_name = $this->setting_admin_name;
			$to = $email; 
				
			$subject = "Thanks on ".$this->setting_site_name;
			$message = '<table width="100%" border="0">
					 <tbody>
					 <tr>
					<td>
					<table cellpadding="0" cellspacing="0" align="center" style="width:600px;border:0px none;font-family:\'Open Sans\',\'Trebuchet MS\',Helvetica,Arial,sans-serif;font-size:12px;color:#333;border:1px solid #ffa200">
							<tbody><tr style="background-color:#f6f6f6">
							 <td height="40" style="padding:10px 0px 10px 10px"><img src="https://s3-ap-southeast-1.amazonaws.com/shophunk/cdn/images/logo.png" width="129" height="35" alt="shophunk" title="shophunk"></td>
							</tr>
							<tr>
							 <td height="60" style="padding:0px 10px 0px 10px;font:bold 18px \'Open Sans\',\'Trebuchet MS\',Helvetica,Arial,sans-serif;color:#999; border-bottom:1px solid #CCC;"><b></b><br />
                            <span style="color:#000; font-size:13px;"></span>
                             
                             </td>
							</tr>
							<tr>
							 <td height="10" style="font:bold 16px \'Open Sans\',\'Trebuchet MS\',Helvetica,Arial,sans-serif;padding:10px 15px 10px 15px">Dear '.$name.',</td>
							</tr>
							<tr>
							 <td height="10" style="padding:10px 15px 10px 15px"><p>Thank you for contacting us.<br />
							     <br />
							    
							     We are committed to providing you a satisfactory resolution. One of our Customer Service team members will reply to your email within the next 24-48 hours.</p>
							   
							</tr>
							<tr>
						<td height="10" style="padding:0px 15px 0px 15px;"><br />
						If you need any assistance or have any questions, feel free to contact us at '.$this->setting_phone_no.' or '.$this->setting_admin_email.'</b></td>
					</tr>
					<tr>
					  <td height="10" style="padding:0px 15px 0px 15px;">
							<br />
							Regards,<br />
							'.$this->setting_site_name.' Team
					  </td>
					</tr>
					<tr>
						<td height="10"></td>
					</tr>
                		</table>
                     </td>
					</tr>
                    <tr>
                    <td height="20"></td>
                    </tr>
				   </tbody>
				 </table>';
						 
				if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
					return true;
				} else {
						return false;
				}
		
		
	}
}
?>