<?php
class ScrollPagination extends CI_MODEL
{
	function __construct(){
		parent:: __construct();
		
		// USE THE REPLICA DATABASE
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
	}
		
	public function getTotal($statement="", $echo=false, $cache=true) {
		
		// ECHO THE QUERY IF REQUESTED
		if ($echo == true) {
			echo $statement;
			exit;
		}
		
		// TURN OFF THE CACHE IF REQUESTED
		if ($cache == false) {
			$this->replica_db->cache_off();
		}
		
		if($statement != "") {
			$query = $this->replica_db->query($statement);
			$num_rows = $query->num_rows();
			if ($num_rows > 0){
				return $num_rows;
			}
		}
		
		// DEFAULT RETURN FALSE
		return false;
	}
	
	public function getNextPage($statement, $offset, $perpage, $echo=false, $cache=true) {
		$data = NULL;
		
		// PAGINATION DETAILS
		$data['page']['offset'] 		= $offset;
		$data['page']['perpage']		= $perpage;
		$data['page']['total']			= $this->getTotal($statement);
		$data['available'] 				= true;
		// ECHO THE QUERY IF REQUESTED
		if ($echo == true) {
			echo $statement." limit ".$offset.", ".$perpage;
			exit;
		}
		
		// TURN OFF THE CACHE IF REQUESTED
		if ($cache == false) {
			$this->replica_db->cache_off();
		}
		
		if($data['page']['total'] > $offset) {
			// EXECUTE THE QUERY AND GET THE DATA
			$data['result'] = $this->memcache_driver->get_cache(md5($statement." limit ".$offset.", ".$perpage));
			if (!$data['result']) { 		
				$query = $this->replica_db->query($statement." limit ".$offset.", ".$perpage);
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$data['result'][] = $row;
					}
					$this->memcache_driver->set_cache('homepage_secondslide', $data['result'], true, CACHE_LONGTIME);
					return $data;
				}
			} else {
				$data['result'] = $data['result'];
			}
			
		} else {	
			$data['available'] = false;
			return $data;
		}
		return false;
	}
}