<?php
class order_driver extends CI_MODEL
{
	private $data;
	
	function __construct(){
		parent:: __construct();
		
		// GET ALL ARRAY - START
		$this->data['all_array'] = all_arrays();
		// GET ALL ARRAY - END
	}
	
	public function insert_order_pg($order_id, $amount, $pg, $dt_c) {
		$data = array ( 'order_id' 	=> $order_id,
						'amount' 	=> $amount,
						'pg' 		=> $pg,
						'dt_c' 		=> $dt_c);
		$this->db->insert(ORDER_PG, $data);
		$pg_id = $this->db->insert_id();
		
		// NOTE THE LOGS
		$this->common_model->order_logs(0, 0, 'Order Payment Gateway id : '.$pg_id.' inserted', $_SESSION[SVAIZA_USER_ID], $order_id);
		
		return $pg_id; 
			
	}
	
	public function update_orderpg_id($order_pg_id, $order_id) {
		if($order_pg_id != '') {
			$this->db->where('id', $order_id);
			$this->db->update(ORDER, array('order_id_pg' => $order_pg_id));
			
			// NOTE THE LOGS
			$this->common_model->order_logs(0, 0, 'Order Payment Gateway id : '.$order_pg_id.' Updated in Orders table', $_SESSION[SVAIZA_USER_ID], $order_id);
			return true;
		}
		return false;
	}
	public function set_order_paid($ord_id_pg = "") {
		if($ord_id_pg != "") {
			$this->db->where('order_id_pg', $ord_id_pg);
			$this->db->update(ORDER, array('payment_status' => '1', 'caller_status'  => '2', 'qualified_date' => date('Y-m-d')));
			
			// NOTE THE LOGS
			$this->common_model->order_logs(0, 0, 'Order Payment done by buyer', $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
			
			return true;
		}
		return false;
	}

	public function _insert_point_history($total_amount="0") {
		// INSERT INTO LOG TABLE
		$log_data = array(
		   'order_id' => $_SESSION[USER_ORDER_ID],
		   'points' => $total_amount,
		   'status' => '1'
		);
		
		// NOTE THE LOGS
		$this->common_model->order_logs(0, 0, $total_amount.' Credit point inserted for the order', $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
		
		// INSERT INTO CREDIT POINT USAGE HISTORY TABLE
		$this->db->insert(POINT_HISTORY, $log_data); 
		return true;
	}
	
	public function _count_redeemable_credit_point() {
		$cart_data = $this->db->query("SELECT 
											up.product,	 up.cash_price, c.cat_id 
										FROM 
											".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id), 
											".PRODUCT."	p left join ".CATEGORY." c on (p.cat_id = c.cat_id) 
										WHERE 
											o.id = ".$_SESSION[USER_ORDER_ID]." and 
											up.product = p.prod_id and 
											up.status = 1 
										GROUP BY 
											c.cat_id");
											
		if($cart_data->num_rows() > 0) {
			$total_redemption = 0;
			foreach ($cart_data->result() as $row) {
				$max_redemption = 0;
				
				// GET TOP LEVEL CATEGORY FROM CURRNET CATEGORY
				$parent_cat_id = $this->general->get_top_level_category($row->cat_id);
				
				// GET MAXIMUM REDEMPTION POINT FROM SELECTED CATEGORY
				$max_redemption = $this->general->get_category_max_redemption($parent_cat_id, $row->cash_price);
				
				// INCREASE TOTAL REDEMPTION
				$total_redemption += $max_redemption;
			}
		}
		return ($total_redemption > 0) ? $total_redemption : '0';
	}
	
	public function count_deduction($final_price, $final_shipping, $product_max_redemption, $user_points) {
		$arr_return = array();
		// SET THE MAXIMUM REDEMPTION AMOUNT
		if($product_max_redemption >= $final_price) { 
			$product_max_redemption = $final_price;
		} else if($product_max_redemption < $final_price) {
			$product_max_redemption += $final_shipping; 
		}
		
		if($user_points >= $product_max_redemption) {				
			if($product_max_redemption >= $final_price) {
				$arr_return['final_price'] = $final_price - $final_price;
				$arr_return['final_redemption'] = $final_price;
			} else {
				$arr_return['final_price'] = $final_price - $product_max_redemption;
				$arr_return['final_redemption'] = $product_max_redemption;
			}
		} else if($user_points < $product_max_redemption) {
			$arr_return['final_price'] = $final_price - $user_points;	
			$arr_return['final_redemption'] = $user_points;
		}
		return $arr_return;
	}
	
	public function _calculate_cart_amount() {
		
		// CACHE OFF FOR THIS QUERY
		$this->db->cache_off();
		
		// GENERATE THE USER ORDER DETAILS - START
		$this->get_order_user_details();
		
		if($this->data['cart'] == false) {
			redirect(SITE_URL.'paymentfailure/index/');
		}
		// GENERATE THE USER ORDER DETAILS - STOP
		
		// INSERT INTO ORDERID-PG - START
		$final_amount = $final_shipping = 0;
		if(count($this->data['cart']) > 0) {
			foreach ($this->data['cart'] as $cart_rows) {
				// GET USER DETAILS - START 
				$this->data['user_details']['uname'] = $cart_rows->name;
				$this->data['user_details']['uemail'] = $cart_rows->email;
				$this->data['user_details']['umobile'] = $cart_rows->mobile;
				$this->data['user_details']['uaddress'] = $cart_rows->address;
				$this->data['user_details']['ucity'] = $cart_rows->city;
				$this->data['user_details']['ustate'] = $this->data['all_array']['ARR_STATE'][$cart_rows->state];
				$this->data['user_details']['upincode'] = $cart_rows->pincode;
				// GET USER DETAILS - END
				
				$final_amount += ($cart_rows->cash_price * $cart_rows->quantity);
				$shipping[] = $cart_rows->shipping;
			}
			$final_shipping = max($shipping);
			$this->data['final_amount'] = $final_amount;
			$this->data['final_shipping'] = $final_shipping;
			return $this->data;
		}
		return false;
	}
	
	public function get_order_user_details() {
		// CACHE OFF FOR THIS QUERY 
		$this->db->cache_off();
		$cart_data = $this->db->query("SELECT 
											o.name, 		o.email, 	o.mobile, 		o.address, 
											o.city, 		o.state, 	o.pincode, 		up.quantity, 
											up.cash_price, 	up.shipping 
									   FROM 
											".ORDER." o inner join ".USER_PRODUCTS." up on (o.id = up.order_id) 
									   WHERE 
											up.status = 1 and 
											up.order_id = ".$_SESSION[USER_ORDER_ID]);
		if($cart_data->num_rows() > 0) {
			foreach($cart_data->result() as $row) {
				$this->data['cart'][] = $row;
			}
		} else {
			$this->data['cart'] = false;
		}
	}
		
}