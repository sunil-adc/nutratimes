<?php
class Common_Model extends CI_MODEL {
	
	public $memcache;
	function __construct(){
		parent:: __construct();
	}
	
	function Menu_Array(){ 
		
		$site_manager = array(
			array("Site Settings","sitesettings","sitesettings",1),
			array("Homepage Manager","homemanager","homemanager",2),
			array("Payment Gateway","managepaymentgateway","addpaymentgateway", 3),
			array("Static Pages","managestaticpages","addstaticpages",4),
			array("Blog Content","manageblog","addblog",5),
			//array("Slider Manager","manageslider","addslider",5),
		);
		
		$feedback_manager = array(
			array("Feedback","managefeedback","managefeedback",1),
			array("Complaints","managecomplaints","addreply",2),
			array("Contacts","managecontactus","managecontactus",3),
			array("Affilates","manageaffilate","manage-affilate",4),
		);
		
		$product_menu = array(
			array("Manage Brands","managebrands","addbrands",2),
			//array("Manage Genre","managegenre","addgenre",4),
			array("Manage Categories","managecategory","addcategory",1),
			//array("Manage Color","managecolor","addcolor",3),
			array("Manage Products","manageproducts","addproducts",3),
			//array("Super-duper deal","managecombo","addcombo",4),
			//array("Manage Country","managecountry","addCountry",5),
			array("Purchase Report","reportpurchase","reportpurchase",6),
			array("COD Delievery Report","reportcoddelivery","reportcoddelivery",7),
			array("Agent Wise Delievery Report","reportagentwise","reportagentwise",8),
			array("CPL Network Report","reportnetwise","reportnetwise",9,"reportpubwise"),
			array("Citywise Report","reportcitywise","reportcitywise",10,"reportcitywise"),
			array("L2C Report","reportleadtoconversion","reportleadtoconversion",11),
			array("Contest Report","reportcontest","reportcontest",12),
			array("Support Ticket","supportticket","addticket",13),
			array("Products Logs","manageproductslogs","manageproductslogs",14),
		);
		
		$adminsetting = array(
			array("Change Password","changepassword","changepassword",1),
			array("Update Profile","updateprofile","updateprofile",2),
			array("Manage Cache","deletecache","deletecache",3),
			array("Manage Roles","managerole","addrole",4),
			array("Manage Users","manageadminuser","addadminuser",5),
			array("Manage Sales Users","managesalesusers","addsalesusers",7),
			array("Manage Sales & QA","managesalesqa","addsalesqa",8),
			array("Admin Login History","manageloginhistory","manageloginhistory",9),
			
		);	
		
		$order_menu = array(
			array("Small Form Lead","smallform_lead","smallform_lead",1,"smallform_lead"),
			array("Quiz Form Lead","quizform_lead","quizform_lead",2,"quizform_lead"),
			array("Create Orders","createorder","createorder",3,"orderproducts"),
			array("Search Orders","searchorders","searchorders",4,"updatepaymentstatus","updatecredits"),
			array("Hairoil Orders","searchorders_hairoil","searchorders_hairoil",5,"updatepaymentstatus","updatecredits"),
			array("Reject Order & Update Payment Status","rejectupdatepaymentstatusorder","rejectupdatepaymentstatusorder",6),
			array("Update Return Received","updatereturnreceived","updatereturnreceived",7),
			array("Update Delivery Status","updatedeliverystatus","updatedeliverystatus",8),
			//array("Update Send","updatesend","updatesend",6),
			//array("Order PG Search","orderpgsearch","orderpgsearch",7),
			array("Send User Password","senduserpassword","senduserpassword",9),
			array("Update User Email","updateemail","updateemail",10),
			array("Send but not delivered","sendbutnotdelivered","sendbutnotdelivered",11),
			array("CASH Pending Shipment","cashpendingshipment","cashpendingshipment",12),
			array("COD Pending Shipment","codpendingshipment","codpendingshipment",13),
			array("Reconcile","reconcile","reconcile",14),
			array("Refund Request","managerefundrequest","addrefundrequest",15)
		);
			
		$inventory_manager = array(
			array("Purchanse Report","managepurchase","mangaepurchase",1),
			array("Out of Stock High Priority Report","manageoutofstock","manageoutofstock",2),			
		);
		
		
		$providers_menu = array(
			array("Manage Providers","manageproviders","updateproviders", 1),
			array("Upload AWB No","uploadawbno","uploadawbno", 2),
			array("Replace AWB No","replaceawbno","replaceawbno", 3),
			array("Assign AWB No","assignawbno","assignawbno", 4),
			//array("Bluedart code uploader","bluedartcodeuploader","bluedartcodeuploader", 5),
		);
		
		$marketing = array(
			array("Parent Pincode","manageparentpincode","updateparentpincode",1),
			array("Manage CPL Networks","managenetworks","addnetworks",2),
			array("Manage CPS Networks","managecpsnetworks","addcpsnetworks",3),
			array("Manage Pubs","managepubs","addpubs",4),
		);
		
		$account = array(
			array("Products","accountproduct","accountproduct",1),
			array("Invoice","accountinvoice","accountinvoice",2),
			array("Manage Keywords","managekeywords","addkeywords",3)
		);
		
		$affiliates = array(
			array("Manage Banners","managebanner","addbanner",1)
		);

		$menu = array(
					array(SITE_NAME,			"manageproducts",		$product_menu,		1, 'svaiza-t'),	
					array("Orders",				"searchorders",			$order_menu,		2, 'order-t'),	
					array("Feedback",			"managefeedback",		$feedback_manager,	3, 'feedback-t'),
					array("Providers",			"assignawbno",			$providers_menu,	4, 'providers-t'),	
					array("Site Manager",		"homemanager",			$site_manager,		5, 'site-manager-t'),	
					array("Misc Ops",			"manageparentpincode",	$marketing,			6, 'misc-ops-t'),
					array("Inventory",			"managepurchase",		$inventory_manager,	7, 'purchase-t'),		
					//array("Affiliates",			"managebanner",			$affiliates,		8, 'feedback-t'),
					array("Account",			"accountproduct",		$account,			9, 'accountant-t'),
					array("Settings",			"updateprofile",		$adminsetting,		10, 'admin-settings-t'),		
				);
		
		return $menu;
	}
	
	function create_combo($name, $id, $tablename, $where="", $field, $value, $default, $onchange='javascript:void(0)', $tabindex=false, $arr_replace="", $is_val_blank=false, $disable_color=false, $extra = ''){
		
		$str = NULL;
		$selected = NULL;
		$color = NULL;
		
		if ($disable_color == true) {
			$this->db->select($field.", ".$value.", status");
		} else {
			$this->db->select($field.", ".$value);
		}
		
		if($where != "") {
			$this->db->where($where);
		}
		
		$this->db->order_by($field, 'asc');
		$this->db->from($tablename);
		
		
		// CACHING OFF FOR THIS QUERY
		$this->db->cache_off();
		$query = $this->db->get();
		
		$tabindex = ($tabindex != false) ? 'tabindex="'.$tabindex.'"' : '';
		
		$str .= '<select class="small-input" name="'.$name.'" id="'.$id.'" onchange="'.$onchange.'" '.$tabindex.' '.$extra.'>';
		
		if($is_val_blank == true){
			$str .= '<option value="">Select Option</option>';
		}else{
			$str .= '<option>Select Option</option>';
		}
		if($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$selected = ($row->$value == $default) ? 'selected="selected"' : "";
				
				if ($disable_color == true) {
					$color = ($row->status < 1) ? 'style="background:#FFD7D7"' : ''; 
				}
				
				if($arr_replace != "" && is_array($arr_replace)) {
					$str .= '<option value="'.$row->$value.'" '.$selected.' '.$color.'>'.$arr_replace[$row->$field].'</option>';
				} else {
					$str .= '<option value="'.$row->$value.'" '.$selected.' '.$color.'>'.$row->$field.'</option>';
				}
			}
		}
		unset($selected);
		$str .= '</select>';
		return $str;
	}
	
	function assign_awbno($tablename, $field, $payment_mode, $primary_field, $id) {
		
		$this->db->cache_off();
		$query = $this->db->query("select ".$field.", ".$primary_field." from ".$tablename." where payment_mode=".$payment_mode." AND status=1 limit 0,1");
	
		if ($query->num_rows() > 0) {
			$awb_no = $query->row_array();
			
			if (trim($awb_no[$field]) != "" && trim($awb_no[$primary_field]) != "") {
				// UPDATE AWBNO AS USED
				$data = array(
					'status' => 2
				);
				$this->db->where($primary_field, $awb_no[$primary_field]); 
				$this->db->update($tablename, $data);
				// UPDATE AWBNO AS USED
			}
			return ($awb_no[$field] != '') ? $awb_no[$field] : false;
		}
		return false;
	}
	
	public function product_upload_statistics($days = 15, $replica_db="") {
		   $arr_date = NULL;
			
		   for($j = 1; $j <= $days; $j++) {
			   $arr_date[date('Y-m-d', strtotime('-'.$j.' day'))] = 0;
		   }
	   
		  $key = 'prodStatsMemcache'; // Unique Words
		  $cache_result = array();
		  $cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		  
	 
		  if ($cache_result) {
				$arr_date = $cache_result;
		  } else {
			   $sel = $replica_db->query('SELECT 
											datecreated, count(PROD_ID) as total_prod
										FROM 
											'.PRODUCT.' 
										WHERE 
											DATECREATED between date_sub(now(),INTERVAL '.$days.' DAY) and now() 
										group by 
											left(datecreated,10)');
				if($sel->num_rows() > 0) {
					foreach ($sel->result() as $row) {
						$arr_date[date('Y-m-d', strtotime($row->datecreated))] = $row->total_prod;
					}
					$this->memcache_driver->set_cache($key, $arr_date, true, CACHE_LAZY); 
				}
		  }
		return $arr_date;
	}
	
	public function find_category_level($tablename = "", $field = "", $catid = "") {
		if($catid != "") {
			return $this->db_function->get_single_value($tablename, $field, "cat_id = ".$catid, $echo = false, $cache_off = true) + 1;
		}
		return '0';
	}
	
	public function fetch_category($cat_id="0", $def_value="", $cur_cat_id="0") {
		$query = $this->db->query("select cat_id, parent_cat_id, cat_level, name from ".CATEGORY." where status = 1 and parent_cat_id = ".$cat_id." and cat_id != ".$cur_cat_id);
		if($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$selected = ($row->cat_id == $def_value) ? 'selected=selected' : '';
				echo "<option value='".$row->cat_id."' ".$selected.">".make_space('&nbsp;', $row->cat_level).$row->name."</option>";
				$this->fetch_category($row->cat_id, $def_value, $cur_cat_id);
			}
		}
	}
	
	public function fetch_menu_category($cat_id="0", $random_id="") {
		$str = "";
		$query = $this->db->query("select cat_id, seourl, name from ".CATEGORY." where status = 1 and parent_cat_id = ".$cat_id);
		if($query->num_rows() > 0) {
			$str .= '<div class="sub-menu" id="'.$random_id.'">';
			foreach ($query->result() as $row) {
				$str .= '<div class="sub-menu-inner left"><a href="'.SITE_URL.'search?&sc='.$row->cat_id.'">'.$row->name.'</a></div>';
				$str .= $this->fetch_menu_category($row->cat_id, $random_id);
			}
			$str .= '</div>';
		}
		return $str;
	}
	
	public function total_orders() {
		$res_cod_num_rows = $res_cash_num_rows = 0;
		$cache_result = $this->memcache_driver->get_cache('dashboardCashOrderTotal'); // Memcached object 
		if ($cache_result) {
			$res_cash_num_rows = $cache_result;
		} else {
			$res_cash = $this->db->query("SELECT 
												count(o.id) as total_order 
										  FROM 
												".ORDER." o 
												inner join ".USER_PRODUCTS." p on (o.id = p.order_id) 
										  WHERE 
												p.status = 1 and 
												p.contest = 0 and 
												p.status != 0 and 
												o.payment_mode = '1' and 
												o.payment_status = '1' 
										  GROUP BY o.id");
			$res_cash_num_rows = $res_cash->num_rows();
			$this->memcache_driver->set_cache('dashboardCashOrderTotal', $res_cash_num_rows, true, 86400);
		}
		
		$cache_result = $this->memcache_driver->get_cache('dashboardCodOrderTotal'); // Memcached object 
		if ($cache_result) {
			$res_cash_num_rows = $cache_result;
		} else {
			$res_cod = $this->db->query("SELECT 
												count(o.id) as total_order 
										  FROM 
												".ORDER." o 
												inner join ".USER_PRODUCTS." p on (o.id = p.order_id)  
										  WHERE 
												p.status = 1 and 
												p.contest = 0 and 
												p.status != 0 and 
												o.payment_mode = '2' 
										  GROUP BY 
												o.id");
			$res_cod_num_rows = $res_cod->num_rows();
			$this->memcache_driver->set_cache('dashboardCodOrderTotal', $res_cod_num_rows, true, 86400);
		}

		$total_order = $res_cash_num_rows + $res_cod_num_rows;
		
		if ($total_order > 0) {
			return $total_order;
		} else {
			return '0';
		}
	}


	public function tot_orders($from_date, $to_date) {

		$total_order = 0;
		
		$res_cash = $this->db->query("SELECT 
												count(o.id) as total_order 
										  FROM 
												".ORDER." o 
												inner join ".USER_PRODUCTS." p on (o.id = p.order_id) 
										WHERE 
										  		p.status = 1 and 
												p.contest = 0 and 
												p.status != 0 and
												(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".$from_date."' and '".$to_date."')
										  GROUP BY o.id");
		
		$total_order = $res_cash->num_rows();
		
		if ($total_order > 0) {
			return $total_order;
		} else {
			return '0';
		}
	}
	
	public function total_notdelivered_order() {
		
		$res_cod_num_rows = $res_cash_num_rows = 0;
		$cache_result = $this->memcache_driver->get_cache('dashboardCashOrderNotDelivered'); // Memcached object 
		if ($cache_result) {
			$res_cash_num_rows = $cache_result;
		} else {
			$res_cash = $this->db->query("SELECT 
											count(o.id) as total_order 
										FROM 
											".ORDER." o 
											inner join ".USER_PRODUCTS." p on (o.id = p.order_id) 
										WHERE 
											p.status = 1 and 
											o.caller_status = '2' and 
											p.contest = 0 and 
											p.status != 0 and 
											o.delivery_status not in (2, 3 ,4) and 
											o.payment_mode = '1' and 
											o.payment_status = '1' 
										GROUP BY  
											o.id");
			$res_cash_num_rows = $res_cash->num_rows();
			$this->memcache_driver->set_cache('dashboardCodOrderNotDelivered', $res_cash_num_rows, true, 86400);
		}
		
		$cache_result = $this->memcache_driver->get_cache('dashboardCodOrderNotDelivered'); // Memcached object 
		if ($cache_result) {
			$res_cod_num_rows = $cache_result;
		} else {
			$res_cod = $this->db->query("SELECT 
												count(o.id) as total_count 
										 FROM 
												".ORDER." o 
												inner join ".USER_PRODUCTS." p on (o.id = p.order_id) 
										 WHERE 
												p.status = 1 and 
												o.qa_status = '2' and 
												o.caller_status = '2' and 
												o.delivery_status not in (2, 3 ,4) and 
												p.contest = 0 and 
												p.status != 0 and 
												o.payment_mode = '2' 
										 GROUP BY o.id");
			$res_cod_num_rows = $res_cod->num_rows();
			$this->memcache_driver->set_cache('dashboardCodOrderNotDelivered', $res_cod_num_rows, true, 86400);
		}
		
		$total_order = $res_cash_num_rows + $res_cod_num_rows;
		
		if ($total_order > 0) {
			return $total_order;
		} else {
			return '0';
		}
	}
	
	public function order_statistics($days = 7, $replica_db) {
		$arr_date = NULL;
		
	   for($j = 1; $j <= $days; $j++) {
			$arr_date['total_order'][date('Y-m-d', strtotime('-'.$j.' day'))] = 0;
			$arr_date['total_qualified'][date('Y-m-d', strtotime('-'.$j.' day'))] = 0;
			$arr_date['total_sent'][date('Y-m-d', strtotime('-'.$j.' day'))] = 0;
			$arr_date['total_return'][date('Y-m-d', strtotime('-'.$j.' day'))] = 0;
	   }
	   
		$key = 'orderTotalStatsMemcache_new'; // Unique Words
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		if ($cache_result) {
			$arr_date['total_order'] = $cache_result;
		} else {
		   // TOTAL ORDERS START
		   $sel = $replica_db->query('SELECT 
										dt_c, count(id) as total_order
									FROM 
										'.ORDER.' 
									WHERE 
										dt_c between date_sub(now(),INTERVAL '.$days.' DAY) and now() 
									group by 
										dt_c');
			if($sel->num_rows() > 0) {
				foreach ($sel->result() as $row) {
					$arr_date['total_order'][date('Y-m-d', strtotime($row->dt_c))] = $row->total_order;
				}
				$this->memcache_driver->set_cache($key, $arr_date['total_order'], true, CACHE_LAZY); 
			}
			// TOTAL ORDERS END
		}
		
		
		$key = 'orderQualifiedStatsMemcache_new'; // Unique Words
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		if ($cache_result) {
			$arr_date['total_qualified'] = $cache_result;
		} else {
		   // TOTAL QUALIFIED START
		   $sel = $replica_db->query('SELECT 
										qualified_date, count(id) as total_qualified_order
									FROM 
										'.ORDER.' 
									WHERE 
										qualified_date between date_sub(now(),INTERVAL '.$days.' DAY) and now() 
									group by 
										qualified_date');
			if($sel->num_rows() > 0) {
				foreach ($sel->result() as $row) {
					$arr_date['total_qualified'][date('Y-m-d', strtotime($row->qualified_date))] = $row->total_qualified_order;
				}
				$this->memcache_driver->set_cache($key, $arr_date['total_qualified'], true, CACHE_LAZY);
			}
			// TOTAL QUALIFIED END
		}
		
		$key = 'orderSentStatsMemcache_new'; // Unique Words
		$cache_result = array();
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$arr_date['total_sent'] = $cache_result;
		} else {		
			// TOTAL SENT START
		   $sel = $replica_db->query('SELECT 
										sent_date, count(id) as total_sent_order
									FROM 
										'.ORDER.' 
									WHERE 
										sent_date between date_sub(now(),INTERVAL '.$days.' DAY) and now() 
									group by 
										sent_date');
			if($sel->num_rows() > 0) {
				foreach ($sel->result() as $row) {
					$arr_date['total_sent'][date('Y-m-d', strtotime($row->sent_date))] = $row->total_sent_order;
				}
				$this->memcache_driver->set_cache($key, $arr_date['total_sent'], true, CACHE_LAZY);
			}
			// TOTAL SENT END
		}
		
		
		$key = 'orderReturnStatsMemcache_new'; // Unique Words
		$cache_result = array();
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$arr_date['total_return'] = $cache_result;
		} else {		
			// TOTAL RETURN START
		   $sel = $replica_db->query('SELECT 
										return_date, count(id) as total_return_order
									FROM 
										'.ORDER.' 
									WHERE 
										return_date between date_sub(now(),INTERVAL '.$days.' DAY) and now() 
									group by 
										return_date');
			if($sel->num_rows() > 0) {
				foreach ($sel->result() as $row) {
					$arr_date['total_return'][date('Y-m-d', strtotime($row->return_date))] = $row->total_return_order;
				}
				$this->memcache_driver->set_cache($key, $arr_date['total_return'], true, CACHE_LAZY);
			}
			// TOTAL RETURN END
		}
		
		return $arr_date;
	}
	public function delete_order_cache() {
		$this->db->cache_delete(CMS_FOLDER_NAME, 'orderdetails');
		$this->db->cache_delete(CMS_FOLDER_NAME, 'orderproducts');		
		$this->db->cache_delete(CMS_FOLDER_NAME, 'searchorders');
	}
	
	public function _get_user_credit_points() {
		$this->db->cache_off();
		$this->db->where('user_id', $_SESSION[SVAIZA_USER_ID]);
		$user_points = $this->db->get(USERS);
		$user_points_data = $user_points->result_array();
		return $user_points_data[0]['points'];
	}
	public function _decide_product_type($orderid = '') {
		
		if ($orderid == '') {
			$orderid = $_SESSION[USER_ORDER_ID];
		}
		
		$decide_pg = $this->db->query("SELECT up.contest FROM ".ORDER." o inner join ".USER_PRODUCTS." up on (o.id = up.order_id) WHERE up.status = 1 and o.id = '".$orderid."'");
		if($decide_pg->num_rows() > 0) {
			$data = array();
			$data['is_contest'] = $data['is_product'] = false;
			foreach ($decide_pg->result() as $row) {
				if($row->contest == '0') {
					$data['is_product'] = true;
				}
				if($row->contest == '1') {
					$data['is_contest'] = true;
				}
			}
			return $data;
		}
		return false;
	}
	public function get_genre_name_by_id ($genre_id = "") {
		if($genre_id != "") {
			$genre = $this->db_function->get_single_value(GENRE, 'genre_name', "genre_id=".$genre_id, false, false);
			if ($genre != false) {
				return $genre;
			}
		}
	}
	public function get_credit_points_by_order_id($ord_id="") {
		if($ord_id != "") {
			$points = $this->db_function->get_single_value(POINT_HISTORY, 'points', "order_id=".$ord_id, false, false);
			if ($points != false) {
				return $points;
			}
		}
	}

	public function count_sale( $replica_db, $from_date, $to_date) {

		$key = 'ordCntSaleStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = "";//$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$sale = $cache_result;
		} else { 
			
				$sale = $replica_db->query("select 
											count(o.id) as total_sale 
									  from 
											".ORDER." o  
									  where 
											o.payment_status = '1' and 
											o.payment_mode = '1' and 
											(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."')");
			
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$sale = ($val->total_sale > 0) ? $val->total_sale : '0';	
				}
			} else {
				$sale = '0';
			}
			//$this->memcache_driver->set_cache($key, $sale, true, CACHE_LAZY);
		}
		return $sale;
	}


	public function count_send_order( $replica_db, $from_date, $to_date, $where) {
		
		$key = 'ordCntCodSaleStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = "";//$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$order_send = $cache_result;
		} else { 
				$sale = $replica_db->query("select 
											count(o.id) as total_send 
									  from 
											".ORDER." o  
									  where 
									  		".$where."
											o.caller_status = '3' and 
											(DATE_FORMAT(o.sent_date, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."')");
			
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$order_send = ($val->total_send > 0) ? $val->total_send : '0';	
				}
			} else {
				$order_send = '0';
			}
			//$this->memcache_driver->set_cache($key, $sale, true, CACHE_LAZY);
		}
		return $order_send;
	}

	public function count_cod_sale( $replica_db, $from_date, $to_date) {
		
		$key = 'ordCntCodSaleStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = "";//$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$sale = $cache_result;
		} else { 
				$sale = $replica_db->query("select 
											count(o.id) as total_sale 
									  from 
											".ORDER." o  
									  where 
											o.payment_status = '1' and
											o.payment_mode = '2' and 
											(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."')");
			
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$sale = ($val->total_sale > 0) ? $val->total_sale : '0';	
				}
			} else {
				$sale = '0';
			}
			//$this->memcache_driver->set_cache($key, $sale, true, CACHE_LAZY);
		}
		return $sale;
	}

	public function _get_fired_leads($day = 1, $replica_db) {
		$key = 'FiredLeadsStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$firedleads = $cache_result;
		} else {
			if($day > 1) {
				$sale = $replica_db->query("select 
												count(o.id) as fire_leads 
										  from 
												".ORDER." o  
										  where 
												o.fire_status = 1 and 
												(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."')");
			} else {
				$sale = $replica_db->query("select 
											count(o.id) as fire_leads
									  from 
											".ORDER." o  
									  where 
											o.fire_status = 1 and 
											o.dt_c between '".date('Y-m-d 00:00:00')."' and '".date('Y-m-d H:i:s')."'");	
			}
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$firedleads = ($val->fire_leads > 0) ? $val->fire_leads : '0';	
				}
			} else {
				$firedleads = '0';
			}
			$this->memcache_driver->set_cache($key, $firedleads, true, CACHE_LAZY);
		}
		return $firedleads;
	}
	
	public function count_revenue($replica_db, $from_date, $to_date) {
		
		$key = 'orderRevenueStatsMemcache_new'; // Unique Words
		$cache_result = array();
		$cache_result = "";//$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			return "sdasd";
			$sale = $cache_result;
		} else {
			
			$sale = $replica_db->query("select 
											sum(up.cash_price * up.quantity + up.shipping) as total_revenue 
									  from 
											".ORDER." o inner join ".USER_PRODUCTS." up on (up.order_id = o.id) 
									  where 
											o.payment_status = 1 and 
											up.status = 1 and 
											(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".$from_date."' and '".$to_date."')");
			
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$sale = ($val->total_revenue > 0) ? $val->total_revenue : '0';	
				}
			} else {
				$sale = '0';
			}
			//$this->memcache_driver->set_cache($key, $sale, true, CACHE_LAZY);
		}
		return $sale;
	}

	public function qa_sales_action($day=1, $action=2, $qa_sales = "caller_status", $replica_db) {
		
		$key = 'qaSales'.$qa_sales.'StatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = ""; //$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$qa_sales_action = $cache_result;
		} else {
			if($day > 1) {
				$sale = $replica_db->query("select 
											count(o.id) as total 
									  from 
											".ORDER." o  
									  where 
											o.payment_mode = 2 and 
											o.".$qa_sales." = ".$action." and 
											(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."')");
			} else {
				$sale = $replica_db->query("select 
											count(o.id) as total 
									  from 
											".ORDER." o  
									  where 
											o.payment_mode = 2 and 
											o.".$qa_sales." = ".$action." and 
											o.dt_c between '".date('Y-m-d 00:00:00')."' and '".date('Y-m-d H:i:s')."'");
			}
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$qa_sales_action = number_format($val->total);	
				}
			}
			//$this->memcache_driver->set_cache($key, $qa_sales_action, true, CACHE_LAZY);
		}
		return $qa_sales_action;
	}
	
	public function _get_growth_rate($replica_db) {
		
		$key = 'GrowthRateStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = $this->memcache_driver->get_cache($key); // Memcached object 
		
		
		if ($cache_result) {
			$growth_rate = $cache_result;
		} else {
			$today_yesterday_sale = "select 
										'today', sum(up.cash_price * up.quantity + up.shipping) as total_sale 
								  from 
										".ORDER." o inner join ".USER_PRODUCTS." up on (up.order_id = o.id)
								  where 
										o.payment_status = 1 and 
										up.status = 1 and 
										(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."')
											
										union all
										
								  select 
										'yesterday', sum(up.cash_price * up.quantity + up.shipping) as total_sale 
								  from 
										".ORDER." o inner join ".USER_PRODUCTS." up on (up.order_id = o.id)  
								  where 
										o.payment_status = 1 and 
										up.status = 1 and 
										o.dt_c between '".date('Y-m-d 00:00:00', strtotime('-1 day'))."' and 
										'".date('Y-m-d H:i:s', strtotime('-1 day'))."'";
			$res_growth = $replica_db->query($today_yesterday_sale);
			
			if($res_growth->num_rows() > 0) {
				foreach ($res_growth->result() as $val) {
					if($val->today == 'today') {
						$today_sale = $val->total_sale;
					} else if($val->today == 'yesterday') {
						$yesterday_sale = $val->total_sale;
					}
				}
			}
			
			if($yesterday_sale > 0) {
				if($yesterday_sale > $today_sale) {
					$growth_rate = "-".($today_sale * 100) / $yesterday_sale;
				} else if($yesterday_sale <= $today_sale){
					$growth_rate = ($today_sale * 100) / $yesterday_sale;
				}
			} else {
				$growth_rate = 100;
			}
			$this->memcache_driver->set_cache($key, $growth_rate, true, CACHE_LAZY);
		}
		return $growth_rate;
	}
	
	public function get_awb_no_arr($table) {
		$return_array = array();
		$this->db->cache_off();
		$res = $this->db->query("select awb_no from ".$table);
		if($res->num_rows() > 0) {
			foreach ($res->result() as $val) {
				$return_array[] = $val->awb_no;
			}
		}
		return $return_array;
	}
	
	public function count_user($replica_db, $from_date, $to_date){
		$key = 'countUsersStatsMemcache'; // Unique Words
		$cache_result = array();
		$cache_result = "";//$this->memcache_driver->get_cache($key); // Memcached object 
		
		
		$total_users = 0;
		if ($cache_result) {
			$total_users = $cache_result;
		} else {
			$replica_db->cache_off();
			
			$sale = $replica_db->query("select count(*) as total_user from ".USERS." where (DATE_FORMAT(datecreated, '%Y-%m-%d') BETWEEN  '".$from_date."' and '".$to_date."')");
			if($sale->num_rows() > 0) {
				foreach ($sale->result() as $val) {
					$total_users = ($val->total_user);	
				}
				//$this->memcache_driver->set_cache($key, $total_users, true, CACHE_LAZY);
			}
		}
		return $total_users;
	}
	public function product_logs($prodid, $userid, $role_id, $action) {
		$data = array(
					  	'prod_id' => $prodid,
						'user_id' => $userid,
						'role_id' => $role_id,
						'action' => $action,
						'log_date' => date('Y-m-d H:i:s')
					  );
		$this->db->insert(PRODUCT_LOGS, $data);
	}
	
	public function credit_logs($userid, $role_id, $points, $action, $credit_user_id) {
		$data = array(
						'user_id' => $userid,
						'role_id' => $role_id,
						'points' => $points,
						'credit_user_id' => $credit_user_id,
						'action' => $action,
						'log_date' => date('Y-m-d H:i:s')
					  );
		$this->db->insert(CREDIT_LOGS, $data);
	}
	
	
	public function order_logs($userid, $role_id, $action, $order_user_id, $order_id) {

		date_default_timezone_set('Asia/Calcutta');

		$data = array(
						'user_id' => $userid,
						'role_id' => $role_id,
						'action' => $action,
						'order_user_id' => $order_user_id,
						'order_id' => $order_id,
						'log_date' => date('Y-m-d H:i:s')
					  );
		$this->db->insert(ORDER_LOGS, $data);
	}
	
	public function can_order_proceed($mobile = 0 ) {
		if ( array_constant_check ($this->session->userdata('uid'), ADMIN_SAVE_ORDER)) {
			if ($mobile > 0) {
				$res = $this->db->query("select 
										id,
										is_fraud,
										delivery_status,
										payment_status,
										payment_mode,
										caller_status,
										return_date,
										dt_c
								  from 
										".ORDER." 
								  where 
										mobile = '".$mobile."'");
				
				if ($res->result() > 0) {
					$total_return_receive = $paid_order = $paid_date = $return_date = $rejected = $pending_shipment = 0;
					foreach ($res->result() as $val) {
						
						if ($val->payment_status == 2 && $val->caller_status == 4) {
							$rejected++;
						}
						
						if ($val->caller_status == 2 && $val->delivery_status == 1) {
							$pending_shipment++;
						}
						
						if ($val->payment_status == 1 && $val->payment_mode == 1) {
							$paid_order++;
							if ($paid_date == '') {
								$paid_date = $val->dt_c;
							} else if (strtotime($paid_date) < strtotime($val->dt_c)) {
								$paid_date = $val->dt_c;
							}
						}
						
						if ($val->payment_mode == 2) {
							if ($val->delivery_status == 4) {
								$total_return_receive++;
								if ($return_date == '') {
									$return_date = $val->return_date;
								} else if (strtotime($return_date) < strtotime($val->return_date)) {
									$return_date = $val->return_date;
								}
							}
						}
					}
					
					if (($total_return_receive >= 2 && strtotime($paid_date) < strtotime($return_date)) || 
						$rejected > 0 || $pending_shipment > 0) {
						return false;
					} else {
						return true;
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}
}
?>