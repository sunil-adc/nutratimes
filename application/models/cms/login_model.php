<?php
class Login_model extends CI_MODEL
{
	function __construct(){
		parent:: __construct();
		$this->load->library('user_agent');
	}
	
	function validatelogin($logindata){
		if(isset($logindata['username'])){
			$username = $logindata['username'];					
		}else{
			$username = "";
		}
		
		if(isset($logindata['password'])){
			$password = $logindata['password'];					
		}else{
			$password = "";
		}
		
		if(strlen($username) > 0 && strlen($password) > 0){
			// OFF THE CACHE FOR THIS QUERY
			$this->db->cache_off();
			$query = $this->db->query("select 
											u.id,
									   		u.adm_role_id as adm_role_id,
											u.username,
											u.fname,
											u.lname,
											u.email,
											u.status as user_status,
											r.role_name,
											r.role_details,
											r.status as role_status
									   from 
									   		".ADMIN." u inner join ".ADMIN_ROLE." r on (u.adm_role_id = r.adm_role_id) 
									   where 
									   		u.username = '".$username."' and 
											u.password = '".md5($password)."'");
			
			if($query->num_rows() > 0){
				$row = $query->row();
				
				// CHECK FOR ROLE STATUS / DEPARTMENT STATUS
				if($row->role_status == '1') {				
					// CHECK FOR USER STATUS
					if($row->user_status == '1') { 
						//if (array_constant_check($row->id, ADMIN_USER_QUICK_LOGIN)) {
							//$this->session->set_userdata('username', $row->fname . " " . $row->lname);
							$this->session->set_userdata('username', $row->username);
							$this->session->set_userdata('uid',$row->id);
							$this->session->set_userdata('admin_role',$row->role_name);
							$this->session->set_userdata('admin_role_id',$row->adm_role_id);
							$this->session->set_userdata('admin_role_details',$row->role_details);
							$this->session->set_userdata('login_time', date('Y-m-d H:i:s'));
							$this->session->set_userdata('login_ip', $_SERVER['REMOTE_ADDR']);	
						//} else {
							/*if (!is_ip_login_allowed()) {
								$this->session->set_userdata('temp_username', $row->fname . " " . $row->lname);
								$this->session->set_userdata('temp_uid',$row->id);
								$this->session->set_userdata('temp_admin_role',$row->role_name);
								$this->session->set_userdata('temp_admin_role_id',$row->adm_role_id);
								$this->session->set_userdata('temp_admin_role_details',$row->role_details);
								$this->session->set_userdata('temp_otp', get_random_chracter(6, 12, true, true, false));
							} else {
								$this->session->set_userdata('username', $row->fname . " " . $row->lname);
								$this->session->set_userdata('uid',$row->id);
								$this->session->set_userdata('admin_role',$row->role_name);
								$this->session->set_userdata('admin_role_id',$row->adm_role_id);
								$this->session->set_userdata('admin_role_details',$row->role_details);
								$this->session->set_userdata('login_time', date('Y-m-d H:i:s'));
								$this->session->set_userdata('login_ip', $_SERVER['REMOTE_ADDR']);
							}*/
						//}
						
						// INSERT THE LOGIN DETAIL IN LOGIN HISTORY START
						
						// GET THE BROWSER DETAILS START
						if ($this->agent->is_browser()){
							$agent = $this->agent->browser().' '.$this->agent->version();
						} else if ($this->agent->is_robot()) {
							$agent = $this->agent->robot();
						} elseif ($this->agent->is_mobile()) {
							$agent = $this->agent->mobile();
						} else {
							$agent = 'Unidentified User Agent';
						}
						// GET THE BROWSER DETAILS END
						 
						$current_ip_addr = get_IP();
						 
						$ins_data = array(
											'adminid' => $this->session->userdata('uid'),
											'ip_address' => ($current_ip_addr == '::1') ? '127.0.0.1' : $current_ip_addr,
											'adm_role_id' => $row->adm_role_id,
											'date_time' => date("Y-m-d H:i:s"),
											'browser_name' => $agent);
					
						$this->db->insert(ADMIN_LOGIN,$ins_data);
						// INSERT THE LOGIN DETAIL IN LOGIN HISTORY END
						return 1;
					} else {
						// USER IS BLOCKED
						return 2;	
					}	
				} else {
					return 3;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		
	}
}
?>