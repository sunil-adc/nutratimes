<?php
class general extends CI_MODEL  {
	function __construct(){
		parent:: __construct();
	}
	
	public function zipdial_cod_confirm($order_id = '', $mobile = '', $total_amount = '', $pincode = '') {
		return true;
		exit;
		if ($order_id > 0 && $mobile != '' && $total_amount > 0 && $pincode != '') {
			
			$url = 'https://www.zipdial.com/z2cod/startTransaction.action?customerToken=a83481f8b71601d9332b735a6b991250db4fb48b&orderRefId='.$order_id.'&callerPhone='.$mobile.'&duration=240&countryCode=91&amount='.$total_amount.'&pincode='.$pincode;
			
			// CURL 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$result = curl_exec($ch);
			curl_close($ch);
			
			mysql_query("update ".ORDER." set zipdial_status = 1 where id = ".$order_id." and mobile = ".$mobile);			
			
			$file = "./zipdial/zipdial.txt";
			$fh = fopen($file, 'a+');
			$data = '{"orderId": "'.$_GET['orderRefId'].'", "Transaction Token": "'.$_GET['transaction_token'].'", "Order Status": "'.$_GET['orderStatus'].'", "Satus": "'.$status.'", "QA User": "449"},';
			fwrite($fh, $data);
			fclose($fh);
						
			return true;
		}
		return false;
	}
	
	public function getCategory($only_parent = true, $inside_cat = false, $cache_off = false, $echo = false, $args_whr = " 1 = 1 ") {
		
		// TURN OFF THE CACHE FOR THIS QUERY
		if($cache_off == true) {
			$this->db->cache_off();
		}
		
		if($only_parent == true) {
			$whr = " and parent_cat_id = 0";
		}
		
		if($inside_cat != false){
			$whr .= " and parent_cat_id = ".$inside_cat;
		}
		
		// GET CACHE
		$results = $this->memcache_driver->get_cache('_n_level_category');
		if (!$results) { 
			$query = $this->db->query("select name, parent_cat_id, seourl, total_products, cat_id from ".CATEGORY." where status = '1' ".$whr." and ".$args_whr);
			
			if($echo == true)
				echo $this->db->last_query();
				
			if ($query->num_rows() > 0) {
				// GET THE RESULT IN CACHE
				$results = $query->result();
		
				// STORE IN MEMCACHE
				$this->memcache_driver->set_cache('_n_level_category', $results, true, CACHE_LONGTIME);
				return $results;
			} else{
				return false;
			}
		} else {
			return $results;
		}
	}
	
	public function getBrand($cache_off = false, $echo = false) {
		
		// TURN OFF THE CACHE FOR THIS QUERY
		if($cache_off == true) {
			$this->db->cache_off();
		}
		
		// GET CACHE
		$results = $this->memcache_driver->get_cache('_getBrand');
		if (!$results) { 
			$query = $this->db->query("select name, seourl, brand_id, image from ".BRANDS." where status = '1' and brand_id in (1,2,3,4,5,6,7,8,9,10,21,23,28,87,89,90,91,80,81,82,83) and image != '' group by name order by datecreated desc, dateupdated desc limit 0, 14");
			
			if($echo == true)
				echo $this->db->last_query();
				
			if ($query->num_rows() > 0) {
				$results = $query->result();
				
				// STORE IN MEMCACHE
				$this->memcache_driver->set_cache('_getBrand', $results, true, CACHE_LONGTIME);
				
				return $results;
			} else {
				return false;
			}
		} else {
			return $results;
		}
	}
	public function checkNetworkPubId($net = "", $pubid = "", $bnr = "", $clkid = "", $sub_id = "", $prg_id = "", $zoneid = "", $utm_source="", $utm_campaign="", $pub_id) {
		
		// NETWORK - START
		if (trim($net) != "") {
			if (isset($_COOKIE['net'])) {
				if (trim($_COOKIE['net']) != trim($net)) {
					// SET COOKIE IF REQUESTED OR ALREADY SET
					$this->session->set_userdata('net',$net);
				}
			}else {
				// SET COOKIE IF REQUESTED OR ALREADY SET
				$this->session->set_userdata('net',$net);		
			}
		}
		// NETWORK - END
		
		// PUBID - START
		if (trim($pubid) != "") {
			/*if (isset($_COOKIE['pubid'])) {
				if (trim($_COOKIE['pubid']) != trim($pubid)) {
					// SET COOKIE IF REQUESTED OR ALREADY SET
					$this->session->set_userdata('pubid',$pubid);
				}
			}else {*/
				// SET COOKIE IF REQUESTED OR ALREADY SET

				$this->session->set_userdata('pubid',$pubid);		
			//}
		}
		// PUBID - END
		

		if (trim($utm_source) != "") {
			if (isset($_COOKIE['net'])) {
				if (trim($_COOKIE['net']) != trim($utm_source)) {
					// SET COOKIE IF REQUESTED OR ALREADY SET
					$this->session->set_userdata('net',$utm_source);
				}
			}else {
				// SET COOKIE IF REQUESTED OR ALREADY SET
				$this->session->set_userdata('net',$utm_source);		
			}
		}
		

		if (trim($bnr) != "") {
			$this->session->set_userdata('bnr', $bnr);	
		}
		if (trim($clkid) != "") {
			$this->session->set_userdata('clkid', $clkid);	
		}
		if (trim($sub_id) != "") {
			$this->session->set_userdata('sub_id', $sub_id);	
		}
		if (trim($zoneid) != "") {
			$this->session->set_userdata('zoneid', $zoneid);	
		}
		if (trim($prg_id) != "") {
			$this->session->set_userdata('prg_id', $prg_id);	
		}
		/*echo "<pre>"; print_r(unserialize($_COOKIE['svz_ci_session'])); exit;*/
		return false;
	}
	
	public function is_duplicate_add ($tablename="", $field="", $value="", $cache_off=true, $echo=false) {
		
		if($cache_off == true) {
			$this->db->cache_off();
		}
			
		$query = $this->db->query("select ".$field." from ".$tablename." where ".$field." = ".$value);
		
		if($echo == true)
			echo $this->db->last_query();
		
		if ($query->num_rows() > 0) {
			return false;
		}else{
			return true;
		}
	}
	
	public function get_settings() {
		$this->db->cache_off();
		
		$setting = $this->memcache_driver->get_cache('general_get_settings');
		if (!$setting) { 
			$settings_query = $this->db->query("SELECT attr_name, attr_value FROM ".SETTINGS." where status = '1' and attr_name in ('phone_no','admin_name','site_name','admin_email','admin_note')");
			if ($settings_query->num_rows() > 0) {
				foreach ($settings_query->result() as $setting_data) {
					if($setting_data->attr_name == 'phone_no') {	
						$setting['phone_no'] 	= $setting_data->attr_value;
					} else if($setting_data->attr_name == 'admin_name') {	
						$setting['admin_name'] 	= $setting_data->attr_value;
					} else if($setting_data->attr_name == 'site_name') {	
						$setting['site_name'] 	= $setting_data->attr_value;
					} else if($setting_data->attr_name == 'admin_email') {	
						$setting['admin_email'] 	= $setting_data->attr_value;
					} else if($setting_data->attr_name == 'admin_note') {	
						$setting['admin_note'] 	= $setting_data->attr_value;
					}
				}
				$this->memcache_driver->set_cache('general_get_settings', $setting, true, CACHE_LONGTIME);
			}
		} else {
			$setting = $setting;
		}
		return $setting;
	}
	
	public function get_nlevel_category($cur_controller, $is_front = true) {
		$str = '';
		$category = $this->getCategory($only_parent = true, $inside_cat = false, $cache_off = false, $echo = false, ($is_front == true) ? ' cat_id <> 4 ' : ' 1 = 1');
		if($category != false) {
			$is_id_set = ($cur_controller != 'homepage') ? 'id="category-menu"' : '';
			$str .= '<div class="category-wrapper" '.$is_id_set.'>';
			if(count($category) > 0) {
				$str .= '<ul class="subCtg">';
				foreach ($category as $k => $v) {
					$random_id = get_random_chracter(8, 15, true, true, false);
					$str .= '<li class="menu-listing" data-id="'.$random_id.'"><a href="'.SITE_URL.'search?&c='.$v->cat_id.'">'.ucfirst($v->name).'</a>';
					// FETCH CURRENT MENU SUB CATEGORY
					$str .= $this->common_model->fetch_menu_category($v->cat_id, $random_id);
					$str .= '</li>';
				}
				$str .= '</ul>';
			}
			$str .= '</div>';
		}
		return $str;
	}
	
	public function get_top_level_category($catid = 0) {
		if($catid > 0) {
			$cur_cat = $this->db->query("select parent_cat_id from ".CATEGORY." where cat_id = ".$catid);
			foreach ($cur_cat->result() as $category) {
				$parent_cat_id = $category->parent_cat_id;
			}
			if($parent_cat_id == 0) {
				return $catid;
			} else if ($parent_cat_id > 0) {
				return $this->get_top_level_category($parent_cat_id);	
			}
		} else {
			return false;
		}
	}
	
	public function get_network_categorywise_cps($net) {
		if($net != "") {
			$res_net = $this->db->query("select * from ".PIXELS." where is_cpl_cps = 2 and lower(trim(net)) = '".trim(strtolower($net))."'");
			if($res_net->num_rows() > 0) {
				foreach ($res_net->result() as $val) {
					return $val;
				}
			}
		}
		return false;
	}
	
	public function get_category_max_redemption($catid = 0, $product_price = 0) {
		if ($catid > 0) {
			
			$second_slab = '0';
			$this->db->cache_off();
			$res_user = $this->db->query("select cod_flag from ".USERS." where user_id = ".$_SESSION[SVAIZA_USER_ID]);
			if($res_user->num_rows() > 0) {
				foreach ($res_user->result() as $val_user) {
					if ($val_user->cod_flag == '1') {
						$second_slab = '1';	
					}
				}
			}
			
			$this->db->cache_on();
			$redemption_point = $this->db->query("select max_redemption, cod_max_redemption from ".CATEGORY." where cat_id = ".$catid);
			if($redemption_point->num_rows() > 0) {
				foreach ($redemption_point->result() as $row) {
					if ($second_slab == '1') {
						return ($row->cod_max_redemption > 0) ? $row->cod_max_redemption : $product_price;
					} else {
						return ($row->max_redemption > 0) ? $row->max_redemption : $product_price;
					}
				}
			}
		} 
		return 0;
	}
	
	public function _get_provider_from_pincode($user_id = "0", $mode="1") {
		if($user_id != 0) {
			$val_pincode = $this->db->query("select pincode from ".USERS." where user_id = '".$user_id."'");
			if($val_pincode->num_rows() > 0) {
				foreach ($val_pincode->result() as $row) {
					$pincode = $row->pincode;
				}
				
				// SELECT PROVIDER AS PER PAYMENT MODE
				// 1 = 'CASH', 2 = 'COD', 3 = 'CBD'
			
				$mode_pincode = $this->db->query("select provider, pincodes from ".PROVIDERS." where payment_mode = '".$mode."' order by priority asc");
				if($mode_pincode->num_rows() > 0) {
					foreach ($mode_pincode->result() as $val) {
						// CHECK PINCODE IS AVAILABLE IN THIS PROVIDER
						$provider_pincode = trim(trim($val->pincodes,"@"),",");
						$arr_pincode = explode(",",$provider_pincode);
						if(in_array($pincode, $arr_pincode)) {
							return $val->provider;
						}
					}
					return 6;	
				}
			}
		}
		return false;
	}
	
	public function _payment_mode_wise_provider($pincode, $payment_mode) {
		$this->db->cache_off();
		$mode_pincode = $this->db->query("select provider, pincodes from ".PROVIDERS." order by priority asc");
		if($mode_pincode->num_rows() > 0) {
			foreach ($mode_pincode->result() as $val) {
				// CHECK PINCODE IS AVAILABLE IN THIS PROVIDER
				$provider_pincode = trim(trim($val->pincodes,","),"@");
				$arr_pincode = explode(",",$provider_pincode);
				if(in_array($pincode, $arr_pincode)) {
					return $val->provider;
				}
			}
		}
	}

	public function _set_provider_order($provider, $order_id) {
		if (is_numeric($provider) && is_numeric($order_id)) {
			$this->db->query("update ".ORDER." set provider = '".$provider."' where id = '".$order_id."'");
			
			$user_id = ($_SESSION[SVAIZA_USER_ID] > 0) ? $_SESSION[SVAIZA_USER_ID] : '0';
			$cms_user_id = ($this->session->userdata('uid') > 0) ? $this->session->userdata('uid') : '0';
			$role_id = ($this->session->userdata('admin_role_id') > 0) ? $this->session->userdata('admin_role_id') : '0';
			
			// NOTE THE LOGS
			$this->common_model->order_logs($cms_user_id, $role_id, 'Order Provider changed to '.$provider.' Provider', $user_id, $order_id);
			
			return true;
		}
		return false;
	}

	public function getNetPopup($prod_detail, $size_qty_available) {
		$display = false;
		if ($prod_detail->check_inventory == '1') {
			if ($prod_detail->total_stock > 0 && $prod_detail->is_size != 1) {
				$display = true;
			} else if ($prod_detail->is_size == 1 && $size_qty_available == true) {
				$display = true;
			}
		} else {
			$display = true;
		}
		
		if ($this->session->userdata('net') != '') {
			$res = $this->db->query("select ispopup from ".PIXELS." where net = '".$this->session->userdata('net')."'");
			if ($res->num_rows() > 0) {
				foreach ($res->result() as $val) {
					return ($val->ispopup == 1 && $display == true) ? true : false;
				}
			}
		}
		return false;
	}


	public function email_verify($email_token = "")
    {
        
        if ($email_token != "") {
            //if($this->session->userdata('fire_status') == "1"){    
            $user_login = $this->db->query("Select user_id, is_optin_email From " . USERS . " where passwd = '" . $email_token . "'");
            
            if ($user_login->num_rows() > 0) {
                $user_data = $user_login->result();
                
                if ($user_data[0]->is_optin_email != 1) {
                    
                    $this->db->where("passwd", $email_token);
                    $user_verify = array(
                        'is_optin_email' => 1,
                    );
                    
                    $this->db->update(USERS, $user_verify);
                    
                }
                
            }
            //}
        }
        return true;
    }	
}