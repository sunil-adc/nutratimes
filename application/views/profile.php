<?php
	$all_array = all_arrays();
	// ECHO ALL CATEOGRIES
	echo $category;
?>
<div class="full-wrapper"><br />
  <div class="profile-wraper">
    <div class="cart-wrapper-header">Profile</div>
    <?php echo profile_sidebar(); ?>
    <div class="profile-right">
     <div class="credit_points">Available Credit Points : <?php echo $user_details->points;?></div>
     <div class="account-infor">Account Information</div>
      <div class="profile-form">
        <div class="form_wrp">
          <ul id="sign_up_form-account">
         
        <form action="<?php echo SITE_URL?>profile/save" method="post" enctype="multipart/form-data">
              <li>
                <label>Name :</label>
                <input placeholder="Full name" value="<?php echo ucfirst($user_details->name);?>" class="txtbxx alpha_space" type="text" id="name" name="name" />
              </li>
              <li>
                <label>Email :</label> 
				<input type="text" disabled="disabled"  class="txtbxx" value="<?php echo $user_details->email;?>" />
              </li>
              <li>
              <label>Mobile :</label> 
				<input type="text" disabled="disabled" maxlength="10" class="txtbxx"  value="<?php echo $user_details->mobile;?>" />
              </li>
              <li>
                <label>Address :</label>
                <textarea placeholder="Address" class="txtbxx" id="address" name="address"><?php echo $user_details->address;?></textarea>
              </li>
              <li>
                <label>City :</label>
                <input placeholder="City" class="txtbxx" type="text" id="city" name="city" value="<?php echo $user_details->city;?>" />
              </li>
              <li>
                <label>State :</label>
                <select name="state" id="state" class="txtbx" style="width:255px !important;">
                    <option value="">Select Option</option>
                    <?php
                        if (count($all_array['ARR_STATE']) > 0) {
                            asort($all_array['ARR_STATE']);
                            foreach ($all_array['ARR_STATE'] as $k => $v) {
								$selected = ($k == $user_details->state) ? "selected='selected'" : "";
                                echo "<option value='".$k."' ".$selected.">".$v."</option>";
                            }
                        } else {
                            echo "<option>Select Option</option>";
                        }
                    ?>
                </select>
              </li>
              <li>
                <label>Pincode :</label>
                <input placeholder="Pincode" maxlength="6" class="txtbxx only_numeric" type="text" id="pincode" name="pincode" value="<?php echo $user_details->pincode;?>" />
              </li>
              <li>
                <div class="gender">
                  <div class="gender-text">Gender : 
                    <span class="radio-male"><input type="radio"  name="gender" value="1"
					<?php echo ($user_details->gender == '1') ? "checked='checked'" : "";?> />Male</span>
                    <span class="radio-male"><input type="radio" name="gender" value="2"
                    <?php echo ($user_details->gender == '2') ? "checked='checked'" : "";?> />Female</span>
                  </div>
                </div>
              </li>
              <li> <input type="submit" name="submit" value="Save" id="submit" class="profile-save-btn" /></li>
            </form>
          </ul>
        </div>
      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>