<?php $this->all_array = all_arrays(); ?>
<div class="container">
	<div class="row">
		<div id="content" class="col-sm-12">
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4" style="border: 1px solid #efefef;padding: 30px;box-shadow:0px 20px 9px 7px #888888}">
					<div class="heading-bg mb-30">
							<h2 class="heading m-0">Register</h2>
							<div id="abosolute_reg_div" style="color:#e54646;font-weight:500;"></div>
					</div>
					<span id="ajax-sign-in-up">
						<form method="post" action="javascript:void(0);" id="user_signup" name="use$r_signup" onsubmit="ajaxReg('<?php echo SITE_URL.PD_PAGE?>/user_register', '<?php echo SITE_URL?>');">
                  <div class="sign-up-wraper-left">
                    <div class="form-group"> 
                        <input type="text" placeholder="Full Name" class="form-control alpha_space" id="name" name="name">
                      <div class="clr"></div>
                    </div>
                    <div class="form-group"> 
                        <input type="text" placeholder="Mobile Number" class="form-control" id="mobile" name="mobile" value="<?php echo $this->input->post('mobile');?>" pattern="\d*" maxlength="10">
                      <div class="clr"></div>
                    </div>
                    <div class="form-group"> 
                        <input type="text" placeholder="Valid Email Address" class="form-control" id="email" value="" name="email">
                      <div class="clr"></div>
                    </div>
                    <div class="form-group"> 
                        <input type="text" placeholder="Full Address" class="form-control" id="address" name="address">
                      <div class="clr"></div>
                    </div>
                    <div class="form-group"> 
                        <input type="text" placeholder="City" class="form-control" id="city" name="city">
                      <div class="clr"></div>
                    </div>
                    <div class="form-group">
                        <select name="state" id="state" class="form-control" >
                            <option value="">Select State</option>
							<?php
                                if (count($this->all_array['ARR_STATE']) > 0) {
                                    asort($this->all_array['ARR_STATE']);
                                    foreach ($this->all_array['ARR_STATE'] as $k => $v) {
                                        echo "<option value='".$k."'>".$v."</option>";
                                    }
                                } else {
                                    echo "<option>Select State</option>";
									
                                }
                            ?>
                        </select>
                      <div class="clr"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Pincode" class="form-control only_numeric" id="pincode" name="pincode" maxlength=6>                   
                      <div class="clr"></div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="Submit" id="register_btn" class="btn btn-black" />
                      
                    </div>
                  </div>
                </form>
					</span>
				</div>
				<div class="col-sm-4"></div>
			</div>
		</div>

		<!--<div id="content" class="col-sm-12">
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4" style="border: 1px solid #efefef;padding: 30px;box-shadow:0px 20px 9px 7px #888888}">
					<div class="heading-bg mb-30">
						<h2 class="heading m-0">Register</h2>
					</div>
					
					<form action="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/login" method="post" enctype="multipart/form-data">
						<div class="form-group"> 
							<input type="text" name="Name" value="" placeholder="Full Name" id="full-name" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="tel" name="Name" value="" placeholder="Mobile Number" id="mobile-number" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="email" name="Email" value="" placeholder="Email Address" id="email-address" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="tel" name="Address" value="" placeholder="Address" id="Address" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="text" name="City" value="" placeholder="City" id="city" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="text" name="State" value="" placeholder="State" id="State" class="form-control">
						</div>
						<div class="form-group"> 
							<input type="text" name="Pincode" value="" placeholder="Pincode" id="pincode" class="form-control">
						</div>
						<div class="form-group">
							<input type="submit" value="Register" class="btn btn-black">
						</div>
					</form>
					
				</div>
				<div class="col-sm-4"></div> 
			</div>
		</div>-->
		
	</div>
</div>
<?php
   		if (isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'mae') {
			?><script type="text/javascript">$("#abosolute_reg_div").show().html('Mobile you try to register is already exist try login.')</script><?php
		}
   ?>