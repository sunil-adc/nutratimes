<?php
	$all_array = all_arrays();
	// ECHO ALL CATEOGRIES
	echo $category;
?>
<div class="full-wrapper"><br />
  <div class="profile-wraper">
    <div class="cart-wrapper-header">Profile</div>
    <?php echo profile_sidebar(); ?>
    <div class="profile-right">
     <?php
     	if (isset ($_REQUEST['msg']) && $_REQUEST['msg'] == 'success') {
			?><span style="color:green">Your request has been taken for refund.</span><?php
		} else if (isset ($_REQUEST['msg']) && $_REQUEST['msg'] == 'err') {
			?><span style="color:red">(*) denotes mandatory fields.</span><?php
		} else if (isset ($_REQUEST['msg']) && $_REQUEST['msg'] == 'alreq') {
			?><span style="color:red">Your request is already initiated.</span><?php
		} else if (isset ($_REQUEST['msg']) && $_REQUEST['msg'] == 'ierr') {
			?><span style="color:red">Order id is invalid please provide valid details.</span><?php
		}
		
	 ?>
     <div class="account-infor">Fill this form to get refund</div>
      <div class="profile-form">
        <div class="form_wrp">
          <ul id="sign_up_form-account">
         
        <form action="<?php echo SITE_URL?>refund/save" method="post" enctype="multipart/form-data">
              <li>
                <label>Name :</label>
                <input placeholder="Full name" value="<?php echo ucfirst($user_details->name);?>" class="txtbxx alpha_space" type="text" id="name" name="name" />
              </li>
              <li>
                <label>Email :</label> 
				<input type="text" name="email" readonly="readonly"  class="txtbxx" value="<?php echo $user_details->email;?>" />
              </li>
              <li>
              <label>Mobile :</label> 
				<input type="text" name="mobile" readonly="readonly" maxlength="10" class="txtbxx"  value="<?php echo $user_details->mobile;?>" />
              </li>
              <li>
                <label>Order Id  :</label>
                <input placeholder="Order id" class="txtbxx" type="text" id="order_id" name="order_id" value="" />
              </li>
              <li> <input type="submit" name="submit" value="Refund Request" id="submit" class="profile-save-btn" /></li>
            </form>
          </ul>
        </div>
      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>