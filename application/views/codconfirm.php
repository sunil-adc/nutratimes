<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo S3_URL.CONTENT_DIR;?>/css/cod.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="codbox">
  <div class="ui-box-full-w">
    <h1>
      <label>Confirmation <span>(COD - Cash On Delivery)</span></label>
    </h1>
    <div class="right-form-wrap left">
      <div class="title">
        <label class="label">Product</label>
        <label class="label">Quantity</label>
        <label class="label">Price</label>
        <label class="label">Total Price</label>
      </div>
      <?php
		if (isset($cart)) {
			if (is_array($cart) > 0) {
			
			// SET TOTAL PRICE TO ZERO
			$total_price = 0;
			$i = 0;
			$cod_available = true;
				foreach ($cart as $k => $v) {
					// STORE SHIPPING IN ARRAY FOR FINDING MAXIMUM
					$shipping[] = $v->shipping;
					$total_price += $v->cod_price * $v->quantity;
					?>
					<ul class="info_display_common">
						<li> <a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$v->seourl;?>" title="<?php echo $v->name?>">
                            <img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_120_150_PATH.$v->image)?>" 
                            title="<?php echo $v->name;?>" alt="<?php echo $v->name;?>" width="80" height="80" /></a>
                            <span class="product-name right"> <?php echo $v->name;?></span> </li>
						<li class="quant"><?php echo $v->quantity; ?> </li>
						<li class="price"><?php echo _r($v->cod_price); ?> </li>
						<li class="price"><?php echo _r($v->cod_price * $v->quantity); ?> </li>
					</ul>
					<?php
				}
				?>
				<div class="total-price">
                	<h2>Total : <?php echo $total_price;?>/-</h2>
                </div>
                <?php
			} else {
				?><script>window.location.href='<?php echo SITE_URL;?>cart';</script><?php
			}
		} else {
			?><script>window.location.href='<?php echo SITE_URL;?>cart';</script><?php
		}
		?>
      <div class="clr"></div>
      <div class="box_left left">
          <div class="title-second">User Details</div>
          <ul class="info_display_common_second">
            <li>
              <label><?php echo $user_details->name;?></label>
            </li>
            <li>
              <label><?php echo $user_details->mobile;?></label>
            </li>
            <li>
              <label><?php echo $user_details->city;?></label>
            </li>
            <li>
              <label><?php echo $user_details->state;?></label>
            </li>
            <li>
              <label><?php echo $user_details->address;?></label>
            </li>
          </ul>
       </div>
       <div class="box_left right">
          <div class="title-third">Cod Captcha</div>
         <ul class="info_display_common_fourth">
      <li>
        <form method="post" action="<?php echo SITE_URL?>codconfirm/confirmorder">
          <div><?php echo $recaptcha_html;?></div>
        	<input type="submit" name="submit" value="Proceed" id="account-recover" class="btn-common">
        
        </form>
      </li>
    </ul>
       </div>
      <div class="clr"></div>
    </div>
  </div>
</div>
</div>
<style>
	#recaptcha_privacy{
		width:100px;
	}
</style>
</body>
</html>
