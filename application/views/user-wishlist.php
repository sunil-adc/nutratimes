<?php
	// ECHO ALL CATEOGRIES
	echo $category;
?>
<div class="full-wrapper"><br />
<div class="profile-wraper">
  <div class="cart-wrapper-header">Wish-List </div>
  <?php echo profile_sidebar(); ?>
  <div class="profile-right">
    <div class="wi-order-main">
        <div class="wi-order-header">
          <div class="wi-label-orderno order-title left">Image </div>
          <div class="wi-label-item order-title left">Products</div>
          <div class="wi-label-total order-title left">Size</div>
          <div class="wi-label-paidstatus order-title left">Price</div>
          <div class="wi-label-date order-title left">Action</div>
          <div class="clr"></div>
        </div>
        <?php
        	if(isset($order_history) && is_array($order_history)) {
				foreach ($order_history as $row) {
					?>
					<div class="wi-order-content">
						<div class="wi-label-orderno left"><a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl;?>" title="<?php echo $row->name?>"><img class="wish-images" src="<?php echo S3_URL.PRODUCT_THUMB_120_150_PATH.$row->image; ?>" alt="<?php echo $row->name;?>" /></A></div>
                        <div class="wi-label-item left"><a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl;?>" title="<?php echo $row->name?>"><?php echo $row->name; ?></a></div>
                        <div class="wi-label-total left"><a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl;?>" title="<?php echo $row->name?>"><?php echo $row->size; ?></a></div>
                        <div class="wi-label-paidstatus left"><a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl;?>" title="<?php echo $row->name?>"><?php echo $row->sale_price; ?></a></div>
                        <div class="wi-label-date left"><a href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$row->seourl;?>">Buy Now</a></div>
                        <div class="clr"></div>                 
					</div>
					<?php
				}
			} else {
				?>
				<div class="wi-order-content">
					<div class="wi-order_blank">You haven't added any product in your wishlist.</div>
                </div>
				<?php
			}
		?>
    </div>
  </div>
  <div class="clr"></div>
</div>
</div>