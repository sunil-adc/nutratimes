<?php
	// ECHO ALL CATEOGRIES
	echo $category;
?>
<div class="full-wrapper"><br />
  <div class="brand-wraper">
    <div class="cart-wrapper-header">
      <div class="brands-head">All Brands</div>
    </div>
    <div class="a-z">
      <div class="alfa"> <a class="alfa" href="#">A</a> <a class="alfa" href="#">B</a> <a class="alfa" href="#">C</a> <a class="alfa" href="#">D</a> <a class="alfa" href="#">E</a> <a class="alfa" href="#">F</a> <a class="alfa" href="#">G</a> <a class="alfa" href="#">H</a> <a class="alfa" href="#">I</a> <a class="alfa" href="#">J</a> <a class="alfa" href="#">K</a> <a class="alfa" href="#">L</a> <a class="alfa" href="#">M</a> <a class="alfa" href="#">N</a> <a class="alfa" href="#">O</a> <a class="alfa" href="#">P</a> <a class="alfa" href="#">Q</a> <a class="alfa" href="#">R</a> <a class="alfa" href="#">S</a> <a class="alfa" href="#">T</a> <a class="alfa" href="#">U</a> <a class="alfa" href="#">V</a> <a class="alfa" href="#">W</a> <a class="alfa" href="#">X</a> <a class="alfa" href="#">Y</a> <a class="alfa" href="#">Z</a> </div>
    </div>
    <div>
     <?php
      	if($brand != false) {
			echo '<ul class="brands-list">';
			$i = 1;
			foreach ($brand as $k => $v) {
				?>
                <li><a href="<?php echo SITE_URL.'search?&b='.$v->brand_id?>" class="no-decoration-link">
                            <img src="<?php echo S3_URL.product_or_placeholder('./'.BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$v->image)?>" 
                            width="120" alt="<?php echo $v->name?>"> 
                            <span><?php echo $v->name?></span>
                        </a>
                </li>
				<?php
				$i++;
			}
			echo '</ul>';
		}
	  ?>
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
  </div>
</div>