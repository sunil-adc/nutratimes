<?php

if($payment_gateway == '1') {
	?>
<form name="paymentForm" id="payuForm" method="post" action="https://www.timesofmoney.com/direcpay/secure/dpMerchantPayment.jsp">
  <input type="hidden" name="requestparameter" value="<?php echo $requestparameter;?>">
  <input type="hidden" name="billingDtls" value="<?php echo $billingparameter; ?>">
  <input type="hidden" name="shippingDtls" value="<?php echo $shippingparameter; ?>">
</form>
<?php
} else if ($payment_gateway == '2') {
?>
<form action="https://secure.payu.in/_payment" method="post" name="paymentForm" id="payuForm">
  <input type="hidden" name="key" value="<?php echo $payu_key;?>" />
  <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
  <input type="hidden" name="txnid" value="<?php echo $txnid; ?>" />
  <input type="hidden" name="amount" value="<?php echo $amount;?>" />
  <input type="hidden" name="firstname" id="firstname" value="<?php echo $uname; ?>" />
  <input type="hidden" name="email" id="email" value="<?php echo $uemail; ?>" />
  <input type="hidden" name="phone" value="<?php echo $umobile; ?>" />
  <input type="hidden" name="productinfo" value="<?php echo "products"; ?>" size="64" />
  <input type="hidden" name="lastname" id="lastname" value="" />
  <input type="hidden" name="city" value="<?php echo $ucity; ?>" />
  <input type="hidden" name="state" value="<?php echo $ustate; ?>" />
  <input type="hidden" name="country" value="<?php echo "INDIA"; ?>" />
  <input type="hidden" name="zipcode" value="<?php echo $upincode; ?>" />
  <input type="hidden" name="surl" value="<?php echo SITE_URL;?>paymentconfirm/index/?pg=payu" size="64" />
  <input type="hidden" name="furl" value="<?php echo SITE_URL;?>paymentfailure/index/" size="64" />
  <input type="hidden" name="curl" value="<?php echo SITE_URL;?>paymentcancel/index/" />
  <input type="hidden" name="address1" value="<?php echo $uaddress; ?>" />
  <input type="hidden" name="address2" value="" />
  <input type="hidden" name="udf1" value="<?php echo $use_redeem?>" />
  <input type="hidden" name="udf2" value="" />
  <input type="hidden" name="udf3" value="" />
  <input type="hidden" name="udf4" value="" />
  <input type="hidden" name="udf5" value="" />
  <input type="hidden" name="pg" value="" />
</form>
<?php
} else if ($payment_gateway == '3') {
	?>
<form method="post" action="https://secure.ebs.in/pg/ma/sale/pay/" name="paymentForm" id="EBSForm">
  <input name="account_id" type="hidden" value="<?php echo $ebs_account_id;?>">
  <input name="return_url" type="hidden" value="<?php echo SITE_URL;?>paymentconfirm/index/?pg=ebs<?php echo $redeem_string?>&DR={DR}" />
  <input name="mode" type="hidden" value="<?php echo $mode; ?>" />
  <input name="reference_no" type="hidden" value="<?php echo $reference_no; ?>" />
  <input name="amount" type="hidden" value="<?php echo $amount; ?>"/>
  <input name="description" type="hidden" value="<?php echo "Description: Payment for Order ID - ".$reference_no; ?>" />
  <input name="name" type="hidden" value="<?php echo $uname; ?>" />
  <input name="address" type="hidden" value="<?php echo $uaddress; ?>" />
  <input name="city" type="hidden" value="<?php echo $ucity; ?>" />
  <input name="state" type="hidden" value="<?php echo $ustate; ?>" />
  <input name="postal_code" type="hidden" value="<?php echo $upincode; ?>" />
  <input name="country" type="hidden" value="IND" />
  <input name="phone" type="hidden" value="<?php echo $umobile; ?>" />
  <input name="email" type="hidden" value="<?php echo $uemail; ?>" />
  <input name="secure_hash" type="hidden"  value="<?php echo $secure_hash;?>" />
</form>
<?php
} else if ($payment_gateway == '4') {
	?>
<!DOCTYPE html>
<html>
<head>
<title>Checkout Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="<?php echo S3_URL.CONTENT_DIR?>/css/paymentcss.css">
<link rel="stylesheet" href="<?php echo S3_URL.CONTENT_DIR?>/css/pcss3t.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo S3_URL.CONTENT_DIR?>/css/pcss3t-ie8.css">
    <script src="<?php echo S3_URL.CONTENT_DIR?>/js/pcss3t-ie8.js"></script>
<![endif]-->
</head>
<body>
<div class="page"> 
  <!-- tabs -->
  <div class="pcss3t pcss3t-height-auto">
    <input type="radio" name="pcss3t" checked  id="tab1"class="tab-content-first">
    <label for="tab1"> Debit Card</label>
    <input type="radio" name="pcss3t" id="tab2" class="tab-content-2">
    <label for="tab2"> Credit Card</label>
    <input type="radio" name="pcss3t" id="tab3" class="tab-content-3">
    <label for="tab3"> Net Banking</label>
    <ul>
      <li class="tab-content tab-content-first typography">
        <h1 style="background:url('<?php echo S3_URL.CONTENT_DIR?>/images/Debitcards.png') no-repeat 170px -1px;  height:32px;">Pay Using Debit Card </h1>
        <form  action="<?php echo SITE_URL?>citruscheck/index" class="contact-form" method="post" name="contact-form">
          <ul>
            <li>
            	<label for="name">* Card Type :</label>
            	<select name="cardType" required>
                    <option value="">- Select -</option>
                    <option value="Visa">VISA</option>
                    <option value="Master">Master Card</option>
                    <option value="Maestro">Maestro</option>
                </select>
            </li>
            <li>
              <label for="name">* Name on Card :</label>
              <input type="text" name="cardHolderName" class="alpha_space" required />
            </li>
            <li>
              <label for="name">* Card  Number :</label>
              <input type="text" maxlength="16" minlength="15" name="cardNumber" class="only_numeric" required>
            </li>
            <li>
              <label for="name">* CVV Number : <a href="#" class="tooltip"> <img src="<?php echo S3_URL.CONTENT_DIR?>/images/info.png" style="border:none;" /> <span> <img class="callout" src="<?php echo S3_URL.CONTENT_DIR?>/images/callout.png" style="float:right; border:none;" /> <img src="<?php echo S3_URL.CONTENT_DIR?>/images/card_back.png" style="float:left; border:none; margin-top:5px; " /> The CVV Number is the last 3 digits printed on the signature panel on the back side of the card. </span> </a> </label>
              <input type="text" name="cvvNumber" class="only_numeric" required>
            </li>
            <li>
              <label for="name">* Card Expiry :</label>
              <select name="expiryMonth" required style="margin-right:10px;">
                <option value="">- Expiry Month -</option>
                <?php
					for ($i = 1; $i <= 12; $i++) {
						if (strlen($i) == 1) {
							$i = '0'.$i;
						}
						echo '<option value="'.$i.'">'.$i.'</option>';	
					}
                ?>
              </select>
              <select name="expiryYear" required>
                <option value="">- Expiry Year -</option>
                <?php
					for ($i = 0; $i <= 30; $i++) {
						$year = date('Y') + $i;
						echo '<option value="'.$year.'">'.$year.'</option>';	
					}
                ?>
              </select>
            </li>
            </li>
              <label for="name">Choose your bank :</label>
        	  <select name="issuerCode" style="width:200px;">
                <option value="">Select Bank</option>
                <option value="CID001">ICICI Bank</option>
                <option value="CID002">AXIS Bank</option>
                <option value="CID003">CITI Bank</option>
                <option value="CID004">YES Bank</option>
                <option value="CID005">SBI Bank</option>
                <option value="CID006">DEUTSCHE Bank</option>
                <option value="CID007">UNION Bank</option>
                <option value="CID008">Indian Bank</option>
                <option value="CID009">Federal Bank</option>
                <option value="CID010">HDFC Bank</option>
                <option value="CID011">IDBI Bank</option>
            </select>
            </li>
          </ul>
        	<p>By placing this order, you're agreeing to the Terms of Use of shophunk.com</p><br>
            <input name="merchantAccessKey" type="hidden" value="<?php echo CITRUS_ACCESS_KEY;?>" />
            <input name="firstName" type="hidden" value="<?php echo $uname; ?>" />
            <input name="lastName" type="hidden" value="<?php echo $uname; ?>" />
            <input name="phoneNumber" type="hidden" value="<?php echo $umobile; ?>" />
            <input name="email" type="hidden" value="<?php echo $uemail; ?>" />
            <input name="addressCity" type="hidden" value="<?php echo $ucity; ?>" />
            <input name="paymentMode" type="hidden" value="DEBIT_CARD" />
            <input name="returnUrl" type="hidden" value="<?php echo $returnUrl?>" />
            <input name="amount" type="hidden" value="<?php echo $amount; ?>" />
            <input name="addressZip" type="hidden" value="<?php echo $upincode; ?>" />
            <input name="addressStreet1" type="hidden" value="<?php echo $uaddress; ?>" />
            <input name="addressCountry" type="hidden" value="India" />
            <input name="addressState" type="hidden" value="<?php echo $ustate; ?>" />
            <input name="transactionID" type="hidden" value="<?php echo $txnid; ?>" />
            <input name="citrusubmit" type="submit" value="Pay Now" class="buttn">
        </form>
      </li>
      <li class="tab-content tab-content-2 typography">
        <h1 style="background:url(<?php echo S3_URL.CONTENT_DIR?>/images/Creditcards.png) no-repeat 170px -1px; height:32px;">Pay Using Credit Card </h1>
        <form  action="<?php echo SITE_URL?>citruscheck/index" class="contact-form" method="post" name="contact-form">
          <ul>
            <li>
            	<label for="name">* Card Type :</label>
            	<select name="cardType" required>
                    <option value="">- Select -</option>
                    <option value="Visa">VISA</option>
                    <option value="Master">Master Card</option>
                    <option value="Maestro">Maestro</option>
                </select>
            </li>
            <li>
              <label for="name">* Name on Card :</label>
              <input type="text" name="cardHolderName" class="alpha_space" required />
            </li>
            <li>
              <label for="name">* Card  Number :</label>
              <input type="text" maxlength="16" minlength="15" name="cardNumber" class="only_numeric" required />
            </li>
            <li>
              <label for="name">* CVV Number : <a href="#" class="tooltip"> <img src="<?php echo S3_URL.CONTENT_DIR?>/images/info.png" style="border:none;" /> <span> <img class="callout" src="<?php echo S3_URL.CONTENT_DIR?>/images/callout.png" style="float:right; border:none;" /> <img src="<?php echo S3_URL.CONTENT_DIR?>/images/card_back.png" style="float:left; border:none; margin-top:5px; " /> The CVV Number is the last 3 digits printed on the signature panel on the back side of the card. </span> </a> </label>
              <input type="text" name="cvvNumber" class="only_numeric" required />
            </li>
            <li>
              <label for="name">* Card Expiry :</label>
              <select name="expiryMonth" required style="margin-right:10px;">
                <option value="">- Expiry Month -</option>
                <?php
					for ($i = 1; $i <= 12; $i++) {
						if (strlen($i) == 1) {
							$i = '0'.$i;
						}
						echo '<option value="'.$i.'">'.$i.'</option>';	
					}
                ?>
              </select>
              <select name="expiryYear" required>
                <option value="">- Expiry Year -</option>
                <?php
					for ($i = 0; $i <= 30; $i++) {
						$year = date('Y') + $i;
						echo '<option value="'.$year.'">'.$year.'</option>';	
					}
                ?>
              </select>
            </li>
            </li>
              <label for="name">Choose your bank :</label>
        	  <select name="issuerCode" style="width:200px;">
                <option value="">Select Bank</option>
                <option value="CID001">ICICI Bank</option>
                <option value="CID002">AXIS Bank</option>
                <option value="CID003">CITI Bank</option>
                <option value="CID004">YES Bank</option>
                <option value="CID005">SBI Bank</option>
                <option value="CID006">DEUTSCHE Bank</option>
                <option value="CID007">UNION Bank</option>
                <option value="CID008">Indian Bank</option>
                <option value="CID009">Federal Bank</option>
                <option value="CID010">HDFC Bank</option>
                <option value="CID011">IDBI Bank</option>
            </select>
            </li>
          </ul>
        	<p>By placing this order, you're agreeing to the Terms of Use of shophunk.com</p><br>
            <input name="merchantAccessKey" type="hidden" value="<?php echo CITRUS_ACCESS_KEY;?>" />
            <input name="firstName" type="hidden" value="<?php echo $uname; ?>" />
            <input name="lastName" type="hidden" value="<?php echo $uname; ?>" />
            <input name="phoneNumber" type="hidden" value="<?php echo $umobile; ?>" />
            <input name="email" type="hidden" value="<?php echo $uemail; ?>" />
            <input name="addressCity" type="hidden" value="<?php echo $ucity; ?>" />
            <input name="paymentMode" type="hidden" value="CREDIT_CARD" />
            <input name="returnUrl" type="hidden" value="<?php echo $returnUrl?>" />
            <input name="amount" type="hidden" value="<?php echo $amount; ?>" />
            <input name="addressZip" type="hidden" value="<?php echo $upincode; ?>" />
            <input name="addressStreet1" type="hidden" value="<?php echo $uaddress; ?>" />
            <input name="addressCountry" type="hidden" value="India" />
            <input name="addressState" type="hidden" value="<?php echo $ustate; ?>" />
            <input name="transactionID" type="hidden" value="<?php echo $txnid; ?>" />
            <input name="citrusubmit" type="submit" value="Pay Now" class="buttn">
        </form>
        <p>By placing this order, you're agreeing to the Terms of Use of shophunk.com</p>
        <br>
        
      </li>
      <li class="tab-content tab-content-3 typography">
        <p style="color:#F93">Pay using our Net Banking service.</p>
        <form action="<?php echo SITE_URL?>citruscheck/index" class="contact-form" method="post" name="contact-form">
          <ul>
            <li>
              <label for="name">Choose your bank :</label>
        	  <select name="issuerCode" style="width:200px;">
                <option value="">Select Bank</option>
                <option value="CID001">ICICI Bank</option>
                <option value="CID002">AXIS Bank</option>
                <option value="CID003">CITI Bank</option>
                <option value="CID004">YES Bank</option>
                <option value="CID005">SBI Bank</option>
                <option value="CID006">DEUTSCHE Bank</option>
                <option value="CID007">UNION Bank</option>
                <option value="CID008">Indian Bank</option>
                <option value="CID009">Federal Bank</option>
                <option value="CID010">HDFC Bank</option>
                <option value="CID011">IDBI Bank</option>
            </select>
                <input name="merchantAccessKey" type="hidden" value="<?php echo CITRUS_ACCESS_KEY;?>" />
                <input name="firstName" type="hidden" value="<?php echo $uname; ?>" />
                <input name="lastName" type="hidden" value="<?php echo $uname; ?>" />
                <input name="phoneNumber" type="hidden" value="<?php echo $umobile; ?>" />
                <input name="email" type="hidden" value="<?php echo $uemail; ?>" />
                <input name="addressCity" type="hidden" value="<?php echo $ucity; ?>" />
                <input name="paymentMode" type="hidden" value="NET_BANKING" />
                <input name="returnUrl" type="hidden" value="<?php echo $returnUrl?>" />
                <input name="amount" type="hidden" value="<?php echo $amount; ?>" />
                <input name="addressZip" type="hidden" value="<?php echo $upincode; ?>" />
                <input name="addressStreet1" type="hidden" value="<?php echo $uaddress; ?>" />
                <input name="addressCountry" type="hidden" value="India" />
                <input name="addressState" type="hidden" value="<?php echo $ustate; ?>" />
                <input name="transactionID" type="hidden" value="<?php echo $txnid; ?>" />
                <input name="citrusubmit" type="submit" value="Pay Now" class="buttn">
              <br>
              <span style="font-size:10px; margin-left:127px;">You will be redirected to a secure payment gateway</span> </li>
          </ul>
          	<p>By placing this order, you're agreeing to the Terms of Use of shophunk.com</p><br>
        </form>
      </li>
    </ul>
  </div>
  <!--/ tabs --> 
</div>
</body>
</html>
<?php
} else if ($payment_gateway == '5') {
?>

<form method="post" name="paymentForm" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">

<!--<form method="post" name="paymentForm" action="https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> -->

<input type="hidden" name="encRequest" value="<?php echo $encrypted_data?>">
<input type="hidden" name="access_code" value="<?php echo $access_code?>">

</form>

<?php
}
?>
<script type="text/javascript">document.paymentForm.submit();</script>
<script>
$(document).ready(function(e) {
	$(document).on("keyup",".alpha_space",function(e){
			if(e.keyCode>=37&&e.keyCode<=40||e.keyCode>=65&&e.keyCode<=90||e.keyCode==32||e.keyCode==8||e.keyCode==46||e.keyCode==17||e.keyCode==9||e.keyCode==13||e.keyCode==17||e.keyCode==18){}else{e.preventDefault()}
		});
	
	$(document).on("keyup",".only_numeric",function(e){
		$(this).val($(this).val().replace(/\s/g, ""));
	})
});
</script>