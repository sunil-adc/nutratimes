
<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1065107/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

<?php
	$product_purchase_id = '';
	
	$all_array = all_arrays();
	
	// ECHO ALL CATEOGRIES
	//echo $category;
	
	if ($order_type['is_contest'] == false && $order_type['is_product'] == true) {
		if($order_product != "") {
			if($order_product->num_rows() > 0) {
			
			foreach ($order_product->result() as $val_mode) { 
				$payment_mode_msg = $val_mode->payment_mode;
			}
		?>
		
		<?php
			if ($user_details != "") {
				if($user_details->num_rows() > 0) {
					foreach ($user_details->result() as $user_data) {
						$user_data = $user_data;
					}
				}
			}
		?>
		
		<section class="order-summary">
		
		
            <?php
				if ($payment_mode_msg == 2) { 
					?>
					<!--<div class="order-text font15">
                        Hurry up! Check mobile & reply back to get order confirmed | 
                        Order No. <span class="order-text-span"><?php echo $order_id;?></span>
                    </div>
                    <div class="order-description bold">The order you placed with us is in ‘pending’ status. In order to confirm the order you have to respond to the message we have sent at your registered mobile number.</div>-->
					
					<div class="sucess-card">
						<div class="icon">
							<img src="<?php echo S3_URL?>cdn/image/tick-icon.png">
						</div>
						<div class="message">
							<h6>Hey
								<span class="name"><?php echo $user_data->name;?>,</span>
							</h6>
							<h4>Your Order is Confirmed | Order No. <span class="order-text-span"><?php echo $order_id;?></span></h4>
							<p>We'll send you a shipping confirmation email as soon as your order ships.</p>
							<label class="btn">Order Summary</label>
						</div>
					</div>
					
					<?php
				} else {
					?>
					
					<div class="sucess-card">
						<div class="icon">
							<img src="<?php echo S3_URL?>cdn/image/tick-icon.png">
						</div>
						<div class="message">
							<h6>Hey
								<span class="name"><?php echo $user_data->name;?>,</span>
							</h6>
							<h4>Your Order is Confirmed | Order No. <span class="order-text-span"><?php echo $order_id;?></span></h4>
							<p>We'll send you a shipping confirmation email as soon as your order ships.</p>
							<label class="btn">Order Summary</label>
						</div>
					</div>
					<?php
				}
			?>
			
			<div class="container">
			<div class="row">
			<div id="content" class="col-sm-12">
			<div class="table-responsive">
              <table class="table table-bordered table-cart-block">
			  	<thead>
				<tr>
                    <th class="text-left">Sl.No</th>
					<th class="text-left">Items Name</th>
                    <th class="text-left">Price</th>
                    <th class="text-left">Qty</th>
                    <th class="text-left">Total</th>
                </thead>
				</tr>
                <?php
				$total = 0;
				$i = 1;
				foreach ($order_product->result() as $val) {
					$product_purchase_id .= $val->product.",";
					$price = $val->payment_mode == 1 ? $val->cash_price : $val->cod_price;
					$shipping[]  = $val->shipping;
					$total += ($val->quantity * $price);
					?>
                    <tr>
					
                        <td class="text-left"><?php echo $i;?></td>
                        <td class="text-left"><?php echo $val->name;?>
                        <?php 
							if($val->size > 0) {
								echo " / SIZE ".$val->genre_name;
							}
						?></td>
                        <td class="text-left"><?php echo $price?>/-</td>
                        <td class="text-left"><?php echo $val->quantity;?></td>
                        <td class="text-left"><?php echo $val->quantity * $price;?>/-</td>
                    </tr>
                    <?php
					$i++;
				}
				$shipping = ($val->payment_mode == 1) ? max($shipping) : '0';
				$product_purchase_id = trim($product_purchase_id, ",");  
			  ?>
			<!--<tr>
                <td></td>
                <td colspan="3" align="right">Sub Total </td>
                 <td align="right"><?php echo _r($total);?>/-</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3" align="right">Shipping Charge</td>
                <td align="right"><?php echo _r($shipping);?>/-</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3" align="right">Total</td>
                <td align="right"><?php echo _r($total + $shipping);?>/-</td>
            </tr>-->
            </table>
			</div>
			
			<div class="row">
				<div class="col-sm-4 pull-left secure-pay">
					<div class="order-address">
						<h5>Shipping Address</h5>
						<div class="address-detail">
							<p><?php echo $user_data->name;?></p>
							<p><?php echo $user_data->address;?>,</p>
							<p><?php echo $user_data->city;?> - <?php echo $user_data->pincode;?></p>
							<p><?php echo $all_array['ARR_STATE'][$user_data->state];?></p>
							<p><?php echo $user_data->mobile;?></p>
						</div>
					</div>
				</div>
			
				<div class="col-sm-4 pull-right">
					<table class="table table-bordered total-cart-table">
						<tr>
							<td class="text-right">
								<strong>Sub-Total:</strong>
							</td>
							<td class="text-right"><?php echo _r($total);?>/-</td>
						</tr>
						<tr>
							<td class="text-right">
								<strong>Shipping Charge:</strong>
							</td>
							<td class="text-right"><?php echo _r($shipping);?>/-</td>
						</tr>
						<tr>
							<td class="text-right">
								<strong>Total:</strong>
							</td>
							<td class="text-right"><?php echo _r($total + $shipping);?>/-</td>
						</tr>
					</table>
					
				</div>	
				
			</div>
			<div class="buttons clearfix">					
				<div class="pull-right">		
					<a href="<?php echo SITE_URL?>" class="btn btn-black">Continue Shopping</a>
				</div>
			</div>
			
			
			<div class="confir-mail">
			  <?php echo $val->payment_mode == 2 ? '<div class="cancelation-widget right">
              		<div style="font-size: 17px; font-weight: 600; color: #183086; margin-bottom: 10px;">Cacellation Process</div>
                   <div class="cancel-2nd">If you want to cancel the order, Please contact <strong>Customer Care 08046328320 </strong><br />Note : Order cannot be cancelled once product is dispatched</div></div>' : '';
			  ?>
            </div>
			
            <div class="left" style="margin:11px 0px 0px 50px"></div>
            <div class="clr"></div>
		  </div>
		</div>
		</div>
		</section>
		<?php 
			$retargeting_amount = $total + $shipping;
			
			}
			
		}
	}
	
	
	if($this->session->userdata('cps_fired_pixels') != "") {
		$cps_fired_pixels = $this->session->userdata('cps_fired_pixels');
		
		// UNSET CPS FIRED PIXELS CODE
		$this->session->unset_userdata('cps_fired_pixels');
		
		foreach ($cps_fired_pixels as $key => $val) {
			echo $val;
		}
	}
?>



<!-- Mgid Sensor -->
<script type="text/javascript">
    (function() {
        var d = document, w = window;
        w.MgSensorData = w.MgSensorData || [];
        w.MgSensorData.push({
            cid:210034,
            lng:"us",
            nosafari:true,
            project: "a.mgid.com"
        });
        var l = "a.mgid.com";
        var n = d.getElementsByTagName("script")[0];
        var s = d.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        var dt = !Date.now?new Date().valueOf():Date.now();
        s.src = "//" + l + "/mgsensor.js?d=" + dt;
        n.parentNode.insertBefore(s, n);
    })();
</script>
<!-- /Mgid Sensor -->