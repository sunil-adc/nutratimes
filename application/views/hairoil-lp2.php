<!DOCTYPE html>
<html lang="en">

<head>
  <title>Best Anti Hair Fall Oil and Shampoo brand in India - Nutratimes</title>
  <meta charset="utf-8">
  
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Buy Nutratimes anti hair fall oil and shampoo made up of 40 natural herbs at reasonable  prices in India.A perfect hair loss treatment for hair loss, baldness and damaged hairs.">
  <meta name="keywords" content="hair loss treatment,anti hair fall shampoo,hair fall shampoo,sulfate free shampoo,best organic shampoo,best shampoo for hair fall in India,best shampoo for hair growth and thickening,hair thickening shampoo.">


  <link href="<?php echo S3_URL?>cdn/hairoil-lp/2/images/favicon.png" rel="icon">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    
  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>cdn/hairoil-lp/2/css/slick-theme.css"/>

  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>cdn/hairoil-lp/2/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?php echo S3_URL?>cdn/hairoil-lp/2/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>cdn/hairoil-lp/2/css/styles.css">

  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

  <style type="text/css">
    .help-block{
      color: #cc0033;
      font-size: 12px;
      line-height: 15px;
      margin: 5px 0 0;
  }
</style>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149373751-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149373751-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 798083341 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-798083341"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-798083341');
</script>

<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1065107/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

</head>

<body>

 <div class="container-fluid">
  <?php $all_array = all_arrays(); ?>
    <!-- Banner Starts Here -->

    <section id="banner">
        <div class="notification-bar web_banner">
            <p>
              <strong>WARNING:-</strong> We currently have limited Bottles IN STOCK. Stocks sell out fast,
              <strong>act today!</strong>
              <a href="#feedbackForm" class="btn btn-order go-to-form">Place My Order
                <i></i>
              </a>
            </p>
          </div>
          <div class="wrapper">
            <div class="bg_banner web_banner">
              <div class="container">
                    <div class="col-md-12 bannerText">
                       
                         <div class="col-md-4" style="float: right; margin-top: 10em; margin-right: -1em;">
                          <form role="form" id="feedbackForm" name="user_signup" class="feedbackForm" action="javascript:void(0)" onsubmit="javascript:return ajaxReg_hairoil('<?php echo SITE_URL?>buyregister', '1');" >
                            <div class="col heading">
                              <h4>TELL US WHERE TO SEND</h4>                           
                            </div> 
      
                            <div class="form_items">       
                            
                              <div class="col">   
                                    
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="name1" name="name1" placeholder="Name" data-attr="Please enter correct Name" tabindex="1">
                                      <span class="help-block" id="name_err1"></span>
                                  </div>

                                  <div class="form-group">
                                    <input type="text" class="form-control only_numeric phone" id="mobile1" name="mobile1" pattern="\d*" maxlength="10" placeholder="Mobile Number" data-attr="Please enter correct Mobile number" tabindex="2">
                                    <span class="help-block" id="mobile_err1"> </span>
                                </div> 
                              
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="email1" name="email1" placeholder="Email Address" data-attr="Please enter correct email" tabindex="3">
                                      <span class="help-block" id="email_err1"></span>
                                  </div>
                                  
                                  
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="address1" name="adderss1" placeholder="Street Address" data-attr="Please Enter Correct Address" tabindex="4">
                                      <span class="help-block" id="address_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="city1" name="city1" placeholder="Enter City" data-attr="Please Enter Correct City" tabindex="5">
                                      <span class="help-block" id="city_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                    <select class="form-control" name="state1" id="state1" tabindex="6">
                                      <option value="">Select State</option>
                                      <?php
                                        if (count($all_array['ARR_STATE']) > 0) {
                                          
                                          asort($all_array['ARR_STATE']);
                                          foreach ($all_array['ARR_STATE'] as $k => $v) {
                                            
                                            echo "<option value='".$k."' >".$v."</option>";
                                          }
                                        } else {
                                          echo "<option>Select Option</option>";
                                          echo "<option value='1'>ka</option>";
                                        }
                                      ?>
                                    </select>
                                    <span class="help-block" id="state_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="pincode1" name="pincode1" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode" tabindex="7">
                                    <span class="help-block" id="pincode_err1"> </span>
                                  </div>
                
                                   <input type="hidden" name="oos" value="0" id="oos" />
                                   <input type="hidden" name="pid" value="6" id="pid" />
                                   <input type="hidden" name="siteurl" value="<?php echo FULL_SITE_URL?>" id="siteurl" />

                                <div class="submitbtncontainer">
                                    <button type="submit" id="register_btn1" value="Submit" name="submit" class="button">
                                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/risk-form.gif" alt="buynow">
                                    </button>
                                </div>
      
                                <div class="secure_text">
                                  SECURE 256-BIT SSL ENCRYPTION
                                </div>
                                <div class="secure">
                                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/secure.jpg" alt="secure">
                                </div>
                              </div>
                            </div>
                          </form>


                        </div>

                     </div>
              </div>
    
            </div>
            <div class="clearfix"></div>
            <div class="m_banner">

            </div>  
          </div>
    </section>

    <!-- Banner Ends Here -->

    <div class="clearfix"></div>

    <!-- Mobile Form Starts Here -->

    <div class="mobilform">

        <form role="form" id="feedbackForm" name="user_signup" class="feedbackForm" action="javascript:void(0)" onsubmit="javascript:return ajaxReg_hairoil('<?php echo SITE_URL?>buyregister', '2');" >
          <div class="col heading">
            <h4>TELL US WHERE TO SEND</h4>                           
          </div> 

          <div class="form_items">       
          
            <div class="col">   
                <span class="help-block" id="global_err2"></span>
                <div class="form-group">
                    <input type="text" class="form-control" id="name2" name="name2" placeholder="Name" data-attr="Please enter correct Name" tabindex="1">
                    <span class="help-block" id="name_err2"></span>
                </div>

                <div class="form-group">
                  <input type="text" class="form-control only_numeric phone" id="mobile2" name="mobile2" pattern="\d*" placeholder="Mobile Number" data-attr="Please enter correct Mobile number" tabindex="2">
                  <span class="help-block" id="mobile_err2"> </span>
              </div> 
            
                <div class="form-group">
                    <input type="text" class="form-control" id="email2" name="email2" placeholder="Email Address" data-attr="Please enter correct email" tabindex="3">
                    <span class="help-block" id="email_err2"></span>
                </div>
                
                
                <div class="form-group">
                    <input type="text" class="form-control" id="address2" name="adderss2" placeholder="Street Address" data-attr="Please Enter Correct Address" tabindex="4">
                    <span class="help-block" id="address_err2"> </span>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="city2" name="city2" placeholder="Enter City" data-attr="Please Enter Correct City" tabindex="5">
                    <span class="help-block" id="city_err2"> </span>
                </div>

                <div class="form-group">
                  <select class="form-control" name="state2" id="state2" tabindex="6">
                    <option value="">Select State</option>
                    <?php
                      if (count($all_array['ARR_STATE']) > 0) {
                        
                        asort($all_array['ARR_STATE']);
                        foreach ($all_array['ARR_STATE'] as $k => $v) {
                          
                          echo "<option value='".$k."' >".$v."</option>";
                        }
                      } else {
                        echo "<option>Select Option</option>";
                        echo "<option value='1'>ka</option>";
                      }
                    ?>
                  </select>
                  <span class="help-block" id="state_err2"> </span>
                </div>

                <div class="form-group">
                  <input type="text" class="form-control" id="pincode2" name="pincode2" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode" tabindex="7">
                  <span class="help-block" id="pincode_err2"> </span>
                </div>

                 <input type="hidden" name="oos" value="0" id="oos" />
                 <input type="hidden" name="pid" value="6" id="pid" />
                 <input type="hidden" name="siteurl" value="<?php echo FULL_SITE_URL?>" id="siteurl" />

              <div class="submitbtncontainer">
                  <button type="submit" id="register_btn2" value="Submit" name="submit" class="button">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/risk-form.gif" alt="buynow">
                  </button>
              </div>

              <div class="secure_text">
                SECURE 256-BIT SSL ENCRYPTION
              </div>
              <div class="secure">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/secure.jpg" alt="secure">
              </div>
            </div>
          </div>
        </form>


    </div>

    <!-- Mobile Form Ends Here -->

    <div class="clerfix"></div>

    <!-- About Section Starts Here --> 
    
    <section class="container about mtop70">
      <div class="col-md-12 aboutSection">
        <h3> <b>The Sad Truth About  </b> HAIR LOSS</h3>

        <p class="mtop35 centerText parapading">Millions of men around the world suffer Male Pattern Baldness, also known as, Androgenetic Alopecia. This condition is genetic and happens over a period of time, which is well before the age of 50. This condition is observed in women as well.</p>
   
        <br>
        <br>

        <div class="col-md-12 about2ndSection">
          <div class="col-md-6 description">
            <p>
              There is no set age in men at which you can assert the onset of hair loss symptoms. It is noticed over a period of time and the symptoms are:

            </p>           

          </div>

          <div class="col-md-6 centerText mtop30 mleft">
            <div class="col-md-6">
              <img src="./images/hair-loss_1.png" alt="">
              <div class="mtop10"><strong>Before</strong></div>
              
            </div>
 
            <div class="col-md-6 centerText mtop30">
              <img src="./images/hair-loss.png" alt="">
              <div class="mtop10">
                <strong>After</strong>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    <!--About Section Ends Here -->

    <div class="clearfix"></div>

    <!-- Problems Section Starts Here -->

    <section class="container problems" >
          <div class="col-md-12 hair_problems">

            <div class="col-md-3 mtop30">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/what-to-do-about-your-thinning-hair-1080x673.png" alt="">
              <h5> <strong>Thinning Hair</strong></h5>
            </div>
            <div class="col-md-3 mtop30">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/hair-problem-june032019-min.png" alt="">
                <h5> <strong>Hair Breakage</strong></h5>
            </div>
            <div class="col-md-3 mtop30">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/621af29360685f88fae4c26f96ed9d8c_XL.png" alt="">
                <h5> <strong>Pattern Baldness</strong> </h5>
            </div>
            <div class="col-md-3 mtop30">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/shutterstock_773494057.png" alt="">
                <h5><strong>Hair Loss</strong> </h5>
            </div>

          </div>
          <div class="col-md-12 description">
            <p>Everyone knows hair is important, and multiple studies prove that hair loss negatively impacts almost every aspect of your life. Men who experience thinning hair and hair loss have trouble finding well-paying jobs and attracting the opposite sex. They experience higher rates of depression, social anxiety and a lack of self confidence.</p>

            <p>
              Nutratimes brings to you a revolutionary hair product to help you with balding scalp and hair loss. Nutratimes hair regrowth oil & shampoo combo is formulated especially for people suffering from hair loss and balding due to various reasons including genetic factors. The oil contains the natural goodness of <b>BRAHMI, ASHWAGANDHA and BHRINGARAJ.</b> These herbs stop hair fall and thinning of hair, and in return helps in hair regrowth and increasing its density. The shampoo contains <b>ALOEVERA, TULASI, AMLA, NEEM and HIBISCUS </b> which leaves your hair feeling fresh and replenished! For best results, it is recommended to use this combo of oil and shampoo provided by Nutratimes.
            </p>
          </div>

    </section>

    <!-- Problems Section Ends Here -->



    <div class="clearfix"></div>

    <!-- Science Section Starts Here -->

    <section class="container science">
        <div class="col-md-12 centerText">
          <h3>THE SCIENCE BEHIND 
             <strong>NUTRATIMES HAIR REGROWTH</strong> </h3>
        </div>

        <div class="col-md-12 centerText science2nd mtop20">
            <p> <b>Nutratimes</b> Ayurvedic Hairoil + Shampoo(Combo) has been formulated by a team of leading dermatologists to support hair nourishment, regrowth, and immunity across various stages of the hair growth cycle.
        </div>

        <div class="col-md-12 cycle">

          <div class="wrapper">
            <div class="col-md-12 mtop30">
                <div class="col-md-4 mleft4em">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/stage1.png" alt="">
                    <h4 class="mtop20"><b><span class="orange">STAGE 1</span>  - ANAGEN - GROWING PHASE</b></h4>
                    <div class="col-md-11 paddingZero">
                        <p>Nourishes the scalp and hair follicles from within, facilitating their emergence from the sebaceous glands.</p>
                    </div>
                    
                </div>

                <div class="col-md-3"></div>
                  
                <div class="col-md-4 mtop">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/stage2.png" alt="">
                    <h4 class="mtop20"><b><span class="orange">STAGE 2</span> - CATAGEN - REGRESSION PHASE</b></h4>
                    <div class="col-md-11 paddingZero">
                        <p>Prevents hair fall and damage while Strengthening  and promoting the growth of your existing hair.</p>
                    </div>
                    
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-4 mtop70 rfloat mtop mright2em">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/stage3.png" alt="">
                    <h4><b><span class="orange">STAGE 3</span> - TELOGEN - RESTING PHASE</b></h4>
                    <div class="col-md-11 paddingZero">
                        <p>This is the resting phase where around 10-15 percent of hairs will fall into. Whilst the old hair is resting, a new thick hair begins the growth with the help of Nutratimes Hairoil.</p>
                    </div>
                    
                </div>


                <div class="col-md-4 mtop70 mtop mleft6em">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/stage4.png" alt="">
                    <h4 class="mtop20"><b><span class="orange">STAGE 4</span> - EXOGEN - SHEDDING PHASE</b></h4>
                    <div class="col-md-11 paddingZero">
                        <p>This is part of the resting phase where the old hair sheds and a new hair continues to grow.  But research shows that Nutratimes shampoo usage    minimize hairs fall on daily basis and simultaneously Nutratimes Hairoil nourishes the hair roots with required nutrients to grow new hair.</p>
                    </div>
                    
                </div>
            </div>

            
          </div>
            

        </div>

    </section>

    <!-- Science Section Ends Here -->

    <div class="clearfix"></div>

   <!-- Advertise Section Starts Here -->

   <section class="buy blue_gradient">

      <div class="container ">

          <div class="col-md-12 marginFourty">

             <div class="col-md-8">
                <h3>NUTRATIMES GUARANTEES HAIR REGROWTH</h3>
                <p>Place your order today and claim your Risk FREE BOTTLES, because time and hair loss wait for no one!</p>
             </div>

             <div class="col-md-4 mtop15">
                <a href="">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/risk.png" alt="">
                </a>
             </div> 

          </div>

      </div>

   </section>

   <!-- Advertise Section Ends Here -->

   <div class="clearfix"></div>
    
   
   <div class="gradient">

     <!-- Strengths Section Starts Here -->
      
     <section class="container strength">

       <div class="col-md-12 centerText">
        <h3 class="mtop70">CLINICAL <b>STRENGTH HAIR REGROWTH MATRIX</b> </h3>
       </div>
      
       <div class="col-md-12 wrapper strengths mleft15">

          <div class="col-md-12">
              <div class="col-md-4">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Brahmi-Olej.png" alt="">
                <h5>BRAHMI</h5>
                <p>Supports circulation of nutrition to hydrate and deep-conditioned the hair follicles. Through the goodness of Brahmi, Nutratimes Hairoil + Shampoo(Combo) helps to repair damaged hair and prevents further hair damage in future.</p>
              </div>
              <div class="col-md-4 "></div>
              <div class="col-md-4 mobile_mtop70">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Ashwagandha-Roots.png" alt="">
                  <h5>ASHWAGANDHA</h5>
                  <p>Ashwagandha in Nutratimes Hairoil + Shampoo(Combo) helps to build collagen and supports absorption of iron, which keeps your hair locks strong and healthy. This advanced formulation of Nutratimes Hairoil + Shampoo(Combo) also helpful in preventing split ends.</p>
              </div>
              
        </div>
  
        <div class="col-md-12">
   
            <div class="col-md-4 mtop70">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Bhringaraj.png" alt="">
              <h5>BHRINGARAJ</h5>
              <p>Nutratimes and its hair regrowth formulation contains ingredient like Bringaraj that boosts elasticity of the cortex. It prevents hair breakage by stimulating dormant follicles for hair regrowth.</p>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 mtop70">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/shankhpushpi.png" alt="">
                <h5>SHANKHPUSHPI</h5>
                <p>It boosts the hormonal equilibrium to support hair health and appearance. It also thickens and restores luster to the hair. Effectively works for Migrane & Headache.</p>
            </div>
            
        </div>

       </div>
      

     </section>

     <!-- Strengths Section Ends Here -->


    <!-- Strengths Section Starts Here --> 

    <section class="carousel">
          <div class="container carasouelContainer">
              <h3 class="carasouelHead"> User <b>Voice</b></h3>
              <div class="col-md-12">
                  <br>
                  <br>
                  
                  <div class="col-md-4">
                      <div class="sliderNav" style="height: 300px !important;">
      
                          <div class="col-md-12">
                              <div class="col-md-6">
                                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Arjun_commentor.jpeg" alt="carouselImg">
      
                              </div>
                              <div class="col-md-6">
                                      <h6>Arjun</h6>
                                      <p>2019-08-06</p>                      
                              </div>
                          </div>
      
                          
                          <div class="col-md-12">
                              <div class="col-md-6">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Arya_commentor.jpeg" alt="">                              
      
                              </div>
                              <div class="col-md-6">
                                      <h6>Arya</h6>
                                      <p>2019-08-06</p>
                              </div>
                          </div>
      
                          
                          <div class="col-md-12">
                              <div class="col-md-6">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Priya_commentor.jpeg" alt="">
      
                              </div>
                              <div class="col-md-6">
                                      <h6>Priya</h6>
                                      <p>2019-08-06</p>
                                  
                              </div>
                          </div>
                          
      
                          
                          <div class="col-md-12">
                              <div class="col-md-6">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/dev_commentor.jpeg" alt="">
      
                              </div>
                              <div class="col-md-6">
                                      <h6>Dev</h6>
                                      <p>2019-08-06</p>
                                  
                              </div>
                          </div>
      
                          
                          <div class="col-md-12">
                              <div class="col-md-6">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/vijay_commentor.jpeg" alt="">
      
                              </div>
                              <div class="col-md-6">
                                      <h6>Vijay</h6>
                                      <p>2019-08-06</p>
                              </div>
                          </div>
                          
      
                          <div class="col-md-12">
                              <div class="col-md-6">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Farhad_commentor.jpeg" alt="">
                              </div>
                              <div class="col-md-6">
                                  <h6>Farhad</h6>
                                  <p>2019-08-06</p>
                              </div>
                          </div>
      
                      </div>
                  </div>
                  <div class="col-md-1">
                          <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/side_bar.png" alt="" height="280" style="margin-top: -15px;">
                  </div>
                  <div class="col-md-6">
                      <div class="sliderFor" style="height: 250px !important;">
                          <div>
                            
                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              When U are 30 and have a bald patch, people criticize you. Thank you Nutratimes, for this great product for hair regrowth.
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                          <div>

                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                          <div>

                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for thisTried a lot of products for hair loss but none have been this effective. I thank Nutratimes for thisTried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                          <div>

                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              A natural herbal product with no side effects. Must try.
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                          <div>

                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              Before it was pay for a product that resulted in more hair fall but now with Nutratimes hair regrowth oil & shampoo I have got thick, long and strong hair.
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                          <div>

                            <span class="leftQuote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
  
                            <h4>
                              This product is worth every penny. I seeing amazing results and it is 100% natural!
                            </h4>
          
                            <span class="rightQuote"><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            <br>

                            <div class="stars">
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star"></i></span>
                                <span ><i class="fa fa-star-half-o"></i></span>  
                            </div>

                          </div>
                      </div>
                      
                  </div>
              </div>
                 
          </div>

    </section>

    <div class="col-md-12 mobile_view user_reviews">
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Arjun_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Arjun</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">When U are 30 and have a bald patch, people criticize you. Thank you Nutratimes, for this great product for hair regrowth.</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Arya_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Arya</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Priya_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Priya</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/dev_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Dev</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">A natural herbal product with no side effects. Must try.</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/vijay_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Vijay</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">Before it was pay for a product that resulted in more hair fall but now with Nutratimes hair regrowth oil & shampoo I have got thick, long and strong hair.</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/Farhad_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Farhad</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">This product is worth every penny. I seeing amazing results and it is 100% natural!</p>
                  </div>
                </div>
          </div>

    </div>

    <!-- Strengths Section Ends Here -->

    <div class="clearfix"></div>


    <!--Footer Section Starts Here  -->

    <footer>

        <div class="container">
            <div class="col-md-12">

                <div class="col-md-12 ">
                    <p class="centerText statement">These statements have not been evaluated by the Food and Drug Administration or any government agency. This product is not intended to diagnose, treat, cure, or prevent disease. As individuals differ, so will results.</p>
                </div>

              </div>
              <div class="clearfix"></div>

             <hr>

              <div class="col-md-12">

                  <div class="nav_links">
                      <a href="<?php echo SITE_URL?>privacy-policy" class="open-popup-link">Privacy Policy </a> |
                      <a href="<?php echo SITE_URL?>terms-conditions" class="open-popup-link">Terms & Condition  </a> |
                      <a  href="<?php echo SITE_URL?>return-policy" class="open-popup-link">Return Policy   </a> |
                      <a  href="<?php echo SITE_URL?>disclaimer-policy" class="open-popup-link">Disclaimer Policy </a> |
                      <a href="<?php echo SITE_URL?>refund-policy" class="open-popup-link">Refund Policy  </a> |
                      <a href="<?php echo SITE_URL?>shipping-policy" class="open-popup-link">Shipping Policy</a>
                  </div>

              </div>
              <!-- <div class="col-md-12 secondaryFooter">

                  <div class="col-md-3 ">
                      <h3>About </h3>
                      <div class="about">
                         <a href="#">Our Story </a> <br>
                         <a href="#">Benfits </a> <br>
                         <a href="#">FAQ </a>
                      </div>
  
                  </div>
                    
                <div class="col-md-3">
                    <h3>LEGAL</h3>

                    <div class="legal">
                        <a href="#">Terms & Conditions </a> <br>
                        <a href="#">Privacy Policy </a> 
                        
                    </div>
                    
                </div>

                <div class="col-md-3 ">
                      <h3>Keep In Touch</h3>
                      <ul class="pad0">
                        <li>Email Us: <a href="#">support@nutratimes.com</a></li>
                        <li>Call Us: +91 08046328320 </li>
                      </ul>
                      
                </div>

                <div class="col-md-3 ">
                    <h3>Follow Us</h3>                   
                    <ul class="pad0">
                        <li> <a href="#">facebook </a> </li>
                        <li><a href="#">Twitter </a></li>
                        <li><a href="#">Linkedin </a></li>
                    </ul>                                          
                    

                </div>
              </div> -->
        </div>
    </footer>

    <!--Footer Section Ends Here  -->

  </div>

 </div>  

  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  
  <script type="text/javascript" src="<?php echo S3_URL?>cdn/hairoil-lp/2/js/main.js"></script>

  <script src="<?php echo S3_URL?>cdn/hairoil-lp/common.js" type="text/javascript"></script>


</body>

</html>