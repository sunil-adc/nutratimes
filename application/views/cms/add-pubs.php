<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Network </span> : </label> 
       		<?php 
				if($net_data->num_rows() > 0){
					echo '<select class="text-input small-input" name="net_id" id="net_id">';
					foreach ($net_data->result() as $row) {
						$selected = ($result_data['net_id'] == $row->id) ? 'selected' : '';
						echo '<option '.$selected.' value="'.$row->id.'">'.$row->net.'</option>';
					}
					echo '</select>';
				}
			?>
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Publisher Name</span> : </label> 
       		<input type="text" class="text-input small-input" name="pubs" id="pubs" value="<?php echo $result_data['pubs'];?>" />
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Pincode Type </span> :</label> 
       		<?php 
				if($pixel_data->num_rows() > 0){
					echo '<select class="text-input small-input" name="pincode_type" id="pincode_type">';
					foreach ($pixel_data->result() as $row) {
						$selected = ($result_data['pincode_type'] == $row->id) ? 'selected' : '';
						echo '<option value="'.$row->id.'" '.$selected.'>'.$row->id.'X</option>';
					}
					echo '</select>';
				}
			?>
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Handle Type</span> : </label> 
       		<select name="handle_type" id="handle_type" class="text-input small-input">
            	<option>Select Option</option>
                <option value="1" <?php echo ($result_data['handle_type'] == 1) ? "selected='selected'" : "";?>>Manual</option>
                <option value="2" <?php echo ($result_data['handle_type'] == 2) ? "selected='selected'" : "";?>>Automatic</option>
            </select>
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Network Type</span> : </label> 
       		<select name="net_type" id="net_type" class="text-input small-input">
            	<option value="0" <?php echo (isset($result_data['net_type']) && $result_data['net_type'] == 0) ? "selected='selected'" : "";?>>
                	Image Pixel</option>
                <option value="1" <?php echo (isset($result_data['net_type']) && $result_data['net_type'] == 1) ? "selected='selected'" : "";?>>
                	S2S Pixel</option>
            </select>
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">CPL (Click Per Lead)</span> : </label> 
       		<input type="text" class="text-input small-input" name="cpl" id="cpl" value="<?php echo $result_data['cpl'];?>" />
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Fire Type</span> : </label> 
       		<select name="fire_type" id="fire_type" class="text-input small-input">
            	<option value="0" <?php echo (isset($result_data['fire_type']) && $result_data['fire_type'] == '0') ? "selected='selected'" : "";?>>
                Currency Basis</option>
                <option value="1" <?php echo (isset($result_data['fire_type']) && $result_data['fire_type'] == '1') ? "selected='selected'" : "";?>>
                Percentage Basis</option>
            </select>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Goal</span> </label>
            <input type="text" class="text-input small-input" name="goal" id="goal" value="<?php echo $result_data['goal'];?>" /> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
            <select name="status" id="status" class="small-input" tabindex="2">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
            	<option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Pixel </span> : </label> 
       		<div class="color_red">HTTP:// or  HTTPS:// is require in case of S2S pixel</div><br />
            <div class="color_red">Replace dynamic zone id with "{ZONEID}", order id with "{ORDID}", programme id with "{PRGID}", click id with "{CLKID}", pub id with "{PUB_ID}" , user id with "{USER_ID}" and  Ip Address with "{IP_ADDR}" automated system will take it dynamic.</div><br />
            <textarea type="text" class="text-input medium-input" name="pixels" id="pixels" rows="10" cols="10"><?php echo $result_data['pixels']; ?></textarea>
        </p>
       	<?php
            if(strtolower($mode) == "edit"){
				?>
				<div class="information_div">
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo time_diff ($result_data['datecreated']); ?></label>
                    </p>
                    <div class="border_bottom"></div>
                	<p class="info_bar">
                      <label><span class="color_blue">Update Date : </span>
                        <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>
				<?php
			}
			
        	echo form_hidden('mode', $mode);
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
