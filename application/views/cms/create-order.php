<?php $arr_all = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <span id="error_msg"></span>
        <fieldset id="order_container">
         <form name="create_order" id="create_order" action="javascript:create_order()">
         <div class="prod_full_div" style="padding-right:10px;">
           <div class="align-left prod_leftside">
              <div class="prod_box">
                <div class="prod_title">Customer Info</div>
                <div class="prod_desc">   
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Name : </span> </label>
                      <input class="text-input small-input" type="text" id="name" name="name" tabindex="1" value="<?php echo $cname?>" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Phone : </span> </label>
                      <input class="text-input small-input only_numeric" type="text" id="mobile" name="mobile" tabindex="2" value="<?php echo $phone?>"/> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Email Address : </span> </label>
                      <input class="text-input small-input" type="text" id="email" name="email" tabindex="3" value="<?php echo $email?>"/> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Address : </span> </label>
                      <textarea class="text-input small-input" id="address" name="address" tabindex="4" rows="5"><?php echo $address?></textarea> 
                    </p>
                 </div>
             </div>
           </div>
           <div class="align-left prod_leftside">
              <div class="prod_box">
                 <div class="prod_title">More Info</div>
                 <div class="prod_desc">
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">City : </span> </label>
                      <input class="text-input small-input" type="text" id="city" name="city" tabindex="5" value="<?php echo $city?>"/> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Area : </span> </label>
                      <input class="text-input small-input" type="text" id="area" name="area" tabindex="6" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">State : </span> </label>
                      <?php echo custom_dropdown('state', '', $arr_all['ARR_STATE'], $state,'class="text-input small-input" tabindex="7"');?> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Pincode : </span> </label>
                      <input class="text-input small-input only_numeric" type="text" id="pincode" name="pincode" tabindex="8" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Payment Status : </span> </label>
                      <select class="text-input small-input" id="payment_status" name="payment_status" tabindex="9">
                      	<option value="">Select Option</option>
                        <option value="1">Paid</option>
                        <option value="0">Not Paid</option>
                      </select> 
                    </p>
                    <p>
                      <label><span class="color_blue">Alternate Phone : </span> </label>
                      <input class="text-input small-input" type="text" id="alternate_phone" name="alternate_phone" tabindex="9" /> 
                    </p>
                    <p>
                      <label><span class="color_blue">Utm Source : </span> </label>
                      <input class="text-input small-input" type="text" id="net" name="net" tabindex="10" value="<?php echo $net?>"/> 
                    </p>
                    <?php
						echo form_hidden('manage_page', $manage_page);
						echo form_hidden('add_page', $add_page);
						//echo form_hidden('cur_url', get_full_url());
            echo form_hidden('cur_url', FULL_CMS_URL."/createorder");
						// SET THE FULL URL USING SEGMENT ARRAY
					?>
                 </div>
              </div>
           </div>
           <input type="hidden" data-siteurl="<?php echo FULL_CMS_URL?>" value="first" id="mode" name="mode" />
           <div class="clear"></div>
        </div>
        <p id="submit_btn"><input class="button" type="submit" name="submit" id="submit" value="NEXT >>" /></p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
