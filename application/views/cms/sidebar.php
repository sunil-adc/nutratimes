<?php
	$cur_sub_menu = NULL;
	$cur_menu = NULL;
	$main_menu_permitted = NULL;
		
	echo '<ul id="main-nav">';
	$class = ($cur_controller == 'dashboard') ? "current" : "";
	echo '<li> <a href="'.FULL_CMS_URL.'/dashboard" class="nav-top-item no-submenu '.$class.'">Dashboard</a>';
	echo '</ul>';
	
	// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING
	$cur_controller = strtolower($cur_controller);
	// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING
	
	// CREATE PERMISSION ARRAY FOR EACH AND EVERY CONTROLLER
	$arr_permitted_controller = create_permission_array($this->session->userdata('admin_role_details'));
	
	if( is_array ($menu)){
		$i = 1;
		$j = 0;
		foreach($menu as $k => $v) {
			echo '<ul id="main-nav">';
			// CHECK FOR MAIN MENU SELECTION
			if( is_array ($v[2]) && count($v[2]) > 0){
				for ($j = 0; $j < count($v[2]); $j++) {
					if (in_array ($cur_controller, array_lower($v[2][$j]))) {
						$cur_menu = "current";
					}
				}
			}
			
			// CHECK FOR PERMISSION FOR PARENT MENU 
			if( is_array ($arr_permitted_controller) && count($arr_permitted_controller) > 0){
				if( is_array ($v[2]) && count($v[2]) > 0){
					for ($j = 0; $j < count($v[2]); $j++) {
						if (in_array ($v[2][$j][1], $arr_permitted_controller) || in_array ($v[2][$j][2], $arr_permitted_controller)) {
							$main_menu_permitted = true;
						}
					}
				}		
			}
			
			// IF MAIN MENU IS PERMITTED THEN START THE UI
			echo ($main_menu_permitted == true) ? '<li> <a href="javascript:void(0)" class="nav-top-item no-submenu '.$cur_menu.'">'.$v[0].'</a>' : '';
			
			if( is_array ($v[2]) && count($v[2]) > 0){
				echo '<ul>';
				foreach($v[2] as $k1 => $v1) {
					// IF USER IS PERMITTED TO VIEW THAT PAGE THEN AND THEN ONLY DISPLAY THAT LINK
					if(in_array(strtolower($v1[1]), $arr_permitted_controller)) {
						$cur_sub_menu = (($cur_controller == strtolower($v1[1])) || ($cur_controller == strtolower($v1[2]))) ? 'class="current"' : '';
						echo '<li><a '.$cur_sub_menu.' href="'.FULL_CMS_URL."/".$v1[1].'">'.$v1[0].'</a></li>';	
					}
				}
				
				// IF MAIN MENU IS PERMITTED THEN CLOSE THE UI
				echo ($main_menu_permitted == true) ? '</li>' : '';
				echo '</ul>';
			}
			echo '</ul>';
			$i++;
			$cur_menu = "";
			$main_menu_permitted = "";
		}
	}
	
	echo '<ul id="main-nav">';
	echo '<li> <a href="'.FULL_CMS_URL.'/logout" class="nav-top-item no-submenu">Logout</a>';
	echo '</ul>';
?>