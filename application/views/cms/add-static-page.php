<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Page Title</span> </label>
          <input class="text-input small-input" type="text" id="page_title" name="page_title" value="<?php _isset($result_data['page_title']); ?>" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Page Name</span> </label>
          <input class="text-input small-input" type="text" id="page_name" name="page_name" value="<?php _isset($result_data['page_name']); ?>" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Meta Title</span> </label>
          <input class="text-input small-input" type="text" id="meta_title" name="meta_title" value="<?php _isset($result_data['meta_title']); ?>" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Meta Keywords</span> </label>
          <input class="text-input small-input" type="text" id="meta_keywords" name="meta_keywords" value="<?php _isset($result_data['meta_keywords']); ?>" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Meta Description</span> </label>
          <input class="text-input small-input" type="text" id="meta_description" name="meta_description" value="<?php _isset($result_data['meta_description']); ?>" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Page Description</span> </label>
          <textarea class="text-input textarea" id="ckeditor_description" name="page_description" cols="79" rows="15"><?php _isset($result_data['page_description']); ?></textarea> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status </span></label>
            <select name="status" id="status" class="small-input">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
            </select> 
        </p>
         <?php
		  	if ( isset($result_data['datecreated'])) {
			?>
        	<p>
            	<label><span class="color_blue">Create Date</span></label>
          		<?php echo time_diff ($result_data['datecreated']); ?>
            </p>
            <?php
			}
			
			if ( isset($result_data['dateupdated'])) {
			?>
            <p>
              <label><span class="color_blue">Update Date</span></label>
                <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?>
            </p>
        	<?php
			}
			
        	echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>