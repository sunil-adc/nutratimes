<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
            <div class="align-left prod_leftside">
                <div class="prod_box">
                    <div class="prod_title">Security</div>
                    <div class="prod_desc">
                         <p>
                          <label><span class="color_red">*</span> <span class="color_blue">User Role : </span> </label>
                          <?php echo $admin_role;?>
                         </p>
                         <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Email Address: </span> </label>
                          <input class="text-input small-input" type="text" id="email" name="email" value="<?php _isset($result_data['email']); ?>" tabindex="2" /> 
                         </p>
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Username : </span> </label>
                          <input class="text-input small-input" type="text" id="username" name="username" value="<?php _isset($result_data['username']); ?>" tabindex="3" /> 
                        </p>
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Password : </span> </label>
                          <input class="text-input small-input" type="password" id="password" name="password" placeholder="<?php echo (strtolower($mode) == 'edit') ? (strtotime($result_data['last_psw_cng']) > 0) ? 'Last password changed '.time_diff ($result_data['last_psw_cng']) : 'Password Not Changed Yet !!' : 'Set password for this user'; ?>" tabindex="4" /> 
                        </p>
                    </div>
                </div>
                <div class="prod_box">
                    <div class="prod_title">General </div>
                    <div class="prod_desc">
        				<p>
                            <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
                            <select name="status" id="status" class="small-input" tabindex="5">
                                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                                <option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                            </select> 
                        </p>
                    </div>
                </div>
            </div>
            <div class="align-left prod_leftside">
                <div class="prod_box">
                    <div class="prod_title">Profile Details</div>
                    <div class="prod_desc">
        				<p>
                          <label><span class="color_red">*</span> <span class="color_blue">First Name : </span> </label>
                          <input class="text-input small-input" type="text" id="fname" name="fname" value="<?php _isset($result_data['fname']); ?>" tabindex="6" /> 
                        </p>
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Last Name : </span> </label>
                          <input class="text-input small-input" type="text" id="lname" name="lname" value="<?php _isset($result_data['lname']); ?>" tabindex="7" /> 
                        </p>
                       
                        <p>
                          <label><span class="color_blue">Facebook URL : </span> </label>
                          <input class="text-input small-input" type="text" id="fb_url" name="fb_url" value="<?php _isset($result_data['fb_url']); ?>" tabindex="8" /> 
                        </p>
                        <p>
                          <label><span class="color_blue">Twitter URL : </span> </label>
                          <input class="text-input small-input" type="text" id="twt_url" name="twt_url" value="<?php _isset($result_data['twt_url']); ?>" tabindex="9" /> 
                        </p>
                    </div>
                </div>
            </div>
        	<div class="clear"></div>
        </div>
        <?php
            if(strtolower($mode) == "edit"){
				?>
				<div class="information_div">
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo time_diff ($result_data['datecreated']); ?></label>
                    </p>
                    <div class="border_bottom"></div>
                    <p class="info_bar">
                      <label><span class="color_blue">Update Date : </span>
                        <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>
        		<?php
			}
			
        	echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
