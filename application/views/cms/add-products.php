<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		
		
		?>
        
        <fieldset>
        	<div class="prod_full_div" style="padding-right:10px;">
            	<div class="align-left prod_leftside">
                	<div class="prod_box">
                    	<div class="prod_title">Categories</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_red">*</span> <span class="color_blue">Category : </span></label>
                              <?php echo $categories; ?>
                            </p>
                            <p>
                              <label><span class="color_blue">Sub Category : </span></label>
                              <span class="color_red" id="ajax_subcat">
                                <select class="small-input" tabindex="2" >
                                    <option value="0">Select Category First !!</option>
                                </select>
                              </span>
                            </p>
                            <p>
                              <label><span class="color_blue">Brand : </span></label>
                              <?php echo $brands; ?>
                            </p>
                    		<br />
                    	</div>
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Product Details</div>
                    	<div class="prod_desc">
                            <p>
                              <label><span class="color_red">*</span> <span class="color_blue">Product Name : </span></label>
                              <input class="text-input small-input" type="text" id="name" name="name" value="<?php _isset($result_data['name']); ?>" onblur="javascript:set_seourl('name','seourl')" tabindex="4" />
                            </p>
                             <p>
                              <label><span class="color_red">*</span> <span class="color_blue">SEO URL</span> </label>
                              <input class="text-input small-input" type="text" id="seourl" data-tbl="<?php echo PRODUCT;?>" name="seourl" value="<?php _isset($result_data['seourl']); ?>" data-name="prod_id" data-id="<?php _isset($result_data['prod_id'])?>" data-siteurl="<?php echo FULL_CMS_URL?>" tabindex="5" /> 
                              <label id="seourl_err"></label>
                            </p>
                            <p>
                              <label><span class="color_blue">Model No : </span></label>
                              <input class="text-input small-input" type="text" id="model_no" name="model_no" value="<?php _isset($result_data['model_no']); ?>" tabindex="6" />
                            </p>
                            <p>
                                  <label><span class="color_blue">Warranty Message : </span></label>
                                  <textarea class="text-input small-input" id="warranty_msg" name="warranty_msg" rows="5" tabindex="7"><?php _isset($result_data['warranty_msg']); ?></textarea>
                            </p>            
                            
                            <p>
                              <label><span class="color_blue">Priority : </span></label>
                              <input class="text-input small-input" type="text" id="priority" name="priority" value="<?php _isset($result_data['priority']); ?>" tabindex="8" />
                            </p>
                            <p>
                              <label><span class="color_blue">Video URL : </span></label>
                              <textarea class="text-input small-input" id="video_url" name="video_url" rows="5" tabindex="9"><?php _isset($result_data['video_url']); ?></textarea>
                            </p>
                    		<p>
                                <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
                                <select name="status" id="status" class="small-input" tabindex="10">
                                    <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected"' : '';?>>Active</option>
                                    <option value="0" <?php echo ($result_data['status'] != '1') ? 'selected' : '';?>>Inactive</option>
                                </select> 
                            </p>
                            <p>
                                <label><span class="color_red">*</span> <span class="color_blue">Out of Stock</span> </label>
                                <select name="oos" id="oos" class="small-input" tabindex="10">
                                    <option value="0" <?php echo ($result_data['oos'] != '0') ? 'selected' : '';?>>No</option>
                                    <option value="1" <?php echo ($result_data['oos'] == '1') ? 'selected' : '';?>>Yes</option>
                                </select> 
                            </p>
                        <br />
                    	</div>
                        
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Product Description</div>
                    	<div class="prod_desc">
                            <p>
                              <label><span class="color_blue">Free Product : </span></label>
                              <select name="free_prod[]" id="free_prod" class="small-input" multiple size="10">
							  <option value="">Select Option</option>
							  <?php 
									if (strlen($result_data['free_prod']) > 0) {
										$arr_free_prod_list = explode(",", trim($result_data['free_prod']));	
									}
									
									if (is_array($arr_product) && $arr_product > 0) {
										foreach ($arr_product as $key_free_product => $val_free_product) {
											$checked =  '';
											if (isset ($arr_free_prod_list) && is_array($arr_free_prod_list) && $arr_free_prod_list != '') {
												if(in_array($val_free_product->prod_id, $arr_free_prod_list)) {
													$checked = 'selected';
												}
											}
											?><option <?php echo $checked;?> value="<?php echo $val_free_product->prod_id?>"><?php echo "(Id : ".$val_free_product->prod_id.") ".$val_free_product->name;?></option><?php
										}
									} else {
										echo '<option value="0">product not available.</option>';
									}
							  ?>
                              </select>
                            </p>
                            <p>
                              <label><span class="color_blue">Small Description : </span></label>
                                <textarea class="text-input small-input" id="small_description" name="small_description" rows="5" tabindex="12"><?php _isset($result_data['small_description']); ?></textarea>
                            </p>
                		</div>
                   	</div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Product Availability</div>
                    	<div class="prod_desc">
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Size Availability : </span></label>
                          <select name="is_size" id="is_size" class="small-input size_availability" data-prodid="<?php echo ($result_data['prod_id'] != "") ? $result_data['prod_id'] : 0; ?>" data-cmsurl="<?php echo FULL_CMS_URL?>" tabindex="13">
                              <option value="1" <?php echo ($result_data['is_size'] == '1') ? 'selected' : '';?>>Available</option>
                              <option value="0" <?php echo ($result_data['is_size'] != '1') ? 'selected' : '';?>>Not Available</option>
                          </select> 
                        </p>
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Color : </span></label>
                          <?php echo $color; ?>
                        </p>
                        <p>
                       	  <label><span class="color_blue">Other Color Products: </span></label>
                          <select name="color_list[]" id="color_list" class="small-input" multiple size="10">
						  	<option value="">Select Option</option>
						    <?php 
						  		if (strlen($result_data['color_list']) > 0) {
									$arr_color_list = explode(",", trim($result_data['color_list']));	
								}
								
						  		if (is_array($arr_product) && $arr_product > 0) {
									foreach ($arr_product as $key_color_product => $val_color_product) {
										$checked =  '';
										if (isset ($arr_color_list) && is_array($arr_color_list) && $arr_color_list != '') {
											if(in_array($val_color_product->prod_id, $arr_color_list)) {
												$checked = 'selected';
											}
										}
										?><option <?php echo $checked;?> value="<?php echo $val_color_product->prod_id?>"><?php echo "(Id : ".$val_color_product->prod_id.") - ".$val_color_product->name;?></option><?php
									}
								} else {
									echo '<option value="0">product not available.</option>';
								}
						  ?>
                          </select>
                        </p>
                        <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Check Inventory : </span></label>
                          <select name="check_inventory" id="check_inventory" class="small-input" tabindex="14">
                              <option value="1" <?php echo ($result_data['check_inventory'] == '1') ? 'selected' : '';?>>Yes</option>
                              <option value="0" <?php echo ($result_data['check_inventory'] != '1') ? 'selected' : '';?>>No</option>
                          </select> 
                        </p>
                        
                        <?php
						
						if(isset($result_data['free_offer_descr']) && $result_data['free_offer_descr'] != '')
						{
							$foDisabled = '';
							$foChecked = 'checked="checked"';
							$freeOffer = $result_data['free_offer_descr'];
							
						}
						else
						{
							$foDisabled = 'disabled="disabled"';
							$freeOffer = 'Free Offer!';
							$foChecked = '';
						}
						?>
                          <p>
                          	<input type="checkbox" id="chk_free_offer" name="chk_free_offer" onclick="javascript:$('#free_offer_descr').attr('disabled',$('#free_offer_descr').attr('disabled') == 'disabled' ? false:'disabled');" <?php echo $foChecked;?> /> <span class="color_blue">Click here to add the free offer for this product : </span>
                              <textarea class="text-input small-input" id="free_offer_descr" tabindex="26" name="free_offer_descr" rows="5" <?php echo $foDisabled;?>><?php echo $freeOffer; ?></textarea>
                            </p>
                			<br />
                        
                        
                        
                        
                        
                        
                    </div>
                    </div>
                        
                </div>
        		<div class="align-left prod_leftside">
                   	<div class="prod_box">
                    	<div class="prod_title">Price & Shipping Details</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_red">*</span> <span class="color_blue">MRP : </span></label>
                              <input class="text-input small-input" type="text" id="mrp" name="mrp" value="<?php _isset($result_data['mrp']); ?>" tabindex="15" />
                            </p>
                            <p>
                              <label><span class="color_blue">Discount : </span></label>
                              <input class="text-input small-input" type="text" id="discount" name="discount" tabindex="16" value="<?php _isset($result_data['discount']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_red">*</span> <span class="color_blue">COD Availability : </span></label>
                              <select name="cod_availability" id="cod_availability" class="small-input" tabindex="17">
                                  <option value="1" <?php echo ($result_data['cod_availability'] == '1') ? 'selected="selectec"' : '';?>>Available</option>
                                  <option value="0" <?php echo ($result_data['cod_availability'] != '1') ? 'selected="selectec"' : '';?>>Not Available</option>
                              </select> 
                            </p>
                            <p>
                              <label><span class="color_blue">COD Price : </span></label>
                              <input class="text-input small-input" type="text" id="cod_price" name="cod_price" tabindex="18" value="<?php _isset($result_data['cod_price']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_blue">Sale Price : </span></label>
                              <input class="text-input small-input" type="text" id="sale_price" name="sale_price" tabindex="19" value="<?php _isset($result_data['sale_price']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_blue">Shipping Price: </span></label>
                              <input class="text-input small-input" type="text" id="shipping" name="shipping" tabindex="20" value="<?php _isset($result_data['shipping']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_blue">CashBack Amount (In Rs): </span></label>
                              <input class="text-input small-input" type="text" id="cash_back_amt" name="cash_back_amt" tabindex="18" value="<?php _isset($result_data['cash_back_amt']); ?>" />
                            </p>
                            
                	<br />
                    	</div>
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">SEO Details</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_red">*</span> <span class="color_blue">Search Keywords : </span></label>
                              <textarea class="text-input small-input" id="search_keywords" name="search_keywords" tabindex="21" rows="2"><?php echo isset($result_data['search_keywords']) ? $result_data['search_keywords'] : ''; ?></textarea>
                            </p>
                            <p>
                              <label><span class="color_red">*</span> <span class="color_blue">Page Title: </span></label>
                              <input class="text-input small-input" type="text" id="title" name="title" tabindex="22" value="<?php _isset($result_data['title']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_red">*</span> <span class="color_blue">Meta Description: </span></label>
                              <input class="text-input small-input" type="text" id="meta_description" tabindex="23" name="meta_description" value="<?php _isset($result_data['meta_description']); ?>" />
                            </p>
                            <p>
                              <label><span class="color_red">*</span> <span class="color_blue">Meta Keywords: </span></label>
                              <input class="text-input small-input" type="text" id="meta_keywords" tabindex="24" name="meta_keywords" value="<?php _isset($result_data['meta_keywords']); ?>" />
                            </p>
                    		<br />
                    	</div>
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Contest Info</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_red">*</span> <span class="color_blue">Contest on this product : </span></label>
                              <select name="contest" id="contest" class="small-input" tabindex="25">
                                <option value="1" <?php echo ($result_data['contest'] == 1) ? "selected='selected'" : ""; ?>>Yes</option>
                                <option value="0" <?php echo ($result_data['contest'] != 1) ? "selected='selected'" : ""; ?>>No</option>
                              </select>
                            </p>
                            <p>
                              <label><span class="color_blue">Contest Description : </span></label>
                              <textarea class="text-input small-input" id="contest_description" tabindex="26" name="contest_description" rows="5"><?php _isset($result_data['contest_description']); ?></textarea>
                            </p>
                			<br />
                    	</div>
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Other Info</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_blue">Average Rating : </span>
                                <?php echo ($result_data['rating_avg'] != "") ? $result_data['rating_avg'] : "0"; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              	<span class="color_blue">Total Rating : </span>
                                <?php echo ($result_data['rating_total'] != "") ? $result_data['rating_total'] : "0"; ?>
                              </label>
                            </p>
                            <p>
                              <label><span class="color_blue">Create Date : </span>
                                <?php echo strtotime($result_data['datecreated']) > 0 ? time_diff ($result_data['datecreated']) : "Info Not Available"; ?>
                              </label>
                            </p>
                            <p>
                              <label><span class="color_blue">Update Date : </span>
                                <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?>
                              </label>
                            </p>
                		</div>
                    </div>
                    
                    <div class="prod_box">
                    	<div class="prod_title">Stock Availability</div>
                    	<div class="prod_desc">                        
                        <span id="ajax_stock_available" data-total-stock="<?php _isset($result_data['total_stock']); ?>">
                        	<p>
                              <label><span class="color_blue">Number of Stocks : </span></label>
                              <input class="text-input small-input" type="text" id="total_stock" name="total_stock" value="<?php _isset($result_data['total_stock']); ?>" tabindex="8" />
                            </p>
                        </span>
                    </div>
                    </div>
                    <div class="clear"></div>
                    <div class="prod_box">
                    	<div class="prod_title">Combo</div>
                    	<div class="prod_desc">
                        	<p>
                              <label><span class="color_blue">Combo Available : </span></label>
                              <select name="is_combo" id="is_combo" class="small-input">
                                <option value="1" <?php echo ($result_data['is_combo'] == 1) ? "selected='selected'" : ""; ?>>Yes</option>
                                <option value="0" <?php echo ($result_data['is_combo'] != 1) ? "selected='selected'" : ""; ?>>No</option>
                              </select>
                            </p>
                            
                            <p>
                              <label><span class="color_blue">Combo Products: </span></label>
                              <select name="combo_product[]" id="combo_product" class="small-input" multiple size="10">
                              <option value="">Select Option</option>
							  <?php 
									if (strlen($result_data['combo_product']) > 0) {
										$arr_combo_list = explode(",", trim($result_data['combo_product']));	
									}
									
									if (is_array($arr_product) && $arr_product > 0) {
										foreach ($arr_product as $key_combo_product => $val_combo_product) {
											$checked =  '';
											if (isset ($arr_combo_list) && is_array($arr_combo_list) && $arr_combo_list != '') {
												if(in_array($val_combo_product->prod_id, $arr_combo_list)) {
													$checked = 'selected';
												}
											}
											?><option <?php echo $checked;?> value="<?php echo $val_combo_product->prod_id?>"><?php echo "(Id : ".$val_combo_product->prod_id.") ".$val_combo_product->name;?></option><?php
										}
									} else {
										echo '<option value="0">product not available.</option>';
									}
							  ?>
                              </select>
                            </p>
                    </div>
                    </div>
                    
                </div>
           		<div class="clear"></div>
                <span id="product_image_box"></span>
           		<div class="prod_box">
                    <div class="prod_title">Product Image Upload :</div>
                    <p>
                    	<label>
                        	Select Image(s) : <input type="file" class="text-input" name="image[]" id="image[]"   multiple="multiple" />
                        	<span class="color_red"> Note : Press CTRL key and select for multiple selection</span>
                        </label>
                    </p>
                    <p class="cms_prod_image_container">
					<?php
                      if(isset($prod_image) && $prod_image->num_rows() > 0) {
                        foreach ($prod_image->result() as $row) {
                            $del_url = FULL_CMS_URL."/".$cur_controller."/delete_prod_img/".$row->prod_id."/".$row->prod_image_id."/".$row->image."/index/".$mode."/".$result_data[$primary_field];
							$set_default_class = ($row->isdefault == 1) ? 'default_product_image' : '';
                            ?>
                            <a href="javascript:void(0)" id="image_id_<?php echo $row->prod_image_id?>"><img src='<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_200_300_PATH.$row->image);?>' name="name" class='cms_prod_image <?php echo $set_default_class;?>' onclick="javascript:set_default_image('<?php echo FULL_CMS_URL.'/Ajaxsetdefaultimage/index/'.$result_data[$primary_field].'/'.$row->prod_image_id;?>', 'image_id_<?php echo $row->prod_image_id?>', this)" /><span onclick="javascript:_delete('<?php echo $del_url;?>', 'Product Image')" class="delete_img"></span></a>
                            <?php
                        }
					  } else {
                        echo "<span class='color_green'>Image's not available</span>";
					  }
                    ?>
                    </p>
                    
                </div>
                <span id="product_image_box"></span>
           		<div class="prod_box">
                    <div class="prod_title">Product Specifications :</div>
                      <p>
                      <textarea class="text-input small-input" id="ckeditor_description" name="description" rows="5"><?php _isset($result_data['description']); ?></textarea>
                      </p>
                </div>
                <?php 
					if (isset($verified_details) && is_array($verified_details)) {
						if (count($verified_details) > 0) {
							?>
                            <div class="prod_box">
                                <div class="prod_title">Verification Status :</div>
                                  <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                  Verification for this product has been done by <span class='color_green'><?php echo $verified_details['fname']." ".$verified_details['lname']." (".$verified_details['email'].")"; ?>
                                  </p>
                            </div>
                            <?php
						}	
					}
				?>
           </div>
        <?php
            
        	echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
<?php
	if(strtolower($mode) == "edit") {
		if($result_data['cat_id'] > 0) {
			?>
			<script type="text/javascript">
				$(document).ready(function(e) {
					get_subcat("<?php echo FULL_CMS_URL; ?>","<?php echo $result_data['cat_id'];?>","<?php echo $result_data['sub_cat_id'];?>","ajax_subcat","edit");                    
				});
			</script>
			<?php
		}
		if($result_data['is_size'] == 1) {
			?>
			<script type="text/javascript">
				$(document).ready(function(e) {
					$(".size_availability").trigger('change');
				});
			</script>
			<?php
		}
	}
?>