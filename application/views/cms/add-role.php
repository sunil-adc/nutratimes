<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Role Name</span> </label>
          <input class="text-input small-input" type="text" id="role_name" name="role_name" value="<?php _isset($result_data['role_name']); ?>" tabindex="1" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Role Details</span> </label>
          <div>
             <div class="role_header">
                <div class="role_header_title align-left">Main Menu</div>
                <div class="role_header_desc align-left">Select Roles</div>			
             	<div class="clear"></div>
             </div>
             <div class="role_body">
             <?php 
			 		if (isset($result_data['role_details'])) {
						$unserialize_role = unserialize($result_data['role_details']);
						if (is_array($unserialize_role)) {
							$role_details = unserialize($result_data['role_details']);
						}
					}
					
					$menu_list = $this->common_model->Menu_Array();
					$total_menu = count($menu_list) - 1;
					$i = 1;
					?>
					<div class="role_main">
                        <div class="role_title align-left">Require Pages</div>
                        <div class="role_desc align-left">						
                             <div class="role_element">
                                <div class="chkbx align-left">
                                    <input type="checkbox" name="chk_roles[]" id="chk_roles[]" value="dashboard" checked="checked" />
                                </div>
                                <div class="chk_desc align-left">Dashboard</div>
                                <div class="clear"></div>
                             </div>
                             <div class="role_element">
                                <div class="chkbx align-left">
                                    <input type="checkbox" name="chk_roles[]" id="chk_roles[]" value="logout" checked="checked" />
                                </div>
                                <div class="chk_desc align-left">Logout</div>
                                <div class="clear"></div>
                             </div>
                        </div>
                        <div class="clear"></div>							
                    </div>
                    <div class="role_border_bottom"></div>
					<?php
					foreach($menu_list as $k => $v) {
						?>
                        <div class="role_main">
							<div class="role_title align-left"><?php echo $v[0]; ?></div>
							<div class="role_desc align-left">						
							<?php
								foreach($v[2] as $k1 => $v1) {
									?>
                                    <div class="role_element">
                                        <div class="chkbx align-left">
                                        <?php 
											if(strtolower($v1[1]) == strtolower($v1[2])) {
												?>
                                                <input type="checkbox" name="chk_roles[]" id="chk_roles[]" value="<?php echo $v1[1]; ?>"
                                                <?php
													if(isset($role_details)) {
														echo (in_array($v1[1], $role_details)) ? "checked='checked'" : "";
													}
												?> /><?php
											} else {
												?>
                                                <input type="checkbox" name="chk_roles[]" id="chk_roles[]" value="<?php echo $v1[1].",".$v1[2]; ?>" 
                                                <?php
													if(isset($role_details)) {
														echo (in_array($v1[1].",".$v1[2], $role_details)) ? "checked='checked'" : "";
													}
												?> />
												<?php
											}
										?>
                                        </div>
                                        <div class="chk_desc align-left"><?php echo $v1[0]; ?></div>
                                        <div class="clear"></div>
                                    </div>
									<?php
                                }
							?>
                            </div>
                            <div class="clear"></div>							
                        </div>
						<?php
						if($total_menu >= $i) { 
							echo '<div class="role_border_bottom"></div>';
						}
						$i++;
					}
				?>
          	</div>
          </div>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
            <select name="status" id="status" class="small-input" tabindex="2">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
            	<option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
       	<?php
            if(strtolower($mode) == "edit"){
				?>
				<div class="information_div">
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo time_diff ($result_data['datecreated']); ?></label>
                    </p>
                    <div class="border_bottom"></div>
                    <p class="info_bar">
                      <label><span class="color_blue">Update Date : </span>
                        <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>
        		<?php
			}
			
        	echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
