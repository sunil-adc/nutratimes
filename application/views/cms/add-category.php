<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Main Category</span></label>
          <select name="parent_cat_id" id="parent_cat_id" class="small-input" tabindex="1">
		  <option value="">Select Option</option>
		  <?php $this->common_model->fetch_category('0', $result_data['parent_cat_id'], isset($result_data['cat_id']) ? $result_data['cat_id'] : 0);?>
          </select>
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Category Name </span></label>
          <input class="text-input small-input" type="text" id="name" name="name" value="<?php echo _isset($result_data['name']); ?>" tabindex="2"  onblur="javascript:set_seourl('name','seourl')"/> 
        </p>
        
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">SEO URL</span> </label>
        <input class="text-input small-input" type="text" id="seourl" data-tbl="<?php echo CATEGORY;?>" name="seourl" value="<?php _isset($result_data['seourl']); ?>" tabindex="3" data-name="cat_id" data-id="<?php echo $result_data['cat_id']?>" data-siteurl="<?php echo FULL_CMS_URL?>" /> 
          <label id="seourl_err"></label>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Image</span> </label>
            <input type="file" name="image" id="image" class="text-input small-input" />
            <br /><br /><?php 
				if(isset($result_data['image'])) {
						echo "<img src='".S3_URL.product_or_placeholder('./'.CATEGORY_THUMB_IMG_DIR.THUMB_PREFIX.$result_data['image'])."' />";
				}
			?> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status </span></label>
            <select name="status" id="status" class="small-input" tabindex="5">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
            	<option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Is it Popular ??</span></label>
            <select name="is_popular" id="is_popular" class="small-input" tabindex="6">
                <option value="0" <?php echo ($result_data['is_popular'] == '0') ? 'selected="selectec"' : '';?>>Not Popular</option>
            	<option value="1" <?php echo ($result_data['is_popular'] == '1') ? 'selected="selectec"' : '';?>>Popular</option>
            </select> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Minimum vat Rate </span></label>
          <input class="text-input small-input" type="text" id="min_vat_rate" name="min_vat_rate" value="<?php echo _isset($result_data['min_vat_rate']); ?>" tabindex="4" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">VAT (In Percentage) </span></label>
          <input class="text-input small-input" type="text" id="vat" name="vat" value="<?php echo _isset($result_data['vat']); ?>" tabindex="4" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Maximum redemption</span></label>
          <input class="text-input small-input" type="text" id="max_redemption" name="max_redemption" value="<?php echo _isset($result_data['max_redemption']); ?>" tabindex="7" /> 
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">COD Maximum redemption</span></label>
          <input class="text-input small-input" type="text" id="cod_max_redemption" name="cod_max_redemption" value="<?php echo _isset($result_data['cod_max_redemption']); ?>" tabindex="7" /> 
        </p>
		<?php
            if(strtolower($mode) == "edit"){
                ?>
                <div class="information_div">
                    <p class="info_bar">
                      <label><span class="color_blue">Category Level : </span>
                      <?php echo $result_data['cat_level']; ?></label>
                    </p>
                    <div class="border_bottom"></div>
                    <!--<p class="info_bar">
                        <label><span class="color_blue">Total Products In this Category : </span>
                        <?php //echo ($result_data['total_products'] > 0) ? $result_data['total_products'] : '0';?></label>
                    </p>-->
                    <div class="border_bottom"></div>
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo strtotime($result_data['datecreated']) > 0 ? time_diff ($result_data['datecreated']) : "Info Not Available"; ?></label>
                    </p>
                    <div class="border_bottom"></div>
                    <p class="info_bar">
                      <label><span class="color_blue">Update Date</span>
                      <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>    
            <?php
			}
                echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
                echo form_hidden('manage_page', $manage_page);
                echo form_hidden('add_page', $add_page);
                echo form_hidden($primary_field, $result_data[$primary_field]);
                echo form_hidden('cur_url', get_full_url());
                // SET THE FULL URL USING SEGMENT ARRAY
            ?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" tabindex="6" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
