<?php $arr_all = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <?php
        	if($this->session->userdata('admin_role_id') == '1') {
				if(isset($result_data['cms_order']) && $result_data['cms_order'] == '1' && $result_data['cms_order_user'] > 0) {
					$user_name = $this->db_function->get_single_value(ADMIN, 'username', "id=".$result_data['cms_order_user'], $echo = false, $cache_off = false);
					if($user_name != false) {
						?><div><div class="notification error png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL;?>'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>This order is added by <?php echo $user_name; ?> from backend at <?php echo date('Y-m-d H:i:s', strtotime($result_data['dt_c']));?></div></div><?php
					} else {
						?><div class="notification error png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL;?>'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>This order is added from backend at <?php echo date('Y-m-d H:i:s', strtotime($result_data['dt_c']));?></div></div><?php
					}
				} else {
					?><div class="notification success png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL;?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>This order is marked as genuine, added by customer it self</div></div><?php
				}
			}
		?>
        <div>
        </div>
        <div class="prod_full_div" style="padding-right:10px;">
           <div class="align-left prod_leftside">
              <div class="prod_box">
                <div class="prod_title">Customer Info</div>
                <div class="prod_desc">   
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Name : </span> </label>
                      <input class="text-input small-input" type="text" id="name" name="name" 
                        value="<?php _isset($result_data['name']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Phone : 
					            <span class="color_red padding_left_10"><?php _isset($result_data['mobile']); ?>
                      <?php 
                      
                          //if ($this->session->userdata('admin_role_id') == 3) { 
                      ?>    
                          <a href="<?php echo SITE_URL."cms/orderdetails/call_triger/".$result_data['id']."/".$result_data['mobile']."/".$this->session->userdata('uid')."-".$this->session->userdata('username') ?>" target="_blank" class="button" style="margin-left: 10px"> CALL </a>
                         <?php // } ?> 
                        
                    </span>
                    
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Email Address : </span> </label>
                      <input class="text-input small-input" type="text" id="email" name="email" 
                        value="<?php _isset($result_data['email']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Address : </span> </label>
                      <textarea class="text-input small-input" id="address" name="address" rows="5"><?php _isset($result_data['address']); ?></textarea> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">City : </span> </label>
                      <input class="text-input small-input" type="text" id="city" name="city" 
                        value="<?php _isset($result_data['city']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Area : </span> </label>
                      <input class="text-input small-input" type="text" id="area" name="area" 
                      value="<?php _isset($result_data['area']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">State : </span> </label>
                      <?php echo custom_dropdown('state', '', $arr_all['ARR_STATE'], $result_data['state'],'class="text-input small-input"');?> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Alternate Phone : </span> </label>
                      <input class="text-input small-input" type="text" id="alternate_phone" name="alternate_phone" 
                      value="<?php _isset($result_data['alternate_phone']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Pincode : </span> </label>
                      <input class="text-input small-input" type="text" id="pincode" name="pincode" 
                      value="<?php _isset($result_data['pincode']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Tracking Number : </span> </label>
                      <span class="color_red padding_left_10"><?php echo ($result_data['tracking_number']) ? $result_data['tracking_number'] : 'Tracking Number not set';?></span>
                    </p>
                </div>
             </div>
             <div class="prod_box">
                 <div class="prod_title">Order Commnets</div>
                 <div class="prod_desc">   
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Order Comments : </span> </label>
                      <textarea class="text-input small-input" id="comments" name="comments"><?php _isset($result_data['comments']); ?></textarea> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Issue Comments : </span> </label>
                      <textarea class="text-input small-input" id="issue_comments" name="issue_comments"><?php _isset($result_data['issue_comments']); ?></textarea> 
                    </p>
              	</div>
             </div>      
           </div>
           <div class="align-left prod_leftside">
             <div class="prod_box">
                 <div class="prod_title">Customer Info</div>
                 <div class="prod_desc">   
                    
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Order Reference Id : </span> </label>
                      <span class="color_red padding_left_10"><?php echo $result_data['id'];?></span>
                    </p>

                    <p>
                      <label><span class="color_blue">Offer Percentage : </span> </label>
                      <span class="color_red padding_left_10"><?php echo ($result_data['offer_percentage'] != 0 ? $arr_all['ARR_OFFER_PER'][$result_data['offer_percentage']] : "N/A") ;//echo custom_dropdown('offer_percentage', '', $arr_all['ARR_OFFER_PER'], $result_data['offer_percentage'],'class="text-input small-input"');?></span>
                    </p>
                    
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Payment Mode : </span> </label>
                      <?php echo custom_dropdown('payment_mode', '', $arr_all['ARR_PAYMENT'], $result_data['payment_mode'],'class="text-input small-input"');?>
                    </p>
                    <?php
						if($result_data['caller_status'] != 1 && $result_data['caller_status'] != 0 && $this->session->userdata('admin_role_id') != 2) {
							?>
							<p>
							  <label><span class="color_red">*</span> <span class="color_blue">Caller Status : </span> </label>
							  <?php echo $arr_all['ARR_CALLER_STATUS'][$result_data['caller_status']];?>
							</p>
                             <p>
                                <label><span class="color_red">*</span> <span class="color_blue">Caller Status : </span> </label>
                                <?php echo custom_dropdown('caller_status', '', $arr_all['ARR_CALLER_STATUS'], $result_data['caller_status'],'class="text-input small-input"');?>
                              </p>
							<?php
						} else {
							if ($this->session->userdata('admin_role_id') == 3) {
								// THIS CODE IS FOR SALES AGENT REMOVE 'ARR_COD_CALLER_STATUS' IF YOUR DONT WANT TWO STATUS
								?>
                                <p>
                                  <label><span class="color_red">*</span> <span class="color_blue">Caller Status : </span> </label>
                                  <?php echo custom_dropdown('caller_status', '', $arr_all['ARR_CALLER_STATUS'], $result_data['caller_status'],'class="text-input small-input"');?>
                                </p>
                                <?php
							} else {
								?>
                                <p>
                                  <label><span class="color_red">*</span> <span class="color_blue">Caller Status : </span> </label>
                                  <?php echo custom_dropdown('caller_status', '', $arr_all['ARR_CALLER_STATUS'], $result_data['caller_status'],'class="text-input small-input"');?>
                                </p>
                                <?php
							}
						}
						
						if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 5) {
							if($result_data['qa_status'] != 1 && $result_data['qa_status'] != 0) {
								?>
								<!--<p>
                                  <label><span class="color_red">*</span> <span class="color_blue">QA Status : </span> </label>
                                  <?php echo $arr_all['ARR_QA_STATUS'][$result_data['qa_status']];?>
                                </p>
                                <p>
                                  <label><span class="color_red">*</span> <span class="color_blue">QA User : </span> </label>
                                  <?php 
                                    if($result_data['qa_user'] > 0) {
                                        echo $this->db_function->get_single_value(ADMIN, 'email', "id=".$result_data['qa_user'], false, true); 
                                    } else {
                                        echo 'Not Set Yet';
                                    }
                                  ?>
                                </p>-->
								<?php
							} else {
								?>
								<!--<p>
                                  <label><span class="color_red">*</span> <span class="color_blue">QA Status : </span> </label>
                                  <?php echo custom_dropdown('qa_status', '', $arr_all['ARR_QA_STATUS'], $result_data['qa_status'],'class="text-input small-input"');?>
                                </p>
                                <p>
                                  <label><span class="color_red">*</span> <span class="color_blue">QA User : </span> </label>
                                  <?php 
                                    if($result_data['qa_user'] > 0) {
                                        echo $this->db_function->get_single_value(ADMIN, 'email', "id=".$result_data['qa_user'], false, true); 
                                    } else {
                                        echo 'Not Set Yet';
                                    }
                                  ?>
                                </p>-->
                                <?php
							}
						}
					?>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Provider : </span> </label>
                      <?php echo custom_dropdown('provider', '', $arr_all['ARR_PROVIDER'], $result_data['provider'],'class="text-input small-input"');?>
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Tracking Number : </span> </label>
                      <input class="text-input small-input" type="text" id="tracking_number" name="tracking_number" 
                      value="<?php _isset($result_data['tracking_number']); ?>" tabindex="1" /> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Delivery Status : </span> </label>
                      <?php echo custom_dropdown('delivery_status', '', $arr_all['ARR_DELIVERY_STATUS'], $result_data['delivery_status'],'class="text-input small-input"');?> 
                    </p>
                    <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Refund Status : </span> </label>
                      <?php echo custom_dropdown('refund_status', '', $arr_all['ARR_REFUND'], $result_data['refund_status'],'class="text-input small-input"');?> 
                    </p>
                    <p>
                        <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
                        <select name="status" id="status" class="small-input" tabindex="2">
                            <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selected"' : '';?>>Select Status</option>
                            <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selected"' : '';?>>Active</option>
                            <option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selected"' : '';?>>Inactive</option>
                        </select> 
                    </p>
                    
                    <p>
                        <label><span class="color_red">*</span> <span class="color_blue">Not Paid Reason</span></label>
                        <?php echo custom_dropdown('notpaid_reason', '', $arr_all['ARR_NOTPAID'], $result_data['notpaid_reason'],'class="text-input small-input"');?> 
                    </p>
                            
                    <?php
						if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_SEND_AFTER_ROLES)) {
							?>
							<!--<p>
                                <label><span class="color_red">*</span> <span class="color_blue">Send Product After</span> </label>
                                <select name="send_after" id="send_after" class="small-input">
                                    <option value="0">Select Option</option>
                                    <option value="0">Today</option>
                                    <?php
                                    	for ($oc = 1; $oc <= 15; $oc++) {
											echo '<option value="'.$oc.'">'.$oc.' days</option>';	
										}
									?>
                                    <option value="30">30 days</option>
                                    <option value="35">35 days</option>
                                    <option value="40">40 days</option>
                                    <option value="45">45 days</option>
                                    <option value="60">60 days</option>
                                    <option value="75">75 days</option>
                                    <option value="90">90 days</option>
                                </select> 
                            </p>-->
                            <?php			
						}
						
						if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
							array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
							?>
							<p>
                  <label><span class="color_red">*</span> <span class="color_blue">Is Fraud</span> </label>
                  <select name="is_fraud" id="is_fraud" class="small-input" tabindex="2">
                      <option value="1" <?php echo ($result_data['is_fraud'] == '1') ? 'selected="selected"' : '';?>>Yes</option>
                      <option value="0" <?php echo ($result_data['is_fraud'] == '0') ? 'selected="selected"' : '';?>>No</option>
                  </select> 
              </p>

              <p>
                
                  <label><span class="color_red">*</span> <span class="color_blue">Cod agents</span> </label>
                  <select name="cod_agents" id="cod_agents" class="small-input" tabindex="2">
                   <?php 
                   
                      $cod_agents_arr = array();

                      if($cod_agents->num_rows() > 0){
                        foreach ($cod_agents->result() as $row1){
                            
                             echo '<option value="'.$row1->id.'" '.($result_data['qa_user'] == $row1->id ? 'selected' : '').' >'.$row1->username.'</option>';
                        }
                     
                    }
                 ?>
                  </select> 
              </p>

              <?php			
						}
						
						if(strtolower($mode) == "edit"){
							?>
							<div class="information_div">
								<p class="info_bar">
									<label><span class="color_blue">Return Date : </span>
									<?php echo strtotime($result_data['return_date']) > 0 ? '<a href="javascript:void(0)"  class="color_black" 
									title="'.$result_data['return_date'].'">'.$result_data['return_date'].'</a>' : "Not Available"; ?></label>
								</p>
								<p class="info_bar">
									<label><span class="color_blue">Qualified Date : </span>
									<?php echo strtotime($result_data['qualified_date']) > 0 ? '<a href="javascript:void(0)"  class="color_black" 
									title="'.$result_data['qualified_date'].'">'.$result_data['qualified_date'].'</a>' : "Not Available"; ?></label>
								</p>
								<p class="info_bar">
									<label><span class="color_blue">Sent Date : </span>
									<?php echo strtotime($result_data['sent_date']) > 0 ? '<a href="javascript:void(0)"  class="color_black" 
									title="'.$result_data['sent_date'].'">'.$result_data['sent_date'].'</a>' : "Not Available"; ?></label>
								</p>
								<p class="info_bar">
									<label><span class="color_blue">Create Date : </span>
									<?php echo strtotime($result_data['dt_c']) > 0 ? '<a href="javascript:void(0)"  class="color_black" 
									title="'.$result_data['dt_c'].'">'.$result_data['dt_c'].'</a>' : "Not Available"; ?></label>
								</p>
								<div class="border_bottom"></div>
								<p class="info_bar">
								  <label><span class="color_blue">Update Date : </span>
								  <?php echo strtotime($result_data['dt_u']) > 0 ? '<a href="javascript:void(0)" class="color_black" 
								  title="'.$result_data['dt_u'].'">'.$result_data['dt_u'].'</a>' : "Not Updated"; ?></label>
								</p>
							</div>
							<?php
						}
						
						if ($this->session->userdata('admin_role_id') == 1) {
							?>
                            <br /><br />
                            <span id="order_logs"><a href="javascript:order_logs('<?php echo FULL_CMS_URL?>', '<?php echo $result_data[$primary_field]; ?>')">Show me Order Logs</a></span>
							<?php
						}
						
						if ($this->session->userdata('admin_role_id') == 2) {
							echo form_hidden('qa_user', $this->session->userdata('uid'));
						} else if ($this->session->userdata('admin_role_id') == 3) {
							echo form_hidden('cms_user', $this->session->userdata('uid'));	
						}
						
						echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'add')  ? 'add' : 'edit');
						echo form_hidden('manage_page', $manage_page);
						echo form_hidden('add_page', $add_page);
						echo form_hidden($primary_field, $result_data[$primary_field]);
						echo form_hidden('cur_url', get_full_url());
						// SET THE FULL URL USING SEGMENT ARRAY
					?>
                 </div>
              </div>
           </div>
           <div class="clear"></div>
        </div>
        <input type="hidden" name="hidmobile" id="hidmobile" value="<?php _isset($result_data['mobile']); ?>" />
		<?php
			if ($result_data['can_order_proceed'] == true) {
				?>
				<p>
				  <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
				  <input class="button" onclick="window.open('<?php echo FULL_CMS_URL."/orderproducts/index/".$result_data['id'].'/#atab';?>','_blank');" type="button" value="View Products" />
          <?php if($result_data['tracking_number'] != ""){ 

              if($result_data['provider'] == 1) {
              ?>

              <input class="button" onclick="window.open('http://www.xpressbees.com/track-shipment.aspx?tracking_id=<?php echo $result_data['tracking_number'];?>','_blank');" type="button" value="Track Order" />
              <?php }else if($result_data['provider'] == 2){

              ?>  
                <input class="button" onclick="window.open('http://api.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=BLR02248&lickey=df452935c5abba8756f0261c73b6f0fd&verno=1.3&scan=1&awb=awb&format=html&numbers=<?php echo $result_data['tracking_number'];?>','_blank');" type="button" value="Track Order" />

               <?php 
               }else if($result_data['provider'] == 6){

              ?>  
                <input class="button" onclick="window.open('https://shiprocket.co/tracking/<?php echo $result_data['tracking_number'];?>','_blank');" type="button" value="Track Order" />

               <?php 
               }
               ?>


				      <a class="button" href="javascript:void(0)" id="diet_id" onclick="send_diet_chart('<?php echo $result_data['id'] ?>', '<?php echo FULL_CMS_URL ?>/orderdetails/dietitian_mail', 'diet_id');" >Dietitian Mail</a>
            
          <?php } ?>
            
           
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
				</p>
				<?php
			} else {
				echo '<span class="color_r0ed">Can\'t proceed because Customer has returned more than 1 orders</span>';
			}
		?>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
