<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo ADMIN_TITLE; ?></title>
<!--CSS-->
<!-- Reset Stylesheet -->
<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/reset.css" type="text/css" media="screen" />
<!-- Main Stylesheet -->
<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/style.css" type="text/css" media="screen" />
<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/blue.css" type="text/css" media="screen" />
 
<!-- Internet Explorer Fixes Stylesheet -->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/ie.css" type="text/css" media="screen" />
<![endif]-->

<!--Javascripts-->
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo S3_CMS_URL;?>scripts/jquery.configuration.js"></script>
<script type="text/javascript" src="<?php echo S3_CMS_URL;?>scripts/common.js"></script>
<!-- Internet Explorer .png-fix -->
<!--[if IE 6]>
    <script type="text/javascript" src="<?php echo S3_CMS_URL;?>scripts/DD_belatedPNG_0.0.7a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix('.png_bg, img, li');
    </script>
<![endif]-->
<link rel="shortcut icon" href="<?php echo S3_URL?>/images/favicon.ico" type="image/png"/>