<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        	<div style="margin:20px 0px 0px 0px">
                <div class="align-left"><label>Currently database has <?php echo $cache_size?> of cache.</label></div>
                <div class="align-right">
                    <input class="button" type="submit" name="submit" id="submit" value="Clear Cache" onclick="$(this).val('Energizing Server...');" /></label>
                </div>
                <div class="clear"></div>
            </div>
            <p>
            	<table>
                    <thead>
                        <tr>
                            <th>Cache Name</th>
                            <th style="text-align:right">Size</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					if($dir_size != false && count($dir_size) > 0) {
                        foreach ($dir_size as $k => $v) {
                        	$bg = (strtolower(substr($v[0],0,3)) == CMS_FOLDER_NAME) ? '#FFF2F2' : '#E4FDE4';
                        ?>
                        <tr>
                            <td style="background:<?php echo $bg;?>"><?php echo $v[0];?></td>
                            <td style="background:<?php echo $bg;?>;text-align:right"><?php echo byteFormat($v[1]);?></td>
                            <td style="background:<?php echo $bg;?>;text-align:center"><a href="<?php echo FULL_CMS_URL.'/'.$cur_controller?>/del_specific_cache/<?php echo str_replace('+','--',$v[0]);?>">Delete</a></td>
                        </tr>
                        <?php
                        }
					} else {
						?>
                        <tr>
                            <td colspan="3" style="text-align:center">Cache is not available, Website is getting 100% fresh data from database.</td>
                        </tr>
                        <?php
					}
                    ?>
                    </tbody>
          	    </table>
            </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
