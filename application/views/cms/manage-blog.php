<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
      if( $this->session->flashdata('error') ) { 
        echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
      }else if( $this->session->flashdata('success') ) { 
        echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
      }
    ?>
       <div class="notification information png_bg">
          <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
       </div>
      <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="blogid" <?php echo ($search_field == "blogid") ? "selected='selected'" : ""; ?>>Blog Id</option>
                        <option value="title" <?php echo ($search_field == "title") ? "selected='selected'" : ""; ?>>Blog Title</option>
                        
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
            <th >Sr No </th>
            <th >Title</th>
            <th >URL</th>
            <th >Create date</th>
            <th >Status</th>
            <th >Action</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
                   foreach ($results as $key => $p) {
                        

                      echo "<tr>";
                      echo "<td>".$i. "</td>";
                      echo "<td>".$p->title."</td>";
                      echo "<td>".$p->url."</td>";
                      echo "<td>".$p->date_created."</td>";
                      echo "<td>".($p->status == 1 ? 'Active' : 'Inactive')."</td>";
                      ?>
                      <td>
                          <a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$p->blogid?>/#atab" title="Edit "><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a>
                      </td>
                      <?php
                      echo "</tr>";

                      $i++;
                  }
                } else {
                    ?>
                    <tr>
                        <td colspan="9" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="9" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>