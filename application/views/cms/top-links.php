<?php
	if(count($menu) > 0) {
		$main_menu_permitted_top = NULL;
		
		echo '<ul class="shortcut-buttons-set">';
		foreach ($menu as $k => $v) {
			
			// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING
			$cur_controller = strtolower($cur_controller);
			// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING
			
			// CREATE PERMISSION ARRAY FOR EACH AND EVERY CONTROLLER
			$arr_permitted_controller_top = create_permission_array($this->session->userdata('admin_role_details'));

			// CHECK FOR PERMISSION FOR PARENT MENU 
			if( is_array ($arr_permitted_controller_top) && count($arr_permitted_controller_top) > 0){
				if( is_array ($v[2]) && count($v[2]) > 0){
					for ($j = 0; $j < count($v[2]); $j++) {
						if (in_array ($v[2][$j][1], $arr_permitted_controller_top) || in_array ($v[2][$j][2], $arr_permitted_controller_top)) {
							$main_menu_permitted_top = true;
						}
					}
				}		
			}
			
			if($main_menu_permitted_top == true) {
				echo '<li>';
				echo '<a class="shortcut-button '.$v[4].'" href="'.FULL_CMS_URL."/".$v[1].'">';
				echo '<span class="png_bg"> '.$v[0].' </span>';
				echo '</a>';
				echo '</li>';
			} 
			$main_menu_permitted_top = NULL;
		}
		echo '</ul>';
	}
?>