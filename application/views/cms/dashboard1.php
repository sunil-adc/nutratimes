<?php 	
	//PRODUCTS STATISTICS START
	if($this->session->userdata('admin_role_id') == 1 || $this->session->userdata('admin_role_id') == 7) {
    ?> 
    <!--<div class="align-left chart-area">
        <div class="content-box" onclick="$('#contentainer_2').slideToggle();">
          <div class="content-box-header">
            <h3>Statistics for Orders</h3>
          </div>
          <div class="content-box-content" id="contentainer_2">
             <div id="order_chart" style="text-align:center">
                <img src="<?php echo CMS_URL?>images/preloader.gif" />
                &nbsp;&nbsp;&nbsp;Preparing the chart...
             </div>
          </div>  
        </div>
    </div>-->
    <div class="stats-area">
    <form method="post" action="javascript:void(0)" onsubmit="load_chart_new('<?php echo FULL_CMS_URL;?>','<?php echo SITE_URL.INTER_USE_DIR;?>', 'product', 'order_chart', '2')">
        <strong><span style="padding: 0px 15px 0px 0px;font-size: 15px;font-weight: 700;"> Date Range : </span> </strong>
        <input type="text" name="reportrange" id="reportrange" class="text-input" style="width: 35%" autocomplete="off">&nbsp;&nbsp;&nbsp;
        
        <input type="submit" class="button" id="btn_search" name="btn_search" value="Search">
    </form>            
    </div>
      <br>

    <div class="align-left stats-area">
      <div class="detailed-statistics-full grey">
           <div class="detailed-statistics-header blue">TOTAL REVENUE </div>
           <div class="detailed-statistics-body" id="total_revenue"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>

        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header blue">Total Leads</div>
           <div class="detailed-statistics-body" id="total_leads"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>


        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header yellow">Today COD Qualify</div>
           <div class="detailed-statistics-body" id="cod_qualify"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>

        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header blue">Total Prepaid Send</div>
           <div class="detailed-statistics-body" id="prepaid_send"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>


        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header yellow">Total COD Send</div>
           <div class="detailed-statistics-body" id="cod_send"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>

    </div>    
    
    <div class="align-right stats-area">

        <div class="detailed-statistics-full grey">
           <div class="detailed-statistics-header blue">TOTAL Conversion </div>
           <div class="detailed-statistics-body" id="total_conversion"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
    	  

        <div class="clear"></div>
        
        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header blue">Total Prepaid</div>
           <div class="detailed-statistics-body" id="total_prepaid"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>
        
         <div class="detailed-statistics grey align-right">
           <div class="detailed-statistics-header yellow">Total COD Delivered</div>
           <div class="detailed-statistics-body" id="total_cod"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        
        
        <div class="clear"></div>
    </div>
    
    <!--<div class="align-right stats-area">
        <table>
        <thead>
          <tr>
            <th >Network</th>
            <th >Total</th>
            <th >Conversion</th>
            <th >Con %</th>
          </tr>
        </thead>
        <tbody>
           <td>1</td>
           <td>2</td>
           <td>3</td>
           <td>4</td> 
        </tbody>
      </table>
    </div>-->
    
    </div>
    <div class="clear"></div>
    <script src="<?php echo S3_CMS_URL;?>scripts/highcharts.js"></script>
	<script src="<?php echo S3_CMS_URL;?>scripts/exporting.js"></script>


  <script type="text/javascript" src="<?php echo SITE_URL?>cms/scripts/moment.min.js"></script> 
  <script type="text/javascript" src="<?php echo SITE_URL?>cms/scripts/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  
	<script type="text/javascript">load_chart_new('<?php echo FULL_CMS_URL;?>','<?php echo SITE_URL.INTER_USE_DIR;?>', 'product', 'order_chart', '1');</script>
	<?php
	}
	//ORDER STATISTICS END
?>
<div class="clear"></div>
