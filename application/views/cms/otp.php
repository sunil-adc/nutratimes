</head>
<?php 
	$browser = getBrowser();
  	if (trim($browser) == 'Internet Explorer') { 
		?><br>
        <div class="blue_bg abosolute_msg highlight">
            <div style="font-weight:500;padding:10px 0px 10px 00px;text-align:center;font-family:Verdana, Geneva, sans-serif">Warning : Internet Explorer will not support some of the functionality so if you fill irritation then switch to Chrome, Firefox, Safari or Opera</div>
        </div>
		<?php
	}
?>
<body style="background:none">
<div id="login-wrapper" style="background:none" class="png_bg">
  <div id="login-top">
    <h1><?php echo strtoupper(SITE_NAME)?> Admin</h1>
    <!-- Logo (221px width) -->
    <!--<img id="logo" src="<?php echo CMS_URL;?>images/logo.png" alt="<?php echo ADMIN_TITLE; ?>" />--></div>
  <!-- End #logn-top -->
  <div id="login-content">
    <h2 style="text-align:center;color:#666 !important"></h2><br>
     <form action="<?php echo CMS_URL.'cmslogin/otp'; ?>" id="otp_form" name="otp_form" method="post" accept-charset="utf-8">
	  <?php
	  	if(validation_errors()){
		  ?>
          <script type="text/javascript">$("#otp_form").addClass("shake");</script><br />
		  <div class="notification error png_bg">
			 <div><?php echo validation_errors(); ?></div>
		  </div>
          <?php
		} else if( $this->session->flashdata('error') ) {
			?>
            <script type="text/javascript">$("#otp_form").addClass("shake");</script><br />
		    <div class="notification error png_bg">
			  <div><?php echo $this->session->flashdata('error'); ?></div>
		    </div>
          <?php
		} else if( $this->session->flashdata('success') ) { 
			?>
            <script type="text/javascript">$("#otp_form").addClass("shake");</script><br />
		    <div class="notification success png_bg">
			  <div><?php echo $this->session->flashdata('success'); ?></div>
		    </div>
          <?php
	    }
	  ?>
      <div class="login-container-inner">
         <input type="text" name="otp" id="otp" title="One time password" class="text-input-login" autocomplete="off" placeholder="One time password"
         <?php if (trim($browser) == 'Internet Explorer') { ?> value="One time password" onblur="if(this.value == '') { this.value='One time password'}" onfocus="if (this.value == 'One time password') {this.value=''}" <?php } else { ?> placeholder="One time password" <?php } ?> style="height:55px" />
         
        <div class="clear"></div>
        <input type="submit" name="submit" id="submit" class="border-top custom-submit" value="VALIDATE OTP" />
        <input type="button" name="otp" class="border-top custom-submit" value="RESEND OTP" onclick="window.location.href='<?php echo SITE_URL?>cms/cmslogin/otp/?status=resend'" />
        </div>
        
    </form>
  </div>
  <!-- End #login-content -->
</div>
<!-- End #login-wrapper -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#otp_form").submit(function(event) {

			if (!$('#otp').val()) {
				if (!$("#otp_form").hasClass("shake")) {
						$("#otp_form").addClass("shake");
				} else {
					$('#otp_form').css('animation-name', 'none');
					$('#otp_form').css('-moz-animation-name', 'none');
					$('#otp_form').css('-webkit-animation-name', 'none');

					setTimeout(function() {
						$('#otp_form').css('-webkit-animation-name', 'shake');
					}, 0);
				}

				return false;
			}
		});
	});
</script>
<style>

@keyframes shake{ 
    0% { transform: translate(25px, 0); }
    50% { transform: translate(-25px, 0); }
    100% { transform: translate(0, 0); }
}
 
@-moz-keyframes shake{ 
    0% { -moz-transform: translate(25px, 0); }
    50% { -moz-transform: translate(-25px, 0); }
    100% { -moz-transform: translate(0, 0); }
}
 
@-webkit-keyframes shake {
    0% { -webkit-transform: translate(25px, 0); }
    50% { -webkit-transform: translate(-25px, 0); }
    100% { -webkit-transform: translate(0, 0); }
}
 
.shake {
	-webkit-transition: all 300ms linear;
-moz-transition: all 300ms linear;
-o-transition: all 300ms linear;
-ms-transition: all 300ms linear;
transition: all 300ms linear;

    animation-name: shake; 
    animation-duration: 150ms; 
    animation-iteration-count: 3; 
    animation-timing-function: linear;
 
    -moz-animation-name: shake; 
    -moz-animation-duration: 150ms; 
    -moz-animation-iteration-count: 3; 
    -moz-animation-timing-function: linear;
 
    -webkit-animation-name: shake;
    -webkit-animation-duration: 150ms;
    -webkit-animation-iteration-count: 3;
    -webkit-animation-timing-function: linear;
}
</style>
</body>
</html>
