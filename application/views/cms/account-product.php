<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
      <div class="notification information png_bg">
        <div>Total <?php echo $total_rows ;?> records found in <?php echo $page_name;?></div>
      </div>
      <div class="notification error png_bg">
        <div><a href="<?php echo $seach_form?>?pending=1">Total <?php echo $total_blank_rows;?> products are pending for rename.</a></div>
      </div>
      <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
        <table>
          <tr>
            <td>
                <strong>Search Keyword :</strong>
                <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>Search Field :</strong> 
                <select name="search_field" id="search_field">
                    <option value="name" <?php echo ($search_field == "name") ? "selected='selected'" : ""; ?>>Product Name</option>
                	<option value="name_acc" <?php echo ($search_field == "name_acc") ? "selected='selected'" : ""; ?>>Account Product Name</option>
                    <option value="prod_id" <?php echo ($search_field == "prod_id") ? "selected='selected'" : ""; ?>>Product Id</option>
                    <option value="mrp" <?php echo ($search_field == "mrp") ? "selected='selected'" : ""; ?>>MRP</option>
                </select>
                &nbsp;&nbsp;  
                <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
            </td>
          </tr>
        </table>
      </form>
      <table>
        <thead>
          <tr>
            <th width="4%">Product Id</th>
            <th width="36%">Name</th>
            <th width="36%">Name for Accountant</th>
            <th width="5%">MRP</th>
            <th width="5%">Status</th>
            <th width="14%">Updated By Accountant</th>
          </tr>
        </thead>
        <tbody>
          <?php	
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                          <tr>
                            <td><?php echo $val->prod_id; ?></td>
                            <td><?php echo $val->name;?></td>
                            <td id="acc_prod_id_<?php echo $val->prod_id;?>">
								<a href="javascript:display_ajax('<?php echo FULL_CMS_URL;?>','acc_prod_id_<?php echo $val->prod_id;?>','<?php echo $val->prod_id;?>','label','')"><?php echo ($val->name_acc == "") ? 'Not Specified' : $val->name_acc;?></a>	
								
								<?php if($val->name_acc == "") { ?> | <a href="javascript:display_ajax('<?php echo FULL_CMS_URL;?>','acc_prod_id_<?php echo $val->prod_id;?>','<?php echo $val->prod_id;?>','copy','')">Copy</a> <?php } ?>
                                </td>
                            <td><?php echo $val->mrp;?></td>
                            <td><?php echo ($val->status == 1) ? 'Active' : 'Inactive';?></td>
                            <td><?php echo (strtotime($val->acc_dateupdated) > 0) ? time_diff ($val->acc_dateupdated) : "Not Updated";?></td>
                          </tr>
                          <?php
                    $i++;
					}
                } else {
                    ?>
          <tr>
            <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
          </tr>
          <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important"><?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?></td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<div class="clear"></div>