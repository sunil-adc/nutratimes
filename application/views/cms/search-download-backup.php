<?php
if($is_address == true) {
	
	$download_invoice = '';
	
	header("Content-Type:  application/vnd.ms-word");
	header("Content-Disposition: attachment; filename=$filename.doc");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	
	
	// USE THE REPLICA DATABASE FOR SEARCH ORDERS
	$this->replica_db = $this->load->database('replicadb', TRUE);
		
	$setting = $this->general->get_settings();
	
	$sql_order_id = "select o.id as order_id, o.payment_mode, pincode  FROM ".stripslashes($table_fields)." WHERE ".stripslashes($where_fields)." and o.caller_status in (2,3)";

	
	$this->replica_db->cache_off();
	$res_order_id = $this->replica_db->query($sql_order_id);
	if($res_order_id->num_rows() > 0) {
		$all_payment_mode = array();
		$all_order_id = $all_pincode = "";
		foreach($res_order_id->result() as $val) {
			$all_order_id .= $val->order_id.",";
			$all_payment_mode[$val->order_id] = $val->payment_mode;
			$all_pincode .= "'".$val->pincode."',";
		} 
		$all_order_id = trim($all_order_id, ",");
		$all_pincode = trim($all_pincode, ',');
	}
	echo $all_order_id;
	if ($all_order_id != "") {
		$this->replica_db->cache_off();
		$order_products = "
					 SELECT 
						p.name, 		up.product, 
						up.cash_price, 	up.size, 
						up.cod_price, 	up.shipping,
						g.genre_name,   up.order_id,
						up.quantity 
					 FROM 
						".USER_PRODUCTS." up inner join ".PRODUCT." p on (up.product = p.prod_id)
						left join ".GENRE." g on (up.size = g.genre_id) 
					 WHERE 
						up.order_id in (".$all_order_id.") and 
						up.status = '1' and 
						(up.contest != '1' )";
						
		
		$res_order_products = $this->replica_db->query($order_products);
		if(!$res_order_products) {
				echo '<br>Order Products : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
		} else {
			if($res_order_products->num_rows() > 0) {
				$arr_all_order_products = "";
				foreach($res_order_products->result() as $val_products) {
					$arr_all_order_products[$val_products->order_id][] = $val_products;
				}
			}
		}
	
	
	$this->replica_db->cache_off();
	
	$res = $this->replica_db->query($full_sql." and o.caller_status in (2,3) group by o.id");
	if($res->num_rows() > 0) {
		$i = 1;
		foreach ($res->result() as $val) {

			$order_products = $total_order_amunt = $invoice_product = $invoice_product_cash = $arr_order_shipping = "";
			$order_shipping = 0;
			
			if (isset($arr_all_order_products[$val->id]) && is_array($arr_all_order_products[$val->id]) && 
				count($arr_all_order_products[$val->id]) > 0) {
				foreach ($arr_all_order_products[$val->id] as $key_product => $val_products) {
					
					//$prod_size = (isset($val_products->genre_name) && trim($val_products->genre_name) != "") ? $val_products->genre_name : 'N/A';
					$order_products .= $val_products->name." Qty : ".$val_products->quantity."), ";
					$val_product_price = $all_payment_mode[$val->id] == '2' ? $val_products->cod_price : $val_products->cash_price;
					$total_order_amunt += ($val_product_price * $val_products->quantity);
					
					$arr_order_shipping[] = $val_products->shipping;
					$base_price = (($val_product_price * $val_products->quantity) /112)*100;
					$igst =  	  ($val_product_price * $val_products->quantity) - $base_price ;
					$cgst       = $igst/2;
					$sgst       = $cgst;
					


					$invoice_product .= '<tr>
										  <td align="left" width="50%" style="border-bottom:1px solid #CCCCCC">'.$val_products->name.'</td>
										  <td align="center" width="10%" style="border-bottom:1px solid #CCCCCC">'.$val_products->quantity.'</td>
										  <td align="center" width="10%" style="border-bottom:1px solid #CCCCCC">'.$base_price.'</td>';
										 
					if($val->state == 2){
          			
          			$invoice_product .=   '<td align="center" width="10%" style="border-bottom:1px solid #CCCCCC">'.$cgst.'</th>
          								   <th align="center" width="10%" style="border-bottom:1px solid #CCCCCC">'.$sgst.'</th>';

		          	}else{
		          		$invoice_product .=  '<th align="center" width="10%" style="border-bottom:1px solid #CCCCCC">'.$igst.'</th>';
			      	}
					
					$invoice_product  .=	  '<td align="right" width="10%" style="border-bottom:1px solid #CCCCCC">
										  	'.($val_product_price * $val_products->quantity).'/-</td></tr>';	
					
					if ($all_payment_mode[$val->id] == 2) {
						$invoice_product_cash .= '<tr>
										  <td align="left" width="60%" style="border-bottom:1px solid #CCCCCC">'.$val_products->name.'</td>
										  <td align="center" width="15%" style="border-bottom:1px solid #CCCCCC">'.$val_products->quantity.'</td>
										  <td align="center" width="10%" style="border-bottom:1px solid #CCCCCC">Rs. '.$val_product_price.'/-</td>
										  <td align="right" width="15%" style="border-bottom:1px solid #CCCCCC">
										  	Rs. '.($val_product_price * $val_products->quantity).'/-</td></tr>';
					} else {
						$invoice_product_cash .= '<tr>
							  <td align="left" width="60%" style="border-bottom:1px solid #CCCCCC">'.$val_products->name.' </td>
							  <td align="center" width="15%" style="border-bottom:1px solid #CCCCCC">'.$val_products->quantity.'</td>
							  <td align="center" width="10%" style="border-bottom:1px solid #CCCCCC">Rs. 0/-</td>
							  <td align="right" width="15%" style="border-bottom:1px solid #CCCCCC">Rs. 0/-</td></tr>';	
					}
				}
				$order_shipping = max($arr_order_shipping);
			}
			
			$name = isset($val->name) ? $val->name : '';
			$mobile = isset($val->mobile) ? $val->mobile : '';
			$address = isset($val->address) ? $val->address : '';
			$city = isset($val->city) ? $val->city : '';
			$area = isset($val->area) ? $val->area : '';
			$pincode = isset($val->pincode) ? $val->pincode : '';
			$id = isset($val->id) ? $val->id : '';
			$order_products = isset($order_products) ? trim($order_products,", ") : '';
				
			$final_total_amount = ($all_payment_mode[$val->id] == '2') ? '<tr><td valign="top" colspan="2" style="width:200px"><strong>Payable Amount : </strong>'.$total_order_amunt.'</td></tr>' : '';
			
			
	
echo '<table align="center" cellspacing="0" cellpadding="" style="width:600px;border-bottom:2px dashed black">
  <tr>
    <td style="border-bottom:2px solid #666" colspan="2"><img src="'.S3_URL.'cdn/image/catalog/sticker-logo.png" /></td>
  </tr>
  <tr valign="top">
    <td width="50%" style="margin:0px 0px 20px 0px">
		<h3>Ship to '.$name.'</h3>
      	'.$address.$area.'<br />'.$city.' '.$pincode.'<br />Phone : '.$mobile.'
	</td>
	<td width="50%" style="margin:0px 0px 20px 0px"  align="center">
		<h3>'.SITE_NAME.'</h3>
      	'.SITE_ADDR_1.' <br> GSTIN - '.GST.'

	</td>
    
  </tr>
  <tr valign="top">
    	<td >
			<div style="font-size:20px;padding:5px 0px 5px 0px;font-weight:bold;">Tracking No: '.$val->tracking_number.'</div>
			<img src="'.SITE_URL.'barcode.php?awb='.$val->tracking_number.'"> <br />
		</td>
		<td align="center">
			<div style="font-size:20px;padding:5px 0px 5px 0px;font-weight:bold;">Invoice No: '.$id.'</div>
			<div style="font-size:20px;padding:5px 0px 5px 0px;font-weight:bold;">Order No: '.$id.'</div>
			<img src="'.SITE_URL.'barcode.php?awb='.$id.'"> <br />
		</td>
		
  </tr>
  <tr>
    <td colspan="2" align="center" > Note : This shipment contain following items </td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" align="center" cellpadding="5" cellspacing="0">
        <tr style="border:2px solid #666">
          <th align="left" style="border-bottom:2px solid black">Product Details</th>
          <th align="center" style="border-bottom:2px solid black">Quantity</th>
          <th align="center" style="border-bottom:2px solid black">Base Price</th>';
          if($val->state == 2){
          		echo '<th align="center" style="border-bottom:2px solid black">CGST (6%)</th>';
          		echo '<th align="center" style="border-bottom:2px solid black">SGST (6%)</th>';
          }else{
          		echo  '<th align="center" style="border-bottom:2px solid black">IGST (12%)</th>';
      	  }
          		echo '<th align="right" style="border-bottom:2px solid black">Subtotal</th>
        </tr>
		'.$invoice_product.'
        <tr>
		<td></td>
          <td></td>	
          <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Grand Total</td>
          <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Rs. '.$total_order_amunt.'/-</td>
        </tr>';
        
		if ($all_payment_mode[$val->id] != '2') {
			echo '<tr>
			<td></td>
			<td></td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Shipping</td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Rs. '.$order_shipping.'/-</td>
			</tr>';
		}
		
		if ($all_payment_mode[$val->id] == '2') {
			echo '<tr>
			<td></td>
			<td></td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Payable Amount</td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Rs. '.$total_order_amunt.'/-</td>
			</tr>';	
		} else {
			echo '<tr>
			 <td></td>
			 <td></td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Paid Amount</td>
			  <td colspan="2" align="right" style="font-size:20px;font-weight:bold;border-bottom:1px solid #CCCCCC">Rs. '.($total_order_amunt + $order_shipping).'/-</td>
			</tr>';
		}
		
      echo '</table></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><table>
        <tr>
          <td width="40%" style="font-size:20px;font-weight:bold;" align="center">Call before Delivery</td>
          <td width="60%" align="center"> If you need any assistance or have any questions, Email us at support@nutratimes.com or 
            Please feel free to call to contact us at <span style="font-size:20px;font-weight:bold">'.$setting['phone_no'].'</span></td>
        </tr>
      </table></td>
  </tr>
</table>';


			$i++;
		}
	}
}
	
} else {
	header("Content-Type:  application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		
	if (is_ip_download_allowed()) {
		echo $data_download;
	} else {
		echo 'Download is not permitted from unknown IP Adress';
	}
}
?>