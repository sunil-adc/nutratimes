<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
      <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
      <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
          <div class="align-left prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Banner & Links</div>
              <div class="prod_desc">
                 <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Home Page top text note : </span></label>
                      <textarea rows="3" class="text-input small-input" id="top_text_note" name="top_text_note"><?php echo $result_data['top_text_note'];?></textarea>
                 </p>
                 <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Top Wide Banner : </span></label>
                  <input type="file" class="text-input small-input" id="banner_top_wide" name="banner_top_wide" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['banner_top_wide']);?>'  width="400" />
                  </p>
                  <p>
                      <label><span class="color_red">*</span> <span class="color_blue">Link For Top Wide Banner : </span></label>
                      <textarea type="text" rows="3" class="text-input small-input" id="banner_top_wide_link" name="banner_top_wide_link"><?php echo urldecode($result_data['banner_top_wide_link']);?></textarea>
                 </p>
                 <p>
                  <label><span class="color_red">*</span> <span class="color_blue">First Small Banner : </span></label>
                  <input type="file" class="text-input small-input" id="top_small_banner" name="top_small_banner" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['top_small_banner']);?>' width="190" />
                </p>
                <p>
                	<label><span class="color_red">*</span> <span class="color_blue">Link for First Small Banner : </span></label>
                  	<textarea type="text" rows="3" class="text-input small-input" id="top_small_banner_link" name="top_small_banner_link"><?php echo urldecode($result_data['top_small_banner_link']);?></textarea>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Second Small Banner : </span></label>
                  <input type="file" class="text-input small-input" id="down_first" name="down_first" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['down_first']);?>' width="169"  />
                </p>
                <p>
                	<label><span class="color_red">*</span> <span class="color_blue">Second small banner : </span></label>
                  	<textarea type="text" rows="3" class="text-input small-input" id="down_first_link" name="down_first_link"><?php echo urldecode($result_data['down_first_link']);?></textarea>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Search Page Top Banner : </span></label>
                  <input type="file" class="text-input small-input" id="search_top_banner" name="search_top_banner" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['search_top_banner']);?>' height="150" />
                </p>
                <p>
                	<label><span class="color_red">*</span> <span class="color_blue">Search Page Top Banner Link : </span></label>
                    <textarea type="text" rows="3" class="text-input small-input" id="search_top_banner_link" name="search_top_banner_link"><?php echo urldecode($result_data['search_top_banner_link']);?></textarea></p>
                    
                 <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Sidebar Banner 1: </span></label>
                  <input type="file" class="text-input small-input" id="homepage_banner_1" name="homepage_banner_1" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['homepage_banner_1']);?>' height="150" />
                </p>
                <p>
                	<label><span class="color_red">*</span> <span class="color_blue">Sidebar Banner 1 Link : </span></label>
                    <textarea type="text" rows="3" class="text-input small-input" id="homepage_banner_1_link" name="homepage_banner_1_link"><?php echo urldecode($result_data['homepage_banner_1_link']);?></textarea></p>
                 <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Sidebar Banner 2 : </span></label>
                  <input type="file" class="text-input small-input" id="homepage_banner_2" name="homepage_banner_2" />
                  <img src='<?php echo S3_URL.product_or_placeholder(CONTENT_DIR.'/images/'.$result_data['homepage_banner_2']);?>' height="150" />
                </p>
                <p>
                	<label><span class="color_red">*</span> <span class="color_blue">Sidebar Banner 2 Link : </span></label>
                    <textarea type="text" rows="3" class="text-input small-input" id="homepage_banner_2_link" name="homepage_banner_2_link"><?php echo urldecode($result_data['homepage_banner_2_link']);?></textarea></p>
              </div>
            </div>
          </div>
          <div class="align-left prod_leftside">
                <div class="prod_box">
                    <div class="prod_title">Homepage Top links</div>
                    <div class="prod_desc">
                      <p>
                          <label><span class="color_red">*</span> <span class="color_blue">First tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_1" name="header_tab_title_1" value="<?php echo urldecode($result_data['header_tab_title_1']);?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">First tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_1" name="header_tab_link_1"><?php echo urldecode($result_data['header_tab_link_1']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Second tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_2" name="header_tab_title_2" value="<?php echo $result_data['header_tab_title_2'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Second tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_2" name="header_tab_link_2"><?php echo urldecode($result_data['header_tab_link_2']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Third tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_3" name="header_tab_title_3" value="<?php echo $result_data['header_tab_title_3'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Third tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_3" name="header_tab_link_3"><?php echo urldecode($result_data['header_tab_link_3']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Forth tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_4" name="header_tab_title_4" value="<?php echo $result_data['header_tab_title_4'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Forth tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_4" name="header_tab_link_4"><?php echo urldecode($result_data['header_tab_link_4']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Fifth tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_5" name="header_tab_title_5" value="<?php echo $result_data['header_tab_title_5'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Fifth tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_5" name="header_tab_link_5"><?php echo urldecode($result_data['header_tab_link_5']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Sixth tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_6" name="header_tab_title_6" value="<?php echo $result_data['header_tab_title_6'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Sixth tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_6" name="header_tab_link_6"><?php echo urldecode($result_data['header_tab_link_6']);?></textarea>
                     </p>
                     
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Seventh tab : </span></label>
                          <input type="text" rows="3" class="text-input small-input" id="header_tab_title_7" name="header_tab_title_7" value="<?php echo $result_data['header_tab_title_7'];?>"  />
                     </p>
                     <p>
                          <label><span class="color_red">*</span> <span class="color_blue">Seventh tab Link : </span></label>
                          <textarea rows="3" class="text-input small-input" id="header_tab_link_7" name="header_tab_link_7"><?php echo urldecode($result_data['header_tab_link_7']);?></textarea>
                     </p>
                    </div>
                </div>
          </div>
          <div class="clear"></div>
        </div>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  </div>
</div>
<div class="clear"></div>
