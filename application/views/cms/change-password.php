<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
        <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
		<fieldset>
        <p>
          <label>Current Password : </label>
          <?php
				$data = array(
						  'name'        		=> 'password',
						  'id'          		=> 'password',
						  'autocomplete'       	=> 'off',
						  'class'       		=> 'text-input small-input',
						);

				echo form_password($data);
		  ?>
        </p>
        <p>
          <label>New Password : </label>
          <?php
				$data = array(
						  'name'        		=> 'new_password',
						  'id'          		=> 'new_password',
						  'autocomplete'       	=> 'off',
						  'class'       		=> 'text-input small-input',
						);

				echo form_password($data);
		  ?>
        </p>
        <p>
          <label>Confirm Password : </label>
          <?php
				$data = array(
						  'name'        		=> 'confirm_password',
						  'id'          		=> 'confirm_password',
						  'autocomplete'       	=> 'off',
						  'class'       		=> 'text-input small-input',
						);

				echo form_password($data);
		  ?>
        </p>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  </div>
</div>
<div class="clear"></div>
