<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
            <label><span class="color_blue">Name : </span>&nbsp;&nbsp;<?php echo $result_data['name'];?></label> 
        </p>
        <p>
            <label><span class="color_blue">Email : </span>&nbsp;&nbsp;<?php echo $result_data['email'];?></label>
        </p>
        <p>
            <label><span class="color_blue">Mobile : </span>&nbsp;&nbsp;<?php echo $result_data['mobile'];?></label>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Credits Points (Currently <?php echo $result_data['points'];?> credit points are in user account) add here additional points not total points: </span></label>
			<input type="text" class="text-input small-input" name="points" id="points" value="" /> 
        </p>
       	<?php
            if ($this->session->userdata('admin_role_id') == 1) {
				?>
                <div id="credit_logs"><a href="javascript:credit_logs('<?php echo FULL_CMS_URL?>', '<?php echo $result_data['user_id']; ?>')">Show me Credit Points Logs</a></div>
                <br /><br />
				<?php
            }
		
            echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Update Credits" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
