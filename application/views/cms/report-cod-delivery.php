<?php $arr_all = all_arrays(); ?>
<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	   ?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d", strtotime("-1 day"));?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important"  />&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> ManPower :</strong>
                    <input type="text" name="manpower" value="<?php echo (isset($manpower)) ? $manpower : '';?>" 
                    class="text-input small-input" autocomplete="off" style="width:10% !important"  />&nbsp;&nbsp;
                    
                    
                    <strong><span class="color_red">*</span> Provider :</strong>
                    <?php echo $provider; ?>
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
           <tr>
		    <th>Total Sent</th>
		    <th>Total Delivered</th>
            <th>Total Return Received</th>
            <th>Total Pending</th>
			<th>Total Cost</th>
			<th>Total Revenue</th>
			<th>Total Margin</th>
			<th>Delivery Percentage</th>
		  </tr>
        </thead>
        <tbody>
            <?php
               if (isset($results)) {
				   ?>
                    <tr valign="middle">
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_sent;?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_delivered; ?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_recieved;?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_pending;?></td>
                   	    <td style="display:table-cell;vertical-align:middle"><?php echo $total_cost;?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_revenue;?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo $total_margin;?></td>
                        <td style="display:table-cell;vertical-align:middle"><?php echo round($delivery_percentage, 1);?></td>
                   </tr>
                   <?php
			   } else {
					?>
					<tr>
                    	<td colspan="8" style="text-align:center">Start generating <?php echo $page_name;?></td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>