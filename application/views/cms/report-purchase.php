<script type="text/javascript">
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	    
			if (isset($total_rows)) {
				?>
			   <div class="notification information png_bg">
				  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
			   </div>
			   <?php
			}
		?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d", strtotime("-1 day"));?>" 
                    class="text-input small-input datepicker" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" />&nbsp;&nbsp;
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
          <tr>
       		<!--<th width="20%">Product Image</th>-->
            <th width="10%">Product Id</th>
            <th width="80%">Product Name</th>
            <th width="5%">Size</th>
            <th width="5%">Total</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (isset($results)) {
				   if (is_array($results) && count($results) > 0) {
					   $i = 1;
					   foreach ($results as $key => $val) {
							?>
							<tr valign="middle">
								<!--<td width="20%" style="display:table-cell;vertical-align:middle">
								<?php 
									/*if($val->image != "") { 
											echo '<img src="'.SITE_URL.product_or_placeholder(PRODUCT_THUMB_200_300_PATH.$val->image).'" height="30px;" />';
									} else {
										echo 'Not Available'; 
									}*/
								?>
                                </td>-->
                                <td style="display:table-cell;vertical-align:middle"><?php echo $val->prod_id; ?></td>
                                <td style="display:table-cell;vertical-align:middle"><?php echo $val->name; ?></td>
                                <td style="display:table-cell;vertical-align:middle">
								<?php echo ($val->genre_name != "") ? $val->genre_name : '<span class="bg_red">N/A</span>'; ?></td>
								<td style="display:table-cell;vertical-align:middle"><?php echo $val->total_prod;?></td>
						   </tr>
						   <?php
						$i++;
						}
					} else {
						?>
						<tr>
							<td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
						</tr>
						<?php
					}
			   } else {
					?>
					<tr>
                    	<td colspan="6" style="text-align:center">Start generating <?php echo $page_name;?></td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important">
                <?php
                  if (isset ($results) && count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>