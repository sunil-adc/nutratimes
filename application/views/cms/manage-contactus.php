<?php
	$all_array = all_arrays();
	
?>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
         </div>
         
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Status :</strong> 
                                       
                    <select name="search_field" id="search_field">
                    <option value="">Select Status</option>
                    <option value="status">Status</option>
                    </select>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong>
                        <select name="search_txt" id="search_txt">
                        <option value="1" <?php echo ($search_txt == "1") ? "selected='selected'" : ""; ?>>Active</option>
                        <option value="0" <?php echo ($search_txt == "0") ? "selected='selected'" : ""; ?>>Inactive</option>
                    
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
       <table>
        <thead>
          <tr>
       		<th width="20%">Contact Id</th>
            <th width="20%">Email</th>
            <th width="20%">Mobile</th>
            <th width="30%">Message</th>
            <th width="20%">Date Created</th>
            <th width="10%">Status</th>
            
          </tr>	
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {                 
				  $i = 1;
				  
				  
			
				  foreach ($results as $key => $val) {
                        ?>
                        <tr>
                           <td><?php echo $val->id; ?></td>
                           <td><?php echo $val->email; ?></td>
                           <td><?php echo $val->mobile; ?></td>
                           <td><?php echo $val->message; ?></td>
                           <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                           
                            <td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td>
                           
                       </tr>
                       <?php
						$i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>