<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			inline: true
		});
	});
</script> 
<?php $arr_all = all_arrays(); ?>
<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
        <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
		<fieldset class="align-left prod_leftside">
        <p>
          <label>Provider : </label>
          <?php echo custom_dropdown('provider', '', $arr_all['ARR_PROVIDER'], '','class="text-input medium-input"');?>
        </p>
        
        <p>
          <label>Payment Mode : </label>
          <?php echo custom_dropdown('payment_mode', '', $arr_all['ARR_PAYMENT'], '','class="text-input medium-input"');?>
        </p>
        <p>
          <label>Sent Date : </label>
          <input type="text" name="sent_date" id="sent_date" class="text-input medium-input datepicker" readonly="readonly" value="<?php echo date('Y-m-d'); ?>" />
        </p>
        <p>
          <label>Order ID(s) : </label>
          <textarea name="tracknums" id="tracknums" class="text-input medium-input" rows="10" cols="10"></textarea>
        </p>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
        <?php
            echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden('cur_url', get_full_url());
		?>	
      </fieldset>
      <fieldset class="align-right prod_leftside">
      	<table>
        	<thead>
            	<tr>
                    <th>Provider</th>
                    <th>Number of Tracking number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Xpress</td>
                    <td><?php echo $xpress;?></td>
                </tr>
                <tr>
                    <td>Ecom Express</td>
                    <td><?php echo $ecom;?></td>
                </tr>
                <tr>
                    <td>Red Express</td>
                    <td><?php echo $total_red;?></td>
                </tr>
                <tr>
                    <td>Bluedart</td>
                    <td><?php echo $total_bluedart;?></td>
                </tr>
                <tr>
                    <td>DTDC</td>
                    <td><?php echo $total_dtdc;?></td>
                </tr>
                <tr>
                    <td>First Flight</td>
                    <td><?php echo $total_ff;?></td>
                </tr>
            </tbody>
        </table>
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  </div>
</div>
<div class="clear"></div>
