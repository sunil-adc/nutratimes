<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
            <div class="align-left prod_leftside">
                <div class="prod_box">
                    <div class="prod_title">QA</div>
                    <div class="prod_desc">
        				<p>
                            <label><span class="color_red">*</span> <span class="color_blue">QA Agent</span> </label>
                            <?php 
								if ( isset($result_data['qa_agents']) && is_array($result_data['qa_agents'])) {
									if($result_data['qa_agents'] != false) {
										echo '<select class="small-input" name="qa_user" id="qa_user">';
										foreach ($result_data['qa_agents'] as $row) {
											$selected = ($row->id == $result_data['id']) ? "selected" : "";
											echo '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->fname).' '.ucfirst($row->lname).' ('.$row->email.')</option>';
										}
										echo '</select>';
									}
								} else {
									?>
									<select size="10" class="small-input" name="id[]">
                                    	<option value="0">QA User Not Available</option>
                                    </select>
									<?php
								}
							?> 
                        </p>
                        <p>
                            <label><span class="color_red">*</span> <span class="color_blue">Sales Agent</span> </label>
                            <?php 
								if ( isset($result_data['sales_agents']) && is_array($result_data['sales_agents'])) {
									if($result_data['sales_agents'] != false) {
										echo '<select size="10" class="small-input" multiple="multiple" name="cod_user[]" id="cod_user[]">';
										foreach ($result_data['sales_agents'] as $row) {
											$selected = (in_array($row->id, $result_data['cod_user'])) ? "selected" : "";
											echo '<option value="'.$row->id.'" '.$selected.' >'.$row->email.'</option>';
										}
										echo '</select>';
									}
								} else {
									?>
									<select size="10" class="small-input" name="cod_user[]" id="cod_user[]">
                                    	<option value="0">COD Users Not Available</option>
                                    </select>
									<?php
								}
							?> 
                        </p> 
                    </div>
                </div>
            </div>
        	<div class="clear"></div>
        </div>
        <?php
            echo form_hidden('mode', 'edit');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
