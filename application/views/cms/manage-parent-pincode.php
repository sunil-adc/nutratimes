<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
         </div>
       <table>
        <thead>
          <tr>
       		<th width="20%">Pixel Name</th>
            <th width="30%">Date Created</th>
            <th width="30%">Date Updated</th>
            <th width="10%">Status</th>
            <th width="10%">Edit</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {                 
				  $i = 1;
				  foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><?php echo $val->id?>X</td>
                            <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                            <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>
                            <td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td>
                            <td><a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>#atab" title="Edit"><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a></td>
                       </tr>
                       <?php
						$i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>