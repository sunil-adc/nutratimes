<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Network </span> : </label> 
       		<input type="text" class="text-input small-input" name="net" id="net" value="<?php echo $result_data['net']; ?>" />
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">CPS (Cost Per Sale)</span> : </label> 
       		<div class="cps_categories scroll">
			<?php
				if(isset($result_data['category_cps']) && trim($result_data['category_cps']) != "") {
					$category_cps = unserialize($result_data['category_cps']);
				}
				foreach ($categories->result() as $cat_val) {
					?>
                    <div class="cps-cat-element">
						<div class="cps-cat-name align-left"><?php echo $cat_val->name;?></div>
                        <div class="cps-cat-value align-left">
                        	<input type="text" class="text-input medium-input" name="category[<?php echo $cat_val->cat_id?>]" value="<?php echo isset($category_cps[$cat_val->cat_id]) ? $category_cps[$cat_val->cat_id] : 0;?>" />
					    </div>
                        <div class="cps_percentage align-left">%</div>
						<div class="clear"></div>
					</div>
					<?php
				}
			?>
            </div>
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Categorywise Pixels</span> : </label> 
       		<span class="color_red">Replace dynamic order id with "{ORDID}", total amount with "{TOTAL_AMOUNT}" and  Ip Address with "{IP_ADDR}" automated system will take it dynamic.<br /></span>
            <div class="cps_categories scroll">
			<?php
				if(isset($result_data['category_pixels']) && trim($result_data['category_pixels']) != "") {
					$category_pixels = unserialize($result_data['category_pixels']);
				}
				foreach ($categories->result() as $cat_val) {
					?>
                    <div class="cps-cat-element">
						<div class="cps-cat-name align-left"><?php echo $cat_val->name;?></div>
                        <div class="cps-cat-value align-left">
                        	<textarea class="text-input medium-input" name="category_pixels[<?php echo $cat_val->cat_id?>]"><?php echo isset($category_pixels[$cat_val->cat_id]) ? $category_pixels[$cat_val->cat_id] : '';?></textarea>
					    </div>
                        <div class="clear"></div>
					</div>
					<?php
				}
			?>
            </div>
        </p>
        
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status</span> </label>
            <select name="status" id="status" class="small-input" tabindex="2">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
            	<option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
       	<?php
            if(strtolower($mode) == "edit"){
				?>
				<div class="information_div">
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo time_diff ($result_data['datecreated']); ?></label>
                    </p>
                    <div class="border_bottom"></div>
                	<p class="info_bar">
                      <label><span class="color_blue">Update Date : </span>
                        <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>
				<?php
			}
			
        	echo form_hidden('mode', $mode);
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
