<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
      <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
      <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
          <div class="align-left prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Basic</div>
              <div class="prod_desc">
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Phone Number: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'phone_no',
                                          'id'          		=> 'phone_no',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['phone_no']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <?php
					if($this->session->userdata('admin_role_id') == '1') {
						?>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Admin Name: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'admin_name',
                                          'id'          		=> 'admin_name',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['admin_name']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Admin Email: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'admin_email',
                                          'id'          		=> 'admin_email',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['admin_email']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <?php
					}
				?>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Site Name: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'site_name',
                                          'id'          		=> 'site_name',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['site_name']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Admin Note: </span></label>
                  <textarea name="admin_note"  id="admin_note" class="text-input small-input" rows="5"><?php echo $result_data['admin_note'];?></textarea>
                </p>
              </div>
            </div>
          </div>
          <?php
			if($this->session->userdata('admin_role_id') == '1') {
				?>
          <div class="align-right prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Payment Gateway Info</div>
              <div class="prod_desc">
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Payu Key: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'payu_key',
                                          'id'          		=> 'payu_key',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['payu_key']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Payu Salt: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'salt',
                                          'id'          		=> 'salt',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['salt']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">EBS Secret Key: </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'ebs_secret_key',
								  'id'          		=> 'ebs_secret_key',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['ebs_secret_key']
								);
		
						echo form_input($data);
				  ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">EBS Account Id: </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'ebs_account_id',
								  'id'          		=> 'ebs_account_id',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['ebs_account_id']
								);
		
						echo form_input($data);
				  ?>
                </p>
              </div>
            </div>
          </div>
          <?php
			}
          ?>
          <div class="align-right prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Social Networks Info</div>
              <div class="prod_desc">
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Facebook URL : </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'fb_url',
								  'id'          		=> 'fb_url',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['fb_url']
								);
		
						echo form_input($data);
				  ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Twitter URL : </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'twt_url',
								  'id'          		=> 'twt_url',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['twt_url']
								);
		
						echo form_input($data);
				  ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Pinterest URL : </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'pin_url',
								  'id'          		=> 'pin_url',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['pin_url']
								);
		
						echo form_input($data);
				  ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">YouTube URL : </span></label>
                  <?php
						$data = array(
								  'name'        		=> 'ytube_url',
								  'id'          		=> 'ytube_url',
								  'autocomplete'       	=> 'off',
								  'class'       		=> 'text-input small-input',
								  'value'				=> $result_data['ytube_url']
								);
		
						echo form_input($data);
				  ?>
                </p>
              </div>
            </div>
          </div>
          
          <div class="clear"></div>
          <div class="prod_box">
            <div class="prod_title">Contest Winner :</div>
            <p>
              <textarea class="text-input small-input" id="ckeditor_description" name="contest_winner" rows="5"><?php _isset($result_data['contest_winner']); ?>
</textarea>
            </p>
          </div>
        </div>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  </div>
</div>
<div class="clear"></div>
