<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
       <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
       </div>
        <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post" >
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="o.mobile" <?php echo ($search_field == "o.mobile") ? "selected='selected'" : ""; ?>>Mobile</option>
                        <option value="o.id" <?php echo ($search_field == "o.id") ? "selected='selected'" : ""; ?>>Order Id</option>
                        <option value="o.tracking_number" <?php echo ($search_field == "o.tracking_number") ? "selected='selected'" : ""; ?>>Tracking No</option>
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table class="search-table">
        <thead>
              <tr>
                <th width="8%">Ref ID</th>
                <th width="8%">PG Order ID</th>
                <th width="12%">Name</th>
                <th width="8%">Mobile</th>
                <th width="7%">NET</th>
                <th width="7%">Amount</th>
				<th width="7%">Tracking Number</th>          
                <th width="7%">Payment Mode</th>
                <th width="8%">Payment Status</th>
                <th width="8%">Actions</th>
                <th width="10%">Order Date</th>
                <th width="10%">Send Date</th>
              </tr>
            </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>
                      <td><?php echo ($val->id != "") ? $val->id : "<span class='color_red'>N/A</span>" ; ?></td>
                      <td><?php echo ($val->order_id_pg != "") ? $val->order_id_pg : "<span class='color_red'>N/A</span>" ; ?></td>
                      <td><?php echo ($val->name != "") ? $val->name : "<span class='color_red'>N/A</span>" ; ?></td>
                      <td><?php echo ($val->mobile != "") ? $val->mobile : "<span class='color_red'>N/A</span>" ; ?></td>
          
                      <td><?php echo ($val->net != "") ? '<a href="javscript:void(0)" title="'.$val->net.'">'.sub_string($val->net, 10).'</a>' : "<span class='color_red'>N/A</span>" ; ?></td> 
                      
                      <td><?php echo $val->cash_price ?></td>
                      <td><?php echo $val->tracking_number ?></td>
                      <td><?php echo ($val->payment_mode == 2) ? "<span class='color_red_bg'>COD</span>" : "<span class='color_green_bg'>Prepaid</span>"; ?></td> 
                      <td><?php echo ($val->payment_status == 1) ? "<span class='color_green_bg'>Paid</span>" : "<span class='color_red_bg'>Not Paid</span>"; ?></td>
                      <td>
                  	<?php 
									if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
										array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
										?>
                    <a href="<?php echo FULL_CMS_URL?>/update_expressbee/update_delivery_status/<?php echo $val->id;?>/#atab" target="_blank">
										<img src="<?php echo CMS_URL?>images/courier-truck.png" title="Update delivery Status" height="15px" /></a>
										<a href="<?php echo FULL_CMS_URL?>/updatepaymentstatus/index/<?php echo $val->id;?>/#atab" target="_blank">
										<img src="<?php echo CMS_URL?>images/order_edit.png" title="Update Payment Status" height="17px" /></a>
										<?php
									}
                      ?>
                      <a href="<?php echo FULL_CMS_URL?>/orderdetails/index/<?php echo $val->id;?>/#atab" target="_blank">
                      <img src="<?php echo CMS_URL?>images/order_details.png" title="Order Details" height="14px" /></a>
                  </td>
                  <td><a href="javascript:void(0)" title="<?php echo date('d-M-Y ',strtotime($val->order_date));?>">
								<?php echo ($val->order_date != "") ? date('d-M-Y ',strtotime($val->order_date)) : "N/A"; ?></a></td>
                  
                  <td><a href="javascript:void(0)" title="<?php echo date('d-M-Y ',strtotime($val->sent_date));?>">
								<?php echo ($val->sent_date != "") ? date('d-M-Y ',strtotime($val->sent_date)) : "N/A"; ?></a></td>         
                  </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="14" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
                <td colspan="14" style="line-height: 1.5em; !important;text-align:right">
                    <?php
                      if (count($results) > 0) {
                         echo $links;
                      }
                    ?>
                </td>
              </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>