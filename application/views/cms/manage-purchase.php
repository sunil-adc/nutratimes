<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
       <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
       </div>
        <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="name" <?php echo ($search_field == "name") ? "selected='selected'" : ""; ?>> Name</option>
                        <option value="prod_id" <?php echo ($search_field == "prod_id") ? "selected='selected'" : ""; ?>>Product ID</option>
                        <option value="status" <?php echo ($search_field == "status") ? "selected='selected'" : ""; ?>> Status</option>
                        
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
            <th width="5%">ID</th>
            <th width="25%">Name</th>
            <th width="10%">Stock</th>
            <th width="10%">COD Price</th>
            <th width="10%">Sale Price</th>
            <th width="15%">Created</th>
            <th width="15%">Updated</th>
            <th width="10%">Status</th>
            
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>                        
                         <td><?php echo $val->prod_id; ?></td>
                         <td> 
                         <?php
						 if($val->oos == 0) {
						 ?>
						 <a class="item-name" href="<?php echo SITE_URL.PD_PAGE."/".PD_FUNC."/".$val->seourl;?>" title="<?php echo $val->name?>"><?php echo $val->name;?></a>
                         <?php
						 } else {
							 echo $val->name;
						 }
						 ?>
						 </td>
                         <td><?php 						 
						 if($val->is_size > 0 ){							 
						 	if(isset($qty[$val->prod_id])){
								echo $qty[$val->prod_id];
							}else {
								echo "0";
							}
						 } else {
							 echo $val->total_stock; 
						 }
						 
						 ?></td>
                         <td><?php echo $val->cod_price; ?></td>
                         <td><?php echo $val->sale_price; ?></td>
                         <td><?php echo time_diff ($val->datecreated);?></td>
                         <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>                        
                         <td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td></td>                            
                       </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="8" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="8" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>