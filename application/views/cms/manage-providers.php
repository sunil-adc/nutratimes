<?php $predefine_array = all_arrays(); ?>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <table>
        <thead>
          <tr>
       		<th width="25%">Provider</th>
            <th width="20%">Priority</th>
            <th width="25%">Date Created</th>
            <th width="25%">Date Updated</th>
            <th width="5%">Edit</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   $payment_mode = NULL;
				   foreach ($results as $key => $val) {
                        if($payment_mode != $val->payment_mode) {
							$payment_mode = $val->payment_mode;
							echo "<tr><th colspan='5' class='role_header'>Payment Mode - ".strtoupper($predefine_array['ARR_PAYMENT'][$payment_mode])."</th></tr>";
						}
						?>
                        <tr>
                            <td><?php echo ucfirst($predefine_array['ARR_PROVIDER'][$val->provider]);?></td>
                            <td><?php echo $val->priority;?></td>
                            <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                            <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>
                            <td><a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>#atab" title="Edit <?php echo ucfirst($predefine_array['ARR_PAYMENT'][$payment_mode])." -> ".ucfirst($predefine_array['ARR_PROVIDER'][$val->provider]); ?>"><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a></td>
                       </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>