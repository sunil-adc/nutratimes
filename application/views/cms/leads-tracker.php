<?php $arr_all = all_arrays(); ?>
<script type="text/javascript">
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
	function checkNetForm(){
		if ($("#pubs").val() != '') {
			window.location.href = '<?php echo FULL_CMS_URL?>/reportpubwise/page/' + $("#from_date").val() + '/' + $("#to_date").val() + '/' + $("#netGetPub").val() + '/' + $("#timezone").val() + '/' + $("#pubs").val();
			return false;
		}
	}
</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <?php echo $label_from_to_date; ?>
  	<div class="tab-content default-tab">
        <span id="quick_upd_msg"></span>
		<?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	   ?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post" onsubmit="javascript:return checkNetForm()">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" id="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important" />&nbsp;&nbsp;&nbsp;&nbsp;
 
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" id="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important"  />&nbsp;&nbsp;
                    
                    <!--<strong><span class="color_red">*</span> Network :</strong>
                    <?php echo $net; ?>
                    
                    <strong><span class="color_red">*</span> Pubs :</strong>
                    <span id="ajaxPublisher">
                    	<select name="pubs" id="pubs" class="small-input" style="width:10% !important"><option value="">N/A</option></select>
                    </span>
                    
                    <strong><span class="color_red">*</span> Timezone :</strong>
                    <select name="timezone" id="timezone" class="text-input small-input datepicker" style="width:10% !important">
                    	<option value="0" <?php echo (isset($timezone) && $timezone == '0') ? 'selected' : '';?>>Asia/Calcutta</option>
                        <option value="-34200" <?php echo (isset($timezone) && $timezone == '-34200') ? 'selected' : '';?>>America/New_York</option>
                    </select>-->
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
           <tr>
		    <th style="text-align:center" width="10%">Utm Source</th>
            <th style="text-align:center" width="10%">Utm Sub</th>
            <th style="text-align:center" width="10%"> Leads</th>
            <th style="text-align:center" width="10%">Total Orders</th>
			<th style="text-align:center" width="10%">Cod</th>
            <th style="text-align:center" width="10%">Prepaid</th>
			<th style="text-align:center" width="10%">Order Amt</th>
			<th style="text-align:center" width="10%">Delivered Orders</th>
			<th style="text-align:center" width="5%">Return Orders</th>
			<th style="text-align:center" width="10%">Delivered Amt</th>
		  </tr>
        </thead>
        <tbody>
            <?php
               $i = 1;
               if (isset($results)) {
				   ?>
                    <tr class="netport_column" valign="middle">
                        <!--<?php //echo FULL_CMS_URL?>/reportpubwise/page/<?php //echo $from_date;?>/<?php //echo $to_date;?>/<?php //echo $this->input->post('net');?>/<?php //echo $this->input->post('timezone');?>-->
                        <td style="vertical-align:middle;text-align:left"><a href="<?php echo FULL_CMS_URL?>/reportpubwise/?net=<?php echo $network;?>"><?php echo $network;?></a></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $pincode_type;?>X</td>
                        <td style="vertical-align:middle;text-align:center">
                        	<span class="<?php echo $handle_type == '1' ? 'color_green_bg': 'color_red_bg';?>">
								<?php echo $handle_type == '1' ? 'Manual': 'Automatic';?></span></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $goal;?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $leads;?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $lead_fired; ?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $sale;?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $spent;?></td>
                   	    <td style="vertical-align:middle;text-align:center"><?php echo $revenue;?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $margin;?></td>
                        <td style="vertical-align:middle;text-align:center"><?php echo $lead_to_conversion;?> %</td>
                        <td style="vertical-align:middle;text-align:center"><?php echo round($cps);?></td>
                   </tr>
                   <tr>
                   		<td colspan="12" class="limited_updation">
                            <form action="javascript:updateQuickNet('<?php echo $i?>')" class="quick_net_update" method="post">
                                <div class="main_quick">
                                    <div class="align-left quick_pincode_type">
                                    Pincode : <?php
                                        if($sel_pp != false) {
                                            echo '<select name="pincode_type_'.$i.'" id="pincode_type_'.$i.'" >';
                                            foreach($sel_pp->result() as $v) {
                                                $selected = $v->id == $pincode_type ? 'selected' : '';
                                                echo '<option value="'.$v->id.'" '.$selected.'>'.$v->id.'X</option>';	
                                            }
                                            echo '</select>';
                                        }
                                   ?> 
                                    </div>
                                    <div class="align-left quick_handle_type">
                                    Handle : <select name="handle_type_<?php echo $i?>" id="handle_type_<?php echo $i?>" >
                                        <option>Select Option</option>
                                        <option value="1" <?php echo $handle_type == '1' ? 'selected' : ''; ?>>Manual</option>
                                        <option value="2" <?php echo $handle_type == '2' ? 'selected' : ''; ?>>Automatic</option>
                                    </select>    
                                    </div>
                                    <div class="align-left quick_fire_type">
                                        Fire Type :<select name="fire_type_<?php echo $i?>" id="fire_type_<?php echo $i?>" >
                                            <option value="0" <?php echo (isset($fire_type) && $fire_type == '0') ? "selected='selected'" : "";?>>
                                            Currency Basis</option>
                                            <option value="1" <?php echo (isset($fire_type) && $fire_type == '1') ? "selected='selected'" : "";?>>
                                            Percentage Basis</option>
                                        </select>
                                    </div>
                                    <div class="align-left quick_goal">
                                        Goal : <input type="text" id="goal_<?php echo $i?>" name="goal_<?php echo $i?>" class="quick_txt" value="<?php echo $goal?>" />
                                    </div>
                                    <div class="align-left quick_upd_btn">
                                        <input class="button" type="submit" id="sbmt_btn_<?php echo $i?>" name="sbmt_btn_<?php echo $i?>" value="SAVE" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <input type="hidden" id="search_form" name="search_form" value="<?php echo $seach_form; ?>" />
                                <input type="hidden" id="data_id" name="data_id" value="<?php echo $i?>" />
                                <input type="hidden" id="primary_id_<?php echo $i?>" name="primary_id" value="id" />
                                <input type="hidden" id="primary_id_value_<?php echo $i?>" name="primary_id_value" value="<?php echo $net_id;?>" />
                                <input type="hidden" id="from_date_<?php echo $i?>" name="from_date" value="<?php echo $from_date;?>" />
                                <input type="hidden" id="to_date_<?php echo $i?>" name="to_date" value="<?php echo $to_date;?>" />
                                <input type="hidden" id="net_id_<?php echo $i?>" name="net_id" value="<?php echo $net_id;?>" />
                                <input type="hidden" id="siteurl" name="siteurl" value="<?php echo FULL_CMS_URL;?>" />
                            </form>
                        </td>
                   </tr>
                   <?php
			   } else {
					?>
					<tr>
                    	<td colspan="12" style="text-align:center">Start generating <?php echo $page_name;?></td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	$(document).ready(function(e) {
        $(".netport_column").hover(function(e){
			$(this).find('.quick_upd_btn').css('display','block');
		});
		<?php
			if ($this->input->post('net') != '' && $this->input->post('net') > 0) {
				?> $("#netGetPub").trigger("change"); <?php
			}
		?>
    });
</script>