<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <?php
        	if ($result_data['user_id'] != '') {
			?>
            <p>
              <label><span class="color_red">*</span> <span class="color_blue">User id</span></label>
              <?php echo _isset($result_data['user_id']); ?> 
            </p>
            <?php
			}
		  	if ($result_data['mobile'] != '') {
			?>
            <p>
              <label><span class="color_red">*</span> <span class="color_blue">Mobile</span></label>
              <?php echo _isset($result_data['mobile']); ?> 
            </p>
            <?php
			}
			if ($result_data['name'] != '') {
			?>
            <p>
              <label><span class="color_red">*</span> <span class="color_blue">Name</span></label>
              <?php echo _isset($result_data['name']); ?> 
            </p>
            <?php
			}
			
			if ($result_data['email'] != '') {
			?>
            <p>
              <label><span class="color_red">*</span> <span class="color_blue">Email</span></label>
              <?php echo _isset($result_data['email']); ?> 
            </p>
        	<?php
			}
		?>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Order id</span></label>
            <input class="text-input small-input" type="text" id="order_id" name="order_id" value="<?php echo _isset($result_data['order_id']); ?>" tabindex="1"/> 
        </p>
      <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status </span></label>
            <select name="status" id="status" class="small-input" tabindex="5">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Active</option>
            	<option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
        <?php
			echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" tabindex="6" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
