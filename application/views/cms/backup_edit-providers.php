<?php
	$predefine_array = $this->common_model->predefine_array();
?>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <table>
        <thead>
          <tr>
       		<th width="2%">No.</th>
            <th width="10%">Provider</th>
            <th width="10%">Priority</th>
            <th width="73%">Pincodes</th>
            <th width="5%">Action</th>
          </tr>
        </thead>
        <tbody>
            <?php
			   $payment_mode = NULL;
			   $i = NULL;
               if ( isset($result_data) && $result_data->num_rows() > 0) {
					foreach ($result_data->result() as $rows) {
						if($payment_mode != ucfirst($predefine_array['ARR_PAYMENT'][$rows->payment_mode])) {
							$payment_mode = ucfirst($predefine_array['ARR_PAYMENT'][$rows->payment_mode]);
							echo '<tr><th colspan="5" class="role_header">'.strtoupper($payment_mode).'</th></tr>';
							$i = 1;
						}
                        ?>
                        <form>
                            <tr>
                                <td style="vertical-align:top"><?php echo $i; ?></td>
                                <td style="vertical-align:top"><?php echo ucfirst($predefine_array['ARR_PROVIDER'][$rows->provider]); ?></td>
                                <td style="vertical-align:top"><p><input class="text-input small-input" type="text" value="<?php echo $rows->priority; ?>" name="priority" /></p></td>
                                <td style="vertical-align:top"><textarea class="text-input small-input" id="pincodes" name="pincodes" rows="10" cols="70"><?php echo _isset($rows->pincodes); ?></textarea></td>
                                <td style="vertical-align:top"><input type="submit" name="submit" /></td>
                           </tr>
                       </form>
					   <?php
						$i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>