<?php $arr_all = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Order Id</span> : </label> 
       		<input type="text" class="text-input small-input" name="order_id" id="order_id" value="<?php echo $result_data['order_id'];?>" />
        </p>
        <p>
          	<label><span class="color_red">*</span> <span class="color_blue">Payment Gateway Id for the order</span> : </label> 
       		<input type="text" class="text-input small-input" name="order_id_pg" id="order_id_pg" value="<?php echo $result_data['order_id_pg'];?>" />
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Email</span> </label>
            <input type="text" class="text-input small-input" name="email" id="email" value="<?php echo $result_data['email'];?>" /> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Name</span> </label>
            <input type="text" class="text-input small-input" name="name" id="name" value="<?php echo $result_data['name'];?>" /> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Mobile</span> </label>
            <input type="text" class="text-input small-input" name="mobile" id="mobile" value="<?php echo $result_data['mobile'];?>" /> 
        </p>
        <p>
          	<label> <span class="color_blue">Ticket Status</span> </label>
           <?php echo custom_dropdown('ticket_status', 'ticket_status', $arr_all['ARR_TICKET_STATUS'], isset($result_data['ticket_status']) ? $result_data['ticket_status'] :'','class="text-input small-input"');?>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Content</span> </label>
            <textarea class="text-input small-input" name="content" id="content"><?php echo $result_data['content'];?></textarea> 
        </p>
        <?php
            if(strtolower($mode) == "edit"){
				?>
				<div class="information_div">
                    <p class="info_bar">
                        <label><span class="color_blue">Create Date : </span>
                        <?php echo time_diff ($result_data['datecreated']); ?></label>
                    </p>
                    <div class="border_bottom"></div>
                	<p class="info_bar">
                      <label><span class="color_blue">Update Date : </span>
                        <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                    </p>
                </div>
				<?php
			}
			
        	echo form_hidden('mode', $mode);
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
<script>
	$(document).ready(function(e) {
        $("#order_id").blur(function(){
			$.ajax({
				url: '<?php echo CMS_URL?>addticket/getorderid',
				type: 'post',
				data: 'data=send&order_id=' + $(this).val(),
				beforeSend:function(){
					$("#order_id_pg").val('').attr('readonly', 'readonly').attr('disabled', 'disabled');
					$("#mobile").val('').attr('readonly', 'readonly').attr('disabled', 'disabled');
					$("#email").val('').attr('readonly', 'readonly').attr('disabled', 'disabled');
					$("#name").val('').attr('readonly', 'readonly').attr('disabled', 'disabled');
				},
				success:function(e){
					if (e.toLowerCase != 'blank') {
						var arr = e.split("|");
						$("#order_id_pg").val(arr[1]).removeAttr('disabled').removeAttr('readonly');
						$("#mobile").val(arr[2]).removeAttr('disabled').removeAttr('readonly');
						$("#email").val(arr[3]).removeAttr('disabled').removeAttr('readonly');
						$("#name").val(arr[4]).removeAttr('disabled').removeAttr('readonly');
					} else {
						alert('Please enter valid order id either its not found or invalid.');
					}
				}
			});
		});
    });
</script>