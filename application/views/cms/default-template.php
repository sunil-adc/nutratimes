<?php
	$data['menu'] = $menu;
	$data['cur_controller'] = $cur_controller;
	$browser = getBrowser();
?>
</head>
<body>
<div id="body-wrapper">
<div class="top-header">
	<div class="align-left user-title">
    	<span class="top-header-link">Hello, </span>
        <a href="javascript:void(0)" class="top-header-link" title="Edit your profile">
			<?php echo $this->session->userdata('username'); ?>
        </a>
    </div>
    <div class="align-left user-role-details" id="page-intro">You are Logged In as <a href="javascript:void(0)" class="top-header-link"><?php echo $this->session->userdata('admin_role'); ?>
    <?php
		if ($this->session->userdata('login_time')) {
			$LTE = session_remain_time($this->session->userdata('login_time'), $this->session->userdata('login_ip'));
			if (!$LTE) {
				redirect(CMS_FOLDER_NAME."/logout");
			} else {
				echo $LTE;
			}
		}
    ?>
    </a>
    </div>
    <div class="align-right user-logout"><a href="<?php echo FULL_CMS_URL?>/logout" class="top-header-link">Power Off</a></div>
    <div class="clear"></div>
</div>
<!-- Wrapper for the radial gradient background -->
<div id="sidebar">
  <div id="sidebar-wrapper">
    <br /><br />
    <div id="profile-links"> 
    	<br /><br />
        <a href="<?php echo SITE_URL?>" target="_blank" title="Go to <?php echo SITE_NAME?>.com (It will open in New tab)">
        	<img src="<?php echo SITE_URL?>cdn/image/catalog/logo.png" style="width: 150px; height: auto;">

        </a> 
    </div>
    <?php echo $this->load->view(CMS_FOLDER_NAME.'/sidebar', $data); ?> </div>
</div>
<div id="main-content">
<?php 
	// CHECK FOR CACHING
	check_is_cached(search_dir($this->db->cachedir, CMS_FOLDER_NAME."+".$class), $this->db->cachedir);
	if($this->session->userdata('admin_role_id') == 1) {
		
		$this->db->cache_off();
		$sel_db = $this->db->query('SELECT  table_schema db_name, sum(data_length) db_data_size, sum(index_length) db_index_size FROM information_schema.TABLES  where table_schema = "'.DB_NAME.'" GROUP BY table_schema');
		$this->db->cache_on();
		
		if($sel_db->num_rows() > 0) {
			foreach ($sel_db->result() as $dbrow) {
				$db_size =  $dbrow->db_data_size + $dbrow->db_index_size;
			}
		}
		?>
		<div class="blue_bg abosolute_msg highlight">
           <div>Total Database size is <?php echo byteFormat($db_size); ?> (I.E. Data file size : <?php echo byteFormat($dbrow->db_data_size);?> and Index size : <?php echo byteFormat($dbrow->db_index_size);?>)</div>
        </div>
		<?php
	}
?>
<noscript>
        <div class="notification error png_bg">
            <div> 
          		Javascript is disabled or is not supported by your browser. Please 
                <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> 
                your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" 
                title="Enable Javascript in your browser">enable</a> 
                Javascript to navigate the interface properly. 
            </div>
        </div>
</noscript>
<?php
	if (trim($browser) == 'Internet Explorer') { 
		?>
        <div class="blue_bg abosolute_msg highlight">
            <div style="font-weight:500;text-align:center;font-family:Verdana, Geneva, sans-serif">Warning : Internet Explorer will not support some of the functionality so if you fill irritation then switch to Chrome, Firefox, Safari or Opera</div>
        </div>
		<?php
	}
	
	@session_start();
	if( isset($_SESSION['permission_warning']) && trim($_SESSION['permission_warning']) != "" ) { 
		echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$_SESSION['permission_warning'].'</div></div>';
		unset($_SESSION['permission_warning']);
	}
?>
<h2>Welcome to <?php echo ADMIN_TITLE; ?></h2>
<?php //echo $this->load->view(CMS_FOLDER_NAME.'/top-links', $data); ?>
<div class="clear"></div>