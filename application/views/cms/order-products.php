<?php $arr_all = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	   ?>
       
       <div class="label_custom">Add Offer In order</div>
       <form action="<?php echo FULL_CMS_URL?>/orderproducts/update_offer_percentage" method="post">
       <fieldset>
            	
       	<?php
        	//if ($user_data->num_rows() > 0) {
			 	//foreach ($user_data->result() as $usr_val) {
					?>
                    
                <p>
                    <label><span class="color_blue">Offer percentage</span><span>
                      
                      <?php if($result_data1->offer_percentage != ""  && $result_data1->offer_percentage != 0  ){ ?>
                      <a href="javascript:void(0)"; onclick="invoice_confirm('<?php echo FULL_CMS_URL."/orderproducts/send_quick_invoice/".$order_id."/".$remaining_amount  ?>')" class="button" style="float: right; background: #5988d1; border: 1px solid #5988d1;" target="_blank"> Send Quick Invoice SMS </a></span>
                      <?php } ?>

                      </label>
                    
                    <?php 
                    if($caller_status != 3 ){ ?>
                    
                    <select class="text-input small-input" name="offer_percentage" id="offer_percentage" >
                    <option value=""> Select percentage</option>   
                      <?php foreach($arr_all['ARR_OFFER_PER'] as $k=>$v ){?> 
                      
                      <option value="<?php echo $k ?>" <?php echo ($k == $result_data1->offer_percentage ? "selected" : " " ); ?> > <?php echo $v?> </option>
                      
                      <?php }  ?> 

                    </select>
                   
                   <?php }else{

                    echo "Not Allowed to change percentage";
                  
                   } ?>
                    <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id?>">
                </p>
                <p>
                 <input class="button" type="submit" name="offer_submit" id="offer_submit" value="Apply Offer " />

                </p>
                <br><br>
                    <!--<div class="align-left prod_leftside">
                      <div class="prod_box">
                    	<div class="prod_title">Address Details</div>
                        	 <p>
                                <label><span class="color_blue">Address</span> </label>
                                <textarea class="text-input small-input" readonly rows="5"><?php echo $usr_val->address;?></textarea> 
                            </p>
                            <p>
                                <label><span class="color_blue">City</span> </label>
                                <input class="text-input small-input" type="text" value="<?php echo $usr_val->city;?>" readonly /> 
                            </p>
                            <p>
                                <label><span class="color_blue">State</span> </label>
                                <input class="text-input small-input" type="text" value="<?php echo $arr_all['ARR_STATE'][$usr_val->state];?>" readonly /> 
                            </p>
                            <p>
                                <label><span class="color_blue">Pincode</span> </label>
                                <input class="text-input small-input" type="text" value="<?php echo $usr_val->pincode;?>" readonly /> 
                            </p>
                            <p>
                                <span class="color_red">* All fields in user details are read only.</span> 
                            </p>
                        </div>
                      </div>-->
                    <div class="clear"></div>
					<?php
				//}
			//}
       ?>
       </fieldset>
       </form>
       
       <div class="label_custom" id="addproduct">Add Product in order</div>
       <?php echo form_open_multipart($form_submit, array('id' => 'add_order_product', 'onsubmit' => 'javascript:return frm_order_product(this)')); ?>
        <fieldset>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Search By</span> </label>
          <select class="text-input small-input" name="search_by" id="search_by">
          	<option value="name">Product Name</option>
            <option value="prod_id">Product Id</option>
            <option value="seourl">URL String</option>
          </select>
          <input type="hidden" name="search_param" id="search_param" value="name" />
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Product</span> </label>
          <input class="text-input small-input" type="text" name="product_name" id="product_name" 
          placeholder="Type more than 3 character for suggestion" autocomplete="off" />
          <input type="hidden" name="cms_url_hidden" id="cms_url_hidden" value="<?php echo FULL_CMS_URL?>" />
          <div id="ajax_auto_suggestion"></div>
          <input type="hidden" id="pid" name="pid" />
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Product Size</span> </label>
          <select class="text-input small-input" name="product_size" id="product_size">
          	<option value="0">Select Product to get size</option>
          </select>
        </p>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Quantity</span> </label>
          <input class="text-input small-input" type="text" id="quantity" name="quantity" placeholder="Specify like 1, 2, 3" tabindex="2" autocomplete="off" /> 
        </p>
        <p>
        <?php
         	 if ($result_data->num_rows() > 0) {
			  	$i = 1;
				foreach ($result_data->result() as $ses_val) {
					$user = $ses_val->user;
				}
			 }
			 echo form_hidden('manage_page', $manage_page);
			 echo form_hidden('cur_url', get_full_url());
			 echo form_hidden('ord_id', $order_id);
			 
        ?>
        <input class="button" type="submit" name="submit" id="submit" value="Add Product" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
        
        <div class="label_custom">Details of ordered products</div>
        <table>
        <thead>
          <tr>
       		<th width="5%">Prod. Id</th>
            <th width="20%">Product Name</th>
            <th width="7%">Contest</th>
            <th width="12%">Created</th>
            <th width="12%">Updated</th>
            <th width="7%">Size</th>
            <th width="7%">Price</th>
            <th width="10%">Quantity</th>
            <th width="6%">Total</th>
            <th width="14%">Actions</th>
          </tr>
        </thead>
        <tbody>
            <?php
			   $cms_user = 0;
			   $credit_points = 0;
               if ($result_data->num_rows() > 0) {
					
					// GET USER CREDIT POINTS
					if($order_id != "") {
						$credit_points = $this->common_model->get_credit_points_by_order_id($order_id);
					}
					
					$i = 1;
					$total_final_price = 0;
					$shipping = array();
					$cod_enable = false;
					foreach ($result_data->result() as $val) {
						$combo_product = '';
						$caller_status = $val->caller_status;
						
						$free_product = '';
						if (isset($val->free_prod) && trim($val->free_prod) != '') {
							$k = 1;
							$free_product = '<span style="color:red">Free Product : </span>';
							$res_freeprod = $this->db->query('select prod_id, name from '.PRODUCT.' where prod_id in ('.$val->free_prod.') group by prod_id');
							if ($res_freeprod->num_rows() > 0) {
								foreach ($res_freeprod->result() as $val_freeprod) {
									$free_product .= '<div>'.$k.') - Prod Id - '.$val_freeprod->prod_id.' | '.$val_freeprod->name.'</div>';
									$k++;
								}
							}
						}
						
 						if($val->payment_mode == 2) {
							$cod_enable = true;
						}
						$payment_check_status = $val->payment_check_status;
						$payment_mode = $val->payment_mode;
						$cms_user = $val->cms_user;
						$shipping[] = $val->shipping;
						$product_price = ($val->payment_mode == 2) ? $val->cod_price : $val->cash_price;
						$total_price = $product_price * $val->quantity;
						?>
                        <tr class="alt-row">
							<td colspan="10">
							<?php echo form_open_multipart("javascript:update_order_product('".FULL_CMS_URL."', '".$i."', '".$val->order_id."', '".$val->ord_prod_id."')"); ?>
                        	<table>
                              <tr id="tbl_ord_prod_<?php echo $i?>" class="alt-row">
                                <td width="5%"><?php echo $val->product; ?></td>
                                <td width="20%"><?php echo $val->product_name;?><br />
                                <?php echo $combo_product.$free_product;?></td>
                                <td width="7%"><?php echo ($val->contest == 1 ) ? 'Yes' : 'No';?></td>
                                <td width="12%"><?php echo $val->datecreated;?></td>
                                <td width="12%" id="dt_upd_<?php echo $i?>">
								<?php echo (strtotime($val->dateupdated) > 0) ? $val->dateupdated : "Not Updated";?></td>
                                <td width="7%"><?php echo ($val->size > 0) ? $this->common_model->get_genre_name_by_id($val->size) : '-';?></td>
                                <td width="7%"><?php echo $product_price; ?></td>
                                <td width="10%">
								<?php
                                    if($val->caller_status == 3){
										echo $val->quantity;
									} else {?>
                                        <input type="text" class="text-input medium-input" value="<?php echo $val->quantity;?>" 
                                        id="prod_qty_<?php echo $i?>" />
                                        <?php
                                    }
                                ?>
                                </td>
                                <td width="6%" id="ttl_price_<?php echo $i?>"><?php echo $total_price; ?></td>
                                <td width="14%">
								<?php 
                                    /*if($val->caller_status == 3){
                                        echo "<span class='color_red'>Product already sent.</span>";
                                    } else {*/
                                        ?>
                                        <div id="msg_<?php echo $i?>"></div>
                                        <div>
                                        <input type="submit" id="btn_upd_<?php echo $i;?>" name="btn_upd_<?php echo $i;?>" class="small_button" value="Update" /><input type="button" onclick="javascript:delete_order_product('<?php echo FULL_CMS_URL?>/orderproducts/delete/<?php echo $val->ord_prod_id?>/<?php echo $val->order_id?>');" class="small_button" value="Delete" />
                                        </div>
                                        <?php	
                                    //}
                                ?>
                                </td>
                       		  </tr>
                              <tr id="loader_<?php echo $i?>" style="display:none" class="alt-row">
                              <td colspan="10" style="text-align:center"><img src="<?php echo CMS_URL?>images/preloader.gif" />&nbsp;&nbsp;Updating...
                              </td></tr>
                            </table>
                            <input type="hidden" id="<?php echo 'product_price_'.$i; ?>" name="<?php echo 'product_price_'.$i; ?>" value="<?php echo $product_price; ?>" /><input type="hidden" id="<?php echo 'quantity_'.$i; ?>" name="<?php echo 'quantity_'.$i; ?>" value="<?php echo $val->quantity; ?>" /><input type="hidden" id="<?php echo 'pay_md_'.$i; ?>" name="<?php echo 'pay_md_'.$i; ?>" value="<?php echo $val->payment_mode; ?>" />      
                       		<?php 
								echo form_close(); 
							?>
                       		</td>
                       </tr>
                       <?php
						$total_final_price += $total_price;
                       $i++;
					}
					?>
					<tr class="bottom-order-details bottom-order-total">
                    	<td colspan="10">
                        	Shipping : <?php echo ($cod_enable == false) ? max($shipping) : 0 ;?>  
                        </td>
                    </tr>
                    <tr class="bottom-order-details bottom-order-total">
                    	<td colspan="10">
                        	Estimated Total : <?php echo ($cod_enable == false) ? ($total_final_price + max($shipping)) : $total_final_price; ?>  
                        </td>
                    </tr>
                    <tr class="bottom-order-details bottom-order-creditpoint">
                    	<td colspan="10">
                        	Credit points Used : <?php echo ($credit_points > 0) ? $credit_points : 0;?>  
                        </td>
                    </tr>
                    <tr class="bottom-order-details bottom-order-total">
                    	<td colspan="10">
                        <?php 
							if ($cod_enable == false) { 
								$remaining_amount = ($total_final_price + max($shipping)) - $credit_points; 
                        	} else {
								$remaining_amount = ($total_final_price) - $credit_points;
							}
						?>
                            Grand Total : <?php echo ($remaining_amount > 0) ? $remaining_amount : 0;?>
                        </td>
                    </tr>
                    <?php 
						if($payment_mode == 1) {
							?>
							<tr class="bottom-order-details bottom-order-total">
								<td colspan="10">
									 <div id="pcsajax">
									 <a href="javascript:void(0)" data-siteurl="<?php echo CMS_URL;?>" data-id="<?php echo $order_id;?>" class="change_payment_status"><?php echo ($payment_check_status == '1') ? '<img src="'.S3_URL.'cms/images/icons/tick_circle.png" />&nbsp;&nbsp;' : '<img src="'.S3_URL.'cms/images/icons/exclamation.png" />&nbsp;&nbsp;'; echo $payment_check_status == 1 ? 'Payment Checked' : 'Payment Not Checked';?></a></div>
								</td>
							</tr>
							<?php
						} else {
							?>
							<tr class="bottom-order-details bottom-order-total">
                            <td colspan="10">
                                 <span class="color_red_bg">Amount is Not Paid</span>
                            </td>
                        </tr>
						<?php
						}
						?>
						<tr class="bottom-order-details bottom-order-total">
							<td colspan="10" id="chkoutparent">
								<?php
                                	if ($payment_mode == 2) {
										if ($caller_status == '1') {
											echo '<input type="button" id="cmscodcheckout" name="cmscodcheckout" data-id="'.$order_id.'" class="small_button" value="Checkout">';
										} else if ($caller_status == '2') {
											echo '<span class="color_green_bg">Order Proccessed</span>';
										} else if ($caller_status == '3') {
											echo '<span class="color_green_bg">Order Sent</span>';
										} else if ($caller_status == '4') {
											echo '<span class="color_green_bg">Order Rejected</span>';
										} else if ($caller_status == '5') {
											echo '<span class="color_green_bg">Order Not Send</span>';
										} else if ($caller_status == '6') {
											echo '<span class="color_green_bg">Cheque Qualified</span>';
										} 
									}
								
                                 ?>
							</td>
						</tr>
						<?php
			   } else {
                    ?>
                    <tr>
                        <td colspan="10" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        </table>
    </div>
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	$(document).ready(function(e) {
        $("#cmscodcheckout").click(function() {
			var ord_id = $(this).attr("data-id");	
			$.ajax({
				url: "<?php echo FULL_CMS_URL;?>/orderproducts/qualify_codlead/",
				type: "post",
				data: "&data=send&order_id=" + ord_id + "&payment_mode=<?php echo $payment_mode?>",
				beforeSend:function(){
					$("#cmscodcheckout").val("Checkingout...").addClass("disable_button");
				},
				success:function(e){
					if (e == 'done') {
						$("#chkoutparent").html('<span class="color_green_bg">Order Proccessed</span>');
					}
				},
				error:function(e,t,n){
					alert(e.responseText)
				}
			})
		});
    });
</script>

 <script type="text/javascript"> 
  
            function invoice_confirm(u){ 
               var Val = confirm("Do you want to Send quick Payment Sms" ); 
               if( Val == true ){ 
                  
                  window.open(u, '_blank').focus();
                  return true; 
               } 
               else{ 
                  return false; 
               } 
            } 
  
      </script> 