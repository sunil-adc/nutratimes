
<?php

if($is_address == true) {?>


	<!DOCTYPE html>
<html>

<head>
    <title>Adcanopus</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <style>
        @media print
            {
                .page-break  { display:block; page-break-before:always; }

            }
    </style>

    <script>
        function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
  
       document.body.innerHTML = printContents;
  
       window.print();
  
       document.body.innerHTML = originalContents;
  }
      
      </script>

</head>

<body>

	<input type="button" onclick="printDiv('printableArea')" value="Print or Download" style="position: fixed; right: 4%; background: #673AB7; color: #fff; border: none; padding: 10px 14px; font-size: 14px; font-weight: bold;">






	
	<?php
	
	$download_invoice = '';
	
	// USE THE REPLICA DATABASE FOR SEARCH ORDERS
	$this->replica_db = $this->load->database('replicadb', TRUE);
		
	$setting = $this->general->get_settings();
	

	$sql_order_id = "select o.id as order_id, o.payment_mode, pincode  FROM ".stripslashes($table_fields)." WHERE ".stripslashes($where_fields)." and o.caller_status in (3)";
 
	
	$this->replica_db->cache_off();
	$res_order_id = $this->replica_db->query($sql_order_id);
	if($res_order_id->num_rows() > 0) {
		$all_payment_mode = array();
		$all_order_id = $all_pincode = "";
		foreach($res_order_id->result() as $val) {
			$all_order_id .= $val->order_id.",";
			$all_payment_mode[$val->order_id] = $val->payment_mode;
			$all_pincode .= "'".$val->pincode."',";
		} 
		$all_order_id = trim($all_order_id, ",");
		$all_pincode = trim($all_pincode, ',');
	}
	
	if ($all_order_id != "") {
		$this->replica_db->cache_off();
		$order_products = "
					 SELECT 
						p.name, 		up.product, 
						up.cash_price, 	up.size, 
						up.cod_price, 	up.shipping,
						g.genre_name,   up.order_id,
						up.quantity 
					 FROM 
						".USER_PRODUCTS." up inner join ".PRODUCT." p on (up.product = p.prod_id)
						left join ".GENRE." g on (up.size = g.genre_id) 
					 WHERE 
						up.order_id in (".$all_order_id.") and 
						up.status = '1' and 
						(up.contest != '1' )";
						
		
		$res_order_products = $this->replica_db->query($order_products);
		if(!$res_order_products) {
		
			echo '<br>Order Products : Error Occured While download !! Try again, mostly this error will come when you try to download huge data';
		
		} else {
			if($res_order_products->num_rows() > 0) {
				$arr_all_order_products = "";
				foreach($res_order_products->result() as $val_products) {
					$arr_all_order_products[$val_products->order_id][] = $val_products;
				}
			}
		}
	
	
	$this->replica_db->cache_off();
	
	$res = $this->replica_db->query($full_sql." and o.caller_status in (3) group by o.id");

	if($res->num_rows() > 0) {
		$i = 1;
		foreach ($res->result() as $val) {

			$order_products = $total_order_amunt = $invoice_product = $invoice_product_cash = $arr_order_shipping = "";
			$order_shipping = 0;
			
			if (isset($arr_all_order_products[$val->id]) && is_array($arr_all_order_products[$val->id]) && 
				count($arr_all_order_products[$val->id]) > 0) {
				
				echo '<div id="printableArea">';

				foreach ($arr_all_order_products[$val->id] as $key_product => $val_products) {
					
					//$prod_size = (isset($val_products->genre_name) && trim($val_products->genre_name) != "") ? $val_products->genre_name : 'N/A';
					$order_products .= $val_products->name." Qty : ".$val_products->quantity."), ";
					$val_product_price = $all_payment_mode[$val->id] == '2' ? $val_products->cod_price : $val_products->cash_price;
					$total_order_amunt += ($val_product_price * $val_products->quantity);
					
					$arr_order_shipping[] = $val_products->shipping;
					$base_price = (($val_product_price * $val_products->quantity) /112)*100;
					$igst =  	  ($val_product_price * $val_products->quantity) - $base_price ;
					$cgst       = $igst/2;
					$sgst       = $cgst;
					
					if($val->state == 2){
			          		$th = '<th>CGST (6%)</th><th>SGST (6%)</th>';
			        }else{
			          		$th =  '<th >IGST (12%)</th>';
			      	}


					$invoice_product .= '<span style="display: block;margin: 8px 0;font-size: 12px;font-weight: 600;">'.$val_products->name.'</span>

										 <table style="width: 100%;text-align: center;font-size: 12px;border-spacing: 0;">
										 <thead><tr><th>Qty</th><th>Base price</th>
										 '.$th.'	
										 <th>Total</th></tr></thead>
										 <tbody><tr><td>'.$val_products->quantity.'</td><td>'.round($base_price,2).'</td>';	
										  
										 
					if($val->state == 2){
          			
          			$invoice_product .=   '<td >'.round($cgst,2).'</td>
          								   <td >'.round($sgst,2).'</td>';

		          	}else{
		          		$invoice_product .=  '<td>'.round($igst,2).'</td>';
			      	}
					
					$invoice_product  .=	 '<td>'.($val_product_price * $val_products->quantity).'/-</td></tr></tbody></table>';	
					
								            
				}
				$order_shipping = max($arr_order_shipping);
			}
			
			$name = isset($val->name) ? $val->name : '';
			$mobile = isset($val->mobile) ? $val->mobile : '';
			$address = isset($val->address) ? $val->address : '';
			$city = isset($val->city) ? $val->city : '';
			$area = isset($val->area) ? $val->area : '';
			$pincode = isset($val->pincode) ? $val->pincode : '';
			$id = isset($val->id) ? $val->id : '';
			$order_products = isset($order_products) ? trim($order_products,", ") : '';
				
			$final_total_amount = ($all_payment_mode[$val->id] == '2') ? '<tr><td valign="top" colspan="2" style="width:200px"><strong>Payable Amount : </strong>'.$total_order_amunt.'</td></tr>' : '';
			?>



			
            <section class="invoice" style="width: 600px;margin: 0 auto;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;">
               
                <div style="padding-top: 5px;border-bottom: 2px;text-align: center;margin-bottom: -20px;">
                <span style="border-bottom: 2px solid;font-weight: bold;font-size: 16px;">INVOICE</span>
                </div>
                <div style="display: flex;justify-content: space-between;align-items: center;padding-top: 25px;">
                        <div style="width: 80%;">
                            <img width="200" id="_x0000_i1042" src="<?php echo SITE_URL?>cdn/image/catalog/logo.png" alt="<?php echo SITE_URL?>cdn/image/catalog/sticker-logo.png" style="margin-bottom: 5px;">
                            <span style=" line-height: 1.4;font-weight: 500;font-size: 12px;display: block;"><?php echo SITE_NAME ?> ,<br>
                                 #565, Axis Pennmark, 4th Floor, <br>
                                 JP Nagar 3rd Phase, <br>
                                 9th Cross, Near Sony Center, <br>
                                 <?php echo office_city." - ".office_pincode ?>
                            </span>
                            <span style="font-weight: 600; font-size: 12px; display: block; margin: 5px 0; ">Telephone - <?php echo $setting['phone_no'] ?></span>
                            <span style=" font-weight: 600; display: block; font-size: 12px;">GSTIN - <?php echo GST ?></span>
                        </div>
                        
                        <div style="text-align: right;">
                            <?php if ($val->provider == 2){ ?>
                            	<span style="float: left;font-size: 17px;font-weight: 600;margin-left: 10px;"> <?php echo $val->bluedart_area_code ?></span>
                            <?php }else{ ?>
                            	<span style="font-size: 14px;font-weight: 600;">Tracking No: <?php echo $val->tracking_number?></span>
                            <?php } ?>
                            <img width="249" height="80" id="_x0000_i1041" src="<?php echo SITE_URL?>barcode.php?awb=<?php echo $val->tracking_number?>" alt="<?php echo SITE_URL?>barcode.php?awb=<?php echo $val->tracking_number?>">
                        </div>
                
                    </div>
                    
                    <div style="display: flex; justify-content: space-between; margin-top: 15px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid;">
                        <div style="width: 60%;">
                        <h6 style="margin: 0; padding: 14px 0 6px; font-size: 14px; text-align: center;"> <?php echo ($val->payment_mode == 1 ? "Prepaid Order" : "Cash On Delivery") ?></h6>
	                        <div style="display: flex;align-items: center;"><div>
	                                <img width="89" height="80" id="_x0000_i1040" src="<?php echo SITE_URL?>barcode.php?awb=<?php echo $val->id?>" alt="<?php echo SITE_URL?>barcode.php?awb=<?php echo $val->id?>" style="transform: rotate(-90deg);">
	                		</div>
				                <div style="width: 200px;margin-left: 16px;">                            
				            
				                <div style="display: flex;font-size: 14px;font-weight: 600;justify-content: space-between;margin: 15px 0;">
				                    <span>Order No:<br><small style="font-weight: 400;margin-top: 4px;display: block;">Order Date</small></span>
				                    <span style=""><?php echo $val->id ?><br><small style="font-weight: 400;margin-top: 4px;display: block;"><?php echo date('d-M-Y', strtotime($val->order_date)) ?></small></span>
				                    </div>
				                    
				                    <div style="display: flex;font-size: 14px;font-weight: 600;justify-content: space-between;margin: 15px 0;">
				                        <span>Invoice No:<br><small style="font-weight: 400;margin-top: 4px;display: block;">Invoice Date</small></span>
				                        <span style=""><?php echo $val->id ?><br><small style="font-weight: 400;margin-top: 4px;display: block;"><?php echo date('d-M-Y', strtotime($val->sent_date)) ?></small></span>
	                                </div>
	                                <div style="display: flex;font-size: 14px;font-weight: 600;justify-content: space-between;margin: 15px 0;">
	                				<span>Total Amount<br><small style="font-weight: 400;">(Include All Tax)</small></span>
	                            	<span style="">Rs.<?php echo $total_order_amunt ?>/-</span>
	                        		</div>
	                           </div>
	                        </div>
                            
                            
                        </div>
                        
                        <div style="width: 40%;border-left: 1px solid;border-right: 1px solid;height: 100%;">
			                <div style="padding: 15px; border-bottom: 1px solid; font-size: 14px; font-weight: 600;">Name &amp; Address</div>
			                <div style="padding: 4px 15px;">

			                <h6 style="margin: 5px 0 0;font-size: 14px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?php echo $name ?></font></font></h6>
			                <span style="line-height: 1.4; font-weight: 400; font-size: 12px; margin: 10px 0; display: block;"><?php echo $address ?> ,<br> <?php echo $area ?>, 
			                	<br><?php echo $city ?>, <br><?php echo $pincode ?></span>
                            
                            </div>
                        	<span style="display: block;border-top: 1px solid;padding: 10px 15px;font-weight: bold;font-size: 14px;">Phone : <?php echo $mobile ?></span>
                        </div>
            </div>

            <div style="text-align: center;padding: 15px 0;font-size: 13px;"><span style="display: block;text-align: center;">Invoice Details</span>
                <span><strong>Note:</strong>This shipment contain following items</span> 
            </div>
            
            <div style="padding: 5px 0;border-bottom: 1px solid;border-top: 1px solid;">
            
	            <?php echo $invoice_product ?>
            
            </div>
            <div style="margin: 17px 0;border: 1px solid;text-align: center;padding: 5px 5px;font-size: 14px;">If you need any assistance or have any questions, Email us at <?php echo $setting['phone_no'] ?> or Please feel free to call to contact us at 
                <span style="font-size: 16px;font-weight: 600;display: block;margin: 8px 0 0;"><?php echo $setting['phone_no'] ?></span></div>
            </section>
            <div class="page-break"></div>
        



			<?php			
			$i++;
		}
		echo "</div>";
	}
}
	
} else {
	header("Content-Type:  application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		
	if (is_ip_download_allowed()) {
		echo $data_download;
	} else {
		echo 'Download is not permitted from unknown IP Adress';
	}
}
?>



</body>

</html>
