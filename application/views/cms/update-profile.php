<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
      <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
      <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
          <div class="align-left prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Basic</div>
              <div class="prod_desc">
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Email Address: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'email',
                                          'id'          		=> 'email',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['email']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Username: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'username',
                                          'id'          		=> 'username',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['username']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">First Name: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'fname',
                                          'id'          		=> 'fname',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['fname']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_red">*</span> <span class="color_blue">Last Name: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'lname',
                                          'id'          		=> 'lname',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['lname']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
              </div>
            </div>
          </div>
          <div class="align-right prod_leftside">
            <div class="prod_box">
              <div class="prod_title">Network</div>
              <div class="prod_desc">
                <p>
                  <label><span class="color_blue">Facebook URL: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'fb_url',
                                          'id'          		=> 'fb_url',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['fb_url']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
                <p>
                  <label><span class="color_blue">Twitter URL: </span></label>
                  <?php
                                $data = array(
                                          'name'        		=> 'twt_url',
                                          'id'          		=> 'twt_url',
                                          'autocomplete'       	=> 'off',
                                          'class'       		=> 'text-input small-input',
                                          'value'				=> $result_data['twt_url']
                                        );
                
                                echo form_input($data);
                          ?>
                </p>
              </div>
            </div>
            <div class="prod_box">
              <div class="prod_title">Information</div>
              <div class="prod_desc">
                <p>
                  <label> <span class="color_blue">Created : </span> <?php echo strtotime($result_data['datecreated']) > 0 ? time_diff ($result_data['datecreated']) : "Not Available"; ?></label>
                </p>
                <p>
                  <label> <span class="color_blue">Updated : </span> <?php echo strtotime($result_data['dateupdated']) > 0 ? time_diff ($result_data['dateupdated']) : "Not Updated"; ?></label>
                </p>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  </div>
</div>
<div class="clear"></div>
