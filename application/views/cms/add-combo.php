<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Super-duper deal Main Product</span> </label>
            <select name="prod_id" id="prod_id" class="medium-input chosen-select" tabindex="2">
            	<option value="">Select Product</option>
				<?php
					foreach ($all_products as $val) {
						$selected = ($val->prod_id == $result_data['prod_id']) ? 'selected' : '';
                        echo '<option value="'.$val->prod_id.'" '.$selected.'>'.$val->name.' (Id : '.$val->prod_id.')</option>';
                    }
                ?>
            </select> 
        </p>
        <?php
		if(isset($result_data['seourl']))
		{ ?>
        <p>
        <label> <span class="color_blue">SEO URL</span></label>
        <?php 
			echo $result_data['seourl'];
		}?>
        </p>
       	<p>
            <label><span class="color_red">*</span> <span class="color_blue">Add deal Products</span> </label>
            <select multiple="multiple" size="10" name="combo_product[]" id="combo_product[]" class="medium-input chosen-select" tabindex="3">
            	<option value="">Select deal Product</option>
				<?php
					$arr_combo = explode(",", $result_data['combo_product']);
                    foreach ($all_products as $val) {
                     	$selected = (in_array($val->prod_id, $arr_combo)) ? 'selected' : '';
                        echo '<option value="'.$val->prod_id.'" '.$selected.'>'.$val->name.' (Id : '.$val->prod_id.')</option>';
                    }
                ?>
            </select> 
        </p>
       	<?php
			
        	echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
<!--<link rel="stylesheet" href="<?php echo S3_CMS_URL;?>css/dropdown/chosen.css" />
<script src="<?php echo S3_CMS_URL;?>scripts/dropdown/chosen.jquery.js" type="text/javascript"></script>-->

<link rel="stylesheet" href="https://s3-ap-southeast-1.amazonaws.com/shophunk/cms/css/dropdown/chosen.css" />
<script src="https://s3-ap-southeast-1.amazonaws.com/shophunk/cms/scripts/dropdown/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(e) {
	   var config = {
		  '.chosen-select'           : {},
		  '.chosen-select-deselect'  : {allow_single_deselect:true},
		  '.chosen-select-no-single' : {disable_search_threshold:10},
		  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		  '.chosen-select-width'     : {width:"95%"}
		}
		for (var selector in config) {
		  $(selector).chosen(config[selector]);
		} 
	});
</script>
