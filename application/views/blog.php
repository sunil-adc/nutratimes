	<div class="container">
		<div class="row">
			<div id="content" class="col-md-9 col-sm-8">
				<div class="pb-xs-60">
					<div class="blog-listing">
						<div class="row">
							
						<?php if(is_array($blogdetail) && count($blogdetail) > 0 ){ 
							foreach($blogdetail as $val){
							?>	
							
							<div class="col-xs-12">
								<div class="blog-item mb-30">
									<div class="row">
										<div class="col-sm-4">
											<div class="blog-media">
												<div class="blog-image">
													<img src="<?php echo S3_URL.BLOG_CROP_FOLDER.THUMB_PREFIX.$val->blog_image ?>" alt="<?php echo $val->title ?>"
													 title="<?php echo $val->title?>" />
													<a class="read" href="<?php echo SITE_URL."blog/".$val->url?>">
														<!--Read more--></a>
												</div>
											</div>
										</div>
										<div class="col-sm-8">
											<div class="blog-detail">
												<div class="blog-contant">
													<h1 class="blog-title"><a href="<?php echo SITE_URL."blog/".$val->url?>"><b><?php echo $val->title ?></b></a></h1>
													<div class="post-date"><span class="value"> <?php echo date("d/m/Y", strtotime($val->date_created));?></span></div>
													<p><?php echo strip_tags(substr($val->blogcontent , 0, 100)); ?></p>
													<a class="btn btn-color" href="<?php echo SITE_URL."blog/".$val->url ?>">
														Read more</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						
					<?php 
					} 
				}?>	
					</div>
					</div>
				</div>
			
		</div>

		<aside id="column-right" class="col-md-3 col-sm-4 hidden-xs sidebar-block" style="position: sticky; top: 50px;">
				<a href="<?php echo SITE_URL."products" ?>" class="side-add">
					<img style="border: 1px solid #81af4d40;" src="<?php echo SITE_URL?>cdn/image/nutratimes-ad1.jpg" />
				</a>
			</aside>

		</div>
	</div>