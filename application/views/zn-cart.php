<script>
function validate_cart_items()
{
	if($("#proceed_cart").val() == 0)
	{
		$('#loginScreen').show();
		   javascript:location.replace('#loginScreen'); 
		return false;
	}
	else
	{
		return true;	
	}
}
</script>

<!-- Global site tag (gtag.js) - Google Ads: 798083341 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-798083341"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-798083341');
</script>

<script>
  gtag('event', 'conversion', {'send_to': 'AW-798083341/wPEgCLPzkK0BEI2Sx_wC'});
</script>

<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1065107/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

<script>
  $(document).ready(function(){
    if(window.innerWidth < 600){
        $(".mobileSection").show();
        $(".webSection").hide();
      }else{
        $(".mobileSection").hide();
        $(".webSection").show();
      }
  });

</script>


<div class="container">
   <div class="row">
      <div id="content" class="col-sm-12">
      
        
		  <div style="font-size: 19px; font-weight: normal; text-align: center; color: blue; margin-bottom: 19px;">We are being trusted by 7787 prepaid customers and have saved Rs.510 outrightly. </div>

    <?php
    
		if ($cod_availability == true) {
			?>
			<div class="left">
				<span style="padding-left:10px; font-size:14px; font-weight:normal;">
					Payment mode :  
					<input type="radio" name="paywithcod" data-siteurl="<?php echo SITE_URL;?>" value="2"  class="paywithcod" 
					<?php echo ($payment_mode == '1') ? 'checked' : '';?> onclick="chageprdAjax('<?php echo SITE_URL?>')" > Online
				</span> &nbsp; - OR - &nbsp; 
				<span style="padding-left:5px; font-size:14px; font-weight:normal;">
					<input type="radio" name="paywithcod" data-siteurl="<?php echo SITE_URL;?>" value="1"  class="paywithcod" 
					<?php echo ($payment_mode == '2') ? 'checked' : '';?> onclick="chageprdAjax('<?php echo SITE_URL?>')"> COD 
				</span>
			</div>
			<?php
				$paybtntitle = 'Order Now';
			}
		?>
         <div class='cart-error' id="cart-error" data-redirect-url="<?php echo SITE_URL?>cart" style="text-align: center; color: #337ab7; font-size: 17px; font-weight: 700; padding: 10px;"></div>
            <?php
            if(is_array($cart) && count($cart)){ 
            ?>   
            <div class="table-responsive">
               <table class="table table-bordered table-cart-block ">
                  <thead class="webSection">
                     <tr>
                        <td class="text-center">Image</td>
                        <td class="text-left">Product Name</td>
                        <td class="text-left">Quantity</td>
                        <td class="text-left">Unit Price</td>
                        <td class="text-left">Total</td>
                        <td class="text-left">Remove</td>
                     </tr>
                  </thead>
                  <tbody>
    				  <?php
    					// SET TOTAL PRICE TO ZERO
    					$total_price = 0;
    					$i = 0;
    					$cod_available = true;
    					$arr_buy_more = array();
    					$cart_prod_ids = array();
                   
                  foreach ($cart as $k => $v) {
      							
                    $cart_prod_ids[] = $v->product;
      							$cart_items .= $v->product.",";
      							// STORE SHIPPING IN ARRAY FOR FINDING MAXIMUM
      							$shipping[] = $v->shipping;
                    $cod_price  = $v->cod_price;
      							$item_price = $v->payment_mode == 1 ? $v->cash_price : $v->cod_price;
      							$total_price += $item_price * $v->quantity;
      							$total_cod_price += $cod_price * $v->quantity;

      							if ($v->cod_availability == '0') {
      								$cod_available = false;
      							}
    							
    							?>
						
			  
				  
                     <tr class="webSection">
                        <td class="text-center"> 
						
                           <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl ?>">

						              <img src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_120_150_PATH.$v->image) ?>" width="60" title="<?php echo $v->name;?>" alt="<?php echo $v->name;?>" class="img-thumbnail"/>
						            </a>
                        </td>
                        <td class="text-left"><?php echo '<a href="'.SITE_URL.PD_PAGE."/".$v->seourl.'" title="'.$v->name.'">'.$v->name.'</a>' ?>
                        </td>
                        <td class="text-left">
                           <div class="input-group btn-block" style="max-width: 200px;">
                            <div class="input-group spinner">
							             <input type="text" class="form-control small-textbox crt-quantity only_numeric upd-quantity" data-lnum="<?php echo $i;?>" id="quantity_<?php echo $i?>" value="<?php echo $v->quantity;?>" data-cur-qty="<?php echo $v->quantity;?>" data-url="<?php echo SITE_URL?>" data-lum="<?php echo $i?>" data-oid="<?php echo $v->order_id?>" data-cpid="<?php echo $v->cp_id?>" />
							        
                              </div>
                           </div>
						              <input type="hidden" disabled="disabled" id='iscombo_<?php echo $i;?>' value="<?php echo $v->is_combo;?>"/>
                           <input type="hidden" disabled="disabled" id='product_id<?php echo $i;?>' value="<?php echo $v->product;?>"/>
                        </td>
                        <td class="text-left"> 

                           <span style="color: #4fbe13;"> <b>Rs. <?php  echo ($item_price);?></b> </span> &nbsp; 
                           <?php  
                                 $disnt = $v->mrp - $item_price;
                                 
                                 $mrp_sale_per = ($disnt/$v->mrp)*100 ;         
                            ?>
                           <small> <span > <del> <?php echo ($v->mrp);?> </del> </span> &nbsp;<span > Save Rs. <?php echo $disnt ?> &nbsp;( <?php echo round($mrp_sale_per, 1)?>%)</span> </small> 
                          <?php ?>
                                                  
                        </td>

                        <td class="text-left"><?php echo _r($item_price * $v->quantity);?></td>
                        <td class="text-left">
                           
						          <span class="input-group-btn btn-remove-block">
						          <a style="background: #4fbe13;color: #ffffff;padding: 5px;margin: 3px;" class="del-item btn" data-lum="<?php echo $i?>" data-oid="<?php echo $v->order_id?>" data-cpid="<?php echo $v->cp_id?>" data-url="<?php echo SITE_URL?>" href="javascript:void(0)"><i class="fa fa-times"></i></a></span>
						   
                        </td>
                     </tr>



                     <!-- MOBILE VIEW START-->




                     <div  class="row mobileSection" style="padding: 15px; padding-top: 0px; background-color: #dbefd5; display: ">
                         <span class="input-group-btn btn-remove-block">
                             <a style="background: #4fbe13;color: #ffffff;padding: 5px;margin: 3px; float: right" class="del-item btn" data-lum="<?php echo $i?>" data-oid="<?php echo $v->order_id?>" data-cpid="<?php echo $v->cp_id?>" data-url="<?php echo SITE_URL?>" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                         </span>          ​
                         <div class="col-sm-12" style="padding: 0; position: relative; top: -10px;">
                            <div class="col-sm-8" style="width: 70%; display: inline-block; padding-left: 10px;">
                                  <?php echo '<a href="'.SITE_URL.PD_PAGE."/".$v->seourl.'" title="'.$v->name.'">'.$v->name.'</a>' ?>
                            </div>
                            <div class="col-sm-4" style="width: 30%; display: inline; padding: 0;">
                               <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl ?>">
                                <img src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_120_150_PATH.$v->image) ?>" width="60" title="<?php echo $v->name;?>" alt="<?php echo $v->name;?>" class="img-thumbnail"/>
                              </a>
                            </div>
             
                         </div>
          ​
                         <div class="col-sm-12" style="padding: 0; position: relative; top: -10px;">
                            <div class="col-sm-7" style="width: 60%; display: inline-block; padding-left: 10px;">
                               <span style="color: #4fbe13;"> <b>Rs. <?php  echo ($item_price);?></b> </span> &nbsp; <small>
                                  <br>
                                  <span> <del> Rs. <?php echo ($v->mrp);?> </del> </span> &nbsp;<span> Save Rs. <?php echo $disnt ?> &nbsp;(<?php echo round($mrp_sale_per, 1)?>%)</span> </small> 
                            </div>
                            <div class="col-sm-5" style="width: 19%; display: inline-block; padding: 0; right: -36px;">
                              <input type="text" class="form-control small-textbox crt-quantity only_numeric upd-quantity" data-lnum="<?php echo $i;?>" id="quantity_<?php echo $i?>" value="<?php echo $v->quantity;?>" data-cur-qty="<?php echo $v->quantity;?>" data-url="<?php echo SITE_URL?>" data-lum="<?php echo $i?>" data-oid="<?php echo $v->order_id?>" data-cpid="<?php echo $v->cp_id?>" />
                            </div>
                         </div>
          ​
                      </div>

                      <!-- MOBILE VIEW END-->






					     <?php
					     }?>
            
                  </tbody>
               </table>
            </div>
            <?php
            }else{
            ?>   
                  
                  <div class="bread-crumb mb-30 center-xs">
                     <div class="container">
                        <div class="page-title" style="margin-left: 20px;">
                           Shopping Cart is empty
                        </div>
                        <div class="bread-crumb-inner right-side float-none-xs">
                           <ul>
                              <li>
                                 <a href="<?php echo SITE_URL?>">
                                    <i class="fa fa-home"></i>
                                 </a>
                              </li>
                              <li>
                                 <a href="<?php echo SITE_URL?>cart">Shopping Cart</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="container">
                     <div class="row">
                        <div id="content" class="col-sm-12">
                              <div class="empty-cart text-center">
                                    <div class="cart-icon">
                                          <img src="<?php echo SITE_URL?>cdn/image/shoppingbag.png" class="img-responsive">
                                    </div>
                                    <h4>Your Shopping Bag is empty</h4>
                                    <a href="<?php echo SITE_URL?>search?c=1" class="btn-link">Shop Now</a>
                              </div>
                        </div>
                     </div>
                  </div>
                  

            
            <?php
             } ?>

            <?php if(is_array($cart) && count($cart) ){ ?>



         <div class="row">
            <div class="col-sm-4 pull-left secure-pay">
               <img alt="" src="image/secure-pay.png" class="img-responsive" />
            </div>
            <div class="col-sm-4 pull-right">
            
            <?php 
                if( in_array('6', $cart_prod_ids) && $payment_mode == 1){ 

                    if($this->session->userdata('promocode') != ""){ 
                     
                     echo "<div class='promocode_box'><span>Promo : <span class='applied'>".strtoupper($this->session->userdata('promocode'))." </span> Applied</span></div>";

                    }else{ 
              ?>

              <!-- PROMOCODE --> 
              <form id="promocode_div" action="javascript:void(0)" onsubmit="apply_promo('<?php echo SITE_URL?>')" class="promocode_box">
                 <span>Promo Code</span>
                 <input type="text" name="promocode" id="promocode" value="<?php echo $this->session->userdata('promocode'); ?>">
                 <input type="hidden" name="payment_type" id="payment_type" value="<?php echo $payment_mode ?>">
                 <input type="submit" name="promocode" id="promocode" value="Apply">
                  
              </form>

          <?php } 

              }?>









               <table class="table table-bordered total-cart-table">
                  <tr>
                     <td class="text-right"><strong>Cart Total :</strong></td>
                     <td class="text-right"><?php echo _r($total_price);?></td>
                  </tr>
                  <?php 
                  if($v->payment_mode == 1){ ?>
                    <tr>
                     <td class="text-right"><strong>Discount(15%) :</strong></td>
                     <td class="text-right" style="text-decoration: line-through;"><?php echo _r($total_cod_price - $total_price);?></td>
                  </tr>
                  <?php 
                  } ?>
                  <tr>
                     <td class="text-right"><strong>Shipping :</strong></td>
                     <td class="text-right"><?php echo $final_shipping == 0 ? Rs. "0" : _r($final_shipping);?></td>
                 </tr>
                  <tr>
                     <?php $final_price = ($total_price + $final_shipping);?>
					           <td class="text-right"><strong>Grand Total:</strong></td>
                     <td class="text-right"><?php echo _r($final_price); ?></td>
                  </tr>
               </table>
            </div>
         </div>
        

         <div class="buttons clearfix">
            
            <!--<div class="pull-left"><a href="<?php echo SITE_URL?>" class="btn btn-black">Continue Shopping</a></div>-->
            <?php $form_action = ($payment_mode == '2') ? SITE_URL.'codconfirm/confirmorder' : SITE_URL.'payment/index'; ?>
      			<?php //$form_action = SITE_URL.'payment/index'; ?>
      		<form action="javascript:cart_redirect('<?php echo $form_action;?>', '<?php echo SITE_URL;?>')" method="post" onsubmit="javascript:return validate_cart_items();" class="cartpaymentform" data-siteurl="<?php echo SITE_URL?>">
      				<?php
      					if ($user_points > 0 && $is_product == true ) {
      						$redeem_0_1 = ($redeem_credit == '1') ? $redeem_credit : '0'; 
      					} else {
      						$redeem_0_1 = 0;
      					}
      				?>
      				<input type="hidden" name="redeem_point" id="redeem_point" value="<?php echo $redeem_0_1; ?>" />
      				<input type="submit" name="submit" value="<?php echo "Checkout";//$paybtntitle;?>" id="checkout" class="pull-right chkout-btn btn btn-black"/>
			  </form>
         </div>

      <?php 
         }
         ?>

      </div>
   </div>
</div>

<?php 
                      
  if(isset($pixel_3rd_party)) {
      echo '<div id="netpubscript" style="display:none">'.$pixel_3rd_party.' </div>';
  }else{
    echo '<div id="netpubscript" style="display:none">No pixel</div>';
  }
  ?>