<?php $all_array = all_arrays(); ?>
<div class="container"> 
  <div class="row">
      
        <div id="content" class="col-sm-12 product-details-cat">      
    
      <h2 class="search-title-big">Search <span title="<?php echo $this->input->get('q');?>"><?php echo ucfirst(strtolower(trim($this->input->get('q')) != '' ? ": ".sub_string(trim($this->input->get('q')), 60) : ''));?></span></h2>
           
      <div class="shorting mb-30">
        <div class="row">
          <div class="col-md-6">
            <!--<div class="view">
              <button type="button" id="grid-view" class="btn btn-default"  title="Grid"></button>
              <button type="button" id="list-view" class="btn btn-default"  title="List"></button>            
            </div>-->
            <!--<div class="short-by float-right-sm">    
              <span>Sort By:</span>        
              <div class="select-item">
                <select id="input-sort" onchange="location = this.value;">
      					<option value="#" selected="selected">Default</option>
      						  <option value="#">Name (A - Z)</option>
      						  <option value="#">Name (Z - A)</option>
      						  <option value="#">Price (Low &gt; High)</option>
      						  <option value="#">Price (High &gt; Low)</option>
      						  <option value="#">Rating (Highest)</option>
      						  <option value="#">Rating (Lowest)</option>
      						  <option value="#">Model (A - Z)</option>
      						  <option value="#">Model (Z - A)</option>
      				</select>
				<?php
                $sort_var = "";
                if(isset($_REQUEST['_sort'])){
                    $sort_var = $_REQUEST['_sort'];
                }
				echo custom_dropdown('sort_by', 'sort_by', $all_array['ARR_SORTBY'], $sort_var,'class="sort-select"',true); ?>
                
              </div>
            </div>-->
          </div>        
          <div class="col-md-6">
            <!--<div class="show-item right-side float-left-sm">      
              <span>Show:</span>     
              <div class="select-item">
                <select id="input-limit" onchange="location = this.value;">
				<option value="#" selected="selected">9</option>
						<option value="#">25</option>
						<option value="#">50</option>
						<option value="#">75</option>
						<option value="#">100</option>
				</select>
              </div>
              
            </div>  -->
          </div>
        </div>
      </div>
      <div class="row">
          <?php
		  $total_result = count($pagination['result']);
			foreach ($pagination['result'] as $k => $v) {
				normal_product_list_search($v, $i);
				$total_result--;
			}
			?>
		  
		  
		
			
        </div>
        <div class="toolbar toolbar-products">
        <div class="col-sm-6 text-left page-counts"></div>
        <div class="col-sm-6 text-right page-results">Showing 1 to 1 of 1 (1 Pages)</div>
        </div>
        </div>
      </div>
</div>
    
    
<script type="text/javascript"><!--
    $('#button-search').bind('click', function() {
        url = 'index.php?route=product/search';

        var search = $('#content input[name=\'search\']').prop('value');

        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');

        if (category_id > 0) {
            url += '&category_id=' + encodeURIComponent(category_id);
        }

        var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

        if (sub_category) {
            url += '&sub_category=true';
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');

        if (filter_description) {
            url += '&description=true';
        }

        location = url;
    });

    $('#content input[name=\'search\']').bind('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#button-search').trigger('click');
        }
    });

    $('select[name=\'category_id\']').on('change', function() {
        if (this.value == '0') {
            $('input[name=\'sub_category\']').prop('disabled', true);
        } else {
            $('input[name=\'sub_category\']').prop('disabled', false);
        }
    });

    $('select[name=\'category_id\']').trigger('change');
    --></script>
