<div class="side-wrapper left <?php //echo ($cur_controller == 'search') ? 'jvsidebar' : ''; ?>">
 <?php
 	// ECHO ALL CATEOGRIES
	//echo $category;
		
	if ($cur_controller == 'search') {
	  
	  // IF CATEGORY EXIST THEN SHOW FILTER - START
	  if (isset($search_sub_category) && $search_sub_category->num_rows() > 0){
		  ?>
          	<div class="category-wrapper">
            <div class="category-container">
              <div class="entity-header">Sub Category</div>
              <div class="entity-container">
                <div class="entity-search">
                  <input type="text" id="search_sub_category" class="entity-search-txt" placeholder="Search in Sub Category" />
                </div>
                <div class="category-container-inner">
                  <?php
                            if(isset($_GET['sc']) && trim($_GET['sc']) != "") {
                                $arr_sub_category = explode('|',$_GET['sc']);
                            }
                            foreach($search_sub_category->result() as $sc){
                                $check = (isset($arr_sub_category) && is_array($arr_sub_category) && in_array($sc->cat_id, $arr_sub_category))  ? $check = "checked" : "";
                                ?>
                  <div class="genre-entity sub-category-list" data-value="<?php echo $sc->name; ?>">
                    <div class="left entity-chk">
                      <input <?php echo $check;?> type="checkbox" class="subcategory-chkbox-sidebar" value="<?php echo $sc->cat_id; ?>" name="cat_id[]" id="cat_id[]" />
                    </div>
                    <div class="left entity-chk-desc"><?php echo ucfirst(strtolower($sc->name)); ?></div>
                    <div class="clr"></div>
                  </div>
                  <?php
                            }
                        ?>
                </div>
              </div>
            </div>
          </div>
  		  <?php
	   }
	  // IF CATEGORY EXIST THEN SHOW FILTER - END
	  
	  
	  // IF BRAND EXIST THEN SHOW FILTER - START
	  if($search_brand != "") {
		  
		 if($search_brand->num_rows() > 0){
		 	?>
            <div class="category-wrapper">
            <div class="category-container">
                <div class="entity-header">Brand</div>
                <div class="entity-container">
                    <div class="entity-search"><input type="text" id="search_brand" class="entity-search-txt" placeholder="Search in Brand" /></div>
        			<div class="category-container-inner">
					<?php
						if(isset($_GET['b']) && trim($_GET['b']) != "") {
							$arr_brand = explode('|',$_GET['b']);
						}
						foreach($search_brand->result() as $b){
							$check = (isset($arr_brand) && is_array($arr_brand) && in_array($b->brand_id, $arr_brand))  ? $check = "checked" : "";
							?>
							<div class="genre-entity brand-list" data-value="<?php echo $b->name; ?>">
								<div class="left entity-chk">
								<input <?php echo $check;?> type="checkbox" class="brand-chkbox-sidebar" value="<?php echo $b->brand_id; ?>" name="brand[]" id="brand[]" /></div>
								<div class="left entity-chk-desc"><?php echo ucfirst(strtolower($b->name)); ?></div>
								<div class="clr"></div>
							</div>
							<?php
						}
					?>
                    </div>
					</div>
            	</div>
    		</div>
			<?php
		 }
	  
	  }
	  // IF BRAND EXIST THEN SHOW FILTER - END
	  
	  // PRICE RANGE START
	  if($price_range != "") {
		 if($price_range->num_rows() > 0){
			foreach ($price_range->result() as $pr) {				
				$min_price = ($pr->min_sale_price > 0) ? ($pr->min_sale_price < $pr->min_mrp)  ? $pr->min_sale_price : $pr->min_mrp : $pr->min_mrp;
				$max_price = ($pr->max_sale_price > 0) ? ($pr->max_sale_price < $pr->max_mrp)  ? $pr->max_sale_price : $pr->max_mrp : $pr->max_mrp;
			}
			
			if (($max_price - $min_price) > 0) {
				?>
                <div class="category-wrapper">
                    <div class="category-container">
                        <div class="entity-header">Price</div>
                            <div class="entity-container">
                                <div class="category-container-inner">
                                <?php
                                    generate_smart_price($min_price, $max_price, (isset($_GET['p'])) ? $_GET['p'] : '');
                                ?>
                                </div>
                            </div>
                    </div>
                </div>
				<?php
			}
		 }
	  
	  }
	  // PRICE RANGE END
	  
	  
	// IF CATEGORY EXIST THEN SHOW FILTER - START
	if($search_category->num_rows() > 0){
		?>
		<div class="category-wrapper">
		<div class="category-container">
			<div class="entity-header">Category</div>
			<div class="entity-container">
				<div class="entity-search"><input type="text" id="search_category" class="entity-search-txt" placeholder="Search in Category" /></div>
				<div class="category-container-inner">
				<?php
					if(isset($_GET['c']) && trim($_GET['c']) != "") {
						$arr_category = explode('|',$_GET['c']);
					}
					foreach($search_category->result() as $c){
						$check = (isset($arr_category) && is_array($arr_category) && in_array($c->cat_id, $arr_category))  ? $check = "checked" : "";
                        ?>
						<div class="genre-entity category-list" data-value="<?php echo $c->name; ?>">
							<div class="left entity-chk"><input <?php echo $check;?> type="checkbox" class="category-chkbox-sidebar" value="<?php echo $c->cat_id; ?>" name="cat_id[]" id="cat_id[]" /></div>
							<div class="left entity-chk-desc"><?php echo ucfirst(strtolower($c->name)); ?></div>
							<div class="clr"></div>
						</div>
						<?php
					}
				?>
				</div>
				</div>
			</div>
		</div>
		<?php
	 }
	 // IF CATEGORY EXIST THEN SHOW FILTER - END
	  
	// IF COLOR EXIST THEN SHOW FILTER - START
	  if(isset($search_color) && $search_color != "") {
		  
		 if($search_color->num_rows() > 0){
			?>
			<div class="category-wrapper">
			  <div class="category-container">
				<div class="entity-header">Color</div>
				<div class="entity-container">
				  <div class="entity-search">
					<input type="text" id="search_color" class="entity-search-txt" placeholder="Search in Color" >
				  </div>
				  <div class="category-container-inner">
					<?php
						if(isset($_GET['clr']) && trim($_GET['clr']) != "") {
							$arr_color = explode('|',$_GET['clr']);
						}
						foreach($search_color->result() as $clr){
							$check = (isset($arr_color) && is_array($arr_color) && in_array($clr->color_id, $arr_color))  ? $check = "checked" : "";
							?>
							<div class="genre-entity color-list" data-value="<?php echo $clr->color_name; ?>">
							  <div class="left entity-chk">
								<input <?php echo $check;?> type="checkbox" class="color-chkbox-sidebar" value="<?php echo $clr->color_id; ?>" name="color[]" id="color[]">
							  </div>
							  <div class="color-box left" style="background:#<?php echo $clr->color_code?>"></div>
							  <div class="left entity-chk-desc"><?php echo ucfirst(strtolower($clr->color_name)); ?></div>
							  <div class="clr"></div>
							</div>
							<?php
						}
					?>
				  </div>
				</div>
			  </div>
			</div>
	
			<?php
		 }
	  }
	
		// IF GENRE EXIST THEN SHOW FILTER - START
	if($search_size != "") { 
	  if($search_size->num_rows() > 0){
		?>
		<div class="category-wrapper">
		<div class="category-container">
			<div class="entity-header">Size</div>
			<div class="entity-container">
				<div class="entity-search"><input type="text" id="search_size" class="entity-search-txt" placeholder="Search in Size" /></div>
				<div class="category-container-inner">
				<?php
					if(isset($_GET['sz']) && trim($_GET['sz']) != "") {
						$arr_size = explode('|',$_GET['sz']);
					}
					foreach($search_size->result() as $sz){
						$check = (isset($arr_size) && is_array($arr_size) && in_array($sz->genreid, $arr_size))  ? $check = "checked" : "";
						?>
						<div class="genre-entity size-list" data-value="<?php echo $sz->genre_name; ?>">
							<div class="left entity-chk">
							<input <?php echo $check;?> type="checkbox" class="size-chkbox-sidebar" value="<?php echo $sz->genreid; ?>" name="genreid[]" id="genreid[]" /></div>
							<div class="left entity-chk-desc"><?php echo strtoupper($sz->genre_name); ?></div>
							<div class="clr"></div>
						</div>
						<?php
					}
				?>
				</div>
				</div>
			</div>
		</div>
		<?php
	 }
	
	}
	// IF GENRE EXIST THEN SHOW FILTER - END

	}
	
	if ($cur_controller != 'search') {
	  
	  	// SELECT SETTINGS
		$setting_sidebar_data = $this->memcache_driver->get_cache('sidebar_setting_sidebar_data');
		if (!$setting_sidebar_data) { 
			$setting_sidebar = $this->db->query("SELECT attr_name, attr_value FROM ".SETTINGS." where attr_name in ('homepage_banner_1', 'homepage_banner_1_link', 'homepage_banner_2', 'homepage_banner_2_link')");
			if($setting_sidebar->num_rows() > 0) {
				foreach ($setting_sidebar->result() as $val_setting_sidebar) {
					$setting_sidebar_data[$val_setting_sidebar->attr_name] = $val_setting_sidebar->attr_value;
				}
				$this->memcache_driver->set_cache('sidebar_setting_sidebar_data', $setting_sidebar_data, true, CACHE_LONGTIME);
			}
		} else {
			$setting_sidebar_data = $setting_sidebar_data;
		}
		
	  ?>
      <!--<div class="banner-wrapper">
        <div class="small-banner-wrap">
            <dl class="small-banner-wrap">
                <dt><a href="<?php echo urldecode($setting_sidebar_data['homepage_banner_1_link']);?>"><img src="<?php echo S3_URL.CONTENT_DIR."/images/".$setting_sidebar_data['homepage_banner_1'];?>" width="250" height="260" /></a></dt>
                <dt class="img"><a href="<?php echo urldecode($setting_sidebar_data['homepage_banner_2_link']);?>"><img src="<?php echo S3_URL.CONTENT_DIR."/images/".$setting_sidebar_data['homepage_banner_2'];?>" width="250" height="260" /></a></dt>
            </dl>
        </div>
      </div>-->
      <?php
	}
	?>
</div>
<div id="gototopbg"><div class="gototop"></div></div>