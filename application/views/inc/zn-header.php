
<body class="common-home">
   <header>
      <div class="header-middle">
         <div class="header-top">
            <div class="container">
               <div class="row">
                  <div class="col-md-3 col-sm-12">
                     <div id="logo">
                        <a href="<?php echo SITE_URL?>"><img src="<?php echo S3_URL.CONTENT_DIR?>image/catalog/nutratimes-logo.png" title="Nutratimes" alt="Nutratimes" class="img-responsive" /></a>
                     </div>
                  </div>
                  <div class="col-md-9 col-sm-12">
                     <div class="full-block">
                        <div class="header-right-part float-none-sm col-lg-8 col-sm-8 col-xs-12">
                           <div class="search-responsive float-none-sm">
                              <div class="top-search-bar">
                                 <div id="header-right-search">
                                    <div class="main-search">
                                       <div class="header_search_toggle desktop-view">
									   <form name="search-form" id="search-form" method="get" action="<?php echo SITE_URL?>search">
										  <div class="search-box">
                                             
										  <input type="text" placeholder="Search entire store here..." class="input-text" name="q" id="main-master-search" autocomplete="off" value="<?php echo $this->input->get('q') != "" ? stripslashes(str_replace('"', "\'",$this->input->get('q'))) : "";?>"/>
										  <input type="hidden" value="<?php echo SITE_URL?>" name="url" id="siteurl" />
										  <input type="hidden" value="<?php echo $this->input->get('q') != "" ? stripslashes(str_replace('"', "\'",$this->input->get('q'))) : "";?>" name="s_q" id="s_q" />
										  <!--<div id="product-suggestion"></div>-->
										  <input type="submit" name="s" id="search-submit" value="" class="search-btn" />
										</div>
										</form>
                                          
                                       </div>
                                    </div>
                                 </div>
                                 <script type="text/javascript">
                                    $('#header-right-search .main-search button.search-btn').bind('click', function() {
                                    	url = 'index.php?route=product/search';
                                    	
                                    	var search = $('#header-right-search input[name=\'search\']').prop('value');
                                    
                                    	if (search) {
                                    		url += '&search=' + encodeURIComponent(search);
                                    	}
                                    
                                    	var category_id = $('#header-right-search select[name=\'category_id\']').prop('value');
                                    
                                    	if (category_id > 0) {
                                    		url += '&category_id=' + encodeURIComponent(category_id);
                                    	}
                                    	
                                    		url += '&sub_category=true';
                                    		url += '&description=true';
                                    
                                    //alert (url);
                                    	location = url;
                                    });
                                    
                                    $('#header-right-search input[name=\'search\']').bind('keydown', function(e) {
                                    	if (e.keyCode == 13) {
                                    		$('#header-search .main-search button.search-btn').trigger('click');
                                    	}
                                    });
                                    
                                 </script>
                              </div>
                           </div>
                        </div>
                        <div class="block-rightaccount col-lg-4 col-sm-4 col-xs-12">
                           <div id="cart" class="btn-group btn-block">
                              <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                                 <!-- <i class="fa fa-shopping-cart"></i>  -->
                                 <span class="icon"></span>
                                 <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                 <span id="cart-total">
                                    <small class="cart-notification">
                                    <?php
									   $cart_item = 0;
                                       if($_SESSION[USER_ORDER_ID] != "" && $_SESSION[SVAIZA_USER_ID] != ''){
                                       			$this->db->cache_off();
                                       			$cart_result = $this->db->query("SELECT
                                                                                    o.user,        o.name,        o.email,    o.mobile,
                                                                                    o.address,     o.city,        o.state,    o.alternate_phone,
                                                                                    o.pincode,     o.id order_id, up.product,    up.quantity,   
                                                                                    up.size,    up.free_prod,  up.cod_price,  up.cash_price, 
                                                                                    up.shipping,   up.contest,    up.datecreated,   up.id cp_id,
                                                                                    p.name,        p.model_no,    p.seourl,      pi.image,   
                                                                                    g.genre_id,    g.genre_name,  p.cod_availability, o.payment_mode,
                                                                                    p.is_combo,    p.mrp,         p.sale_price,   p.combo_product,  
                                                                                    up.is_combo as user_combo,     up.combo_product as user_combo_prouct
                                                                                   FROM
                                                                                    ".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
                                                                                    left join ".GENRE." g on (up.size = g.genre_id),
                                                                                    ".PRODUCT." p left join ".PRODUCT_IMAGE." pi on (p.prod_id = pi.prod_id)
                                                                                   WHERE
                                                                                    o.id = ".$_SESSION[USER_ORDER_ID]." and 
                                                                                    up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
                                                                                    up.product = p.prod_id and 
                                                                                    up.status = 1 and 
                                                                                    p.is_verified = 1 and 
                                                                                    p.status = 1");
																				

                                       			$cart_item = $cart_result->num_rows();
												            echo ($cart_item == 0 ? " " : $cart_item." - ");
                                       		} else {
                                       			//echo "0";
                                       		}
                                       	?>
                                     </small> 
                                    <div class="cart-total hidden-xs">
                                       Items
                                       <div class="cart-total-price"></div>
                                    </div>
                                 </span>
                              </button>
                              
                              <ul class="dropdown-menu pull-right">

                                	<?php
										if($cart_item != 0){
											foreach($cart_result->result() as $v) {
												$cart_prod_ids[] = $v->product;
												$cart_items .= $v->product.",";
												// STORE SHIPPING IN ARRAY FOR FINDING MAXIMUM
												$shipping[] = $v->shipping;
												$item_price = $v->payment_mode == 1 ? $v->cash_price : $v->cash_price;
												$total_price += $item_price * $v->quantity;
                                    ?>
												
												<li>
                                    <table class="table table-striped">
                                              <tbody><tr>
                                        <td class="text-center"> 
                                          <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl ?>">
                     <img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_120_150_PATH.$v->image)?>" title="<?php echo $v->name;?>" alt="<?php echo $v->name;?>" class="img-thumbnail"/></a>
                                          </td>
                                        <td class="text-left" style="font-size: 13px">
                                          <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl ?>"><?php echo $v->name?></a>
                                          
                                          
                                            <div class="cart-pro-detail">x <?php echo $v->quantity?>   </div>
                                          <div class="cart-pro-detail"><?php echo _r($v->cash_price)?> </div>
                                        </td>
                                        <!-- <td class="text-right"></td>
                                        <td class="text-right"></td> 
                                        <td class="text-center"><button type="button" onclick="cart.remove('21');" title="Remove" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>-->
                                      </tr>
                                        </tbody></table>
                                  </li>
                                 <?php } ?> 
                                  <li>
                                    <div>
                                      <table class="table table-bordered">
                                          <tbody><tr>
                                          <td><strong>Sub-Total</strong></td>
                                          <td><?php echo _r($total_price)?></td>
                                        </tr>
                                          <tr>
                                          <td><strong>Total</strong></td>
                                          <td><?php echo _r($total_price ); //_r($v->cash_price*$v->quantity) ?></td>
                                        </tr>
                                          </tbody></table>
                                      <p>
                                       <!--<a href="<?php echo SITE_URL?>cart" class="btn-color btn"><strong><i class="fa fa-shopping-cart"></i> Cart</strong></a>&nbsp;&nbsp;&nbsp;-->
                                       <a href="<?php echo SITE_URL?>cart" class="btn-color btn right-side"><strong><i class="fa fa-share"></i> Cart</strong></a></p>
                                    </div>
                                  </li>
											
                                 <?php
                                 
										
                              }else{
											echo '<li><p class="text-center">Your shopping cart is empty!</p></li>';
										}
									?>									
								 
								
                              </ul>
                           </div>
                           <div id="cart" class="btn-group btn-block account-block">
                              <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                                 <!-- <i class="fa fa-shopping-cart"></i>  -->
                                 <span class="icon"></span>
                                 <span class="account-span">
                                    <i class="fa fa-user"></i> 
                                    <div class="cart-total hidden-xs">
                                       <div class="cart-total-price"></div>
                                    </div>
                                 </span>
                              </button>
                              <ul class="dropdown-menu dropdown-account pull-right">
                                 <?php
                                    if(!is_user_login($_SESSION[SVAIZA_USER_ID],
                                    				  $_SESSION[SVAIZA_MOBILE],
                                    				  $_SESSION[SVAIZA_EMAIL])) {
                                    		
                                    	if (strtolower($this->uri->segment(1)) == strtolower(PD_PAGE) && strtolower($this->uri->segment(2)) == strtolower(PD_FUNC)) {
                                    				$redirect = "javascript:scroll_div('#user_details')";
                                    	} else {
                                    		$redirect = SITE_URL.'login?action=register';
                                    	}
                                    ?>
                                 <li><a href="<?php echo SITE_URL?>register">Register</a></li>
                                 <li><a href="<?php echo SITE_URL?>login">Login</a></li>
                                 <?php 
                                    }else{
                                       ?> 		
                                 <li><a href="<?php echo SITE_URL?>profile">Myaccount</a></li>
                                 <li><a href="<?php echo SITE_URL?>logout">Logout</a></li>
                                 <?php 
                                    } ?> 
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Mobile-Menu Start -->
            <?php if($this->router->fetch_class() != "cart"){ ?>
            <div id="wrapper">
               <div class="overlay"></div>
               <nav id="sidebar-wrapper" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                  <ul class="nav navbar-nav sidebar-nav">
                     <li><a href="<?php echo SITE_URL?>">Home</a></li>
                     <li><a href="<?php echo SITE_URL?>/how-it-work">How it Works</a></li>
                     <li class="dropdown">
                        <a href="<?php echo SITE_URL?>products" class="dropdown-toggle">All Products</a>
                        
                     </li>
                     <li><a href="<?php echo SITE_URL?>garcinia-benefits">Garcinia Benefits</a></li>
                     <li><a href="<?php echo SITE_URL?>healthy">healthy</a></li>
                     <li><a href="<?php echo SITE_URL?>garcinia-cambogia-ingredients">ingredients</a></li>
                     <li><a href="<?php echo SITE_URL?>garcinia-cambogia-testimonials">testimonials</a></li> 
                     <li><a href="<?php echo SITE_URL?>blog">Blog</a></li> 
                  </ul>
               </nav>
               <div id="page-content-wrapper">
                  <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                  <span class="hamb-top"></span>
                  <span class="hamb-middle"></span>
                  <span class="hamb-bottom"></span>
                  </button>
               </div>
            </div>
          <?php } ?>
            <!-- --Mobile-Menu End ----> 
         </div>
      </div>
      
      <?php if($this->router->fetch_class() != "cart"){ ?>
      <div class="header-bottom ">
         <div class="container">
            <!-- Desktop-Menu Start ---->
            <nav id="menu" class="navbar col-sm-12">
               <div class="navbar-header"><span id="category" class="visible-xs">Menu</span>
                  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
               </div>
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                  <ul class="nav navbar-nav">
                     <li><a href="<?php echo SITE_URL?>">Home</a></li>
                     <li><a href="<?php echo SITE_URL?>how-it-work">How it Works</a></li>
                     <li class="dropdown">
                        <a href="<?php echo SITE_URL?>products" class="dropdown-toggle">All Products</a>
                        
                     </li>
                     <li><a href="<?php echo SITE_URL?>garcinia-benefits">Garcinia Benefits</a></li>
                     <li><a href="<?php echo SITE_URL?>healthy">healthy</a></li>
                     <li><a href="<?php echo SITE_URL?>garcinia-cambogia-ingredients">ingredients</a></li>
                     <li><a href="<?php echo SITE_URL?>garcinia-cambogia-testimonials">testimonials</a></li> 
                     <li><a href="<?php echo SITE_URL?>blog">Blog</a></li>
                  </ul>
               </div>
            </nav>
            <!-- Desktop-Menu End -->
         </div>
      </div>
    <?php } ?>

      </div>
   </header>