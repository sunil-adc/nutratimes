<footer class="footer">
	<div class="footer-inner">
		<div class="footer-top">
			<div class="container">
				<div class="footer-top-outer">
				</div>
				<div class="footer-middle mtb-60">
					<div class="row">

						<div class="col-md-3 f-col">
							<div class="footer-static-block">
								<span class="opener plus"></span>
								<div class="footer-static-block">
									<span class="opener plus"></span>
									<div class="f-logo">
										<a class="" href="<?php echo SITE_URL?>">
											<img src="<?php echo S3_URL.CONTENT_DIR?>image/logo-white.png" class="img-responsive"></a>
									
									</div>
									<ul class="footer-block-contant address-footer">
										<li class="item">
											<i class="fa fa-map-marker"></i>
											<p>#565, Axis Pennmark, 4th Floor,<br>
											 JP Nagar 3rd Phase, 9th Cross, Near Sony Center, <br>
											 Bengaluru-560078
											</p>
										</li>
										<li class="item">
											<i class="fa fa-envelope-o"></i>
											<p>
												<a>support@nutratimes.com</a>
											</p>
										</li>
										<li class="item">
											<i class="fa fa-phone"></i>
											<p>08046328320</p>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-3 f-col">
							<div class="footer-static-block">
								<span class="opener plus"></span>
								<h3 class="main_title title">INFORMATION</h3>
								<ul class="list-unstyled footer-block-contant">
									<li><a href="<?php echo SITE_URL?>about-us">About us</a></li>
									<li><a href="<?php echo SITE_URL?>privacy-policy">Privacy Policy</a></li>
									<li><a href="<?php echo SITE_URL?>disclaimer-policy">Disclaimer Policy</a></li>
									<li><a href="<?php echo SITE_URL?>terms-and-conditions">Terms & Conditions</a></li>
									<li><a href="<?php echo SITE_URL?>cancellation-policy">Cancellation Policy</a></li>
									<li><a href="<?php echo SITE_URL?>refund-policy">Refund Policy</a></li>
									<li><a href="<?php echo SITE_URL?>shipping-policy">Shipping Policy</a></li>
									<li><a href="<?php echo SITE_URL?>return-policy">Return Policy</a></li>
									<li><a href="<?php echo SITE_URL?>contact-us">Contact Us</a></li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-3 f-col">
							<div class="footer-static-block">
								<span class="opener plus"></span>
								<h3 class="main_title title">Extras</h3>
								<ul class="list-unstyled footer-block-contant">
									<li><a href="<?php echo SITE_URL?>sitemap">Sitemap</a></li>
								</ul>
								<div class="footer_social  pt-xs-15 mt-xs-15">
									
									<h3 class="title">Follow Us</h3>
									  <ul class="social-icon">
									    <li><a href="https://www.facebook.com/nutratimesofficial/" target="_blank" class="facebook" title="Facebook"><i class="fa fa-facebook"> </i></a></li>
									    <li><a href="https://twitter.com/nutratimes" class="twitter" target="_blank" title="Twitter"><i class="fa fa-twitter"> </i></a></li>
									    <li><a href="https://www.instagram.com/nutratimesofficial/" target="_blank" class="instagram" title="Insta"><i class="fa fa-instagram"> </i></a></li>
									    <li><a href="https://in.pinterest.com/nutratimes/" target="_blank" class="pinterest" title="Pinterest"><i class="fa fa-pinterest"> </i></a></li>
									  </ul>

									<h3 class="title">Payment  ON</h3>
									  
									<div class="payment left-side float-none-xs center-xs">
										<ul class="payment_icon">
											<li class="discover"><a></a>
											</li>
											<li class="visa"><a></a>
											</li>
											<li class="mastro"><a></a>
											</li>
											<li class="paypal"><a></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-3 f-col">
							<div class="footer-static-block">
								<span class="opener plus"></span>
								<h3 class="main_title title">My Account</h3>
								<ul class="list-unstyled footer-block-contant">
									<li><a href="<?php echo SITE_URL?>profile">My Account</a>
									</li>
									<li><a href="<?php echo SITE_URL?>/orderhistory">Order History</a>
									</li>  
									<br />
									<span> <img src="https://www.nutratimes.com/cdn/image/gpay_qrcode.jpg" width="150"> </span>
								</ul>


							</div>




						</div>
						<div class="col-md-3 f-col">
							<div class="footer-static-block">
								<span class="opener plus"></span>
								
								<!--<div class="newsletter-inner mb-20">
										<div class="newsletter">
											<div class="theme-newsletter newsletter-title">
												<h3 class="title">Subscribe Here</h3>
												
												<form id="newsletter-validate-detailnew" class="form subscribe" action="#" method="post">
													<div class="form-group required field newsletter">
														<div class="control newsletter-box">				
															  <input type="email" name="txtemail1" id="txtemail1" value="" placeholder="Your email address" class="form-control input-text" /> 				
															<div class="action subscribe primary">
																<button type="submit" class="btn btn-color" onclick="return subscribe1();">Go</button>  
															</div>
															
														</div>
													</div>
													
												</form>
												   
											</div>
										</div>
									</div>-->
									
								
							</div>
						</div>
					</div>
					<div class="clearfix"><br><br><hr><br></div>
					<div class="row text-center">
					© 2019 Nutratimes.com All rights reserved
					</div>
				</div>

			</div>
		</div>



	</div>

</footer>
<div class="scroll-top">
	<div id="scrollup"></div>
</div>






<div id="toTop" class="hidden-xs"></div>



<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/magnific/magnific-popup.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/component.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/carousel-arrow.css" />
<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/owl.transitions.css" />

<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/responsive.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo S3_URL.CONTENT_DIR?>css/slider.css"/>


<link href="<?php echo S3_URL.CONTENT_DIR?>css/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />

<script src="<?php echo S3_URL.CONTENT_DIR?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo S3_URL.CONTENT_DIR?>js/common.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery.cookie-1.4.1.min.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery.slider.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/slider.js"></script>

<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/modal.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo S3_URL.CONTENT_DIR?>js/customweb.js"></script>


<script src="<?php echo S3_URL.CONTENT_DIR?>js/modal.js" type="text/javascript"></script>
<script src="<?php echo S3_URL.CONTENT_DIR?>js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo S3_URL.CONTENT_DIR?>js/moment.js" type="text/javascript"></script>
<script src="<?php echo S3_URL.CONTENT_DIR?>js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


<script type="text/javascript"><!--
$('#slideshow0').owlCarousel({
	items: 3,
	autoPlay: 3000,
	singleItem: true,
	navigation: true,
	//navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: false
});
-->
</script>



</body>


</html>