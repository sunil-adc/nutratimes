<link rel="stylesheet" href="<?php echo S3_URL.CONTENT_DIR?>/css/easyzoom.css" />
<script src="<?php echo S3_URL.CONTENT_DIR?>/js/easyzoom.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
       // Start easyZoom
		var $zoom = $('#zoom-target').easyZoom({
			parent: 'div.zoom-container'
		}); 
    });
</script>