<?php
	// ECHO ALL CATEOGRIES
	
?>  



<div class="container">
    <div class="row">
  <aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
    <div class="list-group">
    <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
    <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
    <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
    <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
    <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>-->
    <a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a>
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>-->
    <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a>
  </div>
  </aside>

 <div id="content" class="col-sm-9">      
  <div class="mb-30">
      <div class="heading-bg mb-20">
       <h2 class="heading m-0">My Account</h2>
      </div> 
      <ul class="list-unstyled">
        <li><a href="<?php echo SITE_URL?>profile">Edit your account information</a></li>
        <li><a href="<?php echo SITE_URL?>changePasswordUser">Change your password</a></li>
        <li><a href="<?php echo SITE_URL?>fulladdress">Modify your address book entries</a></li>
        <!--<li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/wishlist">Modify your wish list</a></li>-->
      </ul>
    </div>
            <div class="mb-30">
      <div class="heading-bg mb-20">
       <h2 class="heading m-0">My Orders</h2>
      </div> 
      <ul class="list-unstyled">
        <li><a href="<?php echo SITE_URL?>orderhistory">View your order history</a></li>
        <!--<li><a href="<?php echo SITE_URL?>returnorders">View your return requests</a></li>-->
        <!--<li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/download">Downloads</a></li>
        <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/reward">Your Reward Points</a></li>
        <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/transaction">Your Transactions</a></li>
        <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring">Recurring payments</a></li>-->
      </ul>
      </div> 
      
      </div>
    </div>
</div>
