<?php
	$all_array = all_arrays();
	echo $category;
?>
<div class="full-wrapper"><br />
  <div class="profile-wraper">
    <div class="cart-wrapper-header">Feedback</div>
    <?php echo profile_sidebar(); ?>
    <div class="profile-right">
     <div class="credit_points">Available Credit Points : <?php echo $user_details->points;?></div>
     <div class="account-infor">Feedback</div>
      <div class="profile-form">
        <div class="form_wrp">
          <ul id="sign_up_form-account">
         
        <form action="<?php echo SITE_URL?>addfeedback/save" method="post" enctype="multipart/form-data">
              <li>
                <label>Suggestion* :</label>
                <?php echo custom_dropdown('feedback_suggestion', 'id="feedback_suggestion"', $all_array['ARR_FEEDBACK'], '1','class="txtbxx"'); ?>
              </li>
              <li>
                <label>Comments* :</label> 
				<textarea name="comments"  class="txtbxx" placeholder="Comments" ></textarea>
              </li>
              
             
              <li>
                <label>Order ID* :</label>
                <input placeholder="Order ID" class="txtbxx" type="text" id="order_id" name="order_id" />
              </li>

                 <li> <input type="submit" name="submit" value="Save" id="submit" class="profile-save-btn" /></li>
            </form>
          </ul>
        </div>
      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>