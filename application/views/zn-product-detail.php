<?php $all_array = all_arrays(); ?>
<style>
	.quick-form{background:#0d9443; padding: 20px 20px 50px}
	.quick-form h2.heading{color:white; font-size: 15px; line-height: 15px}
	 .quick-form  .mb-30{margin-bottom: 0px;}
	.quick-form .form-control{border:none; height: 30px }   
	.quick-form .form-horizontal .form-group{margin-bottom: 7px;}
	.prod-btn-color{border: none; background: #60c100; width: 100%}
</style>	
<!--<div class="bread-crumb mb-30 center-xs">
   <div class="container">
      <div id="page-title" class="page-title"><?php echo $prod_detail->name?></div>
      <div class="bread-crumb-inner right-side float-none-xs">
         <ul>
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="details.html"><?php echo $prod_detail->name?></a></li>
         </ul>
      </div>
   </div>
</div>-->
<div class="container">
   <div class="row">
      <div id="content" class="product-page col-sm-12">
         <div class="row">
            <div class="col-sm-4 mb-xs-30">
               <div class="thumbnails-image ">
                  <a title="<?php echo $prod_detail->name?>">
                  <img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_350_450_PATH.$prod_detail->image)?>" title="<?php echo $prod_detail->name;?>" alt="<?php echo $prod_detail->name;?>" data-zoom-image="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_DIR_PATH.$prod_detail->image)?>" />
                  </a>
               </div>
               <!-- Multipul Product Start -->
               <!--<div class="position-r">
                  <ul class="product-additional owl-slider">
                  <?php
				   if(isset($prod_image) && count($prod_image) > 1){
					 if(isset($prod_image)) {
						foreach ($prod_image as $v) { ?>
							 
					 <li class="image-additional" id="gallery_01">
                        <a href="javascript:void(0);" data-image="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_120_150_PATH.$v->image)?>" data-zoom-image="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_120_150_PATH.$v->image)?>" title="Emetrol">
                        <img src="<?php echo S3_URL.product_or_placeholder("./".PRODUCT_THUMB_120_150_PATH.$v->image)?>" title="Emetrol" alt="Emetrol" />
                        </a>
                     </li>
				   <?php
						}
					 }
				  } ?>
                  </ul>
               </div>-->
               <!-- Multipul Product End -->
            </div>
			
            <div class="col-sm-5">
               <div class="product-detail-main">
                  <div class="product-item-details">
                     <h1 class="product-item-name"><?php echo $prod_detail->name?></h1>
                     <div class="rating-summary-block">
                        <div class="rating mb-30">
                           <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star fa-star-half-o"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star fa-star-half-o"></i></span>
                           <span class="label-review">
                           <!--<a class="reviewCount" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo ($prod_detail->prod_id + 20) ?> reviews</a> 
                           </span>
                           <a class="reviewwrite" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Write a review</a>-->
                        </div>
                     </div>
                     <ul class="list-unstyled product-page-price">
                        <!-- <li>Ex Tax: $80.00</li>-->
                        <!---->
                     </ul>
                     <ul class="list-unstyled mb-30">
                        <li><label>Brand:</label> <a href="#">Nutratimes</a></li>
                        <li><label>Product Code:</label> <?php echo $prod_detail->model_no?></li>
                        <li><label>Availability:</label> In Stock</li>
                     </ul>
                     <hr>
                     <div id="product" class="mt-20">
                        <!--<h3>Available Options</h3>-->
                        <div class="form-group required">
                           <!-- <label class="control-label" for="input-option225">Delivery Date</label>-->
                           <!--<div class="input-group date">
                              <input type="text" name="option[225]" value="2011-04-22" data-date-format="YYYY-MM-DD" id="input-option225" class="form-control" />
                              <span class="input-group-btn">
                              <button class="btn btn-black" type="button"><i class="fa fa-calendar"></i></button>
                              </span></div>-->
                        </div>

                        <ul class="list-unstyled product-page-price">               
                          <li class="today-price">
                            <span class="price-title">Today price:</span>
                            <div class="product-price"><?php echo _r($prod_detail->sale_price) ?></div>
                          </li>
                          <li class="actual-price"> 
                              <span class="price-title">Actual  price:</span>
                              <div class="product-price"><?php echo _r($prod_detail->mrp) ?></div>
                          </li>     
                          <li class="save-price"> 
                              <span class="price-title">You save:</span>
                              <div class="product-price"><?php echo _r($prod_detail->mrp - $prod_detail->sale_price) ?></div>
                          </li>    
                        </ul>

                        <div class="form-group">
                           <!-- <label class="control-label" for="input-quantity">Qty</label>
                              <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
                              <input type="hidden" name="product_id" value="47" />-->
                           <!--<div class="input-group btn-block" style="max-width: 200px;">
                              <div class="input-group spinner">
                                 <input type="text" class="form-control" value="42">
                                 <div class="input-group-btn-vertical">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                 </div>
                              </div>
                           </div>-->
                           <br />
                           <div class="button-group main-product-btn product-item-action">
                              <div class="button-inner">
                                 <div class="hover-actions">
                                    
              										<?php 
                                  if ($prod_detail->check_inventory == '1') {
                                    if ($prod_detail->total_stock > 0 ) {
                                    ?>    
                                      <a href="javascript:add_cart('add','<?php echo $prod_detail->prod_id?>','<?php echo SITE_URL?>');scroll_div('header');" class="addtocart prod-btn-color button btn" data-sizeid="" id="product-buy-now" style="font-size: 20px; background: #0d9443; color: #fff; text-transform: uppercase; font-weight: 600; letter-spacing: 1px; padding: 18px ">

                											<?php
                                      if(is_user_login($_SESSION[SVAIZA_USER_ID],
                											  $_SESSION[SVAIZA_MOBILE],
                											  $_SESSION[SVAIZA_EMAIL])) {
                												  echo 'ADD TO CART';
                											 } else {
                												  echo 'Buy Now';
                											 }

                                       echo "</a>";
              										}else{
                                      $outofstock = true;
                                      echo 'NOT AVAILABLE';
                                      }
                                  }else{
                                      ?>
                                      <a href="javascript:add_cart('add','<?php echo $prod_detail->prod_id?>','<?php echo SITE_URL?>');scroll_div('header');" class="addtocart prod-btn-color button btn" data-sizeid="" id="product-buy-now" style="font-size: 20px; background: #0d9443; color: #fff; text-transform: uppercase; font-weight: 600; letter-spacing: 1px; padding: 18px ">
                                      <?php    
                                      if(is_user_login($_SESSION[SVAIZA_USER_ID],
                                        $_SESSION[SVAIZA_MOBILE],
                                        $_SESSION[SVAIZA_EMAIL])) {
                                        echo 'ADD TO CART';
                                      } else {
                                        echo 'Buy Now';
                                     }
                                     echo "</a>";
                                  }
                                  ?>
									             
									 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--<div class="rating">
                        <hr>
                        
                        <div class="addthis_toolbox addthis_default_style mt-30" data-url="http://aaryaweb.info/opc/healthstore/organicstore/index.php?route=product/product&amp;product_id=47"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                        
                     </div>-->
                  </div>
               </div>
            </div>
			<?php
		  	if(!is_user_login($_SESSION[SVAIZA_USER_ID],
							  $_SESSION[SVAIZA_MOBILE],
							  $_SESSION[SVAIZA_EMAIL])) {
			?>
			<div id="prod_reg_div" class="col-sm-3" style="display:none;">
               <div id="content" class="quick-form">       
      		<form method="post" action="javascript:void(0)" onsubmit="javascript:return ajaxReg('<?php echo SITE_URL?>buyregister', '');"  id="user_signup" name="user_signup" class="form-horizontal">

			<fieldset >
			<div class="heading-bg mb-30">
				<h2 class="heading m-0">Registeration Form.</h2>
			</div> 
			<div id="abosolute_reg_div" style="padding: 5px 10px; color: rgb(255, 255, 255); background: #FF9800; margin: 10px 0; font-size: 14px; text-align: center; display: none;"></div>

           <div class="form-group"> 
            <div class="col-sm-12">
              <input placeholder="Full Name" type="text" id="name" name="name" class="form-control">
            </div>
          </div> 
           <div class="form-group"> 
            <div class="col-sm-12">
            <input placeholder="Mobile" type="text" maxlength="10" id="mobile" name="mobile" class="form-control only_numeric">  
            </div>
          </div>
           <div class="form-group required"> 
            <div class="col-sm-12">
              <input placeholder="Valid Email Address"  type="text" id="email" name="email" class="form-control">
            </div>
          </div>
		   <div class="form-group required"> 
            <div class="col-sm-12">
              <input placeholder="Address" type="text" id="address" name="address" class="form-control">
            </div>
          </div>
       
           <div class="form-group required"> 
            <div class="col-sm-12">
              <input placeholder="City" type="text" id="city" name="city" class="form-control">
             </div>
          </div>
		   <div class="form-group required"> 
            <div class="col-sm-12">
              <select name="state" id="state" style="width:267 !important;" class="form-control">
					<option value="">Select State</option>
				<?php
					if (count($all_array['ARR_STATE']) > 0) {
						
						asort($all_array['ARR_STATE']);
						foreach ($all_array['ARR_STATE'] as $k => $v) {
							
							echo "<option value='".$k."' >".$v."</option>";
						}
					} else {
						echo "<option>Select Option</option>";
						echo "<option value='1'>ka</option>";
					}
				?>
				</select>
             </div>
          </div>
         
           <div class="form-group required"> 
            <div class="col-sm-12">
              <input placeholder="Pincode" maxlength="6" type="text" value="" id="pincode" name="pincode" class="form-control only_numeric"> 
             </div>
            </div> 
			<input type="hidden" name="oos" value="<?php echo ($outofstock == true) ? '1' : '0';?>" id="oos" />
			<input type="hidden" name="pid" value="<?php echo $prod_detail->prod_id?>" id="pid" />
			<input type="hidden" name="siteurl" value="<?php echo FULL_SITE_URL?>" id="siteurl" />
						
		    <input type="submit" class="addtocart prod-btn-color button btn" value="Submit" id="register_btn" /> 			
            </fieldset>
			</form>				   
            	</div>
			</div> 
			<?php }?>
			<div class="col-md-12">
               <div class="product-detail-tab pt-85">
                  <div id="tabs">
                     <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                        <!--<li><a href="#tab-specification" data-toggle="tab">Specification</a></li>-->
                        <!--<li><a href="#tab-review" data-toggle="tab">Reviews (0)</a></li>-->
                     </ul>
                  </div>
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab-description">
                        <!--<p>
                           Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you&#39;re at the office
                        </p>-->
						<?php echo $prod_detail->description; ?>
                     </div>
                     <div class="tab-pane" id="tab-specification">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <td colspan="2"><strong>Memory</strong></td>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>test 1</td>
                                 <td>16GB</td>
                              </tr>
                           </tbody>
                           <thead>
                              <tr>
                                 <td colspan="2"><strong>Processor</strong></td>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>No. of Cores</td>
                                 <td>4</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="tab-pane" id="tab-review">
                        <form class="form-horizontal" id="form-review">
                           <div id="review"></div>
                           <h2>Write a review</h2>
                           <div class="form-group required">
                              <div class="col-sm-12">
                                 <label class="control-label" for="input-name">Your Name</label>
                                 <input type="text" name="name" value="" id="input-name" class="form-control" />
                              </div>
                           </div>
                           <div class="form-group required">
                              <div class="col-sm-12">
                                 <label class="control-label" for="input-review">Your Review</label>
                                 <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                 <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                              </div>
                           </div>
                           <div class="form-group required">
                              <div class="col-sm-12">
                                 <label class="control-label">Rating</label>
                                 &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                 <input type="radio" name="rating" value="1" />
                                 &nbsp;
                                 <input type="radio" name="rating" value="2" />
                                 &nbsp;
                                 <input type="radio" name="rating" value="3" />
                                 &nbsp;
                                 <input type="radio" name="rating" value="4" />
                                 &nbsp;
                                 <input type="radio" name="rating" value="5" />
                                 &nbsp;Good
                              </div>
                           </div>
                           <div class="buttons clearfix">
                              <div class="pull-right">
                                 <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-black">Continue</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="pt-85 down-related-box">
            <div class="box featured_box">
               <div class="heading-part mb-40">
                  <h2 class="main_title">Related Products</h2>
               </div>
               <div class="box-products">
                  <div class="carousel-inner" >
                     
					 <?php
						$i = 0;
						if(is_array($related_products) && count($related_products) > 0 ){ 
							foreach ($related_products as $v) {
								
							
					?>
					 
					 <div class="product-thumb transition">
                        <div class="image">
                           <a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>">
                           <img src="<?php echo S3_URL.product_or_placeholder(PRODUCT_THUMB_200_300_PATH.$v->image);?>" alt="<?php echo $v->name?>" title="<?php echo $v->name?>" class="img-responsive" />
                           </a>
                           <div class="sale-label"><span>Sale</span></div>
                           <div class="button-group main-product-btn product-item-action">
                              <div class="button-inner">
                              </div>
                           </div>
                        </div>
                        <div class="caption">
                           <div class="name">
                              <h3><a href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>"><?php echo sub_string($v->name,45)?></a></h3>
                           </div>
                           <!-- <div class="description">
                              ...
                              </div> -->
                           <p class="price">
                              Rs <?php echo $v->sale_price ?></span> <span class="price-old"><?php echo $v->mrp;?>    
                           </p>
                           <div class="button-group main-product-btn product-item-action">
                              <div class="button-inner">
                                 <div class="hover-actions">
                                    <a style="margin: 0 auto;" class="addtocart prod-btn-color button btn"  href="<?php echo SITE_URL.PD_PAGE."/".$v->seourl;?>" data-id="<?php echo $v->prod_id;?>">Buy Now</a>
                                 </div>
                              </div>
                           </div>
                           <div class="rating-summary-block">
                              <div class="rating">
                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                 <span class="fa fa-stack"><i class="fa fa-star fa-star-half-o"></i></span>
                                 <span class="fa fa-stack"><i class="fa fa-star fa-star-o"></i></span>
                              </div>
                           </div>
                        </div>
                     </div>
					 <?php
							}
						}
						?>
					 
					 
                     <!-- relate-product-->
                  </div>
               </div>
               <!-- view-related -->
            </div>
         </div>
      </div>
   </div>
</div>
<!---
<script type="text/javascript"><!--
   $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
       $.ajax({
           url: 'index.php?route=product/product/getRecurringDescription',
           type: 'post',
           data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
           dataType: 'json',
           beforeSend: function () {
               $('#recurring-description').html('');
           },
           success: function (json) {
               $('.alert, .text-danger').remove();
   
               if (json['success']) {
                   $('#recurring-description').html(json['success']);
               }
           }
       });
   });
   
</script>
<script type="text/javascript">
   $('#button-cart').on('click', function () {
       $.ajax({
           url: 'index.php?route=checkout/cart/add',
           type: 'post',
           data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
           dataType: 'json',
           beforeSend: function () {
               $('#button-cart').button('loading');
           },
           complete: function () {
               $('#button-cart').button('reset');
           },
           success: function (json) {
               $('.alert, .text-danger').remove();
               $('.form-group').removeClass('has-error');
   
               if (json['error']) {
                   if (json['error']['option']) {
                       for (i in json['error']['option']) {
                           var element = $('#input-option' + i.replace('_', '-'));
   
                           if (element.parent().hasClass('input-group')) {
                               element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                           } else {
                               element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                           }
                       }
                   }
   
                   if (json['error']['recurring']) {
                       $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                   }
   
                   // Highlight any found errors
                   $('.text-danger').parent().addClass('has-error');
               }
   
               if (json['success']) {
                   $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
   
                   $('#cart > button').html('<span class="icon"></span><span id="cart-total">' + json['total'] + '</span>');
   
                   $('html, body').animate({ scrollTop: 0 }, 'slow');
   
                   $('#cart > ul').load('index.php?route=common/cart/info ul li');
               }
           },
           error: function (xhr, ajaxOptions, thrownError) {
               alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
           }
       });
   });
</script>
<script type="text/javascript">
   $('.date').datetimepicker({
       pickTime: false
   });
   
   $('.datetime').datetimepicker({
       pickDate: true,
       pickTime: true
   });
   
   $('.time').datetimepicker({
       pickDate: false
   });
   
   $('button[id^=\'button-upload\']').on('click', function () {
       var node = this;
   
       $('#form-upload').remove();
   
       $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
   
       $('#form-upload input[name=\'file\']').trigger('click');
   
       if (typeof timer != 'undefined') {
           clearInterval(timer);
       }
   
       timer = setInterval(function () {
           if ($('#form-upload input[name=\'file\']').val() != '') {
               clearInterval(timer);
   
               $.ajax({
                   url: 'index.php?route=tool/upload',
                   type: 'post',
                   dataType: 'json',
                   data: new FormData($('#form-upload')[0]),
                   cache: false,
                   contentType: false,
                   processData: false,
                   beforeSend: function () {
                       $(node).button('loading');
                   },
                   complete: function () {
                       $(node).button('reset');
                   },
                   success: function (json) {
                       $('.text-danger').remove();
   
                       if (json['error']) {
                           $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                       }
   
                       if (json['success']) {
                           alert(json['success']);
   
                           $(node).parent().find('input').attr('value', json['code']);
                       }
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                   }
               });
           }
       }, 500);
   });

</script>
<script type="text/javascript"><!--
   $('#review').delegate('.pagination a', 'click', function (e) {
       e.preventDefault();
   
       $('#review').fadeOut('slow');
   
       $('#review').load(this.href);
   
       $('#review').fadeIn('slow');
   });
   
   $('#review').load('index.php?route=product/product/review&product_id=47');
   
   $('#button-review').on('click', function () {
       $.ajax({
           url: 'index.php?route=product/product/write&product_id=47',
           type: 'post',
           dataType: 'json',
           data: $("#form-review").serialize(),
           beforeSend: function () {
               $('#button-review').button('loading');
           },
           complete: function () {
               $('#button-review').button('reset');
           },
           success: function (json) {
               $('.alert-success, .alert-danger').remove();
   
               if (json['error']) {
                   $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
               }
   
               if (json['success']) {
                   $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
   
                   $('input[name=\'name\']').val('');
                   $('textarea[name=\'text\']').val('');
                   $('input[name=\'rating\']:checked').prop('checked', false);
               }
           }
       });
   });
   
   $(document).ready(function () {
       $('.thumbnails').magnificPopup({
           type: 'image',
           delegate: 'a',
           gallery: {
               enabled: true
           }
       });
   });
</script>
<script type="text/javascript"><!--
   $('.rating a').click(function () {
       $('html, body').animate({
           scrollTop: $(".nav-tabs").offset().top
       }, 600);
   });
</script>

-->