

<div class="bread-crumb mb-30 center-xs">
  <div class="container">
    <div class="page-title">Site Map</div>
    <!--<div class="bread-crumb-inner right-side float-none-xs">
    <ul>
      <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=common/home"><i class="fa fa-home"></i></a></li>
            <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=information/sitemap">Site Map</a></li>
          </ul>
    </div>-->
  </div>
</div>

<div class="container">
  <div class="row">                
  	<div id="content" class="col-sm-12">       
      <div class="row">
        <div class="col-sm-6">
          <ul>
            <li><a href="<?php echo SITE_URL?>about-us">About Us</a>
                <ul>
                    <li><a href="<?php echo SITE_URL?>privacy-policy">Privacy Policy</a>
                        <ul>
                            <li><a href="<?php echo SITE_URL?>disclaimer-policy">Disclaimer Policy</a></li>
                            <li><a href="<?php echo SITE_URL?>terms-and-conditions">Terms and Conditions</a></li>
                            <li><a href="<?php echo SITE_URL?>cancellation-policy">Cancellation Policy</a></li>
                            <li><a href="<?php echo SITE_URL?>refund-policy">Refund Policy</a></li>
                            <li><a href="<?php echo SITE_URL?>shipping-policy">Shipping Policy</a></li>
                            <li><a href="<?php echo SITE_URL?>return-policy">Return Policy</a></li>
                            <li><a href="<?php echo SITE_URL?>contact-us">Contact Us</a></li>
                            <li><a href="<?php echo SITE_URL?>my-account">My Account</a></li>
                          
                          </ul>
                      </li>
                  </ul>
              </li>
          </ul>
        </div>
        <div class="col-sm-6">
          <ul>
            <li><a href="<?php echo SITE_URL?>products">Products</a></li>
            <li><a href="<?php echo SITE_URL?>garcinia-benefits">Benefits</a></li>
            <li><a href="<?php echo SITE_URL?>garcinia-cambogia-ingredients">Garcinia Cambogia Ingredients</a></li>
            <li><a href="<?php echo SITE_URL?>garcinia-cambogia-testimonials">Garcinia Cambogia Testimonials</a></li>
            <li><a href="<?php echo SITE_URL?>blog">Nutratimes Blog </a></li>
            <li><a href="<?php echo SITE_URL?>how-it-Works">How it Works</a></li>
              
          </ul>
        </div>
      </div>
      </div>
    </div>
</div>
