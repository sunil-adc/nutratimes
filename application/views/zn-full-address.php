<?php
  $all_array = all_arrays();
  // ECHO ALL CATEOGRIES  
?>


<div class="container">
    <div class="row">
  <aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
    <div class="list-group">
    <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
    <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
    <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
    <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
    <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>-->
    <!--<a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a>-->
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>-->
    <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a>
  </div>
  </aside>
    <div id="content" class="col-sm-9">         
    <form method="post" action="javascript:void(0)" onsubmit="update_address('<?php echo SITE_URL?>fulladdress/save')" class="form-horizontal">
    
           <fieldset>
          <div class="heading-bg mb-30" style="margin: 20px">
            <h2 class="heading m-0">Address Book</h2>
            <div id="address_div_err" style="margin-top: 15px;color: red;font-size: 14px; font-weight: 600"></div> 
          </div> 
           
                    
            <div class="form-group required"> 
            <label class="col-sm-2 control-label" for="input-email">Address</label>
                        <div class="col-sm-8">
            <textarea placeholder="Address" class="form-control" id="address" name="address" class="form-control"><?php echo $user_details->address;?></textarea>
                </div>
            </div>
            <div class="form-group required"> 
                <label class="col-sm-2 control-label" for="input-email">City</label>
                <div class="col-sm-8">
                <input type="text" placeholder="City" class="form-control" id="city" name="city" value="<?php echo $user_details->city;?>">
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email">State</label>
                <div class="col-sm-8">
                <select name="state" id="state" class="form-control" >
                    <option value="">Select State</option>
            <?php
                if (count($all_array['ARR_STATE']) > 0) {
                  asort($all_array['ARR_STATE']);
                  foreach ($all_array['ARR_STATE'] as $k => $v) {
                    $selected = ($k == $user_details->state) ? "selected='selected'" : "";
                    echo "<option value='".$k."' ".$selected.">".$v."</option>";
                  }
                } else {
                  echo "<option>Select State</option>";
                }
              ?>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                  <label class="col-sm-2 control-label" for="input-email">Pincode</label>
                  <div class="col-sm-8">
                  <input type="text" placeholder="Pincode" class="form-control only_numeric" maxlength="6" id="pincode" name="pincode" value="<?php echo $user_details->pincode;?>">                   
                </div>
              </div>
    
          
              <div class="form-group">
                <input type="submit" value="Submit" id="submit" name="submit" class="btn btn-black" style="margin-left: 18%;margin-top: 30px;" />
              </div>
                  
                </form>
            </fieldset>    
   </div>
   </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
    $('#account .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#account .form-group').length) {
    $('#account .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') == $('#account .form-group').length) {
    $('#account .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#account .form-group').length) {
    $('#account .form-group:first').before(this);
  }
});

$('#address .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
    $('#address .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#address .form-group').length) {
    $('#address .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') == $('#address .form-group').length) {
    $('#address .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#address .form-group').length) {
    $('#address .form-group:first').before(this);
  }
});

$('input[name=\'customer_group_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
    dataType: 'json',
    success: function(json) {
      $('.custom-field').hide();
      $('.custom-field').removeClass('required');

      for (i = 0; i < json.length; i++) {
        custom_field = json[i];

        $('#custom-field' + custom_field['custom_field_id']).show();

        if (custom_field['required']) {
          $('#custom-field' + custom_field['custom_field_id']).addClass('required');
        }
      }


    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $(node).parent().find('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').val(json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.time').datetimepicker({
  pickDate: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value=""> --- Please Select --- </option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"> --- None --- </option>';
      }

      $('select[name=\'zone_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
  <div class="scroll-top">
    <div id="scrollup"></div>
  </div>
  
