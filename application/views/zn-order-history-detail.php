<?php $all_array = all_arrays(); ?>

<div class="container">
      <div class="row"><aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
    <div class="list-group">
    
    <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
    <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
    <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
    <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
    <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>-->
    <!--<a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a>-->
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>-->
    <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a>
  


  </div>
  </aside>
      <div id="content" class="col-sm-9">     
       <h2>Order Information</h2>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left" colspan="2">Order Details</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 50%;"><b>Order ID:</b> #<?php echo $order_detail->id ?><br />
              <b>Date Added:</b> <?php echo date('d-M-Y', strtotime($order_detail->dt_c))?></td>
            <td class="text-left" style="width: 50%;">              
            <b>Payment Method:</b> <?php echo ($order_detail->payment_mode == 1? "Online Payment" : "Cash On Delivery")?><br />
             <b>Shipping Method:</b> Flat Shipping Rate              
             </td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left" style="width: 50%; vertical-align: top;">Payment Address</td>
            <td class="text-left" style="width: 50%; vertical-align: top;">Shipping Address</td>
            </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $user_details_arr->name;?><br /><?php echo $user_details_arr->address;?><br />
            					  <?php echo $user_details_arr->city;?> - <?php echo $user_details_arr->pincode;?><br />
                                  <?php echo $all_array['ARR_STATE'][$user_details_arr->state];?><br />India
            </td>
            
            <td class="text-left"><?php echo $user_details_arr->name;?><br /><?php echo $user_details_arr->address;?><br />
            					  <?php echo $user_details_arr->city;?> - <?php echo $user_details_arr->pincode;?><br />
                                  <?php echo $all_array['ARR_STATE'][$user_details_arr->state];?><br />India
            
            </td>
           </tr>
        </tbody>
      </table>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left">Product Name</td>
              <td class="text-left">Model</td>
              <td class="text-right">Quantity</td>
              <td class="text-right">Price</td>
              <td class="text-right">Total</td>
              <td style="width: 20px;"></td>
           </tr>
          </thead>
          <tbody>
            <?php 
				foreach($order_product_arr as $k){
				$date_added = date('d-M-Y', strtotime($k->dt_c));	 
				$price = $k->cash_price; //$k->payment_mode == 1 ? $val->cash_price : $val->cod_price;
				$shipping[]  = $k->shipping;
				$total += ($k->quantity * $price);
				$shipping = ($k->payment_mode == 1) ? max($shipping) : '0';
					
				?>
              <tr>
              <td class="text-left"><?php echo $k->name ?> <br />
                <!--&nbsp;<small> - Delivery Date: 2011-04-22</small>-->
                </td>
              <td class="text-left">weight Loss</td>
              <td class="text-right"><?php echo $k->quantity ?></td>
              <td class="text-right"><?php echo $price ?></td>
              <td class="text-right"><?php echo $price ?></td>
              <td class="text-right" style="white-space: nowrap;">                
                <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/order/reorder&amp;order_id=13&amp;order_product_id=14" data-toggle="tooltip" title="Reorder" class="btn btn-black"><i class="fa fa-shopping-cart"></i></a>
                <a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/return/add&amp;order_id=13&amp;product_id=47" data-toggle="tooltip" title="Return" class="btn btn-danger"><i class="fa fa-reply"></i></a></td>-->
            </tr>
            <?php  } ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Sub-Total</b></td>
              <td class="text-right"><?php echo _r($total);?></td>
            <td></td>
                </tr>
              <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Flat Shipping Rate</b></td>
              <td class="text-right"><?php echo _r($shipping);?></td>
                  <td></td>
                </tr>
              <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Total</b></td>
              <td class="text-right"><?php echo _r($total + $shipping);?></td>
                  <td></td>
                </tr>
          </tfoot>
        </table>
      </div>
       <h3>Order History</h3>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left">Date Added</td>
            <td class="text-left">Status</td>
            <td class="text-left">Comment</td>
          </tr>
        </thead>
        <tbody>
         <tr>
            <td class="text-left"><?php echo $date_added ?></td>
            <td class="text-left"><?php echo "PENDING" ?></td>
            <td class="text-left"></td>
          </tr></tbody>
      </table>
      <?php if($order_detail->payment_status != 1){ 
           $offer_percentage =  ($order_detail->offer_percentage !=0 ? "with ".$all_array['ARR_OFFER_PER'][$order_detail->offer_percentage]." Discount" : "");
          ?>
      <a style="margin: 0 auto;" class="addtocart btn-color button btn" href="<?php echo SITE_URL?>/cart/paypending_order/<?php echo $order_detail->id ?>" data-id="4">Pay Now <?php echo $offer_percentage; ?></a>
      <?php } ?>
            <div class="buttons clearfix">
      </div>
      </div>
    </div>
</div>
